// import all the module locally

const jsonqlKoa = require('../../packages/koa/index');
const debug = require('debug');
const getDebug = name => debug('jsonql-demo').extend(name);

module.exports = {
  jsonqlKoa,
  getDebug
};
