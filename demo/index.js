const { jsonqlKoa } = require('./src/imports');
const serverIoCore = require('server-io-core');
const { resolve, join } = require('path');

const pkgDir = resolve(join(__dirname, '..', 'packages'));


const server = serverIoCore({
  port: 3388,
  webroot: [
    join(__dirname, 'html'),
    join(__dirname, '..', 'packages', 'client', 'dist')
  ],
  open: true,
  debugger: true,
  reload: false,
  middlewares: [
    jsonqlKoa({
      resolverDir: join(__dirname, 'resolvers'),
      contractDir: join(__dirname, 'contracts')
    })
  ]
});
