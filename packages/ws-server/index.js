// Not going to use the koa-socket-2 due to it's lack of support namespace
// which is completely useless for us if there is no namespace
const { getServer, checkOptions, generator } = require('./lib')
const { getDebug } = require('./lib/share/helpers')
const debug = getDebug('main')
/**
 * @param {object} config this is now diverse from the middleware setup
 * @param {string} config.serverType socket.io or ws in the background
 * @param {object} config.options the actual options to pass to the underlying setups
 * @param {object} config.server this is the http/s server that required to bind to the socket to run on the same connection
 * @return {mixed} return the undelying socket server instance for further use
 */
module.exports = function jsonqlWsServer(config, server) {
  // @TODO check the options
  return checkOptions(config)
    .then(opts => ({
        wsServer: getServer(opts.serverType),
        opts
      })
    )
    .then( ({ opts, wsServer }) => {
      // return is a key value pair object key is the namespace object is the io itself
      const nspObj = wsServer(opts, server)
      return [opts, nspObj]
    })
    .then( args => Reflect.apply(generator, null, args) )
    .catch(err => {
      console.error('Init jsonql ws server error', err)
    })
}
