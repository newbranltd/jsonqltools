// because different ws server has their own structure
// therefore different addProperty will have different implementation
const { SEND_MSG_PROP_NAME } = require('jsonql-constants')
const addHandlerProperty = require('../share/add-handler-property')
const { nil } = require('../share/helpers')
/**
 * Add property to the function
 * @TODO we could extend this idea further to Koa for the userdata
 *
 * @param {function} fn the resolver
 * @param {object} ctx the koa-socket-2 context
 * @param {string} resolverName the name of the resolver
 * @return {function} fn the resolver + new property from ctx
 */
const addProperty = (fn , resolverName, nsp) => {
  let resolver;
  resolver = addHandlerProperty(fn, SEND_MSG_PROP_NAME, function(prop) {
    // we wrap this here to make sure the format is correct
    nsp.emit(resolverName, {data: prop})
  }, nil)
  return resolver;
}

module.exports = addProperty
