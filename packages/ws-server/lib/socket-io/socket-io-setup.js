// socket.io setup event handler
const { finalCatch } = require('jsonql-errors')
const { isNotEmpty, validateAsync } = require('jsonql-params-validator')
const { LOGOUT_EVT_NAME, ACKNOWLEDGE_REPLY_TYPE } = require('jsonql-constants')
const { socketIoGetUserdata, groupByNamespace, socketIoJwtAuth } = require('jsonql-jwt')
const resolveMethod = require('../share/resolve-method')
const addWsProperty = require('../share/add-ws-property')
const { getDebug, packError } = require('../share/helpers')

const debug = getDebug('socket-io-setup')

/**
 * @param {object} socket io socket connection instance
 * @param {object} nsp the namespace instance
 * @return {void} nothing
 */
const logoutHandler = (socket, nsp) => {
  socket.on(LOGOUT_EVT_NAME, () => {
    debug(LOGOUT_EVT_NAME)
    // @TODO need to test this
    nsp.disconnect()
  })
}

/**
 * We no longer use koa-socket-2 because it's hard to implement login
 * @param {object} socket The nsp socket instance
 * @param {object} data pass from client
 * @param {string} fnName name of function as event name
 * @param {object} params from contract
 * @param {object} opts configuration
 * @param {function} cb callback from socket.io
 * @param {object} userdata could be undefined
 * @return {void} noting
 */
const fnHandler = (socket, data, fnName, params, opts, cb, userdata = false) => {
  return validateAsync(data.args, params.params, true)
    .then(args => {
      debug('call resolveMethod', fnName, args)
      // @TODO figure out how to create rooms etc
      return resolveMethod(
        fnName,
        args,
        params,
        opts,
        socket,
        userdata
      )
    })
    .then(result => {
      debug('result', result)
      // decide if we need to call the cb or not here
      if (isNotEmpty(result)) {
        cb({data: result})
      }
    })
    .catch(err => {
      debug('catch error', err)
      cb(packError(err))
    })
}

/**
 * @param {object} nsp namespace instance
 * @param {object} methodsInNsp map of contract
 * @param {object} opts configuration
 * @return {void} noting
 */
const socketIoAuthHandler = (nsp, methodsInNsp, opts) => {
  // @TODO
  let socketIoSetupConfig = {secret: opts.secret}
  debug('using socketIoJwtAuth (roundtrip)', socketIoSetupConfig)
  // setup auth
  socketIoJwtAuth(nsp, socketIoSetupConfig)
    .then(socket => {
      let userdata = socketIoGetUserdata(socket)
      debug('authenticated', userdata)
      for (let fnName in methodsInNsp) {
        let params = methodsInNsp[fnName]
        // actual handler
        socket.on(fnName, (data, cb) => {
          let args = [socket, data, fnName, params, opts, cb, userdata]
          Reflect.apply(fnHandler, null, args)
        })
      }
      logoutHandler(socket, nsp)
    })
    .catch(err => {
      console.error('jwt auth error', err)
    })
}

/**
 * Break out from socketIoSetup than add the auth layer
 * @param {object} nsp the socket.io namespace instance
 * @param {string} namespace what it said
 * @param {object} methodsInNsp as event name
 * @param {object} opts configuration
 * @param {string|boolean} publicNamespace
 * @return {void} nothing
 */
const nspHandler = (nsp, namespace, methodsInNsp, opts, publicNamespace = false) => {
  let userdata;
  if (opts.enableAuth && opts.secret && namespace !== publicNamespace) {
    return socketIoAuthHandler(nsp, methodsInNsp, opts)
  }
  nsp.on('connection', socket => {
    debug('public available nsp connected')
    for (let fnName in methodsInNsp) {
      let params = methodsInNsp[fnName]
      // actual handler
      socket.on(fnName, (data, cb) => {
        debug('Handling event', fnName)
        let args = [socket, data, fnName, params, opts, cb]
        Reflect.apply(fnHandler, null, args)
      })
    }
    // special case
    logoutHandler(socket, nsp)
  })
}

/**
 * Just to determine which map we should use
 * @param {*} nspSet could be undefined if it's not enableAuth
 * @param {string} namespace nsp
 * @param {object} socketFns from contract.socket
 * @return {object} map to the resolvers
 */
const getMethodsInNsp = (nspSet, namespace, socketFns) => (
  nspSet && nspSet[namespace] ? nspSet[namespace] : socketFns
)

/**
 * @param {object} opts configuration
 * @param {object} nspObj key namespace value io
 * @return {object} nspObj just return it
 */
const socketIoSetup = (opts, nspObj) => {
  let nspInfo = {};
  const socketFns = opts.contract.socket;
  if (opts.enableAuth) {
    nspInfo = groupByNamespace(socketFns)
  }
  let { publicNamespace, nspSet } = nspInfo;
  // because this is attach to the Koa.app itself
  // its a bit different in the handling
  for (let namespace in nspObj) {
    let nsp = nspObj[namespace]
    let methodsInNsp = getMethodsInNsp(nspSet, namespace, socketFns)
    let args = [nsp, namespace, methodsInNsp, opts]
    if (publicNamespace) {
      args.push(publicNamespace)
    }
    Reflect.apply(nspHandler, null, args)
  }
  return nspObj;
};

// only need this one to export
module.exports = socketIoSetup;
