// search for the resolver location
const fs = require('fs')
const { join } = require('path')
const { isUndefined } = require('lodash')
const {
  JsonqlAuthorisationError,
  JsonqlResolverNotFoundError,
  JsonqlResolverAppError,
  JsonqlValidationError,
  JsonqlError,
  finalCatch
} = require('jsonql-errors')
const {
  SOCKET_NAME,
  DEFAULT_RESOLVER_IMPORT_FILE_NAME,
  MODULE_TYPE
} = require('jsonql-constants')
const { validateSync } = require('jsonql-params-validator')
const { provideUserdata } = require('jsonql-jwt')
const { dasherize } = require('./helpers')
// the addProperty methods
const socketIoAddProperty = require('../socket-io/add-property')
const wsAddProperty = require('../ws/add-property')

const { SOCKET_IO, WS } = require('./constants')

const debug = require('debug')('jsonql-ws-server:resolve-method')
/**
 * @param {string} name resolverName
 * @param {string} type resolverType
 * @param {object} opts configuration
 * @return {function} resolver function
 */
const getPathToFn = function(name, type, opts) {
  const dir = opts.resolverDir;
  const fileName = dasherize(name)
  let paths = [];
  if (opts.contract && opts.contract[type] && opts.contract[type].path) {
    paths.push(opts.contract[type].path);
  }
  paths.push( join(dir, type, fileName, 'index.js') )
  paths.push( join(dir, type, fileName + '.js') )
  const ctn = paths.length;
  for (let i=0; i<ctn; ++i) {
    if (fs.existsSync(paths[i])) {
      return paths[i];
    }
  }
  return false;
}

/**
 * Using the contract to find the function to call
 * @param {string} type of resolver
 * @param {string} name of resolver
 * @param {object} contract to search from
 * @return {string} file path to function
 */
function findFromContract(name, params) {
  const { file } = params;
  return (file && fs.existsSync(file)) ? file : false;
}

/**
 * search for the file starting with
 * 1. Is the path in the contract (or do we have a contract file)
 * 2. if not then resolvers/query/name-of-call/index.js (query swap with mutation)
 * 3. then resolvers/query/name-of-call.js
 * @param {string} name of the resolver
 * @param {object} params the extracted params
 * @param {object} opts configuration
 * @return {string} the path to function
 */
const searchResolvers = function(name, params, opts) {
  try {
    const search = findFromContract(name, params)
    if (search !== false) {
      return search;
    }
    // search by running
    const filePath = getPathToFn(name, type, opts)
    if (filePath) {
      return filePath;
    }
    const debugMsg = `${name} not found!`;
    debug(debugMsg);
    // const msg = prod ? 'NOT FOUND!' : debugMsg;
    throw new JsonqlResolverNotFoundError(debugMsg)
  } catch(e) {
    if (e instanceof JsonqlResolverNotFoundError) {
      throw new JsonqlResolverNotFoundError(e)
    } else {
      throw new JsonqError(e)
    }
  }
}

/**
 * New for ES6 module features V1.2.0
 * @param {string} resolverDir resolver directory
 * @param {string} type of resolver
 * @param {string} resolverName name of resolver
 * @return {function} the imported resolver
 */
function importFromModule(resolverDir, type, resolverName) {
  debug(resolverDir, type, resolverName)
  const resolvers = require( join(resolverDir, DEFAULT_RESOLVER_IMPORT_FILE_NAME) )
  return resolvers[type + resolverName]
}

/**
 * Wrap this toegether and make sure it only throw ResolverNotFoundError
 * Add in v1.2.0 to check for the sourceType
 * @param {string} resolverName name of resolver
 * @param {object} params from contract
 * @param {object} opts configuration
 * @param {object} ctx the koa-socket-2 context
 * @return {function} the actual resolver function
 */
const getResolver = function(resolverName, params, opts) {
  try {
    const { resolverDir, contract } = opts;
    const { sourceType } = contract;
    if (sourceType === MODULE_TYPE) {
      return importFromModule(resolverDir, SOCKET_NAME, resolverName)
    }
    const pathToFn = searchResolvers(resolverName, params, opts)
    return require(pathToFn)
  } catch(e) {
    throw new JsonqlResolverNotFoundError(`${resolverName} NOT FOUND`, e)
  }
}

/**
 * using the serverType to provide different addProperty method to this
 * @param {string} serverType server type
 * @param {function} fn the resolver function
 * @param {string} resolverName resolver name
 * @param {object} ws the different context object
 * @param {object|boolean} userdata false when there is none
 * @return {function} the applied function
 */
const addProperty = (serverType, fn, resolverName, ws, userdata) => {
  let _fn;
  switch(serverType) {
    case SOCKET_IO:
      _fn = socketIoAddProperty(fn, resolverName, ws)
      break;
    case WS:
      _fn = wsAddProperty(fn, resolverName, ws)
      break;
    default:
      throw new JsonqlError(`No such serverType: ${serverType} for ${resolverName}`)
  }
  // group the provide userdata together
  return isUndefined(userdata) ? _fn : provideUserdata(_fn, userdata)
}

/**
 * similiar to the one in Koa-middleware without the ctx
 * @param {string} resolverName name to call
 * @param {array} args arguments
 * @param {object} params extracted params
 * @param {object} opts for search later
 * @param {object} userdata userdata
 * @param {object} ws io for socket.io
 * @param {object|boolean} userdata false when there is none userdata
 * @return {mixed} depends on the contract
 */
const resolveMethod = function(resolverName, args, params, opts, ws, userdata) {
  const fn = getResolver(resolverName, params, opts)
  const tfn = addProperty(opts.serverType, fn, resolverName, ws, userdata)
  try {
    return Reflect.apply(tfn, null, args)
  } catch(e) {
    throw new JsonqlResolverAppError(resolverName, e)
  }
}

// we only need to export one method
module.exports = resolveMethod
