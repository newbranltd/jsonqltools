// ws setup
const { finalCatch } = require('jsonql-errors')
const { wsGetUserdata, groupByNamespace } = require('jsonql-jwt')
const { isNotEmpty, validateAsync } = require('jsonql-params-validator')
const {
  LOGOUT_EVT_NAME,
  ACKNOWLEDGE_REPLY_TYPE,
  ERROR_TYPE,
  LOGOUT_NAME
} = require('jsonql-constants')
const resolveMethod = require('../share/resolve-method')
const { getDebug, packError } = require('../share/helpers')
const createWsReply = require('./create-ws-reply')

const debug = getDebug('ws-setup')

/**
 * @param {object} ws WebSocket instance
 * @param {object} payload args array
 * @param {string} resolverName name of resolver
 * @param {object} params from contract.json
 * @param {object} opts configuration
 * @param {object} userdata userdata
 */
const fnHandler = (ws, payload, resolverName, params, opts, userdata) => {
  debug('fnHandler', resolverName)
  // need to figure out a way to create a cb using the ws
  // perhaps a ws.socket.id or some kind?
  const cb = data => {
    ws.send(data)
  }
  // run
  return validateAsync(payload.args, params.params, true)
    .then( args  => {
      return resolveMethod(
        resolverName,
        args, // this is the clear value from validateAsync
        params,
        opts,
        ws,
        userdata
      )
    })
    .then(result => {
      debug('result', result)
      // decide if we need to call the cb or not here
      if (isNotEmpty(result)) {
        cb(createWsReply(ACKNOWLEDGE_REPLY_TYPE, resolverName, result))
      }
    })
    .catch(err => {
      debug('catch error', err)
      cb(createWsReply(ERROR_TYPE, resolverName, packError(err)))
    })
}

/**
 * The default single nsp mapping to resolver
 * @param {object} ws websocket socket instance
 * @param {object} json data send from client
 * @param {object} socketFns contract
 * @param {object} opts configuration
 * @param {*} [userdata=false] userdata if any
 * @return {void} nothing
 */
const callNspHandler = (ws, json, socketFns, opts, userdata = false) => {
  for (let resolverName in json) {
    debug('connection call', resolverName)
    let payload = json[resolverName]
    let params = socketFns[resolverName]
    // we need to use the decoded token --> userdata
    // and pass to the resolver
    fnHandler(ws, payload, resolverName, params, opts, userdata)
  }
}

/**
 * This will change based on the WS spec
 * @param {object} ws socket
 */
const handleLogout = ws => {
  ws.close(1, LOGOUT_NAME)
}

/**
 * @param {object} opts configuration
 * @param {object} nspObj the ws nsp instance
 * @return {object} nspObj itself with addtional properties
 */
const wsSetup = (opts, nspObj) => {
  let nspInfo = {};
  const socketFns = opts.contract.socket;
  if (opts.enableAuth) {
    nspInfo = groupByNamespace(socketFns)
  }
  let { publicNamespace, nspSet } = nspInfo;
  for (let namespace in nspObj) {
    let userdata;
    nspObj[namespace].on('connection', (ws, req) => {
      // the data part is different again
      // because there are only two events type in ws
      // send --> message
      // so the data is wrap inside the resolverName
      ws.on('message', data => {
        let json = JSON.parse(data)
        debug('called with: ', json)
        if (nspSet) {
          let methodsInNsp = nspSet[namespace]
          debug(namespace, methodsInNsp)
          for (let resolverName in json) {
            if (resolverName === LOGOUT_EVT_NAME) {
              return handleLogout(ws)
            }
            // only allow the method under that particular namespace
            if (methodsInNsp[resolverName]) {
              // only the private one will get a userdata property
              if (namespace !== publicNamespace) {
                userdata = wsGetUserdata(req)
              }
              callNspHandler(ws, json, socketFns, opts, userdata)
            }
          }
        } else { // just public nsp
          callNspHandler(ws, json, socketFns, opts)
        }
      })
    })
  }
  return nspObj;
}

module.exports = wsSetup
