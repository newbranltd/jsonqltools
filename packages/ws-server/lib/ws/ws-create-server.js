// web socket server based on ws
const url = require('url')
const WebSocket = require('ws')
const { wsVerifyClient } = require('jsonql-jwt')

const { getNamespace, getDebug } = require('../share/helpers')
const debug = getDebug('ws-setup')


/**
 * @param {array} namespace string
 * @param {object} config configuration
 * @return {array} of nsps
 */
const generateWss = (namespace, config) => {
  let verifyClient;
  if (config.enableAuth) {
    let key = config.secret ? config.secret : config.publicKey;
    verifyClient = wsVerifyClient(key)
  }
  return namespace.map((name, i) => {
    let c = { noServer: true };
    if (i>0) {
      c.verifyClient = verifyClient;
    }
    return { [name]: new WebSocket.Server(c) }
  }).reduce((last, next) => Object.assign(last, next), {})
}

/**
 * @param {object} req http.server request object
 * @return {string} the strip slash of pathname
 */
const getPath = req => {
  const { pathname } = url.parse(req.url)
  // debug('pathname', pathname, pathname.substring(0, 1), pathname.substring(1, pathname.length));
  return pathname.substring(0, 1) === '/' ? pathname.substring(1, pathname.length) : pathname;
}

/**
 * @param {array} nsps with name as key
 * @param {string} path of path name to compare
 * @return {object} ws
 */
const getWssByPath = (nsps, path) => {
  for (let name in nsps) {
    if (nsps[path]) {
      return nsps[path]
    }
  }
  return false;
}

/**
 * @param {object} config options
 * @param {object} server http.createServer instance
 * @return {object} ws server instance with namespace as key
 */
module.exports = function(config, server) {
  // debug('got config here', config);
  const namespace = getNamespace(config)
  // debug('namespace', namespace)
  const nsps = generateWss(namespace, config)
  // debug('nsps', nsps)
  // debug('nsps', nsps);
  // we will always call it via a namespace
  server.on('upgrade', function upgrade(req, socket, head) {
    const pathname = getPath(req)
    debug('Hear the upgrade event', pathname)
    let srv = false;
    if ((srv = getWssByPath(nsps, pathname)) !== false) {
      srv.handleUpgrade(req, socket, head, function done(ws) {
        debug('found a srv to handle the call')
        srv.emit('connection', ws, req)
      });
    } else {
      debug('Unhandled socket destroy')
      socket.destroy()
    }
  });
  return nsps;
}
