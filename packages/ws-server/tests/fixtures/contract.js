// generate contract
const { join } = require('path')
const contractApi = require('jsonql-contract');
const contractDir = join(__dirname , 'contract', 'auth')
const resolverDir = join(__dirname , 'resolvers')

contractApi({
  contractDir,
  resolverDir,
  enableAuth: true,
  useJwt: true,
  privateMethodDir: 'private'
})
