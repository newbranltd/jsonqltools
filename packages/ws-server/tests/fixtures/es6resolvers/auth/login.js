
/**
 * create a login method for testing
 * @param {string} username username
 * @return {object} userdata
 */
export default function login(username) {
  return {name: username}
}
