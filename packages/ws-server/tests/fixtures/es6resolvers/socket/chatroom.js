// a private method

/**
 *
 * @param {string} msg message
 * @param {number} timestamp for checking the time
 * @return {string} reply
 */
export default function chatroom(msg, timestamp) {
  const d = Date.now() -  timestamp;
  return msg + ` took ${d} ms`;
}
