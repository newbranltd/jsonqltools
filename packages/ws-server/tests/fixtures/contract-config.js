const { join } = require('path');

module.exports = {
  resolverDir: join(__dirname, 'resolvers'),
  contractDir: join(__dirname, 'contract')
};
