// porting back from jsonql-koa to grab the NODE_ENV.json and remove the files information to public use
const { join } = require('path')
const fsx = require('fs-extra')
const { merge } = require('lodash')
const { JsonqlError } = require('jsonql-errors')
const debug = require('debug')('jsonql-contract:public-contract')
/**
 * we need to remove the file info from the contract if this is for public
 * @param {object} json contract
 * @param {object} config options
 * @return {string} contract without the file field
 */
const cleanForPublic = (json, config) => {
  const {
    enableAuth,
    loginHandlerName,
    logoutHandlerName,
    validatorHandlerName,
    contractWithDesc
  } = config;
  for (let type in json) {
    for (let fn in json[type]) {
      delete json[type][fn].file;
      // @1.7.4 remove the description to reduce the size of the contract
      if (!contractWithDesc) {
        delete json[type][fn].description;
      }
    }
  }
  // also if there is a validator field then delete it
  if (json.auth[validatorHandlerName]) {
    delete json.auth[validatorHandlerName]
  }
  // if it's not enableAuth then remove all of these
  if (!enableAuth) {
    if (json.auth[loginHandlerName]) {
      delete json.auth[loginHandlerName]
    }
    if (json.auth[logoutHandlerName]) {
      delete json.auth[logoutHandlerName]
    }
  }
  // don't need this in the public json
  debug(json.sourceType)
  delete json.sourceType;
  // export
  return json;
}

/**
 * using the NODE_ENV to check if there is extra contract file
 * @param {string} contractDir directory store contract
 * @return {object} empty object when nothing
 */
function getEnvContractFile(contractDir) {
  const nodeEnv = process.env.NODE_ENV;
  const overwriteContractFile = join(contractDir, [nodeEnv, 'json'].join('.'))
  if (fsx.existsSync(overwriteContractFile)) {
    debug('found env contract')
    return fsx.readJsonSync(overwriteContractFile)
  }
  return {};
}

/**
 * add an expired timstamp to the public contract
 * @param {object} config configuration
 * @param {object} contractJson the generated contract
 * @return {object} empty then nothing
 */
function getExpired(config, contractJson) {
  const { expired } = config;
  const { timestamp } = contractJson;
  if (expired && expired > timestamp) {
    return { expired }
  }
  return {}
}

/**
 * @param {object} config the original config
 * @param {object} contractJson the raw json file
 * @return {string} json
 */
module.exports = function publicContractGenerator(config, contractJson) {
  const contractDir = config.contractDir;
  if (!contractDir) {
    throw new JsonqlError('Contract directory is undefined!')
  }
  const baseContract = fsx.readJsonSync(join(__dirname, 'hello-world.json'))
  // env contract file - this should not get written to file
  // @TODO disable this feature for now and decide if we need it later
  const extraContracts = {};
  // const extraContracts = config.raw ? getEnvContractFile(contractDir) : {};
  const expiredEntry = getExpired(config, contractJson)
  // export
  return cleanForPublic(
    merge(
      {},
      baseContract,
      contractJson,
      extraContracts,
      expiredEntry
    ),
    config
  );
}
