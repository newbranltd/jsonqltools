const generator = require('./generator')
const getPaths = require('./get-paths')
const { checkFile, applyDefaultOptions } = require('./utils')
// main
module.exports = {
  applyDefaultOptions,
  generator,
  getPaths,
  checkFile
}
