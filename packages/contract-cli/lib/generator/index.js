// The generator is getting way too big to manage
// and there are several more feature to add in the future
// so we need to break it apart now
// The core generator

// @2019-05-24 Add a queuing system to make sure it runs the generator
// only when there is nothing new

const { merge } = require('lodash')
const colors = require('colors/safe')
const debug = require('debug')('jsonql-contract:generator')
const { EXT, PUBLIC_CONTRACT_FILE_NAME } = require('jsonql-constants')
const publicContractGenerator = require('../public-contract')
const {
  readFilesOutContract,
  generateOutput,
  isContractExisted
} = require('./files')

let ctn = 0;
/**
 * Show a message when run this program
 * @param {object} config input could be not clean
 * @return {void}
 */
const banner = config => {
  // debug('received config', config);
  if (config.banner === true) {
    ++ctn;
    console.log(
      `[${ctn}] in: `,
      colors.cyan(`${config.resolverDir}`),
      'out: ',
      colors.green(`${config.contractDir}`),
      `[${config.returnAs} output] ${config.public ? '[public]': ''}`
    );
  }
  return ctn;
}

/**
 * Wrapper method
 * @param {object} config options
 * @param {object} contract JSON
 * @return {mixed} depends on the returnAs parameter
 */
const callPublicGenerator = (config, contract) => (
  generateOutput(
    merge({}, config, { outputFilename: PUBLIC_CONTRACT_FILE_NAME }),
    publicContractGenerator(config, contract),
    contract // <-- this is when ES6 module require to generate import export files
  )
)

/**
 * This is taken out from the original generator main interface
 * @param {object} config options
 * @return {mixed} depends on the returnAs parameter
 */
const generateNewContract = (config) => {
  const { resolverDir, contractDir } = config;
  return readFilesOutContract(resolverDir, config, config.ext || EXT)
    .then(contract => {
      if (config.public === true) {
        return callPublicGenerator(config, contract)
      }
      return generateOutput(config, contract)
    })
}

// @BUG the async cause no end of problem for the client downstream
// so I take it down and only return promise instead
// main
module.exports = function(config) {
  banner(config)
  // first we need to check if this is a public!
  // then try to read the contract.json file
  if (config.public === true) {
    const baseContract = isContractExisted(config)
    if (baseContract !== false) {
      return callPublicGenerator(config, baseContract)
    }
  }
  // back to the old code
  return generateNewContract(config)
}
