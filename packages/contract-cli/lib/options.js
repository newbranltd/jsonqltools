// default options @TODO add in the next upgrade

const { resolve, join } = require('path')
const fs = require('fs')
const { checkConfigAsync, constructConfig, createConfig } = require('jsonql-params-validator')
const {
  DEFAULT_RESOLVER_DIR,
  DEFAULT_CONTRACT_DIR,
  PUBLIC_KEY,
  PRIVATE_KEY,
  STRING_TYPE,
  BOOLEAN_TYPE,
  NUMBER_TYPE,
  OBJECT_TYPE,
  ACCEPTED_JS_TYPES,
  CJS_TYPE,
  ALIAS_KEY,
  OPTIONAL_KEY,
  ENUM_KEY,
  RETURN_AS_FILE,
  RETURN_AS_ENUM,
  ISSUER_NAME,
  LOGOUT_NAME,
  VALIDATOR_NAME,
  DEFAULT_CONTRACT_FILE_NAME
} = require('jsonql-constants')

const BASE_DIR = process.cwd()

const constProps = {
  BASE_DIR,
  outputFilename: DEFAULT_CONTRACT_FILE_NAME
}

const defaultOptions = {
  // give the contract an expired time
  expired: createConfig(0, [NUMBER_TYPE]),

  extraContractProps: createConfig(false, [OBJECT_TYPE], {[OPTIONAL_KEY]: true}),

  loginHandlerName: createConfig(ISSUER_NAME, [STRING_TYPE]),
  logoutHandlerName: createConfig(LOGOUT_NAME, [STRING_TYPE]),
  validatorHandlerName: createConfig(VALIDATOR_NAME, [STRING_TYPE, BOOLEAN_TYPE]),

  enableAuth: createConfig(false, BOOLEAN_TYPE, {[ALIAS_KEY]: 'auth'}),
  // file or json
  returnAs: createConfig(RETURN_AS_FILE, STRING_TYPE, {[ENUM_KEY]: RETURN_AS_ENUM}),
  // we need to force it to use useDoc = true for using jsdoc API now
  useDoc: constructConfig(true, BOOLEAN_TYPE), // @TODO remove this later
  // there will be cjs, es, ts for different parser
  jsType: constructConfig(CJS_TYPE , STRING_TYPE, false, ACCEPTED_JS_TYPES),
  // matching the name across the project - the above two will become alias to this
  resolverDir: createConfig(resolve(join(BASE_DIR, DEFAULT_RESOLVER_DIR)) , STRING_TYPE, {[ALIAS_KEY]: 'inDir'}),
  contractDir: createConfig(resolve(join(BASE_DIR, DEFAULT_CONTRACT_DIR)), STRING_TYPE, {[ALIAS_KEY]: 'outDir'}),
  contractWithDesc: createConfig(false, [BOOLEAN_TYPE]),
  // where to put the public method
  publicMethodDir: constructConfig(PUBLIC_KEY, STRING_TYPE),
  // just try this with string type first
  privateMethodDir: constructConfig(PRIVATE_KEY, [STRING_TYPE], true),
  public: constructConfig(false, [BOOLEAN_TYPE]),
  banner: constructConfig(true, [BOOLEAN_TYPE]),
  // this are for the cmd mostly
  watch: createConfig(false, [BOOLEAN_TYPE, STRING_TYPE, NUMBER_TYPE], {[OPTIONAL_KEY]: true, [ALIAS_KEY]: 'w'}),
  interval: createConfig(10000, [NUMBER_TYPE, STRING_TYPE], {[OPTIONAL_KEY]: true, [ALIAS_KEY]: 'i'}),
  configFile: createConfig(false, [STRING_TYPE], {[OPTIONAL_KEY]: true, [ALIAS_KEY]: 'c'}),
  announceUrl: createConfig(false, [STRING_TYPE], {[OPTIONAL_KEY]: true, [ALIAS_KEY]: 'a'}),
  logDirectory: constructConfig(false, [STRING_TYPE], true),
  tmpDir: createConfig(join(BASE_DIR, 'tmp'), [STRING_TYPE])
}
// export it
module.exports = config => checkConfigAsync(config, defaultOptions, constProps)
