// separate process to deal with the socket
const socketIoClient = require('socket.io-client')
const WebSocket = require('ws')

const getSocketType = url => url.substr(0,2)

/**
 * Generate the socket client
 * @param {object} config clean options
 * @return {object} socket client instance for use later
 */
const createSocketClient = config => {
  const { announceUrl: url } = config;
  if (url) {
    let client, fn;
    switch (true) {
      case getSocketType(url) === 'ws':
        client = new WebSocket(url)
        fn = client.send;
        break;
      case getSocketType(url) === 'ht';
        client = socketIoClient.connect(url)
        fn = client.emit;
        break;
      default:
        throw new Error(`url: ${url} is not a correct socket path!`)
    }
    return { client, fn };
  }
  return false;
};

// main interface
module.exports = function(config) {
  let result;
  if ((result = createSocketClient(config)) !== false) {
    const { client, fn } = result;
    return (evt, payload) => {
      fn(evt , payload);
    };
  }
  // just return an empty function
  return () => {};
};
