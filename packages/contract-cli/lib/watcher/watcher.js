// watching the file change and execute the generator to create new files
// also if the announceUrl is presented then we create socket.io / ws client
// to announce the change
const chokidar = require('chokidar')
const { join } = require('path')
const { fork } = require('child_process')
const kefir = require('kefir')
const colors = require('colors/safe')
const { EXT, TS_EXT, TS_TYPE } = require('jsonql-constants')
const debug = require('debug')('jsonql-contract:watcher')

let counter = 0;

/**
 * When passing option from cmd, it could turn into a string value
 * @param {object} config clean optios
 * @return {boolean} true OK
 */
const isEnable = config => {
  const { watch } = config;
  if (watch === true || watch === 'true' || watch === '1' || watch === 1) {
    const ext = config.jsType === TS_TYPE ? TS_EXT : EXT;
    return join( config.resolverDir, '**', ['*', ext].join('.'))
  }
  return false;
};

/**
 * main interface
 * @param {object} config clean options
 * @return {void}
 */
module.exports = function(config) {
  let watchPath;
  if ((watchPath = isEnable(config)) !== false) {
    debug('watch this', watchPath);
    // create the watcher
    const watcher = chokidar.watch(watchPath, {})
    // create a fork process
    const ps = fork(join(__dirname, 'run.js'))
    ps.send({ config });
    // now watch
    const stream = kefir.stream(emitter => {
      watcher.on('raw', (evt, path, details) => {
        ++counter;
        debug(`(${counter}) got even here`, evt, path, details)
        emitter.emit({evt, path, details})
      });
      // call exit
      return () => watcher.close()
    });

    stream.throttle(config.interval).observe({
      value(value) {
        ps.send(value)
      }
    })

    // we can remotely shut it down
    ps.on('message', msg => {
      if (msg.end) {
        console.info(colors.green('Watcher is shutting down'))
        watcher.close();
      } else if (msg.txt) {
        console.log(colors.yellow(msg.txt))
      }
    })
  }
}
