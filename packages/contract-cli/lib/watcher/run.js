// this will get call to run the generator
const generator = require('../generator')
const socketClient = require('./socket')
const debug = require('debug')('jsonql-contract:watcher:run')

let fn, config;

process.on('message', m => {
  if (m.config) {
    config = m.config;
    fn = socketClient(m.config)
  } else {
    generator(config)
    fn(m)
  }
})
