// now when ever some one do contract = require('jsonql-contract') will include this one
const {
  applyDefaultOptions,
  generator,
  getPaths
} = require('./lib')
const { KEY_WORD } = require('jsonql-constants')
// const debug = require('debug')('jsonql-contract:api');
// main contract-cli @api public
module.exports = function(config) {
  return applyDefaultOptions(config)
    .then(
      opts => getPaths(opts).then(generator)
    )
}
