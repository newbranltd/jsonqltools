// testing the cmd interface
const test = require('ava')
const { join, resolve } = require('path')
const { spawn } = require('child_process')
const fsx = require('fs-extra')
const { DEFAULT_CONTRACT_FILE_NAME } = require('jsonql-constants')

const debug = require('debug')('jsonql-contract:test:cmd')

const inDir = resolve(join(__dirname, 'fixtures', 'resolvers'))
const outDir = resolve(join(__dirname, 'fixtures', 'tmp', 'cmd'))

const configTestFile = join(outDir, 'config-test', DEFAULT_CONTRACT_FILE_NAME)

const cliFile = resolve(join(__dirname, '..' , 'cli.js'))


test.after(t => {
  fsx.removeSync(join(outDir, DEFAULT_CONTRACT_FILE_NAME))
  // fsx.removeSync(configTestFile)
});

test.cb('It should able to call the cmd and have correct output', t => {
  t.context.ps = spawn('node', [
    cliFile,
    'create',
    inDir,
    outDir,
    '-w',
    true
  ])

  t.plan(1)

  t.context.ps.stdout.on('data', data => {
    debug('stdout: ', data.toString())
  });

  t.context.ps.stderr.on('data', data => {
    debug('stderr: ', data.toString())
  });

  t.context.ps.on('close', code => {
    debug(`(1) Exited with ${code}`)
    t.true(fsx.existsSync(join(outDir, DEFAULT_CONTRACT_FILE_NAME)))
    t.end()
  })
})

test.cb("It should able to pick up the config file", t => {
  const ps = spawn('node', [
    cliFile,
    'config',
    './tests/fixtures/cmd-config-test.js'
  ])

  ps.stdout.on('data', data => {
    debug('stdout: ', data.toString())
  })

  ps.stderr.on('data', data => {
    debug('stderr: ', data.toString())
  })

  ps.on('close', code => {
    debug(`(2) Exited with ${code}`)
    t.truthy(fsx.existsSync(configTestFile))
    t.end()
  })
})
