// export
import {
  checkIsObject,
  notEmpty,
  checkIsAny,
  checkIsString,
  checkIsBoolean,
  checkIsNumber,
  checkIsArray
} from './src'
// PIA syntax
export const isObject = checkIsObject;
export const isAny = checkIsAny;
export const isString = checkIsString;
export const isBoolean = checkIsBoolean;
export const isNumber = checkIsNumber;
export const isArray = checkIsArray;
export const isNotEmpty = notEmpty;

import * as validator from './src/validator'

export const normalizeArgs = validator.normalizeArgs;
export const validateSync = validator.validateSync;
export const validateAsync = validator.validateAsync;

// configuration checking

import * as jsonqlOptions from './src/options'

export const JSONQL_PARAMS_VALIDATOR_INFO = jsonqlOptions.JSONQL_PARAMS_VALIDATOR_INFO;

export const createConfig = jsonqlOptions.createConfig;
export const constructConfig = jsonqlOptions.constructConfigFn;

export const checkConfigAsync = jsonqlOptions.checkConfigAsync(validator.validateSync)
export const checkConfig = jsonqlOptions.checkConfig(validator.validateSync)

import isInArray from './src/is-in-array'

export const inArray = isInArray;

import checkKeyInObject from './src/check-key-in-object'

export const isKeyInObject = checkKeyInObject;

import checkIsContract from './src/check-is-contract'

export const isContract = checkIsContract;

// add in v1.3.0

import * as paramsApi from './src/params-api'

export const createQuery = paramsApi.createQuery;
export const createQueryStr = paramsApi.createQueryStr;
export const createMutation = paramsApi.createMutation;
export const createMutationStr = paramsApi.createMutationStr;
export const getQueryFromArgs = paramsApi.getQueryFromArgs;
export const getQueryFromPayload = paramsApi.getQueryFromPayload;
export const getMutationFromArgs = paramsApi.getMutationFromArgs;
export const getMutationFromPayload = paramsApi.getMutationFromPayload;
export const getNameFromPayload = paramsApi.getNameFromPayload;
