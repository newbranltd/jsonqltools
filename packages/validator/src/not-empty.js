/**
 * Check several parameter that there is something in the param
 * @param {*} param input
 * @return {boolean}
 */
import { trim, isArray } from 'lodash-es'

export default a => {
  if (isArray(a)) {
    return true;
  }
  return a !== undefined && a !== null && trim(a) !== '';
}
