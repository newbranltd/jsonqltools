// validate string type
import { isString, trim } from 'lodash-es'
/**
 * @param {string} value expected value
 * @return {boolean} true if OK
 */
const checkIsString = function(value) {
  return (trim(value) !== '') ? isString(value) : false;
}

export default checkIsString;
