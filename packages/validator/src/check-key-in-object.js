import isInArray from './is-in-array'

/**
 * @param {object} obj for search
 * @param {string} key target
 * @return {boolean} true on success
 */
const checkKeyInObject = function(obj, key) {
  const keys = Object.keys(obj)
  return isInArray(keys, key)
}

export default checkKeyInObject
