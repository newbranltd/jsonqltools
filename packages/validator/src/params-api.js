// craete several helper function to construct / extract the payload
// and make sure they are all the same
import {
  PAYLOAD_PARAM_NAME,
  CONDITION_PARAM_NAME,
  RESOLVER_PARAM_NAME,
  QUERY_ARG_NAME
} from 'jsonql-constants'
import { JsonqlValidationError } from 'jsonql-errors'

import isString from './string'
import { checkIsArray } from './array'
import { checkIsObject } from './object'

// make sure it's an object
const formatPayload = payload => isString(payload) ? JSON.parse(payload) : payload;

/**
 * Get name from the payload (ported back from jsonql-koa)
 * @param {*} payload to extract from
 * @return {string} name
 */
export function getNameFromPayload(payload) {
  return Object.keys(payload)[0]
}

/**
 * @param {string} resolverName name of function
 * @param {array} [args=[]] from the ...args
 * @param {boolean} [jsonp = false] add v1.3.0 to koa
 * @return {object} formatted argument
 */
export function createQuery(resolverName, args = [], jsonp = false) {
  if (isString(resolverName) && checkIsArray(args)) {
    let payload =  { [QUERY_ARG_NAME]: args }
    if (jsonp === true) {
      return payload;
    }
    return { [resolverName]: payload }
  }
  throw new JsonqlValidationError(`[createQuery] expect resolverName to be string and args to be array!`, { resolverName, args })
}

// string version of the above
export function createQueryStr(resolverName, args = [], jsonp = false) {
  return JSON.stringify(createQuery(resolverName, args, jsonp))
}

/**
 * @param {string} resolverName name of function
 * @param {*} payload to send
 * @param {object} [condition={}] for what
 * @param {boolean} [jsonp = false] add v1.3.0 to koa
 * @return {object} formatted argument
 */
export function createMutation(resolverName, payload, condition = {}, jsonp = false) {
  const _payload = {
    [PAYLOAD_PARAM_NAME]: payload,
    [CONDITION_PARAM_NAME]: condition
  }
  if (jsonp === true) {
    return _payload;
  }
  if (isString(resolverName)) {
    return { [resolverName]: _payload }
  }
  throw new JsonqlValidationError(`[createMutation] expect resolverName to be string!`, { resolverName, payload, condition })
}

// string version of above
export function createMutationStr(resolverName, payload, condition = {}, jsonp = false) {
  return JSON.stringify(createMutation(resolverName, payload, condition, jsonp))
}

/**
 * Further break down from method below for use else where
 * @param {string} resolverName name of fn
 * @param {object} payload payload
 * @return {object|boolean} false on failed
 */
export function getQueryFromArgs(resolverName, payload) {
  if (resolverName && checkIsObject(payload)) {
    const args = payload[resolverName];
    if (args[QUERY_ARG_NAME]) {
      return {
        [RESOLVER_PARAM_NAME]: resolverName,
        [QUERY_ARG_NAME]: args[QUERY_ARG_NAME]
      }
    }
  }
  return false;
}

/**
 * extra the payload back
 * @param {*} payload from http call
 * @return {object} resolverName and args
 */
export function getQueryFromPayload(payload) {
  const p = formatPayload(payload);
  const resolverName = getNameFromPayload(p)
  const result = getQueryFromArgs(resolverName, p)
  if (result !== false) {
    return result;
  }
  throw new JsonqlValidationError('[getQueryArgs] Payload is malformed!', payload)
}

/**
 * Further break down from method below for use else where
 * @param {string} resolverName name of fn
 * @param {object} payload payload
 * @return {object|boolean} false on failed
 */
export function getMutationFromArgs(resolverName, payload) {
  if (resolverName && checkIsObject(payload)) {
    const args = payload[resolverName]
    if (args) {
      return {
        [RESOLVER_PARAM_NAME]: resolverName,
        [PAYLOAD_PARAM_NAME]: args[PAYLOAD_PARAM_NAME],
        [CONDITION_PARAM_NAME]: args[CONDITION_PARAM_NAME]
      }
    }
  }
  return false;
}

/**
 * @param {object} payload
 * @return {object} resolverName, payload, conditon
 */
export function getMutationFromPayload(payload) {
  const p = formatPayload(payload);
  const resolverName = getNameFromPayload(p)
  const result = getMutationFromArgs(resolverName, p)
  if (result !== false) {
    return result;
  }
  throw new JsonqlValidationError('[getMutationArgs] Payload is malformed!', payload)
}
