const test = require('ava')
const {
  createQueryStr,
  createMutationStr,
  getQueryFromPayload,
  getMutationFromPayload,
  getQueryFromArgs,
  getMutationFromArgs,
  getNameFromPayload
} = require('../dist/jsonql-params-validator.cjs')
const { PAYLOAD_PARAM_NAME, CONDITION_PARAM_NAME, RESOLVER_PARAM_NAME } = require('jsonql-constants')
const debug = require('debug')('jsonql-params-validator:test:params-api')
import { JsonqlValidationError } from 'jsonql-errors'


test('It should able to construct correct query argument and extract from the other side', t => {

  let name = 'getUser';
  let args = [1];
  const query = createQueryStr(name, args)
  t.true(typeof query === 'string')
  const result = getQueryFromPayload(query)
  t.is(result[RESOLVER_PARAM_NAME], name)
  debug(result.args)
  t.deepEqual(result.args, args)

})

test('It should able to construct correct mutation argument and extract from the other side', t => {

  let name = 'changeSomething'
  let payload = 'whatever'
  let mutation = createMutationStr(name, payload)
  t.true(typeof mutation === 'string')
  const result = getMutationFromPayload(mutation)
  t.is(result[RESOLVER_PARAM_NAME], name)
  t.truthy(result[PAYLOAD_PARAM_NAME])
  t.truthy(result[CONDITION_PARAM_NAME])

})

test('Pass wrong param should throw error', t => {
  let fn = () => createQueryStr('whatever', 'xyz')

  let error = t.throws( () => {
    return fn()
  } , null, 'expect the args to be an array')
  // JsonqlValidationError the expect instance doesn't work???
})

test('It should able to use the jsonp option', t => {
  let name = 'whateverResolver'
  let payload = ['whatever']
  let queryPayload = createQueryStr(name, payload, true)

  debug(queryPayload)

  t.false( queryPayload.indexOf(name) > -1)

})
