# jsonql RX client

This is the Javascript client written in ES6+, using Fetch and [Kefir.js](https://kefirjs.github.io/)

# What's the different

The standard `jsonql-client` is using `Superagent` as it's heart, and return promise.
Where `jsonql-rx-client` always return stream. In practice, it normalized all the
results, be it a HTTP call or socket connection. Therefore you don't need to know
the transport layer, and operate in almost exactly the same like you are using
promise result.
