
export default class JsonqlEnumError extends Error {
  constructor(...args) {
    super(...args);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlEnumError);
    }
  }
}
