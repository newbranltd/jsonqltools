// check for boolean
import { isBoolean } from 'lodash-es';
/**
 * @param {boolean} value expected
 * @return {boolean} true if OK
 */
const checkIsBoolean = function(value) {
  return isBoolean(value);
};

export default checkIsBoolean;
