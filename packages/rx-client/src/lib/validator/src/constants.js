// Good practice rule - No magic number

export const ARGS_NOT_ARRAY_ERR = `args is not an array! You might want to do: ES6 Array.from(arguments) or ES5 Array.prototype.slice.call(arguments)`;
export const PARAMS_NOT_ARRAY_ERR = `params is not an array! Did something gone wrong when you generate the contract.json?`;
export const EXCEPTION_CASE_ERR = 'Could not understand your arguments and parameter structure!';
export const UNUSUAL_CASE_ERR = 'This is an unusual situation where the arguments are more than the params, but not mark as spread';

// re-export
import JSONQL_CONSTANTS from 'jsonql-constants';
// @TODO the jsdoc return array.<type> and we should also allow array<type> syntax
export const DEFAULT_TYPE = JSONQL_CONSTANTS.DEFAULT_TYPE;
export const ARRAY_TYPE_LFT = JSONQL_CONSTANTS.ARRAY_TYPE_LFT;
export const ARRAY_TYPE_RGT = JSONQL_CONSTANTS.ARRAY_TYPE_RGT;

export const TYPE_KEY = JSONQL_CONSTANTS.TYPE_KEY;
export const OPTIONAL_KEY = JSONQL_CONSTANTS.OPTIONAL_KEY;
export const ENUM_KEY = JSONQL_CONSTANTS.ENUM_KEY;
export const ARGS_KEY = JSONQL_CONSTANTS.ARGS_KEY;
export const CHECKER_KEY = JSONQL_CONSTANTS.CHECKER_KEY;
export const ALIAS_KEY = JSONQL_CONSTANTS.ALIAS_KEY;

export const ARRAY_TYPE = JSONQL_CONSTANTS.ARRAY_TYPE;
export const OBJECT_TYPE = JSONQL_CONSTANTS.OBJECT_TYPE;
export const STRING_TYPE = JSONQL_CONSTANTS.STRING_TYPE;
export const BOOLEAN_TYPE = JSONQL_CONSTANTS.BOOLEAN_TYPE;
export const NUMBER_TYPE = JSONQL_CONSTANTS.NUMBER_TYPE;
export const KEY_WORD = JSONQL_CONSTANTS.KEY_WORD;
export const OR_SEPERATOR = JSONQL_CONSTANTS.OR_SEPERATOR;

// not actually in use
// export const NUMBER_TYPES = JSONQL_CONSTANTS.NUMBER_TYPES;
