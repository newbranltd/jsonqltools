// export
import { checkIsObject } from './src';
import * as validator from './src/validator';
// PIA syntax
export const isObject = checkIsObject;
export const normalizeArgs = validator.normalizeArgs;
export const validateSync = validator.validateSync;
export const validateAsync = validator.validateAsync;

// configuration checking

import * as jsonqlOptions from './src/options';

export const createConfig = jsonqlOptions.createConfig;
export const constructConfig = jsonqlOptions.constructConfigFn;
export const checkConfigAsync = jsonqlOptions.checkConfigAsync(validator.validateSync);
export const checkConfig = jsonqlOptions.checkConfig(validator.validateSync);
export const JSONQL_PARAMS_VALIDATOR_INFO = jsonqlOptions.JSONQL_PARAMS_VALIDATOR_INFO;
