// just a simple util method to return the error based on the status code
/**
 * @param {number} statusCode to check
 * @param {boolean} contract if this is a contract call or not
 * @return {string} the error name
 */
export default function getErrorByStatus(statusCode, contract = false) {
  switch (statusCode) {
    case 401:
      return contract ? 'JsonqlContractAuthError' : 'JsonqlAuthorisationError';
    case 403:
      return 'JsonqlForbiddenError';
    case 404:
      return 'JsonqlResolverNotFoundError';
    case 406:
      return 'Jsonql406Error';
    case 500:
      return 'Jsonql500Error';
    default:
      return 'JsonqlError';
  }
}
