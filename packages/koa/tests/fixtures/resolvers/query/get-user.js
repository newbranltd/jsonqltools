// This is a new query method to test out if we actually get the userData props at the end of call
/**
 * This use a spread as parameter
 * @param {...string} args passing unknown number of param
 * @return {any} extract from last of the args
 */
module.exports = function getUser(...args) {
  const ctn = args.length;
  const lastProp = args[ctn - 1];
  // also test with the assigned property
  return getUser.userdata || {userId: 'dummy bear'};
}
