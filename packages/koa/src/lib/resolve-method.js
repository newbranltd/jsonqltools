// this was in the core-middleware now make this standalone for use in
// two middlewares
const { join } = require('path')
const {
  JsonqlResolverNotFoundError,
  JsonqlResolverAppError,
  JsonqlValidationError,
  JsonqlAuthorisationError
} = require('jsonql-errors')
const searchResolvers = require('./search-resolvers')
const validateAndCall = require('./validate-and-call')
const {
  getDebug,
  printError,
  handleOutput,
  extractArgsFromPayload,
  ctxErrorHandler,
  packResult
} = require('./utils')
const { provideUserdata } = require('jsonql-jwt')
const {
  DEFAULT_RESOLVER_IMPORT_FILE_NAME,
  MODULE_TYPE
} = require('jsonql-constants')
const debug = getDebug('resolve-method')

/**
 * New for ES6 module features
 * @param {string} resolverDir resolver directory
 * @param {string} type of resolver
 * @param {string} resolverName name of resolver
 * @return {function} the imported resolver
 */
function importFromModule(resolverDir, type, resolverName) {
  debug(resolverDir, type, resolverName)
  const resolvers = require( join(resolverDir, DEFAULT_RESOLVER_IMPORT_FILE_NAME) )
  return resolvers[type + resolverName]
}

/**
 * The method call has this signature
 * @param {object} ctx Koa context
 * @param {string} type of calls
 * @param {object} opts configuration
 * @param {object} contract to search via the file name info
 * @return {mixed} depends on the contract
 */
const resolveMethod = async (ctx, type, opts, contract) => {
  const { payload, resolverName, userdata } = ctx.state.jsonql;
  debug('resolveMethod', resolverName, payload, type)
  // There must be only one method call
  const renderHandler = handleOutput(opts)
  // first try to catch the resolve error
  try {
    let fn;
    const { sourceType } = contract;
    if (sourceType === MODULE_TYPE) {
      const { resolverDir } = opts;
      fn = importFromModule(resolverDir, type, resolverName)
    } else {
      fn = require(searchResolvers(resolverName, type, opts, contract))
    }
    const args = extractArgsFromPayload(payload, type)
    // here we could apply the userdata to the method
    const result = await validateAndCall(
      provideUserdata(fn, userdata), // always call it
      args,
      contract,
      type,
      resolverName,
      opts)
    // @TODO if we need to check returns in the future
    debug('called and now serve up', result)
    return renderHandler(ctx, packResult(result))
  } catch (e) {
    debug('resolveMethod error', e)
    let errorClassName = 'JsonqlError';
    switch (true) {
      case (e instanceof JsonqlResolverNotFoundError):
        errorClassName = 'JsonqlResolverNotFoundError';
        break;
      case (e instanceof JsonqlAuthorisationError):
        errorClassName = 'JsonqlAuthorisationError';
        break;
      case (e instanceof JsonqlValidationError):
        errorClassName = 'JsonqlValidationError';
        break;
      case (e instanceof JsonqlResolverAppError):
        errorClassName = 'JsonqlResolverAppError';
        break;
    }
    return ctxErrorHandler(ctx, errorClassName, e);
  }
};
// export
module.exports = resolveMethod;
