const fly = require('flyio')
const fsx = require('fsx')
const { createQuery, createMutation } = require('jsonql-params-validator')
const { clientErrorsHandler } = require('jsonql-errors')
const { API_REQUEST_METHODS } = require('jsonql-constants')
const [ POST, PUT ] = API_REQUEST_METHODS;
const { display, getDebug, resultHandler } = require('../utils')
const JsonqlClient = require('./jsonql-base-cls')

class JsonqlHttpClient extends JsonqlClient {

  constructor(config = {}) {
    super(config)
  }

  getContract() {
    return this.__readContract()
      .then(result => {
        if (typeof result === 'string') {
          return fsx.readJsonSync(result)
        }
        return result;
      })
  }

  

}
