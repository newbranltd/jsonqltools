/**
 * Base class to inherit from
 * @TODO we should move all the main back here
 * because the main interface should be a generator based on the contract
 */

const { join } = require('path')
const fsx = require('fs-extra')

const { decodeToken } = require('jsonql-jwt')
const merge = require('lodash.merge')

const { AUTH_HEADER , BEARER } = require('jsonql-constants')

// using node-cache from v1.1.0
const { getter, setter } = require('../cache')
const getRemote = require('./get-remote-contract')
const { getDebug } = require('../utils')
const debug = getDebug('jsonql-base-cls')

class JsonqlClient {

  constructor(config) {
    this.opts = config
    // @TODO we need to figure out an alternative way to store the session data
    this.__store__ = {};
    this.__url__ = [this.opts.hostname, this.opts.jsonqlPath].join('/')
  }

  /**
   * we decode the token using the jwt-decode method when useJwt === true
   * @return {mixed} false if the useJwt options is not on
   */
  get userdata() {
    if (this.opts.useJwt) {
      let token = this.__getAuthToken()
      return token ? decodeToken(token) : false;
    }
    return false;
  }

  /**
   * computed property
   * @return {object} for the accepted header
   */
  get baseHeader() {
    const { contentType } = this.opts;
    return {
      'Accept': contentType,
      'Content-Type': contentType
    };
  }

  /**
   * setter for the issuer method to use
   * @param {string} token to store
   * @return {string} on success
   */
  set token(token) {
    return this.__storeAuthToken(token)
  }

  /**
   * Built-in logout method even if the server doens't have it
   *
   */
  logout() {
    this.token = false;
  }

  /**
   * a phony intercepter for now until we replace it with flyio
   * @param {object} payload send to server
   * @param {mixed} body from server with the data key
   */
  __interceptor(payload, body) {
    const result = typeof body === 'string' ? JSON.parse(body) : body;
    const resolverName = Object.keys(payload)[0];
    const { loginHandlerName, logoutHandlerName } = this.opts;
    switch (true) {
      case loginHandlerName === resolverName:
        debug(`intercept the ${loginHandlerName}`)
        if (result.data) {
          this.token = result.data;
        }
        break;
      case logoutHandlerName === resolverName:
        debug(`intercept the ${logoutHandlerName}`)
        this.logout()
        break;
    }
    return result;
  }

  /**
   * @return {object} cache burster
   * @private
   */
  __cacheBurst() {
    return { _cb: (new Date()).getTime() };
  }

  /**
   * @param {object} header to set
   * @return {object} combined header
   * @private
   */
  __createHeaders(header = {}) {
    const authHeader = this.__getAuthHeader();
    return merge({}, this.baseHeader, header, authHeader);
  }

  /**
   * Check if the contract is expired or not
   * @param {object} contract what it said
   * @return {boolean} false on OK
   */
  __isContractExpired(contract) {
    if (contract) {
      const { expired } = contract;
      if (!expired) {
        return false;
      }
      let now = Math.round(Date.now() / 1000)
      if (now >= expired) {
        return true;
      }
    }
    return false;
  }

  /**
   * Return the stored contract if any
   * @TODO need an option to compare the time and check with the server
   * @return {object} return a promise to resolve it
   */
  __readContract() {
    const { contract } = this.opts;
    // first check if we have a contract already in memory
    if (contract && typeof contract === 'object' && (contract.query || contract.mutation)) {
      debug('return contract from opts')
      if (!this.__isContractExpired(contract)) {
        return Promise.resolve(contract)
      }
    }
    // check if there is a contract store locally
    const file = join(this.opts.contractDir, this.opts.contractFileName);
    if (fsx.existsSync(file)) {
      debug('return contract from file: ', file)
      if (!this.__isContractExpired(contract)) {
        return Promise.resolve(fsx.readJsonSync(file))
      }
    }
    // now grab it from remote
    let authHeader = {}
    if (this.opts.contractKey !== false) {
      authHeader = {[this.opts.contractKeyName]: this.opts.contractKey}
    }
    // use the remote method to get it
    return getRemote(Object.assign({
      remote: this.__url__,
      out: this.opts.contractDir
    }, { authHeader }))
  }

  /**
   * Store the auth token into session storage
   * @return {string} token
   */
  __storeAuthToken(token) {
    let r = setter('token', token) ? token : false;
    debug('store token --> ', r)
    return r;
  }

  /**
   * Get the authorization header
   * @return {object}  Auth header
   */
  __getAuthHeader() {
    let t = this.__getAuthToken()
    return t ? { [AUTH_HEADER]: `${BEARER} ${t}` } : {};
  }

  /**
   * Get the stored auth token from session storage
   * @return {string} auth token
   */
  __getAuthToken() {
    let v = getter('token')
    debug('get token from node-cache', v)
    return v;
  }

  /**
   * When we have the contractKey setup then we need to include this into heading
   * @return {object} combine object
   */
  __getAuth() {
    const extraParams = this.__cacheBurst();
    if (this.opts.contractKey === false) {
      return extraParams;
    }
    const contractAuth = {[this.opts.contractKeyName]: this.opts.contractKey};
    // debug('let see the contract key value pair', contractAuth);
    return merge({}, contractAuth, extraParams);
  }
}

// export
module.exports = JsonqlClient;
