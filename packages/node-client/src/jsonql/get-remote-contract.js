// fetching the remote contract and save locally
// jsonql-contract --remote=http://hostname/jsonql
const request = require('request')
const { join, resolve } = require('path')
const fsx = require('fs-extra')
const {
  CONTENT_TYPE,
  PUBLIC_CONTRACT_FILE_NAME,
  KEY_WORD,
  DEFAULT_HEADER
} = require('jsonql-constants')
const { getDebug } = require('../utils')
const debug = getDebug('get-remote-contract')

// export it
module.exports = function(args) {
  return new Promise((resolver, rejecter) => {
    // defintely we have the remote or not here
    if (!args.remote) {
      return resolver(KEY_WORD) // allow the next chain to do stuff
    }
    // options
    const options = {
      url: args.remote,
      headers: Object.assign(DEFAULT_HEADER, args.authHeader)
    }

    debug('calling remote server to get contract with %O', options)
    // method
    function callback(error, response, body) {
      if (!error && response.statusCode == 200) {
        const json = JSON.parse(body)
        // @v1.4.3 the server now standardize the return in the same format
        if (!json.data) {
          return rejecter(json.error || 'Server return wrong contract format')
        }
        const contract = json.data;
        const outFile = join( resolve(args.out) || process.cwd(), PUBLIC_CONTRACT_FILE_NAME )
        // v1.1.4 add raw output
        if (args.raw) {
          return resolver(contract)
        }
        fsx.outputJson(outFile, contract, {spaces: 2}, err => {
          if (err) {
            return rejecter(err)
          }
          const result = args.returnAs && args.returnAs === 'file' ? outFile : contract;
          resolver(result)
        });
      }
    }
    // execute the remote call
    request(options, callback)
  })
}
