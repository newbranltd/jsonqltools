const { join } = require('path')
const fs = require('fs')
const {
  constructConfig,
  createConfig,
  checkConfigAsync
} = require('jsonql-params-validator')

const {
  JSONQL_PATH,
  CONTENT_TYPE,
  CLIENT_STORAGE_KEY,
  CLIENT_AUTH_KEY,
  CONTRACT_KEY_NAME,
  PUBLIC_CONTRACT_FILE_NAME,
  DEFAULT_HEADER,
  DEFAULT_RESOLVER_DIR,
  DEFAULT_CONTRACT_DIR,
  BOOLEAN_TYPE,
  STRING_TYPE,
  NUMBER_TYPE,
  ARGS_KEY,
  TYPE_KEY,
  ENUM_KEY,
  CHECKER_KEY,
  ACCEPTED_JS_TYPES,
  CJS_TYPE,
  ISSUER_NAME,
  LOGOUT_NAME,
  VALIDATOR_NAME
} = require('jsonql-constants')

const BASE_DIR = process.cwd()
// properties
const constProps = {
  contentType: CONTENT_TYPE,
  contract: {},
  useDoc: true,
  returnAs: 'json'
}
const appProps = {

  useJwt: createConfig(false, [BOOLEAN_TYPE]),

  loginHandlerName: createConfig(ISSUER_NAME, [STRING_TYPE]),
  logoutHandlerName: createConfig(LOGOUT_NAME, [STRING_TYPE]),
  validatorHandlerName: createConfig(VALIDATOR_NAME, [STRING_TYPE]),

  enableAuth: createConfig(false, [BOOLEAN_TYPE]),
  hostname: constructConfig('', STRING_TYPE), // required the hostname
  jsonqlPath: constructConfig(JSONQL_PATH, STRING_TYPE), // The path on the server

  // useLocalstorage: constructConfig(true, BOOLEAN_TYPE), // should we store the contract into localStorage
  storageKey: constructConfig(CLIENT_STORAGE_KEY, STRING_TYPE),// the key to use when store into localStorage
  authKey: constructConfig(CLIENT_AUTH_KEY, STRING_TYPE),// the key to use when store into the sessionStorage

  contractKey: constructConfig(false,  [BOOLEAN_TYPE, STRING_TYPE]), // if the server side is lock by the key you need this
  contractKeyName: constructConfig(CONTRACT_KEY_NAME, STRING_TYPE),// same as above they go in pairs
  // we don't really need to check this, it will get generated
  contractDir: constructConfig( join(BASE_DIR, DEFAULT_RESOLVER_DIR), STRING_TYPE),
  contractFileName: constructConfig(PUBLIC_CONTRACT_FILE_NAME, STRING_TYPE),
  // functions
  storeAuthToken: constructConfig(false, BOOLEAN_TYPE),
  getAuthToken: constructConfig(false, BOOLEAN_TYPE),
  defaultHeader: constructConfig(DEFAULT_HEADER, STRING_TYPE)
}
// debug('appProps', appProps);
// export just one method
module.exports = function(config) {
  let { contract } = config;
  return checkConfigAsync(config, appProps, constProps)
    .then(opts => {
      opts.contract = contract;
      return opts;
    })
}
