// this is the top level of app
const {
  JsonqlRequestClient,
  getContract,
  generator,
  checkOptions
} = require('./src')

// const debug = require('debug')('jsonql-node-client:index');
// finally
/**
 * The config takes two things
 * hostname, contractDir
 * if there is security feature on then the contractKey
 * @param {object} config configuration the only require field is hostname
 * @return {object} promise to resolve the jsonql node client
 */
module.exports = function(config) {
  return checkOptions(config)
    .then(opts => (
      {
        jsonqlInstance: new JsonqlRequestClient(opts),
        opts: opts
      }
    ))
    .then( ({jsonqlInstance, opts}) => (
      getContract(jsonqlInstance, opts.contract)
        .then(
          contract => generator(jsonqlInstance, opts, contract)
        )
      )
    )
}
