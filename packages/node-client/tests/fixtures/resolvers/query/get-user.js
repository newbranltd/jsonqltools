// const debug = require('debug')('jsonql-node-client:test:get-user');
const { users, msg } = require('../../options');
const { JsonqlResolverAppError } = require('jsonql-errors');
/**
 * @param {number} id
 * @return {object|string} user object on ok
 */
module.exports = function getUser(id) {
  const index = parseInt(id, 10);
  const ctn = users.length;
  if (index > ctn) {
    throw new JsonqlResolverAppError('Index exceeds the size of array');
  }
  return users[ index ];
}
