const debug = require('debug')('jsonql-node-client:test:issuer');
const { token } = require('../../options');

/**
 * @param {string} userToken
 * @return {boolean} true on ok
 */
module.exports = function validator(userToken) {
  return token === userToken ? 1 : 0;
}
