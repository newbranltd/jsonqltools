const debug = require('debug')('jsonql-node-client:test:login');
const { loginToken, token } = require('../../options');
/**
 * @param {string} credential to check
 * @return {boolean|string} string on ok
 */
module.exports = function(credential) {
  debug(`I got ${credential}`);
  return credential === loginToken ? token : false;
}
