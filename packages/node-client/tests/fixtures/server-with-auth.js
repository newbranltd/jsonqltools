const server = require('server-io-core');
const jsonql = require('jsonql-koa');
const { contractKey } = require('./options');
const { join } = require('path');

module.exports = function(port = 8889, extra = {}) {
  return server({
    port: port,
    debug: false,
    open: false,
    reload: false,
    middlewares: [
      jsonql(Object.assign({
        contractKey,
        resolverDir: join(__dirname,'resolvers'),
        contractDir: join(__dirname,'contract','tmp','server-with-auth'),
        enableAuth: true
      }, extra))
    ]
  });
}
