const contractApi = require('jsonql-contract');
const fsx = require('fs-extra');
const { join } = require('path');

module.exports = (clean = false) => {
  const outDir = join(__dirname, 'contract', 'tmp' , 'server');
  if (clean === true) {
    return fsx.removeSync(join(ourDir, 'contract.json'));
  }
  return contractApi({
    inDir: join(__dirname, 'resolvers'),
    outDir
  }).then(() => fsx.readJsonSync(join(outDir, 'contract.json')) );
};
