const test = require('ava')
const { join } = require('path')
const server = require('./fixtures/server')
const options = require('./fixtures/options')
const nodeClient = require('../index')

const debug = require('debug')('jsonql-node-client:test:main')

test.before(async t => {
  // create server
  const { stop } = await server();
  t.context.stop = stop;
  // create client
  t.context.client = await nodeClient({
    hostname:'http://localhost:8888',
    contractDir: join(__dirname,'fixtures','contract','tmp', 'client')
  });
});

test.after(async t => {
  t.context.stop();
});

test('Should able to say Hello world!' , async t => {
  // debug(t.context.client);
  const result = await t.context.client.query.helloWorld();
  t.is('Hello world!', result);
});
