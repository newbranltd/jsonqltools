# jsonql-node-server

This is an all in one setup to have your jsonql API server setup in no time.

It includes:

1. Koa
2. bodyparser, cors middleware
3. jsonql-koa middleware
4. jsonql-ws-server

## Configuration

TBC

---

MIT (c) 2019 NEWBRAN LTD / TO1SOURCE CN
