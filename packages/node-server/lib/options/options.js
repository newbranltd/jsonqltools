const { createConfig } = require('jsonql-params-validator');
const {
  BOOLEAN_TYPE
} = require('jsonql-constants');


const options = {
  autoStart: createConfig(true, [BOOLEAN_TYPE])
};


module.exports = {
  options
};
