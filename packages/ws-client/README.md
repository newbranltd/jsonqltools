# jsonql-ws-client

This is the client for [jsonql-ws-server](https://www.npmjs.com/package/jsonql-ws-server)
with [socket.io-client](https://github.com/socketio/socket.io-client) and [isomorphic-ws](https://github.com/heineiuo/isomorphic-ws)

** ONLY THE WebSocket version is working at the moment on node **

# Installation

```sh
$ npm i jsonql-ws-client
```

You don't use this module directly. We published it only for management purpose only.
This will be part of the [jsonql-rx-client](https://www.npmjs.com/package/jsonql-rx-client).

---

MIT

[Joel Chu](https://joelchu.com) (c) 2019

[NEWBRAN LTD](https://newbran.ch) [to1source CN](https://to1source.cn)
