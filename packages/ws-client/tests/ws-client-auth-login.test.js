// standard ws client test without auth
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')

const wsClient = require('../main')
const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))
const { NOT_LOGIN_ERR_MSG } = require('jsonql-constants')
const payload = {name: 'Joel'};

const debug = require('debug')('jsonql-ws-client:test:ws-auth-login')

const port = 8003;

test.before(async t => {

  const { io, app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    serverType: 'ws',
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })
  t.context.server = app.listen(port)
  // without the token
  t.context.client = await wsClient({
    serverType: 'ws',
    hostname: `ws://localhost:${port}`,
    contract: publicContract,
    enableAuth: true,
    useJwt: true
  })

})

test.after(t => {
  t.context.server.close()
})
// access the public namespace

// @BUG the login is not quite 100% yet, but we need to test this more
// in the browser, the problem is this is bi-directional and
// when we add a client to connect to the server, it could create some
// strange effects. We might have to issue an LOGOUT before we attempt to
// LOGIN to clear out all the existing connections

test.serial.cb('It should able to connect to the ws server public namespace', t => {
  let ctn = 0;
  t.plan(2)
  let client = t.context.client;
  client.pinging('Hello')

  client.pinging.onResult= (res) => {
    debug('res', res)
    t.is(res, 'connection established')
    client.pinging.send = 'ping';
  }
  // the send is happen after the result return on the server side
  client.pinging.onMessage = function(msg) {
    if (msg==='pong') {
      client.pinging.send = 'pong';
    } else {
      client.pinging.send = 'giving up';
      debug('TEST SHOULD HALT HERE')
      t.pass()
      t.end()
    }
  }
})

test.serial.cb('It should trigger the login call here', t => {
  t.plan(2)
  let client = t.context.client;
  let ctn = 0;
  // add onReady and wait for the login to happen
  client.onReady = function(namespace) {
    debug('onReady -->', namespace, client.simple.myNamespace)
    if (namespace === client.simple.myNamespace) {
      client.simple(200)
      client.simple.onResult = (result) => {
        debug('simple onResult pass (3)', result)
        t.pass()
        t.end()
      }
    }
  }

  client.simple(100)
  client.simple.onError = (error) => {
    if (!ctn) {
      t.is(NOT_LOGIN_ERR_MSG, error.message, 'pass (2)')
      const token = genToken(payload)
      client.login(token)
      ++ctn;
    }
  }
})
