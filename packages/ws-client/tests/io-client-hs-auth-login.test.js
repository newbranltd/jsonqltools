// standard ws client test without auth
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const { JS_WS_SOCKET_IO_NAME } = require('jsonql-constants')

const wsClient = require('../main')
const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))
const keysDir = join(__dirname, 'fixtures', 'keys')

const debug = require('debug')('jsonql-ws-client:test:io-hs-login')

const port = 8008;

test.before(async t => {
  const { io, app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    keysDir,
    serverType: JS_WS_SOCKET_IO_NAME,
    enableAuth: true,
    useJwt: true
  })

  t.context.server = app.listen(port)

  t.context.client = await wsClient({
    serverType: JS_WS_SOCKET_IO_NAME,
    hostname: `ws://localhost:${port}`,
    contract: publicContract,
    enableAuth: true,
    useJwt: true
  })
})

test.after(t => {
  t.context.server.close()
})

test.serial.cb('It should able to connect to the socket.io server public namespace', t => {
  t.plan(1)
  let client = t.context.client;
  client.pinging('Hello')
  client.pinging.onResult = (res) => {
      debug('res', res)
      t.is(res, 'connection established')
      t.end()
    }
})
// this is confirm fucked becasue of the socket.io bug
// https://github.com/auth0-community/socketio-jwt/issues/95
test.serial.cb('It should able to call the login method and establish socket.io connection using handshake', t => {
  t.plan(1)
  let client = t.context.client;
  const payload = {name: 'Joel'}
  const token = genToken(payload)
  client.login(token)
  // add onReady and wait for the login to happen
  client.onReady = function(namespace) {
    debug('--> onReady', namespace, client.sendExtraMsg.myNamespace)
    if (namespace === client.sendExtraMsg.myNamespace) {
      client.sendExtraMsg(200)
      client.sendExtraMsg.onResult = (result) => {
        debug('login success', result)
        t.is(201, result)
        t.end()
      }
      // this should work
      // but this will happen before the above Promise.resolve
      client.sendExtraMsg.onMessage = function(result) {
        debug('onMessge got news', result)
      }
    }
  }
})
