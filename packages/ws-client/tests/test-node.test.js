/// breaking things apart and try to figure out what went wrong at the last step
const test = require('ava')
const debug = require('debug')('jsonql-ws-client:test:node')
/// SERVER SETUP ///
const { join } = require('path')
const fsx = require('fs-extra')

const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))
const { NOT_LOGIN_ERR_MSG, JS_WS_SOCKET_IO_NAME, JS_WS_NAME } = require('jsonql-constants')
const payload = {name: 'Joel'};
const token = genToken(payload)
const port = 8010;
const url = `ws://localhost:${port}`
////////////////////
const {
  chainCreateNsps,
  clientGenerator,
  es
} = require('./fixtures/node')

/// PREPARE TEST ///
test.before(async t => {
  const { io, app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    serverType: JS_WS_SOCKET_IO_NAME,
    enableAuth: true,
    useJwt: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })

  t.context.server = app.listen(port)

  let config = { opts: { serverType: JS_WS_SOCKET_IO_NAME }, nspMap: {}, ee: es };
  let { opts, ee } =  clientGenerator(config)
  t.context.opts = opts;
  t.context.ee = ee;
})

// real test start here
test.serial.cb('It should able to replace the same event with new method', t => {

  t.plan(3)
  // try a sequence with swapping out the event handler
  let ee = t.context.ee;
  let evtName = 'main-event';
  let fnName = 'testFn';

  ee.$on(evtName, (from, value) => {
    debug(evtName, from, value)
    // (1)
    t.is(from, fnName)
    return ++value;
  })
  // first trigger it
  ee.$call(evtName, [fnName, 1])
  // (2)
  t.is(ee.$done, 2)
  // now replace this event with another callback
  ee.$replace(evtName, (from, value) => {
    debug(evtName, from, value)
    // (3)
    t.is(value, 3)
    t.end()
    return --value;
  })
  ee.$call(evtName, [fnName, 3])
})

test.serial.cb.only('It should able to resolve the promise one after the other', t => {
  t.plan(1)
  let opts = t.context.opts;
  let p1 = () => opts.nspAuthClient([url, 'jsonql/private'].join('/'), token)
  let p2 = () => opts.nspClient([url, 'jsonql/public'].join('/'))
  /*
  let p1 = () => new Promise(resolver => {
      setTimeout(() => {
        resolver('first')
      }, 1000)
    })
  let p2 = () => new Promise(resolver => { resolver('second') })
  */
  chainCreateNsps([
    p1(), p2()
  ]).then(results => {
    debug(results)
    t.pass()
    t.end()
  })
})

test.serial.cb('Just test with the ws client can connect to the server normally', t => {
  t.plan(2)
  let opts = t.context.opts;

  // t.truthy(opts.nspClient)
  // t.truthy(opts.nspAuthClient)

  opts.nspAuthClient([url, 'jsonql/private'].join('/'), token)
    .then(socket => {
      debug('io1 pass')
      t.pass()
      opts.nspClient([url, 'jsonql/public'].join('/'))
        .then( socket => {
          debug('io2 pass')
          t.pass()
          t.end()
        })
    })

  /*
  this was for ws
  ws1.onopen = function() {
    t.pass()
    debug('ws1 connected')
  }
  ws2.onopen = function() {
    t.pass()
    debug('ws2 connected')
    ws1.terminate()
    ws2.terminate()
    t.end()
  }
  */
})


test.serial.cb('It should able to use the chainCreateNsps to run connection in sequence', t => {
  t.plan(1)
  let opts = t.context.opts;

  // t.truthy(opts.nspClientAsync)
  // t.truthy(opts.nspAuthClientAsync)

  opts.nspAuthClientAsync([url, 'jsonql/private'].join('/'), token)
    .then(ws => {
      t.pass()
      t.end()
    })

    // the bug is the chainCreateNsps somehow when we put it in there
    // the connection just hang

  /*
  chainCreateNsps([
    opts.nspAuthClientAsync([url, 'jsonql/private'].join('/'), token),
    // opts.nspClientAsync([url, 'jsonql/public'].join('/'))
  ]).then(nsps => {
    t.is(nsps.length, 2)
    nsps.forEach(nsp => {
      nsp.terminate()
    })
    t.end()
  })
  */
})

test.serial.cb.skip('It should able to wrap the connect call with the onopen callback', t => {
  t.plan(3)
  let opts = t.context.opts;
  let ee = t.context.ee;
  let publicConnect = () => {
    return opts.nspClientAsync([url, 'jsonql/public'].join('/'))
      .then(ws => {
        ws.onopen = function() {
          ee.$trigger('onReady1', ws)
        }
        return 'jsonql/public'
      })
  }

  let privateConnect = () => {
    return opts.nspAuthClientAsync([url, 'jsonql/private'].join('/'), token)
      .then(ws => {
        ws.onopen = function() {
          ee.$trigger('onReady2', ws)
        }
        return 'jsonql/private'
      })
  }

  chainCreateNsps([
    privateConnect(),
    publicConnect()
  ]).then(namespaces => {
    t.is(namespaces.length, 2)
  })

  ee.$on('onReady1', function() {
    t.pass()
    t.end()
  })

  ee.$on('onReady2', function() {
    t.pass()
  })

})
