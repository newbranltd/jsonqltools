// standard ws client test without auth
const test = require('ava')

const wsClient = require('../main')
const serverSetup = require('./fixtures/server-setup')

const { join } = require('path')
const fsx = require('fs-extra')

const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))

const debug = require('debug')('jsonql-ws-client:test:ws-auth')

const payload = {name: 'Joel'};
const token = genToken(payload)
const port = 8002;

test.before(async t => {

  const { io, app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    serverType: 'ws',
    enableAuth: true,
    // useJwt: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })
  t.context.server = app.listen(port)

  t.context.client = await wsClient({
    token,
    serverType: 'ws',
    hostname: `ws://localhost:${port}`,
    contract: publicContract,
    enableAuth: true,
    useJwt: true
  })
})

test.after(t => {
  t.context.server.close()
})

test.serial.cb('It should able to connect to the WebSocket public namespace', t => {
  t.plan(2)
  let client = t.context.client;
  client.pinging('Hello')

  client.pinging.onResult = res => {
      debug('res', res)
      t.is(res, 'connection established')
      client.pinging.send = 'ping';
    }
  // start the ping pong game
  client.pinging.onMessage = function(msg) {
    if (msg==='pong') {
      client.pinging.send = 'pong';
    } else {
      client.pinging.send = 'giving up';
      t.is(msg, 'ping')
      t.end()
    }
  }
})

test.serial.cb('It should able to connect to a WebSocket private namespace', t => {
  t.plan(2)
  let client = t.context.client;
  client.onReady = (w) => {
    debug('onReady again', w)
  }

  client.sendExtraMsg(100)

  client.sendExtraMsg.onResult = (result) => {
    t.is(101, result)
    t.end()
  }

  client.sendExtraMsg.onMessage = function(num) {
    t.is(102, num)
  }

  client.onReady = (w) => {
    debug('onReady -->', w)
    return w;
  }

})
