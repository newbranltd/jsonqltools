// testing the chain methods
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')

const {
  chainPromises,
  wsNodeClient,
  wsNodeAuthClient
} = require('jsonql-jwt')


const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))

const { NOT_LOGIN_ERR_MSG } = require('jsonql-constants')

const payload = {name: 'Joel'};
const token = genToken(payload)

const debug = require('debug')('jsonql-ws-client:test:ws-client-chain')

const port = 8009;

test.before(async t => {
  const { io, app } = await serverSetup({
    contract,
    contractDir,
    resolverDir: join(__dirname, 'fixtures', 'resolvers'),
    serverType: 'ws',
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })
  t.context.server = app.listen(port)

  let baseUrl = `ws://localhost:${port}`;
  t.context.nsp1url = [baseUrl, 'jsonql/private'].join('/')
  t.context.nsp2url = [baseUrl, 'jsonql/public'].join('/')
})

test.serial.cb('First test the connection individually', t => {
  t.plan(2)
  let ws1 = wsNodeAuthClient(t.context.nsp1url, token)
  let ws2 = wsNodeClient(t.context.nsp2url)

  ws1.onopen = function() {
    t.pass()
    ws1.terminate()
  }

  ws2.onopen = function() {
    t.pass()
    ws2.terminate()
    t.end()
  }

})
// @BUG whenever I wrap this code in the promise the ws just hang up
test.serial.cb.skip('Try to create a promise based ws client and using the chainPromises method to login', t => {
  t.plan(1)

  let p1 = () => (
    new Promise((resolver, rejecter) => {
      let ws1 = wsNodeAuthClient(nsp1url, token)
      let timer;
      setTimeout(() => {
        rejecter()
      }, 5000)
      ws1.onopen = () => {
        clearTimeout(timer)
        resolver(ws1)
      }
    })
  )
  let p2 = () => (
    new Promise((resolver, rejecter) => {
      let ws2 = wsNodeClient(nsp2url)
      let timer;
      setTimeout(() => {
        rejecter()
      }, 5000)
      ws2.onopen = () => {
        clearTimeout(timer)
        resolver(ws2)
      }
    })
  )

  chainPromises([p1(), p2()])
    .then(nsps => {
      t.is(nsps.length, 2)
      t.end()
    })

})
