// standard ws client test without auth
const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
const { JS_WS_SOCKET_IO_NAME, IO_ROUNDTRIP_LOGIN } = require('jsonql-constants')
const debug = require('debug')('jsonql-ws-client:test:io-rt')

const wsClient = require('../main')
const serverSetup = require('./fixtures/server-setup')
const genToken = require('./fixtures/token')

const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const publicContract = fsx.readJsonSync(join(contractDir, 'public-contract.json'))
const keysDir = join(__dirname, 'fixtures', 'keys')

const {
  chainPromises,
  socketIoNodeRoundtripLogin,
  socketIoNodeClientAsync
} = require('jsonql-jwt')

const secret = '12345678';
const payload = {name: 'Joel'}
const token = genToken(payload, secret)

const port = 8005;

test.before(async t => {
  const { io, app } = await serverSetup({
    contract,
    contractDir,
    keysDir,
    serverType: JS_WS_SOCKET_IO_NAME,
    enableAuth: true,
    useJwt: secret // <-- this is the secret
  })

  t.context.server = app.listen(port)
})

test.after(t => {
  t.context.server.close()
})

test.cb('Just testing the chainPromises method with socket.io round trip login method', t => {
  t.plan(1)

  let baseUrl = `ws://localhost:${port}/jsonql`
  let nsp1url = [baseUrl, 'private'].join('/')
  let nsp2url = [baseUrl, 'public'].join('/')

  let p1 = () => socketIoNodeRoundtripLogin(nsp1url, token)
  let p2 = () => socketIoNodeClientAsync(nsp2url)

  chainPromises([p1(), p2()])
    .then(nsps => {
      t.is(nsps.length, 2)
      t.end()
    })
})
