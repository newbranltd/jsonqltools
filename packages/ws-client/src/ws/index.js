// we only need to export one interface from now on

import createClient from './create-client'

export default createClient
