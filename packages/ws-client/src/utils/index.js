// export the util methods
import { QUERY_ARG_NAME, JS_WS_SOCKET_IO_NAME } from 'jsonql-constants'
import getNamespaceInOrder from './get-namespace-in-order'
import checkOptions from './check-options'
import ee from './ee'
import * as constants from './constants'
import getDebug from './get-debug'

import processContract from './process-contract'
import { isArray } from 'jsonql-params-validator'

const toArray = (arg) => isArray(arg) ? arg : [arg];

/**
 * very simple tool to create the event name
 * @param {string} [...args] spread
 * @return {string} join by _
 */
const createEvt = (...args) => args.join('_')

/**
 * Unbind the event
 * @param {object} ee EventEmitter
 * @param {string} namespace
 * @return {void}
 */
const clearMainEmitEvt = (ee, namespace) => {
  let nsps = isArray(namespace) ? namespace : [namespace]
  nsps.forEach(n => {
    ee.$off(createEvt(n, constants.EMIT_EVT))
  })
}

/**
 * @param {*} args arguments to send
 *@return {object} formatted payload
 */
const formatPayload = (args) => (
  { [QUERY_ARG_NAME]: args }
)

/**
 * @param {object} nsps namespace as key
 * @param {string} type of server
 */
const disconnect = (nsps, type = JS_WS_SOCKET_IO_NAME) => {
  try {
    const method = type === JS_WS_SOCKET_IO_NAME ? 'disconnect' : 'terminate';
    for (let namespace in nsps) {
      let nsp = nsps[namespace]
      if (nsp && nsp[method]) {
        Reflect.apply(nsp[method], null, [])
      }
    }
  } catch(e) {
    // socket.io throw a this.destroy of undefined?
    console.error('disconnect', e)
  }
}


export {
  getNamespaceInOrder,
  createEvt,
  clearMainEmitEvt,
  checkOptions,
  ee,
  constants,
  getDebug,
  processContract,
  toArray,
  formatPayload,
  disconnect
}
