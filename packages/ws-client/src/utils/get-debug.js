
import debug from 'debug'
/**
 * Try to normalize it to use between browser and node
 * @param {string} name for the debug output
 * @return {function} debug
 */
const getDebug = name => {
  if (debug) {
    return debug('jsonql-ws-client').extend(name)
  }
  return (...args) => {
    console.info.apply(null, [name].concat(args));
  }
}
try {
  if (window && window.localStorage) {
    localStorage.setItem('DEBUG', 'jsonql-ws-client*');
  }
} catch(e) {}
// export it
export default getDebug;
