/**
 * Rollup config for browser version of the
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'

import { terser } from "rollup-plugin-terser"

import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'

import json from 'rollup-plugin-json'

import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
// support async functions
import async from 'rollup-plugin-async'
// get the version info
import { version } from './package.json'

const env = process.env.NODE_ENV

let plugins = [
  json({
    preferConst: true
  }),
  buble({
    objectAssign: 'Object.assign',
    transforms: { dangerousForOf: true }
  }),
  nodeResolve({
    preferBuiltins: false
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__PLACEHOLDER__': `version: ${version} module: umd`
  })
]

if (process.env.NODE_ENV === 'production') {
  plugins.push(  terser() )
}
plugins.push( size() )

let config = {
  input: join(__dirname, 'index.js'),
  output: {
    name: 'jsonqlWsClient',
    file: join(__dirname, 'dist', 'jsonql-ws-client.js'),
    format: 'umd',
    sourcemap: true,
    globals: {
      'WebSocket': 'ws',
      'socket.io-client': 'io',
      'promise-polyfill': 'Promise',
      'debug': 'debug'
    }
  },
  external: [
    'WebSocket',
    'socket.io-client',
    'io',
    'debug',
    'Promise',
    'promise-polyfill'
  ],
  plugins: plugins
};

export default config;
