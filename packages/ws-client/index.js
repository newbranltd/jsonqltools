// This is the module entry point
import clientGenerator from './src/utils/client-generator'
import main from './src/main'

export default main(clientGenerator)
