[![NPM](https://nodei.co/npm/jsonql-client.png?compact=true)](https://npmjs.org/package/jsonql-client)

# json:ql client

~~This is superagent JS client for our json:ql~~ As of the version 1 release. We use the native `Fetch`
for the HTTP transport and no longer require a third party library. If you need to use it with older
browser, there is a different file you can access via the `dist/jsonql-client-with-polyfill.js`  

## Installation

```sh
$ npm install jsonql-client
```

or

```sh
$ yarn add jsonql-client
```

## Example

### V1.1.0 we add a JsonqlClient (class) version

You will have to include the file manually in the `dist/jsonql-client.cls.js` into your build chain or HTML page.

```js
var client = new JsonqlClient();

client.ready(function(cls) {
  cls.query.helloWorld().then(function(msg) {
    console.log(msg);
  });
});

```

### Using the singleton jsonqlClient version

We recommend you use the latest ES6+ with module bundler tools like [rollup](https://rollupjs.org/guide/en).

Here is a recommended setup.

```js
// First create a file call client.js
import jsonqlClient from 'jsonql-client';
// more about this later
import { config, contract } from './options';
// The jsonqlClient actually return a promise
const generator = async () => await jsonqlClient({ contract });
const client = generator();
export default client;
```

Now in the other places where you need to use it.

```js
import { query, mutation, auth } from './client';
// This is a built in resolver, it will always be available
query.helloWorld().then(msg => console.log(msg));

```

As of the beta release of the [jsonql-koa](https://www.npmjs.com/package/jsonql-koa). It supports contract lock down, and you will have to provide a password.

Go back to your client.js

```js
// First create a file call client.js
import jsonqlClient from 'jsonql-client';
// The jsonqlClient actually return a promise
const generator = async () => (
  await jsonqlClient({
    contractKey: 'A_KEY_YOU_NEED_TO_SEND'
  })
);
const client = generator();
export default client;
```

Also its recommended to provide a static contract file, instead of using the dynamic option.
We are currently working on a command line client for you grab the contract, also integrate into
some of the popular work flow.

**This is a non ES6 example, which is what we write during the test in browser with QUnit**

```js
$(function() {
  jsonqlClient() // fully automatic
    .then(function(client) {
      // now you can use it
      client.query.getSomething().then(function(result) {
        // do your thing
      });
    });
});
```

## Server side

We have a node version client [jsonql-node-client](https://www.npmjs.com/package/jsonql-node-client).

## TODOS

- replace Fetch with flyio
- enable multiple profile login
- add jsonp adapter (jsonql-cors)
- add http to socket adapter (jsonql-cors)

---

MIT (c) 2019 https://to1source.cn
in collaboration with https://newbran.ch
