
const runQunitSetup = require('./run-qunit-setup')
const config = {
  "port": 8081,
  "webroot": [
    "./tests/qunit/webroot",
    "./tests/qunit/files",
    "./dist",
    "./node_modules"
  ],
  "open": true,
  "reload": true,
  "testFilePattern": "*-test.js",
  "baseDir": "/home/joel/projects/open-source/jsonqltools/packages/client/tests"
}

runQunitSetup(config)
