// testing the stores
var localStore;
/*
QUnit.module( "store", {
  beforeEach: function( assert ) {

    // assert.ok( true, "one extra assert per test" );
  }, afterEach: function( assert ) {
    // assert.ok( true, "and one extra assert after each test" );

  }
})
*/
QUnit.test('It should able to use the client to contact the server with static contract', function(assert) {
  localStore = JsonqlStores.localStore;
  //// assert.expect(2)

  localStore.set('someKey[0]', 'test data')

  assert.ok('test data', localStore.get('someKey[0]'))

  var done1 = assert.async()
  var toset = 'just some value'

  localStore.watch('being-watch', function(value) {
    console.info('value', value)
    let val = localStore.get('being-watch')
    assert.ok(val, toset)



    done1()
  })

  localStore.set('being-watch', toset)

  setTimeout(function() {
    localStore.remove('someKey[0]')
    localStore.remove('being-watch')
  }, 1000)

})
