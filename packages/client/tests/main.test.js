const { join } = require('path');
const { inspect } = require('util');
const test = require('ava');
// const superagent = require('superagent');
const debug = require('debug')('jsonql:client:test');
const Window = require('window');
const window = new Window();
window.localStorage = {};
window.sessionStorage = {};

const JsonqlClient = require(join(__dirname, '..', 'lib' , 'jsonql-client.js'));
// window.superagent = superagent;
const options = require('./fixtures/options.json');
const contractJson = require('./fixtures/contract.json');
const server = require('./fixtures/server');

test.before(t => {
  t.context.stop = server();
});

test.after(t => {
  t.context.stop();
});

test('It should able to use the client to contact the server with static contract', async (t) => {
  //@BUG @TODO The problem is the dynamic retrieving contract
  const request = await JsonqlClient({
    hostname: ['http://localhost', options.server.port].join(':'),
    contract: contractJson
  });
  const res = await request.query.helloWorld();
  t.is('Hello world!', res.data);
});

test('It should able to use the client to contact the server with just in time contract', async (t) => {
  //@BUG @TODO The problem is the dynamic retrieving contract
  const request = await JsonqlClient({
    hostname: ['http://localhost', options.server.port].join(':'),
  });

  debug('request', inspect(request, null, false));
  const res = await request.query.helloWorld();
  debug('status', res.status);
  t.is('Hello world!', res.data);
});
