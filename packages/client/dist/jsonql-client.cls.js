(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('debug')) :
	typeof define === 'function' && define.amd ? define(['debug'], factory) :
	(global = global || self, global.JsonqlClientOOP = factory(global.debug));
}(this, function (debug) { 'use strict';

	debug = debug && debug.hasOwnProperty('default') ? debug['default'] : debug;

	var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function unwrapExports (x) {
		return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
	}

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var assign = make_assign();
	var create = make_create();
	var trim = make_trim();
	var Global = (typeof window !== 'undefined' ? window : commonjsGlobal);

	var util = {
		assign: assign,
		create: create,
		trim: trim,
		bind: bind,
		slice: slice,
		each: each,
		map: map,
		pluck: pluck,
		isList: isList,
		isFunction: isFunction,
		isObject: isObject,
		Global: Global
	};

	function make_assign() {
		if (Object.assign) {
			return Object.assign
		} else {
			return function shimAssign(obj, props1, props2, etc) {
				var arguments$1 = arguments;

				for (var i = 1; i < arguments.length; i++) {
					each(Object(arguments$1[i]), function(val, key) {
						obj[key] = val;
					});
				}			
				return obj
			}
		}
	}

	function make_create() {
		if (Object.create) {
			return function create(obj, assignProps1, assignProps2, etc) {
				var assignArgsList = slice(arguments, 1);
				return assign.apply(this, [Object.create(obj)].concat(assignArgsList))
			}
		} else {
			function F() {} // eslint-disable-line no-inner-declarations
			return function create(obj, assignProps1, assignProps2, etc) {
				var assignArgsList = slice(arguments, 1);
				F.prototype = obj;
				return assign.apply(this, [new F()].concat(assignArgsList))
			}
		}
	}

	function make_trim() {
		if (String.prototype.trim) {
			return function trim(str) {
				return String.prototype.trim.call(str)
			}
		} else {
			return function trim(str) {
				return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')
			}
		}
	}

	function bind(obj, fn) {
		return function() {
			return fn.apply(obj, Array.prototype.slice.call(arguments, 0))
		}
	}

	function slice(arr, index) {
		return Array.prototype.slice.call(arr, index || 0)
	}

	function each(obj, fn) {
		pluck(obj, function(val, key) {
			fn(val, key);
			return false
		});
	}

	function map(obj, fn) {
		var res = (isList(obj) ? [] : {});
		pluck(obj, function(v, k) {
			res[k] = fn(v, k);
			return false
		});
		return res
	}

	function pluck(obj, fn) {
		if (isList(obj)) {
			for (var i=0; i<obj.length; i++) {
				if (fn(obj[i], i)) {
					return obj[i]
				}
			}
		} else {
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					if (fn(obj[key], key)) {
						return obj[key]
					}
				}
			}
		}
	}

	function isList(val) {
		return (val != null && typeof val != 'function' && typeof val.length == 'number')
	}

	function isFunction(val) {
		return val && {}.toString.call(val) === '[object Function]'
	}

	function isObject(val) {
		return val && {}.toString.call(val) === '[object Object]'
	}

	var slice$1 = util.slice;
	var pluck$1 = util.pluck;
	var each$1 = util.each;
	var bind$1 = util.bind;
	var create$1 = util.create;
	var isList$1 = util.isList;
	var isFunction$1 = util.isFunction;
	var isObject$1 = util.isObject;

	var storeEngine = {
		createStore: createStore
	};

	var storeAPI = {
		version: '2.0.12',
		enabled: false,
		
		// get returns the value of the given key. If that value
		// is undefined, it returns optionalDefaultValue instead.
		get: function(key, optionalDefaultValue) {
			var data = this.storage.read(this._namespacePrefix + key);
			return this._deserialize(data, optionalDefaultValue)
		},

		// set will store the given value at key and returns value.
		// Calling set with value === undefined is equivalent to calling remove.
		set: function(key, value) {
			if (value === undefined) {
				return this.remove(key)
			}
			this.storage.write(this._namespacePrefix + key, this._serialize(value));
			return value
		},

		// remove deletes the key and value stored at the given key.
		remove: function(key) {
			this.storage.remove(this._namespacePrefix + key);
		},

		// each will call the given callback once for each key-value pair
		// in this store.
		each: function(callback) {
			var self = this;
			this.storage.each(function(val, namespacedKey) {
				callback.call(self, self._deserialize(val), (namespacedKey || '').replace(self._namespaceRegexp, ''));
			});
		},

		// clearAll will remove all the stored key-value pairs in this store.
		clearAll: function() {
			this.storage.clearAll();
		},

		// additional functionality that can't live in plugins
		// ---------------------------------------------------

		// hasNamespace returns true if this store instance has the given namespace.
		hasNamespace: function(namespace) {
			return (this._namespacePrefix == '__storejs_'+namespace+'_')
		},

		// createStore creates a store.js instance with the first
		// functioning storage in the list of storage candidates,
		// and applies the the given mixins to the instance.
		createStore: function() {
			return createStore.apply(this, arguments)
		},
		
		addPlugin: function(plugin) {
			this._addPlugin(plugin);
		},
		
		namespace: function(namespace) {
			return createStore(this.storage, this.plugins, namespace)
		}
	};

	function _warn() {
		var _console = (typeof console == 'undefined' ? null : console);
		if (!_console) { return }
		var fn = (_console.warn ? _console.warn : _console.log);
		fn.apply(_console, arguments);
	}

	function createStore(storages, plugins, namespace) {
		if (!namespace) {
			namespace = '';
		}
		if (storages && !isList$1(storages)) {
			storages = [storages];
		}
		if (plugins && !isList$1(plugins)) {
			plugins = [plugins];
		}

		var namespacePrefix = (namespace ? '__storejs_'+namespace+'_' : '');
		var namespaceRegexp = (namespace ? new RegExp('^'+namespacePrefix) : null);
		var legalNamespaces = /^[a-zA-Z0-9_\-]*$/; // alpha-numeric + underscore and dash
		if (!legalNamespaces.test(namespace)) {
			throw new Error('store.js namespaces can only have alphanumerics + underscores and dashes')
		}
		
		var _privateStoreProps = {
			_namespacePrefix: namespacePrefix,
			_namespaceRegexp: namespaceRegexp,

			_testStorage: function(storage) {
				try {
					var testStr = '__storejs__test__';
					storage.write(testStr, testStr);
					var ok = (storage.read(testStr) === testStr);
					storage.remove(testStr);
					return ok
				} catch(e) {
					return false
				}
			},

			_assignPluginFnProp: function(pluginFnProp, propName) {
				var oldFn = this[propName];
				this[propName] = function pluginFn() {
					var args = slice$1(arguments, 0);
					var self = this;

					// super_fn calls the old function which was overwritten by
					// this mixin.
					function super_fn() {
						if (!oldFn) { return }
						each$1(arguments, function(arg, i) {
							args[i] = arg;
						});
						return oldFn.apply(self, args)
					}

					// Give mixing function access to super_fn by prefixing all mixin function
					// arguments with super_fn.
					var newFnArgs = [super_fn].concat(args);

					return pluginFnProp.apply(self, newFnArgs)
				};
			},

			_serialize: function(obj) {
				return JSON.stringify(obj)
			},

			_deserialize: function(strVal, defaultVal) {
				if (!strVal) { return defaultVal }
				// It is possible that a raw string value has been previously stored
				// in a storage without using store.js, meaning it will be a raw
				// string value instead of a JSON serialized string. By defaulting
				// to the raw string value in case of a JSON parse error, we allow
				// for past stored values to be forwards-compatible with store.js
				var val = '';
				try { val = JSON.parse(strVal); }
				catch(e) { val = strVal; }

				return (val !== undefined ? val : defaultVal)
			},
			
			_addStorage: function(storage) {
				if (this.enabled) { return }
				if (this._testStorage(storage)) {
					this.storage = storage;
					this.enabled = true;
				}
			},

			_addPlugin: function(plugin) {
				var self = this;

				// If the plugin is an array, then add all plugins in the array.
				// This allows for a plugin to depend on other plugins.
				if (isList$1(plugin)) {
					each$1(plugin, function(plugin) {
						self._addPlugin(plugin);
					});
					return
				}

				// Keep track of all plugins we've seen so far, so that we
				// don't add any of them twice.
				var seenPlugin = pluck$1(this.plugins, function(seenPlugin) {
					return (plugin === seenPlugin)
				});
				if (seenPlugin) {
					return
				}
				this.plugins.push(plugin);

				// Check that the plugin is properly formed
				if (!isFunction$1(plugin)) {
					throw new Error('Plugins must be function values that return objects')
				}

				var pluginProperties = plugin.call(this);
				if (!isObject$1(pluginProperties)) {
					throw new Error('Plugins must return an object of function properties')
				}

				// Add the plugin function properties to this store instance.
				each$1(pluginProperties, function(pluginFnProp, propName) {
					if (!isFunction$1(pluginFnProp)) {
						throw new Error('Bad plugin property: '+propName+' from plugin '+plugin.name+'. Plugins should only return functions.')
					}
					self._assignPluginFnProp(pluginFnProp, propName);
				});
			},
			
			// Put deprecated properties in the private API, so as to not expose it to accidential
			// discovery through inspection of the store object.
			
			// Deprecated: addStorage
			addStorage: function(storage) {
				_warn('store.addStorage(storage) is deprecated. Use createStore([storages])');
				this._addStorage(storage);
			}
		};

		var store = create$1(_privateStoreProps, storeAPI, {
			plugins: []
		});
		store.raw = {};
		each$1(store, function(prop, propName) {
			if (isFunction$1(prop)) {
				store.raw[propName] = bind$1(store, prop);			
			}
		});
		each$1(storages, function(storage) {
			store._addStorage(storage);
		});
		each$1(plugins, function(plugin) {
			store._addPlugin(plugin);
		});
		return store
	}

	var Global$1 = util.Global;

	var localStorage_1 = {
		name: 'localStorage',
		read: read,
		write: write,
		each: each$2,
		remove: remove,
		clearAll: clearAll,
	};

	function localStorage$1() {
		return Global$1.localStorage
	}

	function read(key) {
		return localStorage$1().getItem(key)
	}

	function write(key, data) {
		return localStorage$1().setItem(key, data)
	}

	function each$2(fn) {
		for (var i = localStorage$1().length - 1; i >= 0; i--) {
			var key = localStorage$1().key(i);
			fn(read(key), key);
		}
	}

	function remove(key) {
		return localStorage$1().removeItem(key)
	}

	function clearAll() {
		return localStorage$1().clear()
	}

	// cookieStorage is useful Safari private browser mode, where localStorage
	// doesn't work but cookies do. This implementation is adopted from
	// https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage


	var Global$2 = util.Global;
	var trim$1 = util.trim;

	var cookieStorage = {
		name: 'cookieStorage',
		read: read$1,
		write: write$1,
		each: each$3,
		remove: remove$1,
		clearAll: clearAll$1,
	};

	var doc = Global$2.document;

	function read$1(key) {
		if (!key || !_has(key)) { return null }
		var regexpStr = "(?:^|.*;\\s*)" +
			escape(key).replace(/[\-\.\+\*]/g, "\\$&") +
			"\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*";
		return unescape(doc.cookie.replace(new RegExp(regexpStr), "$1"))
	}

	function each$3(callback) {
		var cookies = doc.cookie.split(/; ?/g);
		for (var i = cookies.length - 1; i >= 0; i--) {
			if (!trim$1(cookies[i])) {
				continue
			}
			var kvp = cookies[i].split('=');
			var key = unescape(kvp[0]);
			var val = unescape(kvp[1]);
			callback(val, key);
		}
	}

	function write$1(key, data) {
		if(!key) { return }
		doc.cookie = escape(key) + "=" + escape(data) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
	}

	function remove$1(key) {
		if (!key || !_has(key)) {
			return
		}
		doc.cookie = escape(key) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
	}

	function clearAll$1() {
		each$3(function(_, key) {
			remove$1(key);
		});
	}

	function _has(key) {
		return (new RegExp("(?:^|;\\s*)" + escape(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(doc.cookie)
	}

	var defaults = defaultsPlugin;

	function defaultsPlugin() {
		var defaultValues = {};
		
		return {
			defaults: defaults,
			get: get
		}
		
		function defaults(_, values) {
			defaultValues = values;
		}
		
		function get(super_fn, key) {
			var val = super_fn();
			return (val !== undefined ? val : defaultValues[key])
		}
	}

	var namespace = 'expire_mixin';

	var expire = expirePlugin;

	function expirePlugin() {
		var expirations = this.createStore(this.storage, null, this._namespacePrefix+namespace);
		
		return {
			set: expire_set,
			get: expire_get,
			remove: expire_remove,
			getExpiration: getExpiration,
			removeExpiredKeys: removeExpiredKeys
		}
		
		function expire_set(super_fn, key, val, expiration) {
			if (!this.hasNamespace(namespace)) {
				expirations.set(key, expiration);
			}
			return super_fn()
		}
		
		function expire_get(super_fn, key) {
			if (!this.hasNamespace(namespace)) {
				_checkExpiration.call(this, key);
			}
			return super_fn()
		}
		
		function expire_remove(super_fn, key) {
			if (!this.hasNamespace(namespace)) {
				expirations.remove(key);
			}
			return super_fn()
		}
		
		function getExpiration(_, key) {
			return expirations.get(key)
		}
		
		function removeExpiredKeys(_) {
			var keys = [];
			this.each(function(val, key) {
				keys.push(key);
			});
			for (var i=0; i<keys.length; i++) {
				_checkExpiration.call(this, keys[i]);
			}
		}
		
		function _checkExpiration(key) {
			var expiration = expirations.get(key, Number.MAX_VALUE);
			if (expiration <= new Date().getTime()) {
				this.raw.remove(key);
				expirations.remove(key);
			}
		}
		
	}

	var bind$2 = util.bind;
	var each$4 = util.each;
	var create$2 = util.create;
	var slice$2 = util.slice;

	var events = eventsPlugin;

	function eventsPlugin() {
		var pubsub = _newPubSub();

		return {
			watch: watch,
			unwatch: unwatch,
			once: once,

			set: set,
			remove: remove,
			clearAll: clearAll
		}

		// new pubsub functions
		function watch(_, key, listener) {
			return pubsub.on(key, bind$2(this, listener))
		}
		function unwatch(_, subId) {
			pubsub.off(subId);
		}
		function once(_, key, listener) {
			pubsub.once(key, bind$2(this, listener));
		}

		// overwrite function to fire when appropriate
		function set(super_fn, key, val) {
			var oldVal = this.get(key);
			super_fn();
			pubsub.fire(key, val, oldVal);
		}
		function remove(super_fn, key) {
			var oldVal = this.get(key);
			super_fn();
			pubsub.fire(key, undefined, oldVal);
		}
		function clearAll(super_fn) {
			var oldVals = {};
			this.each(function(val, key) {
				oldVals[key] = val;
			});
			super_fn();
			each$4(oldVals, function(oldVal, key) {
				pubsub.fire(key, undefined, oldVal);
			});
		}
	}


	function _newPubSub() {
		return create$2(_pubSubBase, {
			_id: 0,
			_subSignals: {},
			_subCallbacks: {}
		})
	}

	var _pubSubBase = {
		_id: null,
		_subCallbacks: null,
		_subSignals: null,
		on: function(signal, callback) {
			if (!this._subCallbacks[signal]) {
				this._subCallbacks[signal] = {};
			}
			this._id += 1;
			this._subCallbacks[signal][this._id] = callback;
			this._subSignals[this._id] = signal;
			return this._id
		},
		off: function(subId) {
			var signal = this._subSignals[subId];
			delete this._subCallbacks[signal][subId];
			delete this._subSignals[subId];
		},
		once: function(signal, callback) {
			var subId = this.on(signal, bind$2(this, function() {
				callback.apply(this, arguments);
				this.off(subId);
			}));
		},
		fire: function(signal) {
			var args = slice$2(arguments, 1);
			each$4(this._subCallbacks[signal], function(callback) {
				callback.apply(this, args);
			});
		}
	};

	var lzString = createCommonjsModule(function (module) {
	/* eslint-disable */
	// Copyright (c) 2013 Pieroxy <pieroxy@pieroxy.net>
	// This work is free. You can redistribute it and/or modify it
	// under the terms of the WTFPL, Version 2
	// For more information see LICENSE.txt or http://www.wtfpl.net/
	//
	// For more information, the home page:
	// http://pieroxy.net/blog/pages/lz-string/testing.html
	//
	// LZ-based compression algorithm, version 1.4.4
	var LZString = (function() {

	// private property
	var f = String.fromCharCode;
	var keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$";
	var baseReverseDic = {};

	function getBaseValue(alphabet, character) {
	  if (!baseReverseDic[alphabet]) {
	    baseReverseDic[alphabet] = {};
	    for (var i=0 ; i<alphabet.length ; i++) {
	      baseReverseDic[alphabet][alphabet.charAt(i)] = i;
	    }
	  }
	  return baseReverseDic[alphabet][character];
	}

	var LZString = {
	  compressToBase64 : function (input) {
	    if (input == null) { return ""; }
	    var res = LZString._compress(input, 6, function(a){return keyStrBase64.charAt(a);});
	    switch (res.length % 4) { // To produce valid Base64
	    default: // When could this happen ?
	    case 0 : return res;
	    case 1 : return res+"===";
	    case 2 : return res+"==";
	    case 3 : return res+"=";
	    }
	  },

	  decompressFromBase64 : function (input) {
	    if (input == null) { return ""; }
	    if (input == "") { return null; }
	    return LZString._decompress(input.length, 32, function(index) { return getBaseValue(keyStrBase64, input.charAt(index)); });
	  },

	  compressToUTF16 : function (input) {
	    if (input == null) { return ""; }
	    return LZString._compress(input, 15, function(a){return f(a+32);}) + " ";
	  },

	  decompressFromUTF16: function (compressed) {
	    if (compressed == null) { return ""; }
	    if (compressed == "") { return null; }
	    return LZString._decompress(compressed.length, 16384, function(index) { return compressed.charCodeAt(index) - 32; });
	  },

	  //compress into uint8array (UCS-2 big endian format)
	  compressToUint8Array: function (uncompressed) {
	    var compressed = LZString.compress(uncompressed);
	    var buf=new Uint8Array(compressed.length*2); // 2 bytes per character

	    for (var i=0, TotalLen=compressed.length; i<TotalLen; i++) {
	      var current_value = compressed.charCodeAt(i);
	      buf[i*2] = current_value >>> 8;
	      buf[i*2+1] = current_value % 256;
	    }
	    return buf;
	  },

	  //decompress from uint8array (UCS-2 big endian format)
	  decompressFromUint8Array:function (compressed) {
	    if (compressed===null || compressed===undefined){
	        return LZString.decompress(compressed);
	    } else {
	        var buf=new Array(compressed.length/2); // 2 bytes per character
	        for (var i=0, TotalLen=buf.length; i<TotalLen; i++) {
	          buf[i]=compressed[i*2]*256+compressed[i*2+1];
	        }

	        var result = [];
	        buf.forEach(function (c) {
	          result.push(f(c));
	        });
	        return LZString.decompress(result.join(''));

	    }

	  },


	  //compress into a string that is already URI encoded
	  compressToEncodedURIComponent: function (input) {
	    if (input == null) { return ""; }
	    return LZString._compress(input, 6, function(a){return keyStrUriSafe.charAt(a);});
	  },

	  //decompress from an output of compressToEncodedURIComponent
	  decompressFromEncodedURIComponent:function (input) {
	    if (input == null) { return ""; }
	    if (input == "") { return null; }
	    input = input.replace(/ /g, "+");
	    return LZString._decompress(input.length, 32, function(index) { return getBaseValue(keyStrUriSafe, input.charAt(index)); });
	  },

	  compress: function (uncompressed) {
	    return LZString._compress(uncompressed, 16, function(a){return f(a);});
	  },
	  _compress: function (uncompressed, bitsPerChar, getCharFromInt) {
	    if (uncompressed == null) { return ""; }
	    var i, value,
	        context_dictionary= {},
	        context_dictionaryToCreate= {},
	        context_c="",
	        context_wc="",
	        context_w="",
	        context_enlargeIn= 2, // Compensate for the first entry which should not count
	        context_dictSize= 3,
	        context_numBits= 2,
	        context_data=[],
	        context_data_val=0,
	        context_data_position=0,
	        ii;

	    for (ii = 0; ii < uncompressed.length; ii += 1) {
	      context_c = uncompressed.charAt(ii);
	      if (!Object.prototype.hasOwnProperty.call(context_dictionary,context_c)) {
	        context_dictionary[context_c] = context_dictSize++;
	        context_dictionaryToCreate[context_c] = true;
	      }

	      context_wc = context_w + context_c;
	      if (Object.prototype.hasOwnProperty.call(context_dictionary,context_wc)) {
	        context_w = context_wc;
	      } else {
	        if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
	          if (context_w.charCodeAt(0)<256) {
	            for (i=0 ; i<context_numBits ; i++) {
	              context_data_val = (context_data_val << 1);
	              if (context_data_position == bitsPerChar-1) {
	                context_data_position = 0;
	                context_data.push(getCharFromInt(context_data_val));
	                context_data_val = 0;
	              } else {
	                context_data_position++;
	              }
	            }
	            value = context_w.charCodeAt(0);
	            for (i=0 ; i<8 ; i++) {
	              context_data_val = (context_data_val << 1) | (value&1);
	              if (context_data_position == bitsPerChar-1) {
	                context_data_position = 0;
	                context_data.push(getCharFromInt(context_data_val));
	                context_data_val = 0;
	              } else {
	                context_data_position++;
	              }
	              value = value >> 1;
	            }
	          } else {
	            value = 1;
	            for (i=0 ; i<context_numBits ; i++) {
	              context_data_val = (context_data_val << 1) | value;
	              if (context_data_position ==bitsPerChar-1) {
	                context_data_position = 0;
	                context_data.push(getCharFromInt(context_data_val));
	                context_data_val = 0;
	              } else {
	                context_data_position++;
	              }
	              value = 0;
	            }
	            value = context_w.charCodeAt(0);
	            for (i=0 ; i<16 ; i++) {
	              context_data_val = (context_data_val << 1) | (value&1);
	              if (context_data_position == bitsPerChar-1) {
	                context_data_position = 0;
	                context_data.push(getCharFromInt(context_data_val));
	                context_data_val = 0;
	              } else {
	                context_data_position++;
	              }
	              value = value >> 1;
	            }
	          }
	          context_enlargeIn--;
	          if (context_enlargeIn == 0) {
	            context_enlargeIn = Math.pow(2, context_numBits);
	            context_numBits++;
	          }
	          delete context_dictionaryToCreate[context_w];
	        } else {
	          value = context_dictionary[context_w];
	          for (i=0 ; i<context_numBits ; i++) {
	            context_data_val = (context_data_val << 1) | (value&1);
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	            value = value >> 1;
	          }


	        }
	        context_enlargeIn--;
	        if (context_enlargeIn == 0) {
	          context_enlargeIn = Math.pow(2, context_numBits);
	          context_numBits++;
	        }
	        // Add wc to the dictionary.
	        context_dictionary[context_wc] = context_dictSize++;
	        context_w = String(context_c);
	      }
	    }

	    // Output the code for w.
	    if (context_w !== "") {
	      if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
	        if (context_w.charCodeAt(0)<256) {
	          for (i=0 ; i<context_numBits ; i++) {
	            context_data_val = (context_data_val << 1);
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	          }
	          value = context_w.charCodeAt(0);
	          for (i=0 ; i<8 ; i++) {
	            context_data_val = (context_data_val << 1) | (value&1);
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	            value = value >> 1;
	          }
	        } else {
	          value = 1;
	          for (i=0 ; i<context_numBits ; i++) {
	            context_data_val = (context_data_val << 1) | value;
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	            value = 0;
	          }
	          value = context_w.charCodeAt(0);
	          for (i=0 ; i<16 ; i++) {
	            context_data_val = (context_data_val << 1) | (value&1);
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	            value = value >> 1;
	          }
	        }
	        context_enlargeIn--;
	        if (context_enlargeIn == 0) {
	          context_enlargeIn = Math.pow(2, context_numBits);
	          context_numBits++;
	        }
	        delete context_dictionaryToCreate[context_w];
	      } else {
	        value = context_dictionary[context_w];
	        for (i=0 ; i<context_numBits ; i++) {
	          context_data_val = (context_data_val << 1) | (value&1);
	          if (context_data_position == bitsPerChar-1) {
	            context_data_position = 0;
	            context_data.push(getCharFromInt(context_data_val));
	            context_data_val = 0;
	          } else {
	            context_data_position++;
	          }
	          value = value >> 1;
	        }


	      }
	      context_enlargeIn--;
	      if (context_enlargeIn == 0) {
	        context_enlargeIn = Math.pow(2, context_numBits);
	        context_numBits++;
	      }
	    }

	    // Mark the end of the stream
	    value = 2;
	    for (i=0 ; i<context_numBits ; i++) {
	      context_data_val = (context_data_val << 1) | (value&1);
	      if (context_data_position == bitsPerChar-1) {
	        context_data_position = 0;
	        context_data.push(getCharFromInt(context_data_val));
	        context_data_val = 0;
	      } else {
	        context_data_position++;
	      }
	      value = value >> 1;
	    }

	    // Flush the last char
	    while (true) {
	      context_data_val = (context_data_val << 1);
	      if (context_data_position == bitsPerChar-1) {
	        context_data.push(getCharFromInt(context_data_val));
	        break;
	      }
	      else { context_data_position++; }
	    }
	    return context_data.join('');
	  },

	  decompress: function (compressed) {
	    if (compressed == null) { return ""; }
	    if (compressed == "") { return null; }
	    return LZString._decompress(compressed.length, 32768, function(index) { return compressed.charCodeAt(index); });
	  },

	  _decompress: function (length, resetValue, getNextValue) {
	    var dictionary = [],
	        next,
	        enlargeIn = 4,
	        dictSize = 4,
	        numBits = 3,
	        entry = "",
	        result = [],
	        i,
	        w,
	        bits, resb, maxpower, power,
	        c,
	        data = {val:getNextValue(0), position:resetValue, index:1};

	    for (i = 0; i < 3; i += 1) {
	      dictionary[i] = i;
	    }

	    bits = 0;
	    maxpower = Math.pow(2,2);
	    power=1;
	    while (power!=maxpower) {
	      resb = data.val & data.position;
	      data.position >>= 1;
	      if (data.position == 0) {
	        data.position = resetValue;
	        data.val = getNextValue(data.index++);
	      }
	      bits |= (resb>0 ? 1 : 0) * power;
	      power <<= 1;
	    }

	    switch (next = bits) {
	      case 0:
	          bits = 0;
	          maxpower = Math.pow(2,8);
	          power=1;
	          while (power!=maxpower) {
	            resb = data.val & data.position;
	            data.position >>= 1;
	            if (data.position == 0) {
	              data.position = resetValue;
	              data.val = getNextValue(data.index++);
	            }
	            bits |= (resb>0 ? 1 : 0) * power;
	            power <<= 1;
	          }
	        c = f(bits);
	        break;
	      case 1:
	          bits = 0;
	          maxpower = Math.pow(2,16);
	          power=1;
	          while (power!=maxpower) {
	            resb = data.val & data.position;
	            data.position >>= 1;
	            if (data.position == 0) {
	              data.position = resetValue;
	              data.val = getNextValue(data.index++);
	            }
	            bits |= (resb>0 ? 1 : 0) * power;
	            power <<= 1;
	          }
	        c = f(bits);
	        break;
	      case 2:
	        return "";
	    }
	    dictionary[3] = c;
	    w = c;
	    result.push(c);
	    while (true) {
	      if (data.index > length) {
	        return "";
	      }

	      bits = 0;
	      maxpower = Math.pow(2,numBits);
	      power=1;
	      while (power!=maxpower) {
	        resb = data.val & data.position;
	        data.position >>= 1;
	        if (data.position == 0) {
	          data.position = resetValue;
	          data.val = getNextValue(data.index++);
	        }
	        bits |= (resb>0 ? 1 : 0) * power;
	        power <<= 1;
	      }

	      switch (c = bits) {
	        case 0:
	          bits = 0;
	          maxpower = Math.pow(2,8);
	          power=1;
	          while (power!=maxpower) {
	            resb = data.val & data.position;
	            data.position >>= 1;
	            if (data.position == 0) {
	              data.position = resetValue;
	              data.val = getNextValue(data.index++);
	            }
	            bits |= (resb>0 ? 1 : 0) * power;
	            power <<= 1;
	          }

	          dictionary[dictSize++] = f(bits);
	          c = dictSize-1;
	          enlargeIn--;
	          break;
	        case 1:
	          bits = 0;
	          maxpower = Math.pow(2,16);
	          power=1;
	          while (power!=maxpower) {
	            resb = data.val & data.position;
	            data.position >>= 1;
	            if (data.position == 0) {
	              data.position = resetValue;
	              data.val = getNextValue(data.index++);
	            }
	            bits |= (resb>0 ? 1 : 0) * power;
	            power <<= 1;
	          }
	          dictionary[dictSize++] = f(bits);
	          c = dictSize-1;
	          enlargeIn--;
	          break;
	        case 2:
	          return result.join('');
	      }

	      if (enlargeIn == 0) {
	        enlargeIn = Math.pow(2, numBits);
	        numBits++;
	      }

	      if (dictionary[c]) {
	        entry = dictionary[c];
	      } else {
	        if (c === dictSize) {
	          entry = w + w.charAt(0);
	        } else {
	          return null;
	        }
	      }
	      result.push(entry);

	      // Add w+entry[0] to the dictionary.
	      dictionary[dictSize++] = w + entry.charAt(0);
	      enlargeIn--;

	      w = entry;

	      if (enlargeIn == 0) {
	        enlargeIn = Math.pow(2, numBits);
	        numBits++;
	      }

	    }
	  }
	};
	  return LZString;
	})();

	if(  module != null ) {
	  module.exports = LZString;
	}
	});

	var compression = compressionPlugin;

	function compressionPlugin() {
		return {
			get: get,
			set: set,
		}

		function get(super_fn, key) {
			var val = super_fn(key);
			if (!val) { return val }
			var decompressed = lzString.decompress(val);
			// fallback to existing values that are not compressed
			return (decompressed == null) ? val : this._deserialize(decompressed)
		}

		function set(super_fn, key, val) {
			var compressed = lzString.compress(this._serialize(val));
			super_fn(key, compressed);
		}
	}

	// sort of persist on the user side

	var storages = [localStorage_1, cookieStorage];
	var plugins = [defaults, expire, events, compression];

	var localStore = storeEngine.createStore(storages, plugins);

	var Global$3 = util.Global;

	var sessionStorage_1 = {
		name: 'sessionStorage',
		read: read$2,
		write: write$2,
		each: each$5,
		remove: remove$2,
		clearAll: clearAll$2
	};

	function sessionStorage() {
		return Global$3.sessionStorage
	}

	function read$2(key) {
		return sessionStorage().getItem(key)
	}

	function write$2(key, data) {
		return sessionStorage().setItem(key, data)
	}

	function each$5(fn) {
		for (var i = sessionStorage().length - 1; i >= 0; i--) {
			var key = sessionStorage().key(i);
			fn(read$2(key), key);
		}
	}

	function remove$2(key) {
		return sessionStorage().removeItem(key)
	}

	function clearAll$2() {
		return sessionStorage().clear()
	}

	// session store with watch

	var storages$1 = [sessionStorage_1, cookieStorage];
	var plugins$1 = [defaults, expire];

	var sessionStore = storeEngine.createStore(storages$1, plugins$1);

	// export store interface

	// export back the raw version for development purposes
	var localStore$1 = localStore;
	var sessionStore$1 = sessionStore;

	var global$1 = (typeof global !== "undefined" ? global :
	            typeof self !== "undefined" ? self :
	            typeof window !== "undefined" ? window : {});

	/** Detect free variable `global` from Node.js. */
	var freeGlobal = typeof global$1 == 'object' && global$1 && global$1.Object === Object && global$1;

	/** Detect free variable `self`. */
	var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

	/** Used as a reference to the global object. */
	var root = freeGlobal || freeSelf || Function('return this')();

	/** Built-in value references. */
	var Symbol$1 = root.Symbol;

	/** Used for built-in method references. */
	var objectProto = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty = objectProto.hasOwnProperty;

	/**
	 * Used to resolve the
	 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var nativeObjectToString = objectProto.toString;

	/** Built-in value references. */
	var symToStringTag = Symbol$1 ? Symbol$1.toStringTag : undefined;

	/**
	 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @returns {string} Returns the raw `toStringTag`.
	 */
	function getRawTag(value) {
	  var isOwn = hasOwnProperty.call(value, symToStringTag),
	      tag = value[symToStringTag];

	  try {
	    value[symToStringTag] = undefined;
	    var unmasked = true;
	  } catch (e) {}

	  var result = nativeObjectToString.call(value);
	  if (unmasked) {
	    if (isOwn) {
	      value[symToStringTag] = tag;
	    } else {
	      delete value[symToStringTag];
	    }
	  }
	  return result;
	}

	/** Used for built-in method references. */
	var objectProto$1 = Object.prototype;

	/**
	 * Used to resolve the
	 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
	 * of values.
	 */
	var nativeObjectToString$1 = objectProto$1.toString;

	/**
	 * Converts `value` to a string using `Object.prototype.toString`.
	 *
	 * @private
	 * @param {*} value The value to convert.
	 * @returns {string} Returns the converted string.
	 */
	function objectToString(value) {
	  return nativeObjectToString$1.call(value);
	}

	/** `Object#toString` result references. */
	var nullTag = '[object Null]',
	    undefinedTag = '[object Undefined]';

	/** Built-in value references. */
	var symToStringTag$1 = Symbol$1 ? Symbol$1.toStringTag : undefined;

	/**
	 * The base implementation of `getTag` without fallbacks for buggy environments.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @returns {string} Returns the `toStringTag`.
	 */
	function baseGetTag(value) {
	  if (value == null) {
	    return value === undefined ? undefinedTag : nullTag;
	  }
	  return (symToStringTag$1 && symToStringTag$1 in Object(value))
	    ? getRawTag(value)
	    : objectToString(value);
	}

	/**
	 * Checks if `value` is object-like. A value is object-like if it's not `null`
	 * and has a `typeof` result of "object".
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
	 * @example
	 *
	 * _.isObjectLike({});
	 * // => true
	 *
	 * _.isObjectLike([1, 2, 3]);
	 * // => true
	 *
	 * _.isObjectLike(_.noop);
	 * // => false
	 *
	 * _.isObjectLike(null);
	 * // => false
	 */
	function isObjectLike(value) {
	  return value != null && typeof value == 'object';
	}

	/** `Object#toString` result references. */
	var symbolTag = '[object Symbol]';

	/**
	 * Checks if `value` is classified as a `Symbol` primitive or object.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
	 * @example
	 *
	 * _.isSymbol(Symbol.iterator);
	 * // => true
	 *
	 * _.isSymbol('abc');
	 * // => false
	 */
	function isSymbol(value) {
	  return typeof value == 'symbol' ||
	    (isObjectLike(value) && baseGetTag(value) == symbolTag);
	}

	/**
	 * A specialized version of `_.map` for arrays without support for iteratee
	 * shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array} Returns the new mapped array.
	 */
	function arrayMap(array, iteratee) {
	  var index = -1,
	      length = array == null ? 0 : array.length,
	      result = Array(length);

	  while (++index < length) {
	    result[index] = iteratee(array[index], index, array);
	  }
	  return result;
	}

	/**
	 * Checks if `value` is classified as an `Array` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
	 * @example
	 *
	 * _.isArray([1, 2, 3]);
	 * // => true
	 *
	 * _.isArray(document.body.children);
	 * // => false
	 *
	 * _.isArray('abc');
	 * // => false
	 *
	 * _.isArray(_.noop);
	 * // => false
	 */
	var isArray = Array.isArray;

	/** Used as references for various `Number` constants. */
	var INFINITY = 1 / 0;

	/** Used to convert symbols to primitives and strings. */
	var symbolProto = Symbol$1 ? Symbol$1.prototype : undefined,
	    symbolToString = symbolProto ? symbolProto.toString : undefined;

	/**
	 * The base implementation of `_.toString` which doesn't convert nullish
	 * values to empty strings.
	 *
	 * @private
	 * @param {*} value The value to process.
	 * @returns {string} Returns the string.
	 */
	function baseToString(value) {
	  // Exit early for strings to avoid a performance hit in some environments.
	  if (typeof value == 'string') {
	    return value;
	  }
	  if (isArray(value)) {
	    // Recursively convert values (susceptible to call stack limits).
	    return arrayMap(value, baseToString) + '';
	  }
	  if (isSymbol(value)) {
	    return symbolToString ? symbolToString.call(value) : '';
	  }
	  var result = (value + '');
	  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
	}

	/**
	 * Checks if `value` is the
	 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
	 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
	 * @example
	 *
	 * _.isObject({});
	 * // => true
	 *
	 * _.isObject([1, 2, 3]);
	 * // => true
	 *
	 * _.isObject(_.noop);
	 * // => true
	 *
	 * _.isObject(null);
	 * // => false
	 */
	function isObject$2(value) {
	  var type = typeof value;
	  return value != null && (type == 'object' || type == 'function');
	}

	/**
	 * This method returns the first argument it receives.
	 *
	 * @static
	 * @since 0.1.0
	 * @memberOf _
	 * @category Util
	 * @param {*} value Any value.
	 * @returns {*} Returns `value`.
	 * @example
	 *
	 * var object = { 'a': 1 };
	 *
	 * console.log(_.identity(object) === object);
	 * // => true
	 */
	function identity(value) {
	  return value;
	}

	/** `Object#toString` result references. */
	var asyncTag = '[object AsyncFunction]',
	    funcTag = '[object Function]',
	    genTag = '[object GeneratorFunction]',
	    proxyTag = '[object Proxy]';

	/**
	 * Checks if `value` is classified as a `Function` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
	 * @example
	 *
	 * _.isFunction(_);
	 * // => true
	 *
	 * _.isFunction(/abc/);
	 * // => false
	 */
	function isFunction$2(value) {
	  if (!isObject$2(value)) {
	    return false;
	  }
	  // The use of `Object#toString` avoids issues with the `typeof` operator
	  // in Safari 9 which returns 'object' for typed arrays and other constructors.
	  var tag = baseGetTag(value);
	  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
	}

	/** Used to detect overreaching core-js shims. */
	var coreJsData = root['__core-js_shared__'];

	/** Used to detect methods masquerading as native. */
	var maskSrcKey = (function() {
	  var uid = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.IE_PROTO || '');
	  return uid ? ('Symbol(src)_1.' + uid) : '';
	}());

	/**
	 * Checks if `func` has its source masked.
	 *
	 * @private
	 * @param {Function} func The function to check.
	 * @returns {boolean} Returns `true` if `func` is masked, else `false`.
	 */
	function isMasked(func) {
	  return !!maskSrcKey && (maskSrcKey in func);
	}

	/** Used for built-in method references. */
	var funcProto = Function.prototype;

	/** Used to resolve the decompiled source of functions. */
	var funcToString = funcProto.toString;

	/**
	 * Converts `func` to its source code.
	 *
	 * @private
	 * @param {Function} func The function to convert.
	 * @returns {string} Returns the source code.
	 */
	function toSource(func) {
	  if (func != null) {
	    try {
	      return funcToString.call(func);
	    } catch (e) {}
	    try {
	      return (func + '');
	    } catch (e) {}
	  }
	  return '';
	}

	/**
	 * Used to match `RegExp`
	 * [syntax characters](http://ecma-international.org/ecma-262/7.0/#sec-patterns).
	 */
	var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;

	/** Used to detect host constructors (Safari). */
	var reIsHostCtor = /^\[object .+?Constructor\]$/;

	/** Used for built-in method references. */
	var funcProto$1 = Function.prototype,
	    objectProto$2 = Object.prototype;

	/** Used to resolve the decompiled source of functions. */
	var funcToString$1 = funcProto$1.toString;

	/** Used to check objects for own properties. */
	var hasOwnProperty$1 = objectProto$2.hasOwnProperty;

	/** Used to detect if a method is native. */
	var reIsNative = RegExp('^' +
	  funcToString$1.call(hasOwnProperty$1).replace(reRegExpChar, '\\$&')
	  .replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, '$1.*?') + '$'
	);

	/**
	 * The base implementation of `_.isNative` without bad shim checks.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a native function,
	 *  else `false`.
	 */
	function baseIsNative(value) {
	  if (!isObject$2(value) || isMasked(value)) {
	    return false;
	  }
	  var pattern = isFunction$2(value) ? reIsNative : reIsHostCtor;
	  return pattern.test(toSource(value));
	}

	/**
	 * Gets the value at `key` of `object`.
	 *
	 * @private
	 * @param {Object} [object] The object to query.
	 * @param {string} key The key of the property to get.
	 * @returns {*} Returns the property value.
	 */
	function getValue(object, key) {
	  return object == null ? undefined : object[key];
	}

	/**
	 * Gets the native function at `key` of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {string} key The key of the method to get.
	 * @returns {*} Returns the function if it's native, else `undefined`.
	 */
	function getNative(object, key) {
	  var value = getValue(object, key);
	  return baseIsNative(value) ? value : undefined;
	}

	/* Built-in method references that are verified to be native. */
	var WeakMap$1 = getNative(root, 'WeakMap');

	/** Built-in value references. */
	var objectCreate = Object.create;

	/**
	 * The base implementation of `_.create` without support for assigning
	 * properties to the created object.
	 *
	 * @private
	 * @param {Object} proto The object to inherit from.
	 * @returns {Object} Returns the new object.
	 */
	var baseCreate = (function() {
	  function object() {}
	  return function(proto) {
	    if (!isObject$2(proto)) {
	      return {};
	    }
	    if (objectCreate) {
	      return objectCreate(proto);
	    }
	    object.prototype = proto;
	    var result = new object;
	    object.prototype = undefined;
	    return result;
	  };
	}());

	/**
	 * A faster alternative to `Function#apply`, this function invokes `func`
	 * with the `this` binding of `thisArg` and the arguments of `args`.
	 *
	 * @private
	 * @param {Function} func The function to invoke.
	 * @param {*} thisArg The `this` binding of `func`.
	 * @param {Array} args The arguments to invoke `func` with.
	 * @returns {*} Returns the result of `func`.
	 */
	function apply(func, thisArg, args) {
	  switch (args.length) {
	    case 0: return func.call(thisArg);
	    case 1: return func.call(thisArg, args[0]);
	    case 2: return func.call(thisArg, args[0], args[1]);
	    case 3: return func.call(thisArg, args[0], args[1], args[2]);
	  }
	  return func.apply(thisArg, args);
	}

	/**
	 * Copies the values of `source` to `array`.
	 *
	 * @private
	 * @param {Array} source The array to copy values from.
	 * @param {Array} [array=[]] The array to copy values to.
	 * @returns {Array} Returns `array`.
	 */
	function copyArray(source, array) {
	  var index = -1,
	      length = source.length;

	  array || (array = Array(length));
	  while (++index < length) {
	    array[index] = source[index];
	  }
	  return array;
	}

	/** Used to detect hot functions by number of calls within a span of milliseconds. */
	var HOT_COUNT = 800,
	    HOT_SPAN = 16;

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeNow = Date.now;

	/**
	 * Creates a function that'll short out and invoke `identity` instead
	 * of `func` when it's called `HOT_COUNT` or more times in `HOT_SPAN`
	 * milliseconds.
	 *
	 * @private
	 * @param {Function} func The function to restrict.
	 * @returns {Function} Returns the new shortable function.
	 */
	function shortOut(func) {
	  var count = 0,
	      lastCalled = 0;

	  return function() {
	    var stamp = nativeNow(),
	        remaining = HOT_SPAN - (stamp - lastCalled);

	    lastCalled = stamp;
	    if (remaining > 0) {
	      if (++count >= HOT_COUNT) {
	        return arguments[0];
	      }
	    } else {
	      count = 0;
	    }
	    return func.apply(undefined, arguments);
	  };
	}

	/**
	 * Creates a function that returns `value`.
	 *
	 * @static
	 * @memberOf _
	 * @since 2.4.0
	 * @category Util
	 * @param {*} value The value to return from the new function.
	 * @returns {Function} Returns the new constant function.
	 * @example
	 *
	 * var objects = _.times(2, _.constant({ 'a': 1 }));
	 *
	 * console.log(objects);
	 * // => [{ 'a': 1 }, { 'a': 1 }]
	 *
	 * console.log(objects[0] === objects[1]);
	 * // => true
	 */
	function constant(value) {
	  return function() {
	    return value;
	  };
	}

	var defineProperty = (function() {
	  try {
	    var func = getNative(Object, 'defineProperty');
	    func({}, '', {});
	    return func;
	  } catch (e) {}
	}());

	/**
	 * The base implementation of `setToString` without support for hot loop shorting.
	 *
	 * @private
	 * @param {Function} func The function to modify.
	 * @param {Function} string The `toString` result.
	 * @returns {Function} Returns `func`.
	 */
	var baseSetToString = !defineProperty ? identity : function(func, string) {
	  return defineProperty(func, 'toString', {
	    'configurable': true,
	    'enumerable': false,
	    'value': constant(string),
	    'writable': true
	  });
	};

	/**
	 * Sets the `toString` method of `func` to return `string`.
	 *
	 * @private
	 * @param {Function} func The function to modify.
	 * @param {Function} string The `toString` result.
	 * @returns {Function} Returns `func`.
	 */
	var setToString = shortOut(baseSetToString);

	/**
	 * The base implementation of `_.findIndex` and `_.findLastIndex` without
	 * support for iteratee shorthands.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {Function} predicate The function invoked per iteration.
	 * @param {number} fromIndex The index to search from.
	 * @param {boolean} [fromRight] Specify iterating from right to left.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
	function baseFindIndex(array, predicate, fromIndex, fromRight) {
	  var length = array.length,
	      index = fromIndex + (fromRight ? 1 : -1);

	  while ((fromRight ? index-- : ++index < length)) {
	    if (predicate(array[index], index, array)) {
	      return index;
	    }
	  }
	  return -1;
	}

	/**
	 * The base implementation of `_.isNaN` without support for number objects.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
	 */
	function baseIsNaN(value) {
	  return value !== value;
	}

	/**
	 * A specialized version of `_.indexOf` which performs strict equality
	 * comparisons of values, i.e. `===`.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {*} value The value to search for.
	 * @param {number} fromIndex The index to search from.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
	function strictIndexOf(array, value, fromIndex) {
	  var index = fromIndex - 1,
	      length = array.length;

	  while (++index < length) {
	    if (array[index] === value) {
	      return index;
	    }
	  }
	  return -1;
	}

	/**
	 * The base implementation of `_.indexOf` without `fromIndex` bounds checks.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {*} value The value to search for.
	 * @param {number} fromIndex The index to search from.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
	function baseIndexOf(array, value, fromIndex) {
	  return value === value
	    ? strictIndexOf(array, value, fromIndex)
	    : baseFindIndex(array, baseIsNaN, fromIndex);
	}

	/** Used as references for various `Number` constants. */
	var MAX_SAFE_INTEGER = 9007199254740991;

	/** Used to detect unsigned integer values. */
	var reIsUint = /^(?:0|[1-9]\d*)$/;

	/**
	 * Checks if `value` is a valid array-like index.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
	 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
	 */
	function isIndex(value, length) {
	  var type = typeof value;
	  length = length == null ? MAX_SAFE_INTEGER : length;

	  return !!length &&
	    (type == 'number' ||
	      (type != 'symbol' && reIsUint.test(value))) &&
	        (value > -1 && value % 1 == 0 && value < length);
	}

	/**
	 * The base implementation of `assignValue` and `assignMergeValue` without
	 * value checks.
	 *
	 * @private
	 * @param {Object} object The object to modify.
	 * @param {string} key The key of the property to assign.
	 * @param {*} value The value to assign.
	 */
	function baseAssignValue(object, key, value) {
	  if (key == '__proto__' && defineProperty) {
	    defineProperty(object, key, {
	      'configurable': true,
	      'enumerable': true,
	      'value': value,
	      'writable': true
	    });
	  } else {
	    object[key] = value;
	  }
	}

	/**
	 * Performs a
	 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
	 * comparison between two values to determine if they are equivalent.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to compare.
	 * @param {*} other The other value to compare.
	 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
	 * @example
	 *
	 * var object = { 'a': 1 };
	 * var other = { 'a': 1 };
	 *
	 * _.eq(object, object);
	 * // => true
	 *
	 * _.eq(object, other);
	 * // => false
	 *
	 * _.eq('a', 'a');
	 * // => true
	 *
	 * _.eq('a', Object('a'));
	 * // => false
	 *
	 * _.eq(NaN, NaN);
	 * // => true
	 */
	function eq(value, other) {
	  return value === other || (value !== value && other !== other);
	}

	/** Used for built-in method references. */
	var objectProto$3 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$2 = objectProto$3.hasOwnProperty;

	/**
	 * Assigns `value` to `key` of `object` if the existing value is not equivalent
	 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
	 * for equality comparisons.
	 *
	 * @private
	 * @param {Object} object The object to modify.
	 * @param {string} key The key of the property to assign.
	 * @param {*} value The value to assign.
	 */
	function assignValue(object, key, value) {
	  var objValue = object[key];
	  if (!(hasOwnProperty$2.call(object, key) && eq(objValue, value)) ||
	      (value === undefined && !(key in object))) {
	    baseAssignValue(object, key, value);
	  }
	}

	/**
	 * Copies properties of `source` to `object`.
	 *
	 * @private
	 * @param {Object} source The object to copy properties from.
	 * @param {Array} props The property identifiers to copy.
	 * @param {Object} [object={}] The object to copy properties to.
	 * @param {Function} [customizer] The function to customize copied values.
	 * @returns {Object} Returns `object`.
	 */
	function copyObject(source, props, object, customizer) {
	  var isNew = !object;
	  object || (object = {});

	  var index = -1,
	      length = props.length;

	  while (++index < length) {
	    var key = props[index];

	    var newValue = customizer
	      ? customizer(object[key], source[key], key, object, source)
	      : undefined;

	    if (newValue === undefined) {
	      newValue = source[key];
	    }
	    if (isNew) {
	      baseAssignValue(object, key, newValue);
	    } else {
	      assignValue(object, key, newValue);
	    }
	  }
	  return object;
	}

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeMax = Math.max;

	/**
	 * A specialized version of `baseRest` which transforms the rest array.
	 *
	 * @private
	 * @param {Function} func The function to apply a rest parameter to.
	 * @param {number} [start=func.length-1] The start position of the rest parameter.
	 * @param {Function} transform The rest array transform.
	 * @returns {Function} Returns the new function.
	 */
	function overRest(func, start, transform) {
	  start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
	  return function() {
	    var args = arguments,
	        index = -1,
	        length = nativeMax(args.length - start, 0),
	        array = Array(length);

	    while (++index < length) {
	      array[index] = args[start + index];
	    }
	    index = -1;
	    var otherArgs = Array(start + 1);
	    while (++index < start) {
	      otherArgs[index] = args[index];
	    }
	    otherArgs[start] = transform(array);
	    return apply(func, this, otherArgs);
	  };
	}

	/**
	 * The base implementation of `_.rest` which doesn't validate or coerce arguments.
	 *
	 * @private
	 * @param {Function} func The function to apply a rest parameter to.
	 * @param {number} [start=func.length-1] The start position of the rest parameter.
	 * @returns {Function} Returns the new function.
	 */
	function baseRest(func, start) {
	  return setToString(overRest(func, start, identity), func + '');
	}

	/** Used as references for various `Number` constants. */
	var MAX_SAFE_INTEGER$1 = 9007199254740991;

	/**
	 * Checks if `value` is a valid array-like length.
	 *
	 * **Note:** This method is loosely based on
	 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
	 * @example
	 *
	 * _.isLength(3);
	 * // => true
	 *
	 * _.isLength(Number.MIN_VALUE);
	 * // => false
	 *
	 * _.isLength(Infinity);
	 * // => false
	 *
	 * _.isLength('3');
	 * // => false
	 */
	function isLength(value) {
	  return typeof value == 'number' &&
	    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER$1;
	}

	/**
	 * Checks if `value` is array-like. A value is considered array-like if it's
	 * not a function and has a `value.length` that's an integer greater than or
	 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
	 * @example
	 *
	 * _.isArrayLike([1, 2, 3]);
	 * // => true
	 *
	 * _.isArrayLike(document.body.children);
	 * // => true
	 *
	 * _.isArrayLike('abc');
	 * // => true
	 *
	 * _.isArrayLike(_.noop);
	 * // => false
	 */
	function isArrayLike(value) {
	  return value != null && isLength(value.length) && !isFunction$2(value);
	}

	/**
	 * Checks if the given arguments are from an iteratee call.
	 *
	 * @private
	 * @param {*} value The potential iteratee value argument.
	 * @param {*} index The potential iteratee index or key argument.
	 * @param {*} object The potential iteratee object argument.
	 * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
	 *  else `false`.
	 */
	function isIterateeCall(value, index, object) {
	  if (!isObject$2(object)) {
	    return false;
	  }
	  var type = typeof index;
	  if (type == 'number'
	        ? (isArrayLike(object) && isIndex(index, object.length))
	        : (type == 'string' && index in object)
	      ) {
	    return eq(object[index], value);
	  }
	  return false;
	}

	/**
	 * Creates a function like `_.assign`.
	 *
	 * @private
	 * @param {Function} assigner The function to assign values.
	 * @returns {Function} Returns the new assigner function.
	 */
	function createAssigner(assigner) {
	  return baseRest(function(object, sources) {
	    var index = -1,
	        length = sources.length,
	        customizer = length > 1 ? sources[length - 1] : undefined,
	        guard = length > 2 ? sources[2] : undefined;

	    customizer = (assigner.length > 3 && typeof customizer == 'function')
	      ? (length--, customizer)
	      : undefined;

	    if (guard && isIterateeCall(sources[0], sources[1], guard)) {
	      customizer = length < 3 ? undefined : customizer;
	      length = 1;
	    }
	    object = Object(object);
	    while (++index < length) {
	      var source = sources[index];
	      if (source) {
	        assigner(object, source, index, customizer);
	      }
	    }
	    return object;
	  });
	}

	/** Used for built-in method references. */
	var objectProto$4 = Object.prototype;

	/**
	 * Checks if `value` is likely a prototype object.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
	 */
	function isPrototype(value) {
	  var Ctor = value && value.constructor,
	      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto$4;

	  return value === proto;
	}

	/**
	 * The base implementation of `_.times` without support for iteratee shorthands
	 * or max array length checks.
	 *
	 * @private
	 * @param {number} n The number of times to invoke `iteratee`.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Array} Returns the array of results.
	 */
	function baseTimes(n, iteratee) {
	  var index = -1,
	      result = Array(n);

	  while (++index < n) {
	    result[index] = iteratee(index);
	  }
	  return result;
	}

	/** `Object#toString` result references. */
	var argsTag = '[object Arguments]';

	/**
	 * The base implementation of `_.isArguments`.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
	 */
	function baseIsArguments(value) {
	  return isObjectLike(value) && baseGetTag(value) == argsTag;
	}

	/** Used for built-in method references. */
	var objectProto$5 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$3 = objectProto$5.hasOwnProperty;

	/** Built-in value references. */
	var propertyIsEnumerable = objectProto$5.propertyIsEnumerable;

	/**
	 * Checks if `value` is likely an `arguments` object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
	 *  else `false`.
	 * @example
	 *
	 * _.isArguments(function() { return arguments; }());
	 * // => true
	 *
	 * _.isArguments([1, 2, 3]);
	 * // => false
	 */
	var isArguments = baseIsArguments(function() { return arguments; }()) ? baseIsArguments : function(value) {
	  return isObjectLike(value) && hasOwnProperty$3.call(value, 'callee') &&
	    !propertyIsEnumerable.call(value, 'callee');
	};

	/**
	 * This method returns `false`.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.13.0
	 * @category Util
	 * @returns {boolean} Returns `false`.
	 * @example
	 *
	 * _.times(2, _.stubFalse);
	 * // => [false, false]
	 */
	function stubFalse() {
	  return false;
	}

	/** Detect free variable `exports`. */
	var freeExports = typeof exports == 'object' && exports && !exports.nodeType && exports;

	/** Detect free variable `module`. */
	var freeModule = freeExports && typeof module == 'object' && module && !module.nodeType && module;

	/** Detect the popular CommonJS extension `module.exports`. */
	var moduleExports = freeModule && freeModule.exports === freeExports;

	/** Built-in value references. */
	var Buffer = moduleExports ? root.Buffer : undefined;

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;

	/**
	 * Checks if `value` is a buffer.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.3.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a buffer, else `false`.
	 * @example
	 *
	 * _.isBuffer(new Buffer(2));
	 * // => true
	 *
	 * _.isBuffer(new Uint8Array(2));
	 * // => false
	 */
	var isBuffer = nativeIsBuffer || stubFalse;

	/** `Object#toString` result references. */
	var argsTag$1 = '[object Arguments]',
	    arrayTag = '[object Array]',
	    boolTag = '[object Boolean]',
	    dateTag = '[object Date]',
	    errorTag = '[object Error]',
	    funcTag$1 = '[object Function]',
	    mapTag = '[object Map]',
	    numberTag = '[object Number]',
	    objectTag = '[object Object]',
	    regexpTag = '[object RegExp]',
	    setTag = '[object Set]',
	    stringTag = '[object String]',
	    weakMapTag = '[object WeakMap]';

	var arrayBufferTag = '[object ArrayBuffer]',
	    dataViewTag = '[object DataView]',
	    float32Tag = '[object Float32Array]',
	    float64Tag = '[object Float64Array]',
	    int8Tag = '[object Int8Array]',
	    int16Tag = '[object Int16Array]',
	    int32Tag = '[object Int32Array]',
	    uint8Tag = '[object Uint8Array]',
	    uint8ClampedTag = '[object Uint8ClampedArray]',
	    uint16Tag = '[object Uint16Array]',
	    uint32Tag = '[object Uint32Array]';

	/** Used to identify `toStringTag` values of typed arrays. */
	var typedArrayTags = {};
	typedArrayTags[float32Tag] = typedArrayTags[float64Tag] =
	typedArrayTags[int8Tag] = typedArrayTags[int16Tag] =
	typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] =
	typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] =
	typedArrayTags[uint32Tag] = true;
	typedArrayTags[argsTag$1] = typedArrayTags[arrayTag] =
	typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] =
	typedArrayTags[dataViewTag] = typedArrayTags[dateTag] =
	typedArrayTags[errorTag] = typedArrayTags[funcTag$1] =
	typedArrayTags[mapTag] = typedArrayTags[numberTag] =
	typedArrayTags[objectTag] = typedArrayTags[regexpTag] =
	typedArrayTags[setTag] = typedArrayTags[stringTag] =
	typedArrayTags[weakMapTag] = false;

	/**
	 * The base implementation of `_.isTypedArray` without Node.js optimizations.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
	 */
	function baseIsTypedArray(value) {
	  return isObjectLike(value) &&
	    isLength(value.length) && !!typedArrayTags[baseGetTag(value)];
	}

	/**
	 * The base implementation of `_.unary` without support for storing metadata.
	 *
	 * @private
	 * @param {Function} func The function to cap arguments for.
	 * @returns {Function} Returns the new capped function.
	 */
	function baseUnary(func) {
	  return function(value) {
	    return func(value);
	  };
	}

	/** Detect free variable `exports`. */
	var freeExports$1 = typeof exports == 'object' && exports && !exports.nodeType && exports;

	/** Detect free variable `module`. */
	var freeModule$1 = freeExports$1 && typeof module == 'object' && module && !module.nodeType && module;

	/** Detect the popular CommonJS extension `module.exports`. */
	var moduleExports$1 = freeModule$1 && freeModule$1.exports === freeExports$1;

	/** Detect free variable `process` from Node.js. */
	var freeProcess = moduleExports$1 && freeGlobal.process;

	/** Used to access faster Node.js helpers. */
	var nodeUtil = (function() {
	  try {
	    // Use `util.types` for Node.js 10+.
	    var types = freeModule$1 && freeModule$1.require && freeModule$1.require('util').types;

	    if (types) {
	      return types;
	    }

	    // Legacy `process.binding('util')` for Node.js < 10.
	    return freeProcess && freeProcess.binding && freeProcess.binding('util');
	  } catch (e) {}
	}());

	/* Node.js helper references. */
	var nodeIsTypedArray = nodeUtil && nodeUtil.isTypedArray;

	/**
	 * Checks if `value` is classified as a typed array.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a typed array, else `false`.
	 * @example
	 *
	 * _.isTypedArray(new Uint8Array);
	 * // => true
	 *
	 * _.isTypedArray([]);
	 * // => false
	 */
	var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;

	/** Used for built-in method references. */
	var objectProto$6 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$4 = objectProto$6.hasOwnProperty;

	/**
	 * Creates an array of the enumerable property names of the array-like `value`.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @param {boolean} inherited Specify returning inherited property names.
	 * @returns {Array} Returns the array of property names.
	 */
	function arrayLikeKeys(value, inherited) {
	  var isArr = isArray(value),
	      isArg = !isArr && isArguments(value),
	      isBuff = !isArr && !isArg && isBuffer(value),
	      isType = !isArr && !isArg && !isBuff && isTypedArray(value),
	      skipIndexes = isArr || isArg || isBuff || isType,
	      result = skipIndexes ? baseTimes(value.length, String) : [],
	      length = result.length;

	  for (var key in value) {
	    if ((inherited || hasOwnProperty$4.call(value, key)) &&
	        !(skipIndexes && (
	           // Safari 9 has enumerable `arguments.length` in strict mode.
	           key == 'length' ||
	           // Node.js 0.10 has enumerable non-index properties on buffers.
	           (isBuff && (key == 'offset' || key == 'parent')) ||
	           // PhantomJS 2 has enumerable non-index properties on typed arrays.
	           (isType && (key == 'buffer' || key == 'byteLength' || key == 'byteOffset')) ||
	           // Skip index properties.
	           isIndex(key, length)
	        ))) {
	      result.push(key);
	    }
	  }
	  return result;
	}

	/**
	 * Creates a unary function that invokes `func` with its argument transformed.
	 *
	 * @private
	 * @param {Function} func The function to wrap.
	 * @param {Function} transform The argument transform.
	 * @returns {Function} Returns the new function.
	 */
	function overArg(func, transform) {
	  return function(arg) {
	    return func(transform(arg));
	  };
	}

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeKeys = overArg(Object.keys, Object);

	/** Used for built-in method references. */
	var objectProto$7 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$5 = objectProto$7.hasOwnProperty;

	/**
	 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
	function baseKeys(object) {
	  if (!isPrototype(object)) {
	    return nativeKeys(object);
	  }
	  var result = [];
	  for (var key in Object(object)) {
	    if (hasOwnProperty$5.call(object, key) && key != 'constructor') {
	      result.push(key);
	    }
	  }
	  return result;
	}

	/**
	 * Creates an array of the own enumerable property names of `object`.
	 *
	 * **Note:** Non-object values are coerced to objects. See the
	 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
	 * for more details.
	 *
	 * @static
	 * @since 0.1.0
	 * @memberOf _
	 * @category Object
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.keys(new Foo);
	 * // => ['a', 'b'] (iteration order is not guaranteed)
	 *
	 * _.keys('hi');
	 * // => ['0', '1']
	 */
	function keys(object) {
	  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
	}

	/**
	 * This function is like
	 * [`Object.keys`](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
	 * except that it includes inherited enumerable properties.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
	function nativeKeysIn(object) {
	  var result = [];
	  if (object != null) {
	    for (var key in Object(object)) {
	      result.push(key);
	    }
	  }
	  return result;
	}

	/** Used for built-in method references. */
	var objectProto$8 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$6 = objectProto$8.hasOwnProperty;

	/**
	 * The base implementation of `_.keysIn` which doesn't treat sparse arrays as dense.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 */
	function baseKeysIn(object) {
	  if (!isObject$2(object)) {
	    return nativeKeysIn(object);
	  }
	  var isProto = isPrototype(object),
	      result = [];

	  for (var key in object) {
	    if (!(key == 'constructor' && (isProto || !hasOwnProperty$6.call(object, key)))) {
	      result.push(key);
	    }
	  }
	  return result;
	}

	/**
	 * Creates an array of the own and inherited enumerable property names of `object`.
	 *
	 * **Note:** Non-object values are coerced to objects.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.0.0
	 * @category Object
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.keysIn(new Foo);
	 * // => ['a', 'b', 'c'] (iteration order is not guaranteed)
	 */
	function keysIn(object) {
	  return isArrayLike(object) ? arrayLikeKeys(object, true) : baseKeysIn(object);
	}

	/** Used to match property names within property paths. */
	var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
	    reIsPlainProp = /^\w*$/;

	/**
	 * Checks if `value` is a property name and not a property path.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @param {Object} [object] The object to query keys on.
	 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
	 */
	function isKey(value, object) {
	  if (isArray(value)) {
	    return false;
	  }
	  var type = typeof value;
	  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
	      value == null || isSymbol(value)) {
	    return true;
	  }
	  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
	    (object != null && value in Object(object));
	}

	/* Built-in method references that are verified to be native. */
	var nativeCreate = getNative(Object, 'create');

	/**
	 * Removes all key-value entries from the hash.
	 *
	 * @private
	 * @name clear
	 * @memberOf Hash
	 */
	function hashClear() {
	  this.__data__ = nativeCreate ? nativeCreate(null) : {};
	  this.size = 0;
	}

	/**
	 * Removes `key` and its value from the hash.
	 *
	 * @private
	 * @name delete
	 * @memberOf Hash
	 * @param {Object} hash The hash to modify.
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
	function hashDelete(key) {
	  var result = this.has(key) && delete this.__data__[key];
	  this.size -= result ? 1 : 0;
	  return result;
	}

	/** Used to stand-in for `undefined` hash values. */
	var HASH_UNDEFINED = '__lodash_hash_undefined__';

	/** Used for built-in method references. */
	var objectProto$9 = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$7 = objectProto$9.hasOwnProperty;

	/**
	 * Gets the hash value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf Hash
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
	function hashGet(key) {
	  var data = this.__data__;
	  if (nativeCreate) {
	    var result = data[key];
	    return result === HASH_UNDEFINED ? undefined : result;
	  }
	  return hasOwnProperty$7.call(data, key) ? data[key] : undefined;
	}

	/** Used for built-in method references. */
	var objectProto$a = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$8 = objectProto$a.hasOwnProperty;

	/**
	 * Checks if a hash value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf Hash
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function hashHas(key) {
	  var data = this.__data__;
	  return nativeCreate ? (data[key] !== undefined) : hasOwnProperty$8.call(data, key);
	}

	/** Used to stand-in for `undefined` hash values. */
	var HASH_UNDEFINED$1 = '__lodash_hash_undefined__';

	/**
	 * Sets the hash `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf Hash
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the hash instance.
	 */
	function hashSet(key, value) {
	  var data = this.__data__;
	  this.size += this.has(key) ? 0 : 1;
	  data[key] = (nativeCreate && value === undefined) ? HASH_UNDEFINED$1 : value;
	  return this;
	}

	/**
	 * Creates a hash object.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
	function Hash(entries) {
	  var index = -1,
	      length = entries == null ? 0 : entries.length;

	  this.clear();
	  while (++index < length) {
	    var entry = entries[index];
	    this.set(entry[0], entry[1]);
	  }
	}

	// Add methods to `Hash`.
	Hash.prototype.clear = hashClear;
	Hash.prototype['delete'] = hashDelete;
	Hash.prototype.get = hashGet;
	Hash.prototype.has = hashHas;
	Hash.prototype.set = hashSet;

	/**
	 * Removes all key-value entries from the list cache.
	 *
	 * @private
	 * @name clear
	 * @memberOf ListCache
	 */
	function listCacheClear() {
	  this.__data__ = [];
	  this.size = 0;
	}

	/**
	 * Gets the index at which the `key` is found in `array` of key-value pairs.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {*} key The key to search for.
	 * @returns {number} Returns the index of the matched value, else `-1`.
	 */
	function assocIndexOf(array, key) {
	  var length = array.length;
	  while (length--) {
	    if (eq(array[length][0], key)) {
	      return length;
	    }
	  }
	  return -1;
	}

	/** Used for built-in method references. */
	var arrayProto = Array.prototype;

	/** Built-in value references. */
	var splice = arrayProto.splice;

	/**
	 * Removes `key` and its value from the list cache.
	 *
	 * @private
	 * @name delete
	 * @memberOf ListCache
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
	function listCacheDelete(key) {
	  var data = this.__data__,
	      index = assocIndexOf(data, key);

	  if (index < 0) {
	    return false;
	  }
	  var lastIndex = data.length - 1;
	  if (index == lastIndex) {
	    data.pop();
	  } else {
	    splice.call(data, index, 1);
	  }
	  --this.size;
	  return true;
	}

	/**
	 * Gets the list cache value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf ListCache
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
	function listCacheGet(key) {
	  var data = this.__data__,
	      index = assocIndexOf(data, key);

	  return index < 0 ? undefined : data[index][1];
	}

	/**
	 * Checks if a list cache value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf ListCache
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function listCacheHas(key) {
	  return assocIndexOf(this.__data__, key) > -1;
	}

	/**
	 * Sets the list cache `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf ListCache
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the list cache instance.
	 */
	function listCacheSet(key, value) {
	  var data = this.__data__,
	      index = assocIndexOf(data, key);

	  if (index < 0) {
	    ++this.size;
	    data.push([key, value]);
	  } else {
	    data[index][1] = value;
	  }
	  return this;
	}

	/**
	 * Creates an list cache object.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
	function ListCache(entries) {
	  var index = -1,
	      length = entries == null ? 0 : entries.length;

	  this.clear();
	  while (++index < length) {
	    var entry = entries[index];
	    this.set(entry[0], entry[1]);
	  }
	}

	// Add methods to `ListCache`.
	ListCache.prototype.clear = listCacheClear;
	ListCache.prototype['delete'] = listCacheDelete;
	ListCache.prototype.get = listCacheGet;
	ListCache.prototype.has = listCacheHas;
	ListCache.prototype.set = listCacheSet;

	/* Built-in method references that are verified to be native. */
	var Map$1 = getNative(root, 'Map');

	/**
	 * Removes all key-value entries from the map.
	 *
	 * @private
	 * @name clear
	 * @memberOf MapCache
	 */
	function mapCacheClear() {
	  this.size = 0;
	  this.__data__ = {
	    'hash': new Hash,
	    'map': new (Map$1 || ListCache),
	    'string': new Hash
	  };
	}

	/**
	 * Checks if `value` is suitable for use as unique object key.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is suitable, else `false`.
	 */
	function isKeyable(value) {
	  var type = typeof value;
	  return (type == 'string' || type == 'number' || type == 'symbol' || type == 'boolean')
	    ? (value !== '__proto__')
	    : (value === null);
	}

	/**
	 * Gets the data for `map`.
	 *
	 * @private
	 * @param {Object} map The map to query.
	 * @param {string} key The reference key.
	 * @returns {*} Returns the map data.
	 */
	function getMapData(map, key) {
	  var data = map.__data__;
	  return isKeyable(key)
	    ? data[typeof key == 'string' ? 'string' : 'hash']
	    : data.map;
	}

	/**
	 * Removes `key` and its value from the map.
	 *
	 * @private
	 * @name delete
	 * @memberOf MapCache
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
	function mapCacheDelete(key) {
	  var result = getMapData(this, key)['delete'](key);
	  this.size -= result ? 1 : 0;
	  return result;
	}

	/**
	 * Gets the map value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf MapCache
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
	function mapCacheGet(key) {
	  return getMapData(this, key).get(key);
	}

	/**
	 * Checks if a map value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf MapCache
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function mapCacheHas(key) {
	  return getMapData(this, key).has(key);
	}

	/**
	 * Sets the map `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf MapCache
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the map cache instance.
	 */
	function mapCacheSet(key, value) {
	  var data = getMapData(this, key),
	      size = data.size;

	  data.set(key, value);
	  this.size += data.size == size ? 0 : 1;
	  return this;
	}

	/**
	 * Creates a map cache object to store key-value pairs.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
	function MapCache(entries) {
	  var index = -1,
	      length = entries == null ? 0 : entries.length;

	  this.clear();
	  while (++index < length) {
	    var entry = entries[index];
	    this.set(entry[0], entry[1]);
	  }
	}

	// Add methods to `MapCache`.
	MapCache.prototype.clear = mapCacheClear;
	MapCache.prototype['delete'] = mapCacheDelete;
	MapCache.prototype.get = mapCacheGet;
	MapCache.prototype.has = mapCacheHas;
	MapCache.prototype.set = mapCacheSet;

	/** Error message constants. */
	var FUNC_ERROR_TEXT = 'Expected a function';

	/**
	 * Creates a function that memoizes the result of `func`. If `resolver` is
	 * provided, it determines the cache key for storing the result based on the
	 * arguments provided to the memoized function. By default, the first argument
	 * provided to the memoized function is used as the map cache key. The `func`
	 * is invoked with the `this` binding of the memoized function.
	 *
	 * **Note:** The cache is exposed as the `cache` property on the memoized
	 * function. Its creation may be customized by replacing the `_.memoize.Cache`
	 * constructor with one whose instances implement the
	 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
	 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Function
	 * @param {Function} func The function to have its output memoized.
	 * @param {Function} [resolver] The function to resolve the cache key.
	 * @returns {Function} Returns the new memoized function.
	 * @example
	 *
	 * var object = { 'a': 1, 'b': 2 };
	 * var other = { 'c': 3, 'd': 4 };
	 *
	 * var values = _.memoize(_.values);
	 * values(object);
	 * // => [1, 2]
	 *
	 * values(other);
	 * // => [3, 4]
	 *
	 * object.a = 2;
	 * values(object);
	 * // => [1, 2]
	 *
	 * // Modify the result cache.
	 * values.cache.set(object, ['a', 'b']);
	 * values(object);
	 * // => ['a', 'b']
	 *
	 * // Replace `_.memoize.Cache`.
	 * _.memoize.Cache = WeakMap;
	 */
	function memoize(func, resolver) {
	  if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
	    throw new TypeError(FUNC_ERROR_TEXT);
	  }
	  var memoized = function() {
	    var args = arguments,
	        key = resolver ? resolver.apply(this, args) : args[0],
	        cache = memoized.cache;

	    if (cache.has(key)) {
	      return cache.get(key);
	    }
	    var result = func.apply(this, args);
	    memoized.cache = cache.set(key, result) || cache;
	    return result;
	  };
	  memoized.cache = new (memoize.Cache || MapCache);
	  return memoized;
	}

	// Expose `MapCache`.
	memoize.Cache = MapCache;

	/** Used as the maximum memoize cache size. */
	var MAX_MEMOIZE_SIZE = 500;

	/**
	 * A specialized version of `_.memoize` which clears the memoized function's
	 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
	 *
	 * @private
	 * @param {Function} func The function to have its output memoized.
	 * @returns {Function} Returns the new memoized function.
	 */
	function memoizeCapped(func) {
	  var result = memoize(func, function(key) {
	    if (cache.size === MAX_MEMOIZE_SIZE) {
	      cache.clear();
	    }
	    return key;
	  });

	  var cache = result.cache;
	  return result;
	}

	/** Used to match property names within property paths. */
	var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

	/** Used to match backslashes in property paths. */
	var reEscapeChar = /\\(\\)?/g;

	/**
	 * Converts `string` to a property path array.
	 *
	 * @private
	 * @param {string} string The string to convert.
	 * @returns {Array} Returns the property path array.
	 */
	var stringToPath = memoizeCapped(function(string) {
	  var result = [];
	  if (string.charCodeAt(0) === 46 /* . */) {
	    result.push('');
	  }
	  string.replace(rePropName, function(match, number, quote, subString) {
	    result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
	  });
	  return result;
	});

	/**
	 * Converts `value` to a string. An empty string is returned for `null`
	 * and `undefined` values. The sign of `-0` is preserved.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to convert.
	 * @returns {string} Returns the converted string.
	 * @example
	 *
	 * _.toString(null);
	 * // => ''
	 *
	 * _.toString(-0);
	 * // => '-0'
	 *
	 * _.toString([1, 2, 3]);
	 * // => '1,2,3'
	 */
	function toString(value) {
	  return value == null ? '' : baseToString(value);
	}

	/**
	 * Casts `value` to a path array if it's not one.
	 *
	 * @private
	 * @param {*} value The value to inspect.
	 * @param {Object} [object] The object to query keys on.
	 * @returns {Array} Returns the cast property path array.
	 */
	function castPath(value, object) {
	  if (isArray(value)) {
	    return value;
	  }
	  return isKey(value, object) ? [value] : stringToPath(toString(value));
	}

	/** Used as references for various `Number` constants. */
	var INFINITY$1 = 1 / 0;

	/**
	 * Converts `value` to a string key if it's not a string or symbol.
	 *
	 * @private
	 * @param {*} value The value to inspect.
	 * @returns {string|symbol} Returns the key.
	 */
	function toKey(value) {
	  if (typeof value == 'string' || isSymbol(value)) {
	    return value;
	  }
	  var result = (value + '');
	  return (result == '0' && (1 / value) == -INFINITY$1) ? '-0' : result;
	}

	/**
	 * The base implementation of `_.get` without support for default values.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {Array|string} path The path of the property to get.
	 * @returns {*} Returns the resolved value.
	 */
	function baseGet(object, path) {
	  path = castPath(path, object);

	  var index = 0,
	      length = path.length;

	  while (object != null && index < length) {
	    object = object[toKey(path[index++])];
	  }
	  return (index && index == length) ? object : undefined;
	}

	/**
	 * Gets the value at `path` of `object`. If the resolved value is
	 * `undefined`, the `defaultValue` is returned in its place.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.7.0
	 * @category Object
	 * @param {Object} object The object to query.
	 * @param {Array|string} path The path of the property to get.
	 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
	 * @returns {*} Returns the resolved value.
	 * @example
	 *
	 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
	 *
	 * _.get(object, 'a[0].b.c');
	 * // => 3
	 *
	 * _.get(object, ['a', '0', 'b', 'c']);
	 * // => 3
	 *
	 * _.get(object, 'a.b.c', 'default');
	 * // => 'default'
	 */
	function get(object, path, defaultValue) {
	  var result = object == null ? undefined : baseGet(object, path);
	  return result === undefined ? defaultValue : result;
	}

	/**
	 * Appends the elements of `values` to `array`.
	 *
	 * @private
	 * @param {Array} array The array to modify.
	 * @param {Array} values The values to append.
	 * @returns {Array} Returns `array`.
	 */
	function arrayPush(array, values) {
	  var index = -1,
	      length = values.length,
	      offset = array.length;

	  while (++index < length) {
	    array[offset + index] = values[index];
	  }
	  return array;
	}

	/** Built-in value references. */
	var getPrototype = overArg(Object.getPrototypeOf, Object);

	/** `Object#toString` result references. */
	var objectTag$1 = '[object Object]';

	/** Used for built-in method references. */
	var funcProto$2 = Function.prototype,
	    objectProto$b = Object.prototype;

	/** Used to resolve the decompiled source of functions. */
	var funcToString$2 = funcProto$2.toString;

	/** Used to check objects for own properties. */
	var hasOwnProperty$9 = objectProto$b.hasOwnProperty;

	/** Used to infer the `Object` constructor. */
	var objectCtorString = funcToString$2.call(Object);

	/**
	 * Checks if `value` is a plain object, that is, an object created by the
	 * `Object` constructor or one with a `[[Prototype]]` of `null`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.8.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
	 * @example
	 *
	 * function Foo() {
	 *   this.a = 1;
	 * }
	 *
	 * _.isPlainObject(new Foo);
	 * // => false
	 *
	 * _.isPlainObject([1, 2, 3]);
	 * // => false
	 *
	 * _.isPlainObject({ 'x': 0, 'y': 0 });
	 * // => true
	 *
	 * _.isPlainObject(Object.create(null));
	 * // => true
	 */
	function isPlainObject(value) {
	  if (!isObjectLike(value) || baseGetTag(value) != objectTag$1) {
	    return false;
	  }
	  var proto = getPrototype(value);
	  if (proto === null) {
	    return true;
	  }
	  var Ctor = hasOwnProperty$9.call(proto, 'constructor') && proto.constructor;
	  return typeof Ctor == 'function' && Ctor instanceof Ctor &&
	    funcToString$2.call(Ctor) == objectCtorString;
	}

	/**
	 * The base implementation of `_.slice` without an iteratee call guard.
	 *
	 * @private
	 * @param {Array} array The array to slice.
	 * @param {number} [start=0] The start position.
	 * @param {number} [end=array.length] The end position.
	 * @returns {Array} Returns the slice of `array`.
	 */
	function baseSlice(array, start, end) {
	  var index = -1,
	      length = array.length;

	  if (start < 0) {
	    start = -start > length ? 0 : (length + start);
	  }
	  end = end > length ? length : end;
	  if (end < 0) {
	    end += length;
	  }
	  length = start > end ? 0 : ((end - start) >>> 0);
	  start >>>= 0;

	  var result = Array(length);
	  while (++index < length) {
	    result[index] = array[index + start];
	  }
	  return result;
	}

	/**
	 * Casts `array` to a slice if it's needed.
	 *
	 * @private
	 * @param {Array} array The array to inspect.
	 * @param {number} start The start position.
	 * @param {number} [end=array.length] The end position.
	 * @returns {Array} Returns the cast slice.
	 */
	function castSlice(array, start, end) {
	  var length = array.length;
	  end = end === undefined ? length : end;
	  return (!start && end >= length) ? array : baseSlice(array, start, end);
	}

	/** Used to compose unicode character classes. */
	var rsAstralRange = '\\ud800-\\udfff',
	    rsComboMarksRange = '\\u0300-\\u036f',
	    reComboHalfMarksRange = '\\ufe20-\\ufe2f',
	    rsComboSymbolsRange = '\\u20d0-\\u20ff',
	    rsComboRange = rsComboMarksRange + reComboHalfMarksRange + rsComboSymbolsRange,
	    rsVarRange = '\\ufe0e\\ufe0f';

	/** Used to compose unicode capture groups. */
	var rsZWJ = '\\u200d';

	/** Used to detect strings with [zero-width joiners or code points from the astral planes](http://eev.ee/blog/2015/09/12/dark-corners-of-unicode/). */
	var reHasUnicode = RegExp('[' + rsZWJ + rsAstralRange  + rsComboRange + rsVarRange + ']');

	/**
	 * Checks if `string` contains Unicode symbols.
	 *
	 * @private
	 * @param {string} string The string to inspect.
	 * @returns {boolean} Returns `true` if a symbol is found, else `false`.
	 */
	function hasUnicode(string) {
	  return reHasUnicode.test(string);
	}

	/**
	 * Converts an ASCII `string` to an array.
	 *
	 * @private
	 * @param {string} string The string to convert.
	 * @returns {Array} Returns the converted array.
	 */
	function asciiToArray(string) {
	  return string.split('');
	}

	/** Used to compose unicode character classes. */
	var rsAstralRange$1 = '\\ud800-\\udfff',
	    rsComboMarksRange$1 = '\\u0300-\\u036f',
	    reComboHalfMarksRange$1 = '\\ufe20-\\ufe2f',
	    rsComboSymbolsRange$1 = '\\u20d0-\\u20ff',
	    rsComboRange$1 = rsComboMarksRange$1 + reComboHalfMarksRange$1 + rsComboSymbolsRange$1,
	    rsVarRange$1 = '\\ufe0e\\ufe0f';

	/** Used to compose unicode capture groups. */
	var rsAstral = '[' + rsAstralRange$1 + ']',
	    rsCombo = '[' + rsComboRange$1 + ']',
	    rsFitz = '\\ud83c[\\udffb-\\udfff]',
	    rsModifier = '(?:' + rsCombo + '|' + rsFitz + ')',
	    rsNonAstral = '[^' + rsAstralRange$1 + ']',
	    rsRegional = '(?:\\ud83c[\\udde6-\\uddff]){2}',
	    rsSurrPair = '[\\ud800-\\udbff][\\udc00-\\udfff]',
	    rsZWJ$1 = '\\u200d';

	/** Used to compose unicode regexes. */
	var reOptMod = rsModifier + '?',
	    rsOptVar = '[' + rsVarRange$1 + ']?',
	    rsOptJoin = '(?:' + rsZWJ$1 + '(?:' + [rsNonAstral, rsRegional, rsSurrPair].join('|') + ')' + rsOptVar + reOptMod + ')*',
	    rsSeq = rsOptVar + reOptMod + rsOptJoin,
	    rsSymbol = '(?:' + [rsNonAstral + rsCombo + '?', rsCombo, rsRegional, rsSurrPair, rsAstral].join('|') + ')';

	/** Used to match [string symbols](https://mathiasbynens.be/notes/javascript-unicode). */
	var reUnicode = RegExp(rsFitz + '(?=' + rsFitz + ')|' + rsSymbol + rsSeq, 'g');

	/**
	 * Converts a Unicode `string` to an array.
	 *
	 * @private
	 * @param {string} string The string to convert.
	 * @returns {Array} Returns the converted array.
	 */
	function unicodeToArray(string) {
	  return string.match(reUnicode) || [];
	}

	/**
	 * Converts `string` to an array.
	 *
	 * @private
	 * @param {string} string The string to convert.
	 * @returns {Array} Returns the converted array.
	 */
	function stringToArray(string) {
	  return hasUnicode(string)
	    ? unicodeToArray(string)
	    : asciiToArray(string);
	}

	/**
	 * Removes all key-value entries from the stack.
	 *
	 * @private
	 * @name clear
	 * @memberOf Stack
	 */
	function stackClear() {
	  this.__data__ = new ListCache;
	  this.size = 0;
	}

	/**
	 * Removes `key` and its value from the stack.
	 *
	 * @private
	 * @name delete
	 * @memberOf Stack
	 * @param {string} key The key of the value to remove.
	 * @returns {boolean} Returns `true` if the entry was removed, else `false`.
	 */
	function stackDelete(key) {
	  var data = this.__data__,
	      result = data['delete'](key);

	  this.size = data.size;
	  return result;
	}

	/**
	 * Gets the stack value for `key`.
	 *
	 * @private
	 * @name get
	 * @memberOf Stack
	 * @param {string} key The key of the value to get.
	 * @returns {*} Returns the entry value.
	 */
	function stackGet(key) {
	  return this.__data__.get(key);
	}

	/**
	 * Checks if a stack value for `key` exists.
	 *
	 * @private
	 * @name has
	 * @memberOf Stack
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function stackHas(key) {
	  return this.__data__.has(key);
	}

	/** Used as the size to enable large array optimizations. */
	var LARGE_ARRAY_SIZE = 200;

	/**
	 * Sets the stack `key` to `value`.
	 *
	 * @private
	 * @name set
	 * @memberOf Stack
	 * @param {string} key The key of the value to set.
	 * @param {*} value The value to set.
	 * @returns {Object} Returns the stack cache instance.
	 */
	function stackSet(key, value) {
	  var data = this.__data__;
	  if (data instanceof ListCache) {
	    var pairs = data.__data__;
	    if (!Map$1 || (pairs.length < LARGE_ARRAY_SIZE - 1)) {
	      pairs.push([key, value]);
	      this.size = ++data.size;
	      return this;
	    }
	    data = this.__data__ = new MapCache(pairs);
	  }
	  data.set(key, value);
	  this.size = data.size;
	  return this;
	}

	/**
	 * Creates a stack cache object to store key-value pairs.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [entries] The key-value pairs to cache.
	 */
	function Stack(entries) {
	  var data = this.__data__ = new ListCache(entries);
	  this.size = data.size;
	}

	// Add methods to `Stack`.
	Stack.prototype.clear = stackClear;
	Stack.prototype['delete'] = stackDelete;
	Stack.prototype.get = stackGet;
	Stack.prototype.has = stackHas;
	Stack.prototype.set = stackSet;

	/** Detect free variable `exports`. */
	var freeExports$2 = typeof exports == 'object' && exports && !exports.nodeType && exports;

	/** Detect free variable `module`. */
	var freeModule$2 = freeExports$2 && typeof module == 'object' && module && !module.nodeType && module;

	/** Detect the popular CommonJS extension `module.exports`. */
	var moduleExports$2 = freeModule$2 && freeModule$2.exports === freeExports$2;

	/** Built-in value references. */
	var Buffer$1 = moduleExports$2 ? root.Buffer : undefined,
	    allocUnsafe = Buffer$1 ? Buffer$1.allocUnsafe : undefined;

	/**
	 * Creates a clone of  `buffer`.
	 *
	 * @private
	 * @param {Buffer} buffer The buffer to clone.
	 * @param {boolean} [isDeep] Specify a deep clone.
	 * @returns {Buffer} Returns the cloned buffer.
	 */
	function cloneBuffer(buffer, isDeep) {
	  if (isDeep) {
	    return buffer.slice();
	  }
	  var length = buffer.length,
	      result = allocUnsafe ? allocUnsafe(length) : new buffer.constructor(length);

	  buffer.copy(result);
	  return result;
	}

	/**
	 * A specialized version of `_.filter` for arrays without support for
	 * iteratee shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} predicate The function invoked per iteration.
	 * @returns {Array} Returns the new filtered array.
	 */
	function arrayFilter(array, predicate) {
	  var index = -1,
	      length = array == null ? 0 : array.length,
	      resIndex = 0,
	      result = [];

	  while (++index < length) {
	    var value = array[index];
	    if (predicate(value, index, array)) {
	      result[resIndex++] = value;
	    }
	  }
	  return result;
	}

	/**
	 * This method returns a new empty array.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.13.0
	 * @category Util
	 * @returns {Array} Returns the new empty array.
	 * @example
	 *
	 * var arrays = _.times(2, _.stubArray);
	 *
	 * console.log(arrays);
	 * // => [[], []]
	 *
	 * console.log(arrays[0] === arrays[1]);
	 * // => false
	 */
	function stubArray() {
	  return [];
	}

	/** Used for built-in method references. */
	var objectProto$c = Object.prototype;

	/** Built-in value references. */
	var propertyIsEnumerable$1 = objectProto$c.propertyIsEnumerable;

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeGetSymbols = Object.getOwnPropertySymbols;

	/**
	 * Creates an array of the own enumerable symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of symbols.
	 */
	var getSymbols = !nativeGetSymbols ? stubArray : function(object) {
	  if (object == null) {
	    return [];
	  }
	  object = Object(object);
	  return arrayFilter(nativeGetSymbols(object), function(symbol) {
	    return propertyIsEnumerable$1.call(object, symbol);
	  });
	};

	/* Built-in method references for those with the same name as other `lodash` methods. */
	var nativeGetSymbols$1 = Object.getOwnPropertySymbols;

	/**
	 * Creates an array of the own and inherited enumerable symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of symbols.
	 */
	var getSymbolsIn = !nativeGetSymbols$1 ? stubArray : function(object) {
	  var result = [];
	  while (object) {
	    arrayPush(result, getSymbols(object));
	    object = getPrototype(object);
	  }
	  return result;
	};

	/**
	 * The base implementation of `getAllKeys` and `getAllKeysIn` which uses
	 * `keysFunc` and `symbolsFunc` to get the enumerable property names and
	 * symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {Function} keysFunc The function to get the keys of `object`.
	 * @param {Function} symbolsFunc The function to get the symbols of `object`.
	 * @returns {Array} Returns the array of property names and symbols.
	 */
	function baseGetAllKeys(object, keysFunc, symbolsFunc) {
	  var result = keysFunc(object);
	  return isArray(object) ? result : arrayPush(result, symbolsFunc(object));
	}

	/**
	 * Creates an array of own enumerable property names and symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names and symbols.
	 */
	function getAllKeys(object) {
	  return baseGetAllKeys(object, keys, getSymbols);
	}

	/**
	 * Creates an array of own and inherited enumerable property names and
	 * symbols of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the array of property names and symbols.
	 */
	function getAllKeysIn(object) {
	  return baseGetAllKeys(object, keysIn, getSymbolsIn);
	}

	/* Built-in method references that are verified to be native. */
	var DataView = getNative(root, 'DataView');

	/* Built-in method references that are verified to be native. */
	var Promise$1 = getNative(root, 'Promise');

	/* Built-in method references that are verified to be native. */
	var Set$1 = getNative(root, 'Set');

	/** `Object#toString` result references. */
	var mapTag$1 = '[object Map]',
	    objectTag$2 = '[object Object]',
	    promiseTag = '[object Promise]',
	    setTag$1 = '[object Set]',
	    weakMapTag$1 = '[object WeakMap]';

	var dataViewTag$1 = '[object DataView]';

	/** Used to detect maps, sets, and weakmaps. */
	var dataViewCtorString = toSource(DataView),
	    mapCtorString = toSource(Map$1),
	    promiseCtorString = toSource(Promise$1),
	    setCtorString = toSource(Set$1),
	    weakMapCtorString = toSource(WeakMap$1);

	/**
	 * Gets the `toStringTag` of `value`.
	 *
	 * @private
	 * @param {*} value The value to query.
	 * @returns {string} Returns the `toStringTag`.
	 */
	var getTag = baseGetTag;

	// Fallback for data views, maps, sets, and weak maps in IE 11 and promises in Node.js < 6.
	if ((DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag$1) ||
	    (Map$1 && getTag(new Map$1) != mapTag$1) ||
	    (Promise$1 && getTag(Promise$1.resolve()) != promiseTag) ||
	    (Set$1 && getTag(new Set$1) != setTag$1) ||
	    (WeakMap$1 && getTag(new WeakMap$1) != weakMapTag$1)) {
	  getTag = function(value) {
	    var result = baseGetTag(value),
	        Ctor = result == objectTag$2 ? value.constructor : undefined,
	        ctorString = Ctor ? toSource(Ctor) : '';

	    if (ctorString) {
	      switch (ctorString) {
	        case dataViewCtorString: return dataViewTag$1;
	        case mapCtorString: return mapTag$1;
	        case promiseCtorString: return promiseTag;
	        case setCtorString: return setTag$1;
	        case weakMapCtorString: return weakMapTag$1;
	      }
	    }
	    return result;
	  };
	}

	var getTag$1 = getTag;

	/** Built-in value references. */
	var Uint8Array$1 = root.Uint8Array;

	/**
	 * Creates a clone of `arrayBuffer`.
	 *
	 * @private
	 * @param {ArrayBuffer} arrayBuffer The array buffer to clone.
	 * @returns {ArrayBuffer} Returns the cloned array buffer.
	 */
	function cloneArrayBuffer(arrayBuffer) {
	  var result = new arrayBuffer.constructor(arrayBuffer.byteLength);
	  new Uint8Array$1(result).set(new Uint8Array$1(arrayBuffer));
	  return result;
	}

	/**
	 * Creates a clone of `typedArray`.
	 *
	 * @private
	 * @param {Object} typedArray The typed array to clone.
	 * @param {boolean} [isDeep] Specify a deep clone.
	 * @returns {Object} Returns the cloned typed array.
	 */
	function cloneTypedArray(typedArray, isDeep) {
	  var buffer = isDeep ? cloneArrayBuffer(typedArray.buffer) : typedArray.buffer;
	  return new typedArray.constructor(buffer, typedArray.byteOffset, typedArray.length);
	}

	/**
	 * Initializes an object clone.
	 *
	 * @private
	 * @param {Object} object The object to clone.
	 * @returns {Object} Returns the initialized clone.
	 */
	function initCloneObject(object) {
	  return (typeof object.constructor == 'function' && !isPrototype(object))
	    ? baseCreate(getPrototype(object))
	    : {};
	}

	/** Used to stand-in for `undefined` hash values. */
	var HASH_UNDEFINED$2 = '__lodash_hash_undefined__';

	/**
	 * Adds `value` to the array cache.
	 *
	 * @private
	 * @name add
	 * @memberOf SetCache
	 * @alias push
	 * @param {*} value The value to cache.
	 * @returns {Object} Returns the cache instance.
	 */
	function setCacheAdd(value) {
	  this.__data__.set(value, HASH_UNDEFINED$2);
	  return this;
	}

	/**
	 * Checks if `value` is in the array cache.
	 *
	 * @private
	 * @name has
	 * @memberOf SetCache
	 * @param {*} value The value to search for.
	 * @returns {number} Returns `true` if `value` is found, else `false`.
	 */
	function setCacheHas(value) {
	  return this.__data__.has(value);
	}

	/**
	 *
	 * Creates an array cache object to store unique values.
	 *
	 * @private
	 * @constructor
	 * @param {Array} [values] The values to cache.
	 */
	function SetCache(values) {
	  var index = -1,
	      length = values == null ? 0 : values.length;

	  this.__data__ = new MapCache;
	  while (++index < length) {
	    this.add(values[index]);
	  }
	}

	// Add methods to `SetCache`.
	SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
	SetCache.prototype.has = setCacheHas;

	/**
	 * A specialized version of `_.some` for arrays without support for iteratee
	 * shorthands.
	 *
	 * @private
	 * @param {Array} [array] The array to iterate over.
	 * @param {Function} predicate The function invoked per iteration.
	 * @returns {boolean} Returns `true` if any element passes the predicate check,
	 *  else `false`.
	 */
	function arraySome(array, predicate) {
	  var index = -1,
	      length = array == null ? 0 : array.length;

	  while (++index < length) {
	    if (predicate(array[index], index, array)) {
	      return true;
	    }
	  }
	  return false;
	}

	/**
	 * Checks if a `cache` value for `key` exists.
	 *
	 * @private
	 * @param {Object} cache The cache to query.
	 * @param {string} key The key of the entry to check.
	 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
	 */
	function cacheHas(cache, key) {
	  return cache.has(key);
	}

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG = 1,
	    COMPARE_UNORDERED_FLAG = 2;

	/**
	 * A specialized version of `baseIsEqualDeep` for arrays with support for
	 * partial deep comparisons.
	 *
	 * @private
	 * @param {Array} array The array to compare.
	 * @param {Array} other The other array to compare.
	 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
	 * @param {Function} customizer The function to customize comparisons.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Object} stack Tracks traversed `array` and `other` objects.
	 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
	 */
	function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
	  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
	      arrLength = array.length,
	      othLength = other.length;

	  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
	    return false;
	  }
	  // Assume cyclic values are equal.
	  var stacked = stack.get(array);
	  if (stacked && stack.get(other)) {
	    return stacked == other;
	  }
	  var index = -1,
	      result = true,
	      seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

	  stack.set(array, other);
	  stack.set(other, array);

	  // Ignore non-index properties.
	  while (++index < arrLength) {
	    var arrValue = array[index],
	        othValue = other[index];

	    if (customizer) {
	      var compared = isPartial
	        ? customizer(othValue, arrValue, index, other, array, stack)
	        : customizer(arrValue, othValue, index, array, other, stack);
	    }
	    if (compared !== undefined) {
	      if (compared) {
	        continue;
	      }
	      result = false;
	      break;
	    }
	    // Recursively compare arrays (susceptible to call stack limits).
	    if (seen) {
	      if (!arraySome(other, function(othValue, othIndex) {
	            if (!cacheHas(seen, othIndex) &&
	                (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
	              return seen.push(othIndex);
	            }
	          })) {
	        result = false;
	        break;
	      }
	    } else if (!(
	          arrValue === othValue ||
	            equalFunc(arrValue, othValue, bitmask, customizer, stack)
	        )) {
	      result = false;
	      break;
	    }
	  }
	  stack['delete'](array);
	  stack['delete'](other);
	  return result;
	}

	/**
	 * Converts `map` to its key-value pairs.
	 *
	 * @private
	 * @param {Object} map The map to convert.
	 * @returns {Array} Returns the key-value pairs.
	 */
	function mapToArray(map) {
	  var index = -1,
	      result = Array(map.size);

	  map.forEach(function(value, key) {
	    result[++index] = [key, value];
	  });
	  return result;
	}

	/**
	 * Converts `set` to an array of its values.
	 *
	 * @private
	 * @param {Object} set The set to convert.
	 * @returns {Array} Returns the values.
	 */
	function setToArray(set) {
	  var index = -1,
	      result = Array(set.size);

	  set.forEach(function(value) {
	    result[++index] = value;
	  });
	  return result;
	}

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$1 = 1,
	    COMPARE_UNORDERED_FLAG$1 = 2;

	/** `Object#toString` result references. */
	var boolTag$1 = '[object Boolean]',
	    dateTag$1 = '[object Date]',
	    errorTag$1 = '[object Error]',
	    mapTag$2 = '[object Map]',
	    numberTag$1 = '[object Number]',
	    regexpTag$1 = '[object RegExp]',
	    setTag$2 = '[object Set]',
	    stringTag$1 = '[object String]',
	    symbolTag$1 = '[object Symbol]';

	var arrayBufferTag$1 = '[object ArrayBuffer]',
	    dataViewTag$2 = '[object DataView]';

	/** Used to convert symbols to primitives and strings. */
	var symbolProto$1 = Symbol$1 ? Symbol$1.prototype : undefined,
	    symbolValueOf = symbolProto$1 ? symbolProto$1.valueOf : undefined;

	/**
	 * A specialized version of `baseIsEqualDeep` for comparing objects of
	 * the same `toStringTag`.
	 *
	 * **Note:** This function only supports comparing values with tags of
	 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {string} tag The `toStringTag` of the objects to compare.
	 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
	 * @param {Function} customizer The function to customize comparisons.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Object} stack Tracks traversed `object` and `other` objects.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
	  switch (tag) {
	    case dataViewTag$2:
	      if ((object.byteLength != other.byteLength) ||
	          (object.byteOffset != other.byteOffset)) {
	        return false;
	      }
	      object = object.buffer;
	      other = other.buffer;

	    case arrayBufferTag$1:
	      if ((object.byteLength != other.byteLength) ||
	          !equalFunc(new Uint8Array$1(object), new Uint8Array$1(other))) {
	        return false;
	      }
	      return true;

	    case boolTag$1:
	    case dateTag$1:
	    case numberTag$1:
	      // Coerce booleans to `1` or `0` and dates to milliseconds.
	      // Invalid dates are coerced to `NaN`.
	      return eq(+object, +other);

	    case errorTag$1:
	      return object.name == other.name && object.message == other.message;

	    case regexpTag$1:
	    case stringTag$1:
	      // Coerce regexes to strings and treat strings, primitives and objects,
	      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
	      // for more details.
	      return object == (other + '');

	    case mapTag$2:
	      var convert = mapToArray;

	    case setTag$2:
	      var isPartial = bitmask & COMPARE_PARTIAL_FLAG$1;
	      convert || (convert = setToArray);

	      if (object.size != other.size && !isPartial) {
	        return false;
	      }
	      // Assume cyclic values are equal.
	      var stacked = stack.get(object);
	      if (stacked) {
	        return stacked == other;
	      }
	      bitmask |= COMPARE_UNORDERED_FLAG$1;

	      // Recursively compare objects (susceptible to call stack limits).
	      stack.set(object, other);
	      var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
	      stack['delete'](object);
	      return result;

	    case symbolTag$1:
	      if (symbolValueOf) {
	        return symbolValueOf.call(object) == symbolValueOf.call(other);
	      }
	  }
	  return false;
	}

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$2 = 1;

	/** Used for built-in method references. */
	var objectProto$d = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$a = objectProto$d.hasOwnProperty;

	/**
	 * A specialized version of `baseIsEqualDeep` for objects with support for
	 * partial deep comparisons.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
	 * @param {Function} customizer The function to customize comparisons.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Object} stack Tracks traversed `object` and `other` objects.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
	  var isPartial = bitmask & COMPARE_PARTIAL_FLAG$2,
	      objProps = getAllKeys(object),
	      objLength = objProps.length,
	      othProps = getAllKeys(other),
	      othLength = othProps.length;

	  if (objLength != othLength && !isPartial) {
	    return false;
	  }
	  var index = objLength;
	  while (index--) {
	    var key = objProps[index];
	    if (!(isPartial ? key in other : hasOwnProperty$a.call(other, key))) {
	      return false;
	    }
	  }
	  // Assume cyclic values are equal.
	  var stacked = stack.get(object);
	  if (stacked && stack.get(other)) {
	    return stacked == other;
	  }
	  var result = true;
	  stack.set(object, other);
	  stack.set(other, object);

	  var skipCtor = isPartial;
	  while (++index < objLength) {
	    key = objProps[index];
	    var objValue = object[key],
	        othValue = other[key];

	    if (customizer) {
	      var compared = isPartial
	        ? customizer(othValue, objValue, key, other, object, stack)
	        : customizer(objValue, othValue, key, object, other, stack);
	    }
	    // Recursively compare objects (susceptible to call stack limits).
	    if (!(compared === undefined
	          ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
	          : compared
	        )) {
	      result = false;
	      break;
	    }
	    skipCtor || (skipCtor = key == 'constructor');
	  }
	  if (result && !skipCtor) {
	    var objCtor = object.constructor,
	        othCtor = other.constructor;

	    // Non `Object` object instances with different constructors are not equal.
	    if (objCtor != othCtor &&
	        ('constructor' in object && 'constructor' in other) &&
	        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
	          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
	      result = false;
	    }
	  }
	  stack['delete'](object);
	  stack['delete'](other);
	  return result;
	}

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$3 = 1;

	/** `Object#toString` result references. */
	var argsTag$2 = '[object Arguments]',
	    arrayTag$1 = '[object Array]',
	    objectTag$3 = '[object Object]';

	/** Used for built-in method references. */
	var objectProto$e = Object.prototype;

	/** Used to check objects for own properties. */
	var hasOwnProperty$b = objectProto$e.hasOwnProperty;

	/**
	 * A specialized version of `baseIsEqual` for arrays and objects which performs
	 * deep comparisons and tracks traversed objects enabling objects with circular
	 * references to be compared.
	 *
	 * @private
	 * @param {Object} object The object to compare.
	 * @param {Object} other The other object to compare.
	 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
	 * @param {Function} customizer The function to customize comparisons.
	 * @param {Function} equalFunc The function to determine equivalents of values.
	 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
	 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
	 */
	function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
	  var objIsArr = isArray(object),
	      othIsArr = isArray(other),
	      objTag = objIsArr ? arrayTag$1 : getTag$1(object),
	      othTag = othIsArr ? arrayTag$1 : getTag$1(other);

	  objTag = objTag == argsTag$2 ? objectTag$3 : objTag;
	  othTag = othTag == argsTag$2 ? objectTag$3 : othTag;

	  var objIsObj = objTag == objectTag$3,
	      othIsObj = othTag == objectTag$3,
	      isSameTag = objTag == othTag;

	  if (isSameTag && isBuffer(object)) {
	    if (!isBuffer(other)) {
	      return false;
	    }
	    objIsArr = true;
	    objIsObj = false;
	  }
	  if (isSameTag && !objIsObj) {
	    stack || (stack = new Stack);
	    return (objIsArr || isTypedArray(object))
	      ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
	      : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
	  }
	  if (!(bitmask & COMPARE_PARTIAL_FLAG$3)) {
	    var objIsWrapped = objIsObj && hasOwnProperty$b.call(object, '__wrapped__'),
	        othIsWrapped = othIsObj && hasOwnProperty$b.call(other, '__wrapped__');

	    if (objIsWrapped || othIsWrapped) {
	      var objUnwrapped = objIsWrapped ? object.value() : object,
	          othUnwrapped = othIsWrapped ? other.value() : other;

	      stack || (stack = new Stack);
	      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
	    }
	  }
	  if (!isSameTag) {
	    return false;
	  }
	  stack || (stack = new Stack);
	  return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
	}

	/**
	 * The base implementation of `_.isEqual` which supports partial comparisons
	 * and tracks traversed objects.
	 *
	 * @private
	 * @param {*} value The value to compare.
	 * @param {*} other The other value to compare.
	 * @param {boolean} bitmask The bitmask flags.
	 *  1 - Unordered comparison
	 *  2 - Partial comparison
	 * @param {Function} [customizer] The function to customize comparisons.
	 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
	 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
	 */
	function baseIsEqual(value, other, bitmask, customizer, stack) {
	  if (value === other) {
	    return true;
	  }
	  if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
	    return value !== value && other !== other;
	  }
	  return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
	}

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$4 = 1,
	    COMPARE_UNORDERED_FLAG$2 = 2;

	/**
	 * The base implementation of `_.isMatch` without support for iteratee shorthands.
	 *
	 * @private
	 * @param {Object} object The object to inspect.
	 * @param {Object} source The object of property values to match.
	 * @param {Array} matchData The property names, values, and compare flags to match.
	 * @param {Function} [customizer] The function to customize comparisons.
	 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
	 */
	function baseIsMatch(object, source, matchData, customizer) {
	  var index = matchData.length,
	      length = index,
	      noCustomizer = !customizer;

	  if (object == null) {
	    return !length;
	  }
	  object = Object(object);
	  while (index--) {
	    var data = matchData[index];
	    if ((noCustomizer && data[2])
	          ? data[1] !== object[data[0]]
	          : !(data[0] in object)
	        ) {
	      return false;
	    }
	  }
	  while (++index < length) {
	    data = matchData[index];
	    var key = data[0],
	        objValue = object[key],
	        srcValue = data[1];

	    if (noCustomizer && data[2]) {
	      if (objValue === undefined && !(key in object)) {
	        return false;
	      }
	    } else {
	      var stack = new Stack;
	      if (customizer) {
	        var result = customizer(objValue, srcValue, key, object, source, stack);
	      }
	      if (!(result === undefined
	            ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$4 | COMPARE_UNORDERED_FLAG$2, customizer, stack)
	            : result
	          )) {
	        return false;
	      }
	    }
	  }
	  return true;
	}

	/**
	 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
	 *
	 * @private
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` if suitable for strict
	 *  equality comparisons, else `false`.
	 */
	function isStrictComparable(value) {
	  return value === value && !isObject$2(value);
	}

	/**
	 * Gets the property names, values, and compare flags of `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @returns {Array} Returns the match data of `object`.
	 */
	function getMatchData(object) {
	  var result = keys(object),
	      length = result.length;

	  while (length--) {
	    var key = result[length],
	        value = object[key];

	    result[length] = [key, value, isStrictComparable(value)];
	  }
	  return result;
	}

	/**
	 * A specialized version of `matchesProperty` for source values suitable
	 * for strict equality comparisons, i.e. `===`.
	 *
	 * @private
	 * @param {string} key The key of the property to get.
	 * @param {*} srcValue The value to match.
	 * @returns {Function} Returns the new spec function.
	 */
	function matchesStrictComparable(key, srcValue) {
	  return function(object) {
	    if (object == null) {
	      return false;
	    }
	    return object[key] === srcValue &&
	      (srcValue !== undefined || (key in Object(object)));
	  };
	}

	/**
	 * The base implementation of `_.matches` which doesn't clone `source`.
	 *
	 * @private
	 * @param {Object} source The object of property values to match.
	 * @returns {Function} Returns the new spec function.
	 */
	function baseMatches(source) {
	  var matchData = getMatchData(source);
	  if (matchData.length == 1 && matchData[0][2]) {
	    return matchesStrictComparable(matchData[0][0], matchData[0][1]);
	  }
	  return function(object) {
	    return object === source || baseIsMatch(object, source, matchData);
	  };
	}

	/**
	 * The base implementation of `_.hasIn` without support for deep paths.
	 *
	 * @private
	 * @param {Object} [object] The object to query.
	 * @param {Array|string} key The key to check.
	 * @returns {boolean} Returns `true` if `key` exists, else `false`.
	 */
	function baseHasIn(object, key) {
	  return object != null && key in Object(object);
	}

	/**
	 * Checks if `path` exists on `object`.
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {Array|string} path The path to check.
	 * @param {Function} hasFunc The function to check properties.
	 * @returns {boolean} Returns `true` if `path` exists, else `false`.
	 */
	function hasPath(object, path, hasFunc) {
	  path = castPath(path, object);

	  var index = -1,
	      length = path.length,
	      result = false;

	  while (++index < length) {
	    var key = toKey(path[index]);
	    if (!(result = object != null && hasFunc(object, key))) {
	      break;
	    }
	    object = object[key];
	  }
	  if (result || ++index != length) {
	    return result;
	  }
	  length = object == null ? 0 : object.length;
	  return !!length && isLength(length) && isIndex(key, length) &&
	    (isArray(object) || isArguments(object));
	}

	/**
	 * Checks if `path` is a direct or inherited property of `object`.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Object
	 * @param {Object} object The object to query.
	 * @param {Array|string} path The path to check.
	 * @returns {boolean} Returns `true` if `path` exists, else `false`.
	 * @example
	 *
	 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
	 *
	 * _.hasIn(object, 'a');
	 * // => true
	 *
	 * _.hasIn(object, 'a.b');
	 * // => true
	 *
	 * _.hasIn(object, ['a', 'b']);
	 * // => true
	 *
	 * _.hasIn(object, 'b');
	 * // => false
	 */
	function hasIn(object, path) {
	  return object != null && hasPath(object, path, baseHasIn);
	}

	/** Used to compose bitmasks for value comparisons. */
	var COMPARE_PARTIAL_FLAG$5 = 1,
	    COMPARE_UNORDERED_FLAG$3 = 2;

	/**
	 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
	 *
	 * @private
	 * @param {string} path The path of the property to get.
	 * @param {*} srcValue The value to match.
	 * @returns {Function} Returns the new spec function.
	 */
	function baseMatchesProperty(path, srcValue) {
	  if (isKey(path) && isStrictComparable(srcValue)) {
	    return matchesStrictComparable(toKey(path), srcValue);
	  }
	  return function(object) {
	    var objValue = get(object, path);
	    return (objValue === undefined && objValue === srcValue)
	      ? hasIn(object, path)
	      : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG$5 | COMPARE_UNORDERED_FLAG$3);
	  };
	}

	/**
	 * The base implementation of `_.property` without support for deep paths.
	 *
	 * @private
	 * @param {string} key The key of the property to get.
	 * @returns {Function} Returns the new accessor function.
	 */
	function baseProperty(key) {
	  return function(object) {
	    return object == null ? undefined : object[key];
	  };
	}

	/**
	 * A specialized version of `baseProperty` which supports deep paths.
	 *
	 * @private
	 * @param {Array|string} path The path of the property to get.
	 * @returns {Function} Returns the new accessor function.
	 */
	function basePropertyDeep(path) {
	  return function(object) {
	    return baseGet(object, path);
	  };
	}

	/**
	 * Creates a function that returns the value at `path` of a given object.
	 *
	 * @static
	 * @memberOf _
	 * @since 2.4.0
	 * @category Util
	 * @param {Array|string} path The path of the property to get.
	 * @returns {Function} Returns the new accessor function.
	 * @example
	 *
	 * var objects = [
	 *   { 'a': { 'b': 2 } },
	 *   { 'a': { 'b': 1 } }
	 * ];
	 *
	 * _.map(objects, _.property('a.b'));
	 * // => [2, 1]
	 *
	 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
	 * // => [1, 2]
	 */
	function property(path) {
	  return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
	}

	/**
	 * The base implementation of `_.iteratee`.
	 *
	 * @private
	 * @param {*} [value=_.identity] The value to convert to an iteratee.
	 * @returns {Function} Returns the iteratee.
	 */
	function baseIteratee(value) {
	  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
	  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
	  if (typeof value == 'function') {
	    return value;
	  }
	  if (value == null) {
	    return identity;
	  }
	  if (typeof value == 'object') {
	    return isArray(value)
	      ? baseMatchesProperty(value[0], value[1])
	      : baseMatches(value);
	  }
	  return property(value);
	}

	/**
	 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
	 *
	 * @private
	 * @param {boolean} [fromRight] Specify iterating from right to left.
	 * @returns {Function} Returns the new base function.
	 */
	function createBaseFor(fromRight) {
	  return function(object, iteratee, keysFunc) {
	    var index = -1,
	        iterable = Object(object),
	        props = keysFunc(object),
	        length = props.length;

	    while (length--) {
	      var key = props[fromRight ? length : ++index];
	      if (iteratee(iterable[key], key, iterable) === false) {
	        break;
	      }
	    }
	    return object;
	  };
	}

	/**
	 * The base implementation of `baseForOwn` which iterates over `object`
	 * properties returned by `keysFunc` and invokes `iteratee` for each property.
	 * Iteratee functions may exit iteration early by explicitly returning `false`.
	 *
	 * @private
	 * @param {Object} object The object to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @param {Function} keysFunc The function to get the keys of `object`.
	 * @returns {Object} Returns `object`.
	 */
	var baseFor = createBaseFor();

	/**
	 * The base implementation of `_.forOwn` without support for iteratee shorthands.
	 *
	 * @private
	 * @param {Object} object The object to iterate over.
	 * @param {Function} iteratee The function invoked per iteration.
	 * @returns {Object} Returns `object`.
	 */
	function baseForOwn(object, iteratee) {
	  return object && baseFor(object, iteratee, keys);
	}

	/**
	 * This function is like `assignValue` except that it doesn't assign
	 * `undefined` values.
	 *
	 * @private
	 * @param {Object} object The object to modify.
	 * @param {string} key The key of the property to assign.
	 * @param {*} value The value to assign.
	 */
	function assignMergeValue(object, key, value) {
	  if ((value !== undefined && !eq(object[key], value)) ||
	      (value === undefined && !(key in object))) {
	    baseAssignValue(object, key, value);
	  }
	}

	/**
	 * This method is like `_.isArrayLike` except that it also checks if `value`
	 * is an object.
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is an array-like object,
	 *  else `false`.
	 * @example
	 *
	 * _.isArrayLikeObject([1, 2, 3]);
	 * // => true
	 *
	 * _.isArrayLikeObject(document.body.children);
	 * // => true
	 *
	 * _.isArrayLikeObject('abc');
	 * // => false
	 *
	 * _.isArrayLikeObject(_.noop);
	 * // => false
	 */
	function isArrayLikeObject(value) {
	  return isObjectLike(value) && isArrayLike(value);
	}

	/**
	 * Gets the value at `key`, unless `key` is "__proto__".
	 *
	 * @private
	 * @param {Object} object The object to query.
	 * @param {string} key The key of the property to get.
	 * @returns {*} Returns the property value.
	 */
	function safeGet(object, key) {
	  if (key == '__proto__') {
	    return;
	  }

	  return object[key];
	}

	/**
	 * Converts `value` to a plain object flattening inherited enumerable string
	 * keyed properties of `value` to own properties of the plain object.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.0.0
	 * @category Lang
	 * @param {*} value The value to convert.
	 * @returns {Object} Returns the converted plain object.
	 * @example
	 *
	 * function Foo() {
	 *   this.b = 2;
	 * }
	 *
	 * Foo.prototype.c = 3;
	 *
	 * _.assign({ 'a': 1 }, new Foo);
	 * // => { 'a': 1, 'b': 2 }
	 *
	 * _.assign({ 'a': 1 }, _.toPlainObject(new Foo));
	 * // => { 'a': 1, 'b': 2, 'c': 3 }
	 */
	function toPlainObject(value) {
	  return copyObject(value, keysIn(value));
	}

	/**
	 * A specialized version of `baseMerge` for arrays and objects which performs
	 * deep merges and tracks traversed objects enabling objects with circular
	 * references to be merged.
	 *
	 * @private
	 * @param {Object} object The destination object.
	 * @param {Object} source The source object.
	 * @param {string} key The key of the value to merge.
	 * @param {number} srcIndex The index of `source`.
	 * @param {Function} mergeFunc The function to merge values.
	 * @param {Function} [customizer] The function to customize assigned values.
	 * @param {Object} [stack] Tracks traversed source values and their merged
	 *  counterparts.
	 */
	function baseMergeDeep(object, source, key, srcIndex, mergeFunc, customizer, stack) {
	  var objValue = safeGet(object, key),
	      srcValue = safeGet(source, key),
	      stacked = stack.get(srcValue);

	  if (stacked) {
	    assignMergeValue(object, key, stacked);
	    return;
	  }
	  var newValue = customizer
	    ? customizer(objValue, srcValue, (key + ''), object, source, stack)
	    : undefined;

	  var isCommon = newValue === undefined;

	  if (isCommon) {
	    var isArr = isArray(srcValue),
	        isBuff = !isArr && isBuffer(srcValue),
	        isTyped = !isArr && !isBuff && isTypedArray(srcValue);

	    newValue = srcValue;
	    if (isArr || isBuff || isTyped) {
	      if (isArray(objValue)) {
	        newValue = objValue;
	      }
	      else if (isArrayLikeObject(objValue)) {
	        newValue = copyArray(objValue);
	      }
	      else if (isBuff) {
	        isCommon = false;
	        newValue = cloneBuffer(srcValue, true);
	      }
	      else if (isTyped) {
	        isCommon = false;
	        newValue = cloneTypedArray(srcValue, true);
	      }
	      else {
	        newValue = [];
	      }
	    }
	    else if (isPlainObject(srcValue) || isArguments(srcValue)) {
	      newValue = objValue;
	      if (isArguments(objValue)) {
	        newValue = toPlainObject(objValue);
	      }
	      else if (!isObject$2(objValue) || isFunction$2(objValue)) {
	        newValue = initCloneObject(srcValue);
	      }
	    }
	    else {
	      isCommon = false;
	    }
	  }
	  if (isCommon) {
	    // Recursively merge objects and arrays (susceptible to call stack limits).
	    stack.set(srcValue, newValue);
	    mergeFunc(newValue, srcValue, srcIndex, customizer, stack);
	    stack['delete'](srcValue);
	  }
	  assignMergeValue(object, key, newValue);
	}

	/**
	 * The base implementation of `_.merge` without support for multiple sources.
	 *
	 * @private
	 * @param {Object} object The destination object.
	 * @param {Object} source The source object.
	 * @param {number} srcIndex The index of `source`.
	 * @param {Function} [customizer] The function to customize merged values.
	 * @param {Object} [stack] Tracks traversed source values and their merged
	 *  counterparts.
	 */
	function baseMerge(object, source, srcIndex, customizer, stack) {
	  if (object === source) {
	    return;
	  }
	  baseFor(source, function(srcValue, key) {
	    if (isObject$2(srcValue)) {
	      stack || (stack = new Stack);
	      baseMergeDeep(object, source, key, srcIndex, baseMerge, customizer, stack);
	    }
	    else {
	      var newValue = customizer
	        ? customizer(safeGet(object, key), srcValue, (key + ''), object, source, stack)
	        : undefined;

	      if (newValue === undefined) {
	        newValue = srcValue;
	      }
	      assignMergeValue(object, key, newValue);
	    }
	  }, keysIn);
	}

	/**
	 * The base implementation of methods like `_.findKey` and `_.findLastKey`,
	 * without support for iteratee shorthands, which iterates over `collection`
	 * using `eachFunc`.
	 *
	 * @private
	 * @param {Array|Object} collection The collection to inspect.
	 * @param {Function} predicate The function invoked per iteration.
	 * @param {Function} eachFunc The function to iterate over `collection`.
	 * @returns {*} Returns the found element or its key, else `undefined`.
	 */
	function baseFindKey(collection, predicate, eachFunc) {
	  var result;
	  eachFunc(collection, function(value, key, collection) {
	    if (predicate(value, key, collection)) {
	      result = key;
	      return false;
	    }
	  });
	  return result;
	}

	/**
	 * This method is like `_.find` except that it returns the key of the first
	 * element `predicate` returns truthy for instead of the element itself.
	 *
	 * @static
	 * @memberOf _
	 * @since 1.1.0
	 * @category Object
	 * @param {Object} object The object to inspect.
	 * @param {Function} [predicate=_.identity] The function invoked per iteration.
	 * @returns {string|undefined} Returns the key of the matched element,
	 *  else `undefined`.
	 * @example
	 *
	 * var users = {
	 *   'barney':  { 'age': 36, 'active': true },
	 *   'fred':    { 'age': 40, 'active': false },
	 *   'pebbles': { 'age': 1,  'active': true }
	 * };
	 *
	 * _.findKey(users, function(o) { return o.age < 40; });
	 * // => 'barney' (iteration order is not guaranteed)
	 *
	 * // The `_.matches` iteratee shorthand.
	 * _.findKey(users, { 'age': 1, 'active': true });
	 * // => 'pebbles'
	 *
	 * // The `_.matchesProperty` iteratee shorthand.
	 * _.findKey(users, ['active', false]);
	 * // => 'fred'
	 *
	 * // The `_.property` iteratee shorthand.
	 * _.findKey(users, 'active');
	 * // => 'barney'
	 */
	function findKey(object, predicate) {
	  return baseFindKey(object, baseIteratee(predicate), baseForOwn);
	}

	/** `Object#toString` result references. */
	var stringTag$2 = '[object String]';

	/**
	 * Checks if `value` is classified as a `String` primitive or object.
	 *
	 * @static
	 * @since 0.1.0
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a string, else `false`.
	 * @example
	 *
	 * _.isString('abc');
	 * // => true
	 *
	 * _.isString(1);
	 * // => false
	 */
	function isString(value) {
	  return typeof value == 'string' ||
	    (!isArray(value) && isObjectLike(value) && baseGetTag(value) == stringTag$2);
	}

	/** `Object#toString` result references. */
	var boolTag$2 = '[object Boolean]';

	/**
	 * Checks if `value` is classified as a boolean primitive or object.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a boolean, else `false`.
	 * @example
	 *
	 * _.isBoolean(false);
	 * // => true
	 *
	 * _.isBoolean(null);
	 * // => false
	 */
	function isBoolean(value) {
	  return value === true || value === false ||
	    (isObjectLike(value) && baseGetTag(value) == boolTag$2);
	}

	/**
	 * Performs a deep comparison between two values to determine if they are
	 * equivalent.
	 *
	 * **Note:** This method supports comparing arrays, array buffers, booleans,
	 * date objects, error objects, maps, numbers, `Object` objects, regexes,
	 * sets, strings, symbols, and typed arrays. `Object` objects are compared
	 * by their own, not inherited, enumerable properties. Functions and DOM
	 * nodes are compared by strict equality, i.e. `===`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to compare.
	 * @param {*} other The other value to compare.
	 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
	 * @example
	 *
	 * var object = { 'a': 1 };
	 * var other = { 'a': 1 };
	 *
	 * _.isEqual(object, other);
	 * // => true
	 *
	 * object === other;
	 * // => false
	 */
	function isEqual(value, other) {
	  return baseIsEqual(value, other);
	}

	/** `Object#toString` result references. */
	var numberTag$2 = '[object Number]';

	/**
	 * Checks if `value` is classified as a `Number` primitive or object.
	 *
	 * **Note:** To exclude `Infinity`, `-Infinity`, and `NaN`, which are
	 * classified as numbers, use the `_.isFinite` method.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is a number, else `false`.
	 * @example
	 *
	 * _.isNumber(3);
	 * // => true
	 *
	 * _.isNumber(Number.MIN_VALUE);
	 * // => true
	 *
	 * _.isNumber(Infinity);
	 * // => true
	 *
	 * _.isNumber('3');
	 * // => false
	 */
	function isNumber(value) {
	  return typeof value == 'number' ||
	    (isObjectLike(value) && baseGetTag(value) == numberTag$2);
	}

	/**
	 * Checks if `value` is `NaN`.
	 *
	 * **Note:** This method is based on
	 * [`Number.isNaN`](https://mdn.io/Number/isNaN) and is not the same as
	 * global [`isNaN`](https://mdn.io/isNaN) which returns `true` for
	 * `undefined` and other non-number values.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is `NaN`, else `false`.
	 * @example
	 *
	 * _.isNaN(NaN);
	 * // => true
	 *
	 * _.isNaN(new Number(NaN));
	 * // => true
	 *
	 * isNaN(undefined);
	 * // => true
	 *
	 * _.isNaN(undefined);
	 * // => false
	 */
	function isNaN$1(value) {
	  // An `NaN` primitive is the only value that is not equal to itself.
	  // Perform the `toStringTag` check first to avoid errors with some
	  // ActiveX objects in IE.
	  return isNumber(value) && value != +value;
	}

	/**
	 * Checks if `value` is `null`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.1.0
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is `null`, else `false`.
	 * @example
	 *
	 * _.isNull(null);
	 * // => true
	 *
	 * _.isNull(void 0);
	 * // => false
	 */
	function isNull(value) {
	  return value === null;
	}

	/**
	 * Checks if `value` is `undefined`.
	 *
	 * @static
	 * @since 0.1.0
	 * @memberOf _
	 * @category Lang
	 * @param {*} value The value to check.
	 * @returns {boolean} Returns `true` if `value` is `undefined`, else `false`.
	 * @example
	 *
	 * _.isUndefined(void 0);
	 * // => true
	 *
	 * _.isUndefined(null);
	 * // => false
	 */
	function isUndefined(value) {
	  return value === undefined;
	}

	/**
	 * The opposite of `_.mapValues`; this method creates an object with the
	 * same values as `object` and keys generated by running each own enumerable
	 * string keyed property of `object` thru `iteratee`. The iteratee is invoked
	 * with three arguments: (value, key, object).
	 *
	 * @static
	 * @memberOf _
	 * @since 3.8.0
	 * @category Object
	 * @param {Object} object The object to iterate over.
	 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
	 * @returns {Object} Returns the new mapped object.
	 * @see _.mapValues
	 * @example
	 *
	 * _.mapKeys({ 'a': 1, 'b': 2 }, function(value, key) {
	 *   return key + value;
	 * });
	 * // => { 'a1': 1, 'b2': 2 }
	 */
	function mapKeys(object, iteratee) {
	  var result = {};
	  iteratee = baseIteratee(iteratee);

	  baseForOwn(object, function(value, key, object) {
	    baseAssignValue(result, iteratee(value, key, object), value);
	  });
	  return result;
	}

	/**
	 * Creates an object with the same keys as `object` and values generated
	 * by running each own enumerable string keyed property of `object` thru
	 * `iteratee`. The iteratee is invoked with three arguments:
	 * (value, key, object).
	 *
	 * @static
	 * @memberOf _
	 * @since 2.4.0
	 * @category Object
	 * @param {Object} object The object to iterate over.
	 * @param {Function} [iteratee=_.identity] The function invoked per iteration.
	 * @returns {Object} Returns the new mapped object.
	 * @see _.mapKeys
	 * @example
	 *
	 * var users = {
	 *   'fred':    { 'user': 'fred',    'age': 40 },
	 *   'pebbles': { 'user': 'pebbles', 'age': 1 }
	 * };
	 *
	 * _.mapValues(users, function(o) { return o.age; });
	 * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
	 *
	 * // The `_.property` iteratee shorthand.
	 * _.mapValues(users, 'age');
	 * // => { 'fred': 40, 'pebbles': 1 } (iteration order is not guaranteed)
	 */
	function mapValues(object, iteratee) {
	  var result = {};
	  iteratee = baseIteratee(iteratee);

	  baseForOwn(object, function(value, key, object) {
	    baseAssignValue(result, key, iteratee(value, key, object));
	  });
	  return result;
	}

	/**
	 * This method is like `_.assign` except that it recursively merges own and
	 * inherited enumerable string keyed properties of source objects into the
	 * destination object. Source properties that resolve to `undefined` are
	 * skipped if a destination value exists. Array and plain object properties
	 * are merged recursively. Other objects and value types are overridden by
	 * assignment. Source objects are applied from left to right. Subsequent
	 * sources overwrite property assignments of previous sources.
	 *
	 * **Note:** This method mutates `object`.
	 *
	 * @static
	 * @memberOf _
	 * @since 0.5.0
	 * @category Object
	 * @param {Object} object The destination object.
	 * @param {...Object} [sources] The source objects.
	 * @returns {Object} Returns `object`.
	 * @example
	 *
	 * var object = {
	 *   'a': [{ 'b': 2 }, { 'd': 4 }]
	 * };
	 *
	 * var other = {
	 *   'a': [{ 'c': 3 }, { 'e': 5 }]
	 * };
	 *
	 * _.merge(object, other);
	 * // => { 'a': [{ 'b': 2, 'c': 3 }, { 'd': 4, 'e': 5 }] }
	 */
	var merge = createAssigner(function(object, source, srcIndex) {
	  baseMerge(object, source, srcIndex);
	});

	/** Error message constants. */
	var FUNC_ERROR_TEXT$1 = 'Expected a function';

	/**
	 * Creates a function that negates the result of the predicate `func`. The
	 * `func` predicate is invoked with the `this` binding and arguments of the
	 * created function.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.0.0
	 * @category Function
	 * @param {Function} predicate The predicate to negate.
	 * @returns {Function} Returns the new negated function.
	 * @example
	 *
	 * function isEven(n) {
	 *   return n % 2 == 0;
	 * }
	 *
	 * _.filter([1, 2, 3, 4, 5, 6], _.negate(isEven));
	 * // => [1, 3, 5]
	 */
	function negate(predicate) {
	  if (typeof predicate != 'function') {
	    throw new TypeError(FUNC_ERROR_TEXT$1);
	  }
	  return function() {
	    var args = arguments;
	    switch (args.length) {
	      case 0: return !predicate.call(this);
	      case 1: return !predicate.call(this, args[0]);
	      case 2: return !predicate.call(this, args[0], args[1]);
	      case 3: return !predicate.call(this, args[0], args[1], args[2]);
	    }
	    return !predicate.apply(this, args);
	  };
	}

	/**
	 * The base implementation of `_.set`.
	 *
	 * @private
	 * @param {Object} object The object to modify.
	 * @param {Array|string} path The path of the property to set.
	 * @param {*} value The value to set.
	 * @param {Function} [customizer] The function to customize path creation.
	 * @returns {Object} Returns `object`.
	 */
	function baseSet(object, path, value, customizer) {
	  if (!isObject$2(object)) {
	    return object;
	  }
	  path = castPath(path, object);

	  var index = -1,
	      length = path.length,
	      lastIndex = length - 1,
	      nested = object;

	  while (nested != null && ++index < length) {
	    var key = toKey(path[index]),
	        newValue = value;

	    if (index != lastIndex) {
	      var objValue = nested[key];
	      newValue = customizer ? customizer(objValue, key, nested) : undefined;
	      if (newValue === undefined) {
	        newValue = isObject$2(objValue)
	          ? objValue
	          : (isIndex(path[index + 1]) ? [] : {});
	      }
	    }
	    assignValue(nested, key, newValue);
	    nested = nested[key];
	  }
	  return object;
	}

	/**
	 * The base implementation of  `_.pickBy` without support for iteratee shorthands.
	 *
	 * @private
	 * @param {Object} object The source object.
	 * @param {string[]} paths The property paths to pick.
	 * @param {Function} predicate The function invoked per property.
	 * @returns {Object} Returns the new object.
	 */
	function basePickBy(object, paths, predicate) {
	  var index = -1,
	      length = paths.length,
	      result = {};

	  while (++index < length) {
	    var path = paths[index],
	        value = baseGet(object, path);

	    if (predicate(value, path)) {
	      baseSet(result, castPath(path, object), value);
	    }
	  }
	  return result;
	}

	/**
	 * Creates an object composed of the `object` properties `predicate` returns
	 * truthy for. The predicate is invoked with two arguments: (value, key).
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Object
	 * @param {Object} object The source object.
	 * @param {Function} [predicate=_.identity] The function invoked per property.
	 * @returns {Object} Returns the new object.
	 * @example
	 *
	 * var object = { 'a': 1, 'b': '2', 'c': 3 };
	 *
	 * _.pickBy(object, _.isNumber);
	 * // => { 'a': 1, 'c': 3 }
	 */
	function pickBy(object, predicate) {
	  if (object == null) {
	    return {};
	  }
	  var props = arrayMap(getAllKeysIn(object), function(prop) {
	    return [prop];
	  });
	  predicate = baseIteratee(predicate);
	  return basePickBy(object, props, function(value, path) {
	    return predicate(value, path[0]);
	  });
	}

	/**
	 * The opposite of `_.pickBy`; this method creates an object composed of
	 * the own and inherited enumerable string keyed properties of `object` that
	 * `predicate` doesn't return truthy for. The predicate is invoked with two
	 * arguments: (value, key).
	 *
	 * @static
	 * @memberOf _
	 * @since 4.0.0
	 * @category Object
	 * @param {Object} object The source object.
	 * @param {Function} [predicate=_.identity] The function invoked per property.
	 * @returns {Object} Returns the new object.
	 * @example
	 *
	 * var object = { 'a': 1, 'b': '2', 'c': 3 };
	 *
	 * _.omitBy(object, _.isNumber);
	 * // => { 'b': '2' }
	 */
	function omitBy(object, predicate) {
	  return pickBy(object, negate(baseIteratee(predicate)));
	}

	/**
	 * Used by `_.trim` and `_.trimEnd` to get the index of the last string symbol
	 * that is not found in the character symbols.
	 *
	 * @private
	 * @param {Array} strSymbols The string symbols to inspect.
	 * @param {Array} chrSymbols The character symbols to find.
	 * @returns {number} Returns the index of the last unmatched string symbol.
	 */
	function charsEndIndex(strSymbols, chrSymbols) {
	  var index = strSymbols.length;

	  while (index-- && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
	  return index;
	}

	/**
	 * Used by `_.trim` and `_.trimStart` to get the index of the first string symbol
	 * that is not found in the character symbols.
	 *
	 * @private
	 * @param {Array} strSymbols The string symbols to inspect.
	 * @param {Array} chrSymbols The character symbols to find.
	 * @returns {number} Returns the index of the first unmatched string symbol.
	 */
	function charsStartIndex(strSymbols, chrSymbols) {
	  var index = -1,
	      length = strSymbols.length;

	  while (++index < length && baseIndexOf(chrSymbols, strSymbols[index], 0) > -1) {}
	  return index;
	}

	/** Used to match leading and trailing whitespace. */
	var reTrim = /^\s+|\s+$/g;

	/**
	 * Removes leading and trailing whitespace or specified characters from `string`.
	 *
	 * @static
	 * @memberOf _
	 * @since 3.0.0
	 * @category String
	 * @param {string} [string=''] The string to trim.
	 * @param {string} [chars=whitespace] The characters to trim.
	 * @param- {Object} [guard] Enables use as an iteratee for methods like `_.map`.
	 * @returns {string} Returns the trimmed string.
	 * @example
	 *
	 * _.trim('  abc  ');
	 * // => 'abc'
	 *
	 * _.trim('-_-abc-_-', '_-');
	 * // => 'abc'
	 *
	 * _.map(['  foo  ', '  bar  '], _.trim);
	 * // => ['foo', 'bar']
	 */
	function trim$2(string, chars, guard) {
	  string = toString(string);
	  if (string && (guard || chars === undefined)) {
	    return string.replace(reTrim, '');
	  }
	  if (!string || !(chars = baseToString(chars))) {
	    return string;
	  }
	  var strSymbols = stringToArray(string),
	      chrSymbols = stringToArray(chars),
	      start = charsStartIndex(strSymbols, chrSymbols),
	      end = charsEndIndex(strSymbols, chrSymbols) + 1;

	  return castSlice(strSymbols, start, end).join('');
	}

	/**
	 * Check several parameter that there is something in the param
	 * @param {*} param input
	 * @return {boolean}
	 */

	function notEmpty (a) {
	  if (isArray(a)) {
	    return true;
	  }
	  return a !== undefined && a !== null && trim$2(a) !== '';
	}

	// validator numbers
	/**
	 * @2015-05-04 found a problem if the value is a number like string
	 * it will pass, so add a check if it's string before we pass to next
	 * @param {number} value expected value
	 * @return {boolean} true if OK
	 */
	var checkIsNumber = function(value) {
	  return isString(value) ? false : !isNaN$1( parseFloat(value) )
	};

	// validate string type
	/**
	 * @param {string} value expected value
	 * @return {boolean} true if OK
	 */
	var checkIsString = function(value) {
	  return (trim$2(value) !== '') ? isString(value) : false;
	};

	// check for boolean
	/**
	 * @param {boolean} value expected
	 * @return {boolean} true if OK
	 */
	var checkIsBoolean = function(value) {
	  return isBoolean(value);
	};

	// validate any thing only check if there is something
	/**
	 * @param {*} value the value
	 * @param {boolean} [checkNull=true] strict check if there is null value
	 * @return {boolean} true is OK
	 */
	var checkIsAny = function(value, checkNull) {
	  if ( checkNull === void 0 ) checkNull = true;

	  if (!isUndefined(value) && value !== '' && trim$2(value) !== '') {
	    if (checkNull === false || (checkNull === true && !isNull(value))) {
	      return true;
	    }
	  }
	  return false;
	};

	// the core stuff to id if it's calling with jsonql
	var DATA_KEY = 'data';
	var ERROR_KEY = 'error';

	var JSONQL_PATH = 'jsonql';
	// according to the json query spec
	var CONTENT_TYPE = 'application/vnd.api+json';
	var CHARSET = 'charset=utf-8';
	var DEFAULT_HEADER = {
	  'Accept': CONTENT_TYPE,
	  'Content-Type': [ CONTENT_TYPE, CHARSET ].join(';')
	};
	var DEFAULT_TYPE = 'any';

	// @TODO remove this is not in use
	// export const CLIENT_CONFIG_FILE = '.clients.json';
	// export const CONTRACT_CONFIG_FILE = 'jsonql-contract-config.js';
	// type of resolvers
	var QUERY_NAME = 'query';
	var MUTATION_NAME = 'mutation';
	// for calling the mutation
	var PAYLOAD_PARAM_NAME = 'payload';
	var CONDITION_PARAM_NAME = 'condition';
	var QUERY_ARG_NAME = 'args';
	// new jsonp
	var JSONP_CALLBACK_NAME = 'jsonqlJsonpCallback';

	// methods allow
	var API_REQUEST_METHODS = ['POST', 'PUT'];
	// for  contract-cli
	var KEY_WORD = 'continue';

	var TYPE_KEY = 'type';
	var OPTIONAL_KEY = 'optional';
	var ENUM_KEY = 'enumv';  // need to change this because enum is a reserved word
	var ARGS_KEY = 'args';
	var CHECKER_KEY = 'checker';
	var ALIAS_KEY = 'alias';
	var LOGIN_NAME = 'login';
	var ISSUER_NAME = LOGIN_NAME; // legacy issue need to replace them later
	var LOGOUT_NAME = 'logout';

	var AUTH_HEADER = 'Authorization';
	var BEARER = 'Bearer';

	// for client use @TODO need to clean this up some of them are not in use
	var CREDENTIAL_STORAGE_KEY = 'credential';
	var CLIENT_STORAGE_KEY = 'storageKey';
	var CLIENT_AUTH_KEY = 'authKey';
	// contract key
	var CONTRACT_KEY_NAME = 'X-JSONQL-CV-KEY';

	var OR_SEPERATOR = '|';

	var STRING_TYPE = 'string';
	var BOOLEAN_TYPE = 'boolean';
	var ARRAY_TYPE = 'array';
	var OBJECT_TYPE = 'object';

	var NUMBER_TYPE = 'number';
	var ARRAY_TYPE_LFT = 'array.<';
	var ARRAY_TYPE_RGT = '>';

	var NO_ERROR_MSG = 'No message';
	var NO_STATUS_CODE = -1;
	var HSA_ALGO = 'HS256';

	// Good practice rule - No magic number

	var ARGS_NOT_ARRAY_ERR = "args is not an array! You might want to do: ES6 Array.from(arguments) or ES5 Array.prototype.slice.call(arguments)";
	var PARAMS_NOT_ARRAY_ERR = "params is not an array! Did something gone wrong when you generate the contract.json?";
	var EXCEPTION_CASE_ERR = 'Could not understand your arguments and parameter structure!';
	// @TODO the jsdoc return array.<type> and we should also allow array<type> syntax
	var DEFAULT_TYPE$1 = DEFAULT_TYPE;
	var ARRAY_TYPE_LFT$1 = ARRAY_TYPE_LFT;
	var ARRAY_TYPE_RGT$1 = ARRAY_TYPE_RGT;

	var TYPE_KEY$1 = TYPE_KEY;
	var OPTIONAL_KEY$1 = OPTIONAL_KEY;
	var ENUM_KEY$1 = ENUM_KEY;
	var ARGS_KEY$1 = ARGS_KEY;
	var CHECKER_KEY$1 = CHECKER_KEY;
	var ALIAS_KEY$1 = ALIAS_KEY;

	var ARRAY_TYPE$1 = ARRAY_TYPE;
	var OBJECT_TYPE$1 = OBJECT_TYPE;
	var STRING_TYPE$1 = STRING_TYPE;
	var BOOLEAN_TYPE$1 = BOOLEAN_TYPE;
	var NUMBER_TYPE$1 = NUMBER_TYPE;
	var KEY_WORD$1 = KEY_WORD;
	var OR_SEPERATOR$1 = OR_SEPERATOR;

	// not actually in use
	// export const NUMBER_TYPES = JSONQL_CONSTANTS.NUMBER_TYPES;

	// primitive types

	/**
	 * this is a wrapper method to call different one based on their type
	 * @param {string} type to check
	 * @return {function} a function to handle the type
	 */
	var combineFn = function(type) {
	  switch (type) {
	    case NUMBER_TYPE$1:
	      return checkIsNumber;
	    case STRING_TYPE$1:
	      return checkIsString;
	    case BOOLEAN_TYPE$1:
	      return checkIsBoolean;
	    default:
	      return checkIsAny;
	  }
	};

	// validate array type

	/**
	 * @param {array} value expected
	 * @param {string} [type=''] pass the type if we encounter array.<T> then we need to check the value as well
	 * @return {boolean} true if OK
	 */
	var checkIsArray = function(value, type) {
	  if ( type === void 0 ) type='';

	  if (isArray(value)) {
	    if (type === '' || trim$2(type)==='') {
	      return true;
	    }
	    // we test it in reverse
	    // @TODO if the type is an array (OR) then what?
	    // we need to take into account this could be an array
	    var c = value.filter(function (v) { return !combineFn(type)(v); });
	    return !(c.length > 0)
	  }
	  return false;
	};

	/**
	 * check if it matches the array.<T> pattern
	 * @param {string} type
	 * @return {boolean|array} false means NO, always return array
	 */
	var isArrayLike$1 = function(type) {
	  // @TODO could that have something like array<> instead of array.<>? missing the dot?
	  // because type script is Array<T> without the dot
	  if (type.indexOf(ARRAY_TYPE_LFT$1) > -1 && type.indexOf(ARRAY_TYPE_RGT$1) > -1) {
	    var _type = type.replace(ARRAY_TYPE_LFT$1, '').replace(ARRAY_TYPE_RGT$1, '');
	    if (_type.indexOf(OR_SEPERATOR$1)) {
	      return _type.split(OR_SEPERATOR$1)
	    }
	    return [_type]
	  }
	  return false;
	};

	/**
	 * we might encounter something like array.<T> then we need to take it apart
	 * @param {object} p the prepared object for processing
	 * @param {string|array} type the type came from <T>
	 * @return {boolean} for the filter to operate on
	 */
	var arrayTypeHandler = function(p, type) {
	  var arg = p.arg;
	  // need a special case to handle the OR type
	  // we need to test the args instead of the type(s)
	  if (type.length > 1) {
	    return !arg.filter(function (v) { return (
	      !(type.length > type.filter(function (t) { return !combineFn(t)(v); }).length)
	    ); }).length;
	  }
	  // type is array so this will be or!
	  return type.length > type.filter(function (t) { return !checkIsArray(arg, t); }).length;
	};

	// validate object type
	/**
	 * @TODO if provide with the keys then we need to check if the key:value type as well
	 * @param {object} value expected
	 * @param {array} [keys=null] if it has the keys array to compare as well
	 * @return {boolean} true if OK
	 */
	var checkIsObject = function(value, keys) {
	  if ( keys === void 0 ) keys=null;

	  if (isPlainObject(value)) {
	    if (!keys) {
	      return true;
	    }
	    if (checkIsArray(keys)) {
	      // please note we DON'T care if some is optional
	      // plese refer to the contract.json for the keys
	      return !keys.filter(function (key) {
	        var _value = value[key.name];
	        return !(key.type.length > key.type.filter(function (type) {
	          var tmp;
	          if (!isUndefined(_value)) {
	            if ((tmp = isArrayLike$1(type)) !== false) {
	              return !arrayTypeHandler({arg: _value}, tmp)
	              // return tmp.filter(t => !checkIsArray(_value, t)).length;
	              // @TODO there might be an object within an object with keys as well :S
	            }
	            return !combineFn(type)(_value)
	          }
	          return true;
	        }).length)
	      }).length;
	    }
	  }
	  return false;
	};

	/**
	 * fold this into it's own function to handler different object type
	 * @param {object} p the prepared object for process
	 * @return {boolean}
	 */
	var objectTypeHandler = function(p) {
	  var arg = p.arg;
	  var param = p.param;
	  var _args = [arg];
	  if (Array.isArray(param.keys) && param.keys.length) {
	    _args.push(param.keys);
	  }
	  // just simple check
	  return checkIsObject.apply(null, _args)
	};

	/**
	 * This is a custom error to throw when server throw a 406
	 * This help us to capture the right error, due to the call happens in sequence
	 * @param {string} message to tell what happen
	 * @param {mixed} extra things we want to add, 500?
	 */
	var Jsonql406Error = /*@__PURE__*/(function (Error) {
	  function Jsonql406Error() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);
	    this.message = args[0];
	    this.detail = args[1];
	    // We can't access the static name from an instance
	    // but we can do it like this
	    this.className = Jsonql406Error.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, Jsonql406Error);
	    }
	  }

	  if ( Error ) Jsonql406Error.__proto__ = Error;
	  Jsonql406Error.prototype = Object.create( Error && Error.prototype );
	  Jsonql406Error.prototype.constructor = Jsonql406Error;

	  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

	  staticAccessors.statusCode.get = function () {
	    return 406;
	  };

	  staticAccessors.name.get = function () {
	    return 'Jsonql406Error';
	  };

	  Object.defineProperties( Jsonql406Error, staticAccessors );

	  return Jsonql406Error;
	}(Error));

	/**
	 * This is a custom error to throw when server throw a 500
	 * This help us to capture the right error, due to the call happens in sequence
	 * @param {string} message to tell what happen
	 * @param {mixed} extra things we want to add, 500?
	 */
	var Jsonql500Error = /*@__PURE__*/(function (Error) {
	  function Jsonql500Error() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);

	    this.message = args[0];
	    this.detail = args[1];

	    this.className = Jsonql500Error.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, Jsonql500Error);
	    }
	  }

	  if ( Error ) Jsonql500Error.__proto__ = Error;
	  Jsonql500Error.prototype = Object.create( Error && Error.prototype );
	  Jsonql500Error.prototype.constructor = Jsonql500Error;

	  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

	  staticAccessors.statusCode.get = function () {
	    return 500;
	  };

	  staticAccessors.name.get = function () {
	    return 'Jsonql500Error';
	  };

	  Object.defineProperties( Jsonql500Error, staticAccessors );

	  return Jsonql500Error;
	}(Error));

	/**
	 * This is a custom error to throw when pass credential but fail
	 * This help us to capture the right error, due to the call happens in sequence
	 * @param {string} message to tell what happen
	 * @param {mixed} extra things we want to add, 500?
	 */
	var JsonqlAuthorisationError = /*@__PURE__*/(function (Error) {
	  function JsonqlAuthorisationError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);
	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlAuthorisationError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlAuthorisationError);
	    }
	  }

	  if ( Error ) JsonqlAuthorisationError.__proto__ = Error;
	  JsonqlAuthorisationError.prototype = Object.create( Error && Error.prototype );
	  JsonqlAuthorisationError.prototype.constructor = JsonqlAuthorisationError;

	  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

	  staticAccessors.statusCode.get = function () {
	    return 401;
	  };

	  staticAccessors.name.get = function () {
	    return 'JsonqlAuthorisationError';
	  };

	  Object.defineProperties( JsonqlAuthorisationError, staticAccessors );

	  return JsonqlAuthorisationError;
	}(Error));

	/**
	 * This is a custom error when not supply the credential and try to get contract
	 * This help us to capture the right error, due to the call happens in sequence
	 * @param {string} message to tell what happen
	 * @param {mixed} extra things we want to add, 500?
	 */
	var JsonqlContractAuthError = /*@__PURE__*/(function (Error) {
	  function JsonqlContractAuthError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);
	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlContractAuthError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlContractAuthError);
	    }
	  }

	  if ( Error ) JsonqlContractAuthError.__proto__ = Error;
	  JsonqlContractAuthError.prototype = Object.create( Error && Error.prototype );
	  JsonqlContractAuthError.prototype.constructor = JsonqlContractAuthError;

	  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

	  staticAccessors.statusCode.get = function () {
	    return 401;
	  };

	  staticAccessors.name.get = function () {
	    return 'JsonqlContractAuthError';
	  };

	  Object.defineProperties( JsonqlContractAuthError, staticAccessors );

	  return JsonqlContractAuthError;
	}(Error));

	/**
	 * This is a custom error to throw when the resolver throw error and capture inside the middleware
	 * This help us to capture the right error, due to the call happens in sequence
	 * @param {string} message to tell what happen
	 * @param {mixed} extra things we want to add, 500?
	 */
	var JsonqlResolverAppError = /*@__PURE__*/(function (Error) {
	  function JsonqlResolverAppError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);

	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlResolverAppError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlResolverAppError);
	    }
	  }

	  if ( Error ) JsonqlResolverAppError.__proto__ = Error;
	  JsonqlResolverAppError.prototype = Object.create( Error && Error.prototype );
	  JsonqlResolverAppError.prototype.constructor = JsonqlResolverAppError;

	  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

	  staticAccessors.statusCode.get = function () {
	    return 500;
	  };

	  staticAccessors.name.get = function () {
	    return 'JsonqlResolverAppError';
	  };

	  Object.defineProperties( JsonqlResolverAppError, staticAccessors );

	  return JsonqlResolverAppError;
	}(Error));

	/**
	 * This is a custom error to throw when could not find the resolver
	 * This help us to capture the right error, due to the call happens in sequence
	 * @param {string} message to tell what happen
	 * @param {mixed} extra things we want to add, 500?
	 */
	var JsonqlResolverNotFoundError = /*@__PURE__*/(function (Error) {
	  function JsonqlResolverNotFoundError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);

	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlResolverNotFoundError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlResolverNotFoundError);
	    }
	  }

	  if ( Error ) JsonqlResolverNotFoundError.__proto__ = Error;
	  JsonqlResolverNotFoundError.prototype = Object.create( Error && Error.prototype );
	  JsonqlResolverNotFoundError.prototype.constructor = JsonqlResolverNotFoundError;

	  var staticAccessors = { statusCode: { configurable: true },name: { configurable: true } };

	  staticAccessors.statusCode.get = function () {
	    return 404;
	  };

	  staticAccessors.name.get = function () {
	    return 'JsonqlResolverNotFoundError';
	  };

	  Object.defineProperties( JsonqlResolverNotFoundError, staticAccessors );

	  return JsonqlResolverNotFoundError;
	}(Error));

	// this get throw from within the checkOptions when run through the enum failed
	var JsonqlEnumError = /*@__PURE__*/(function (Error) {
	  function JsonqlEnumError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);
	    
	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlEnumError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlEnumError);
	    }
	  }

	  if ( Error ) JsonqlEnumError.__proto__ = Error;
	  JsonqlEnumError.prototype = Object.create( Error && Error.prototype );
	  JsonqlEnumError.prototype.constructor = JsonqlEnumError;

	  var staticAccessors = { name: { configurable: true } };

	  staticAccessors.name.get = function () {
	    return 'JsonqlEnumError';
	  };

	  Object.defineProperties( JsonqlEnumError, staticAccessors );

	  return JsonqlEnumError;
	}(Error));

	// this will throw from inside the checkOptions
	var JsonqlTypeError = /*@__PURE__*/(function (Error) {
	  function JsonqlTypeError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);

	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlTypeError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlTypeError);
	    }
	  }

	  if ( Error ) JsonqlTypeError.__proto__ = Error;
	  JsonqlTypeError.prototype = Object.create( Error && Error.prototype );
	  JsonqlTypeError.prototype.constructor = JsonqlTypeError;

	  var staticAccessors = { name: { configurable: true } };

	  staticAccessors.name.get = function () {
	    return 'JsonqlTypeError';
	  };

	  Object.defineProperties( JsonqlTypeError, staticAccessors );

	  return JsonqlTypeError;
	}(Error));

	// allow supply a custom checker function
	// if that failed then we throw this error
	var JsonqlCheckerError = /*@__PURE__*/(function (Error) {
	  function JsonqlCheckerError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);
	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlCheckerError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlCheckerError);
	    }
	  }

	  if ( Error ) JsonqlCheckerError.__proto__ = Error;
	  JsonqlCheckerError.prototype = Object.create( Error && Error.prototype );
	  JsonqlCheckerError.prototype.constructor = JsonqlCheckerError;

	  var staticAccessors = { name: { configurable: true } };

	  staticAccessors.name.get = function () {
	    return 'JsonqlCheckerError';
	  };

	  Object.defineProperties( JsonqlCheckerError, staticAccessors );

	  return JsonqlCheckerError;
	}(Error));

	// custom validation error class
	// when validaton failed
	var JsonqlValidationError = /*@__PURE__*/(function (Error) {
	  function JsonqlValidationError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);

	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlValidationError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlValidationError);
	    }
	  }

	  if ( Error ) JsonqlValidationError.__proto__ = Error;
	  JsonqlValidationError.prototype = Object.create( Error && Error.prototype );
	  JsonqlValidationError.prototype.constructor = JsonqlValidationError;

	  var staticAccessors = { name: { configurable: true } };

	  staticAccessors.name.get = function () {
	    return 'JsonqlValidationError';
	  };

	  Object.defineProperties( JsonqlValidationError, staticAccessors );

	  return JsonqlValidationError;
	}(Error));

	/**
	 * This is a custom error to throw whenever a error happen inside the jsonql
	 * This help us to capture the right error, due to the call happens in sequence
	 * @param {string} message to tell what happen
	 * @param {mixed} extra things we want to add, 500?
	 */
	var JsonqlError = /*@__PURE__*/(function (Error) {
	  function JsonqlError() {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    Error.apply(this, args);

	    this.message = args[0];
	    this.detail = args[1];

	    this.className = JsonqlError.name;

	    if (Error.captureStackTrace) {
	      Error.captureStackTrace(this, JsonqlError);
	    }
	  }

	  if ( Error ) JsonqlError.__proto__ = Error;
	  JsonqlError.prototype = Object.create( Error && Error.prototype );
	  JsonqlError.prototype.constructor = JsonqlError;

	  var staticAccessors = { name: { configurable: true },statusCode: { configurable: true } };

	  staticAccessors.name.get = function () {
	    return 'JsonqlError';
	  };

	  staticAccessors.statusCode.get = function () {
	    return NO_STATUS_CODE;
	  };

	  Object.defineProperties( JsonqlError, staticAccessors );

	  return JsonqlError;
	}(Error));

	// this is from an example from Koa team to use for internal middleware ctx.throw
	// but after the test the res.body part is unable to extract the required data
	// I keep this one here for future reference

	var JsonqlServerError = /*@__PURE__*/(function (Error) {
	  function JsonqlServerError(statusCode, message) {
	    Error.call(this, message);
	    this.statusCode = statusCode;
	    this.className = JsonqlServerError.name;
	  }

	  if ( Error ) JsonqlServerError.__proto__ = Error;
	  JsonqlServerError.prototype = Object.create( Error && Error.prototype );
	  JsonqlServerError.prototype.constructor = JsonqlServerError;

	  var staticAccessors = { name: { configurable: true } };

	  staticAccessors.name.get = function () {
	    return 'JsonqlServerError';
	  };

	  Object.defineProperties( JsonqlServerError, staticAccessors );

	  return JsonqlServerError;
	}(Error));

	// server side

	var errors = /*#__PURE__*/Object.freeze({
		Jsonql406Error: Jsonql406Error,
		Jsonql500Error: Jsonql500Error,
		JsonqlAuthorisationError: JsonqlAuthorisationError,
		JsonqlContractAuthError: JsonqlContractAuthError,
		JsonqlResolverAppError: JsonqlResolverAppError,
		JsonqlResolverNotFoundError: JsonqlResolverNotFoundError,
		JsonqlEnumError: JsonqlEnumError,
		JsonqlTypeError: JsonqlTypeError,
		JsonqlCheckerError: JsonqlCheckerError,
		JsonqlValidationError: JsonqlValidationError,
		JsonqlError: JsonqlError,
		JsonqlServerError: JsonqlServerError
	});

	// this will add directly to the then call in each http call
	var JsonqlError$1 = JsonqlError;

	/**
	 * We can not just check something like result.data what if the result if false?
	 * @param {object} obj the result object
	 * @param {string} key we want to check if its exist or not
	 * @return {boolean} true on found
	 */
	var isKeyInObject = function (obj, key) {
	  var keys = Object.keys(obj);
	  return !!keys.filter(function (k) { return key === k; }).length;
	};

	/**
	 * It will ONLY have our own jsonql specific implement check
	 * @param {object} result the server return result
	 * @return {object} this will just throw error
	 */
	function clientErrorsHandler(result) {
	  if (isKeyInObject(result, 'error')) {
	    var error = result.error;
	    var className = error.className;
	    var name = error.name;
	    var errorName = className || name;
	    // just throw the whole thing back
	    var msg = error.message || NO_ERROR_MSG;
	    var detail = error.detail || error;
	    if (errorName && errors[errorName]) {
	      throw new errors[className](msg, detail);
	    }
	    throw new JsonqlError$1(msg, detail);
	  }
	  // pass through to the next
	  return result;
	}

	/**
	 * this will put into generator call at the very end and catch
	 * the error throw from inside then throw again
	 * this is necessary because we split calls inside and the throw
	 * will not reach the actual client unless we do it this way
	 * @param {object} e Error
	 * @return {void} just throw
	 */
	function finalCatch(e) {
	  // this is a hack to get around the validateAsync not actually throw error
	  // instead it just rejected it with the array of failed parameters
	  if (Array.isArray(e)) {
	    // if we want the message then I will have to create yet another function
	    // to wrap this function to provide the name prop
	    throw new JsonqlValidationError('', e);
	  }
	  var msg = e.message || NO_ERROR_MSG;
	  var detail = e.detail || e;
	  switch (true) {
	    case e instanceof Jsonql406Error:
	      throw new Jsonql406Error(msg, detail);
	    case e instanceof Jsonql500Error:
	      throw new Jsonql500Error(msg, detail);
	    case e instanceof JsonqlAuthorisationError:
	      throw new JsonqlAuthorisationError(msg, detail);
	    case e instanceof JsonqlContractAuthError:
	      throw new JsonqlContractAuthError(msg, detail);
	    case e instanceof JsonqlResolverAppError:
	      throw new JsonqlResolverAppError(msg, detail);
	    case e instanceof JsonqlResolverNotFoundError:
	      throw new JsonqlResolverNotFoundError(msg, detail);
	    case e instanceof JsonqlEnumError:
	      throw new JsonqlEnumError(msg, detail);
	    case e instanceof JsonqlTypeError:
	      throw new JsonqlTypeError(msg, detail);
	    case e instanceof JsonqlCheckerError:
	      throw new JsonqlCheckerError(msg, detail);
	    case e instanceof JsonqlValidationError:
	      throw new JsonqlValidationError(msg, detail);
	    case e instanceof JsonqlServerError:
	      throw new JsonqlServerError(msg, detail);
	    default:
	      throw new JsonqlError(msg, detail);
	  }
	}

	// move the index.js code here that make more sense to find where things are

	// import debug from 'debug'
	// const debugFn = debug('jsonql-params-validator:validator')
	// also export this for use in other places

	/**
	 * We need to handle those optional parameter without a default value
	 * @param {object} params from contract.json
	 * @return {boolean} for filter operation false is actually OK
	 */
	var optionalHandler = function( params ) {
	  var arg = params.arg;
	  var param = params.param;
	  if (notEmpty(arg)) {
	    // debug('call optional handler', arg, params);
	    // loop through the type in param
	    return !(param.type.length > param.type.filter(function (type) { return validateHandler(type, params); }
	    ).length)
	  }
	  return false;
	};

	/**
	 * actually picking the validator
	 * @param {*} type for checking
	 * @param {*} value for checking
	 * @return {boolean} true on OK
	 */
	var validateHandler = function(type, value) {
	  var tmp;
	  switch (true) {
	    case type === OBJECT_TYPE$1:
	      // debugFn('call OBJECT_TYPE')
	      return !objectTypeHandler(value)
	    case type === ARRAY_TYPE$1:
	      // debugFn('call ARRAY_TYPE')
	      return !checkIsArray(value.arg)
	    // @TODO when the type is not present, it always fall through here
	    // so we need to find a way to actually pre-check the type first
	    // AKA check the contract.json map before running here
	    case (tmp = isArrayLike$1(type)) !== false:
	      // debugFn('call ARRAY_LIKE: %O', value)
	      return !arrayTypeHandler(value, tmp)
	    default:
	      return !combineFn(type)(value.arg)
	  }
	};

	/**
	 * it get too longer to fit in one line so break it out from the fn below
	 * @param {*} arg value
	 * @param {object} param config
	 * @return {*} value or apply default value
	 */
	var getOptionalValue = function(arg, param) {
	  if (!isUndefined(arg)) {
	    return arg;
	  }
	  return (param.optional === true && !isUndefined(param.defaultvalue) ? param.defaultvalue : null)
	};

	/**
	 * padding the arguments with defaultValue if the arguments did not provide the value
	 * this will be the name export
	 * @param {array} args normalized arguments
	 * @param {array} params from contract.json
	 * @return {array} merge the two together
	 */
	var normalizeArgs = function(args, params) {
	  // first we should check if this call require a validation at all
	  // there will be situation where the function doesn't need args and params
	  if (!checkIsArray(params)) {
	    // debugFn('params value', params)
	    throw new JsonqlError(PARAMS_NOT_ARRAY_ERR)
	  }
	  if (params.length === 0) {
	    return [];
	  }
	  if (!checkIsArray(args)) {
	    throw new JsonqlError(ARGS_NOT_ARRAY_ERR)
	  }
	  // debugFn(args, params);
	  // fall through switch
	  switch(true) {
	    case args.length == params.length: // standard
	      return args.map(function (arg, i) { return (
	        {
	          arg: arg,
	          index: i,
	          param: params[i]
	        }
	      ); });
	    case params[0].variable === true: // using spread syntax
	      var type = params[0].type;
	      return args.map(function (arg, i) { return (
	        {
	          arg: arg,
	          index: i, // keep the index for reference
	          param: params[i] || { type: type, name: '_' }
	        }
	      ); });
	    // with optional defaultValue parameters
	    case args.length < params.length:
	      return params.map(function (param, i) { return (
	        {
	          param: param,
	          index: i,
	          arg: getOptionalValue(args[i], param),
	          optional: param.optional || false
	        }
	      ); });
	    // this one pass more than it should have anything after the args.length will be cast as any type
	    case args.length > params.length && params.length === 1:
	      // this happens when we have those array.<number> type
	      var tmp, _type = [ DEFAULT_TYPE$1 ];
	      // we only looking at the first one, this might be a @BUG!
	      if ((tmp = isArrayLike$1(params[0].type[0])) !== false) {
	        _type = tmp;
	      }
	      // if not then we fall back to the following
	      return args.map(function (arg, i) { return (
	        {
	          arg: arg,
	          index: i,
	          param: params[i] || { type: _type, name: '_' }
	        }
	      ); });
	    // @TODO find out if there is more cases not cover
	    default: // this should never happen
	      // debugFn('args', args)
	      // debugFn('params', params)
	      // this is unknown therefore we just throw it!
	      throw new JsonqlError(EXCEPTION_CASE_ERR, { args: args, params: params })
	  }
	};

	// what we want is after the validaton we also get the normalized result
	// which is with the optional property if the argument didn't provide it
	/**
	 * process the array of params back to their arguments
	 * @param {array} result the params result
	 * @return {array} arguments
	 */
	var processReturn = function (result) { return result.map(function (r) { return r.arg; }); };

	/**
	 * validator main interface
	 * @param {array} args the arguments pass to the method call
	 * @param {array} params from the contract for that method
	 * @param {boolean} [withResul=false] if true then this will return the normalize result as well
	 * @return {array} empty array on success, or failed parameter and reasons
	 */
	var validateSync = function(args, params, withResult) {
	  var obj;

	  if ( withResult === void 0 ) withResult = false;
	  var cleanArgs = normalizeArgs(args, params);
	  var checkResult = cleanArgs.filter(function (p) {
	    if (p.param.optional === true) {
	      return optionalHandler(p)
	    }
	    // because array of types means OR so if one pass means pass
	    return !(p.param.type.length > p.param.type.filter(
	      function (type) { return validateHandler(type, p); }
	    ).length)
	  });
	  // using the same convention we been using all this time
	  return !withResult ? checkResult : ( obj = {}, obj[ERROR_KEY] = checkResult, obj[DATA_KEY] = processReturn(cleanArgs), obj )
	};

	/**
	 * A wrapper method that return promise
	 * @param {array} args arguments
	 * @param {array} params from contract.json
	 * @param {boolean} [withResul=false] if true then this will return the normalize result as well
	 * @return {object} promise.then or catch
	 */
	var validateAsync = function(args, params, withResult) {
	  if ( withResult === void 0 ) withResult = false;

	  return new Promise(function (resolver, rejecter) {
	    var result = validateSync(args, params, withResult);
	    if (withResult) {
	      return result[ERROR_KEY].length ? rejecter(result[ERROR_KEY])
	                                      : resolver(result[DATA_KEY])
	    }
	    // the different is just in the then or catch phrase
	    return result.length ? rejecter(result) : resolver([])
	  })
	};

	/**
	 * @param {array} arr Array for check
	 * @param {*} value target
	 * @return {boolean} true on successs
	 */
	var isInArray = function(arr, value) {
	  return !!arr.filter(function (a) { return a === value; }).length;
	};

	/**
	 * @param {object} obj for search
	 * @param {string} key target
	 * @return {boolean} true on success
	 */
	var checkKeyInObject = function(obj, key) {
	  var keys = Object.keys(obj);
	  return isInArray(keys, key)
	};

	// import debug from 'debug';
	// const debugFn = debug('jsonql-params-validator:options:prepare')

	// just not to make my head hurt
	var isEmpty = function (value) { return !notEmpty(value); };

	/**
	 * Map the alias to their key then grab their value over
	 * @param {object} config the user supplied config
	 * @param {object} appProps the default option map
	 * @return {object} the config keys replaced with the appProps key by the ALIAS
	 */
	function mapAliasConfigKeys(config, appProps) {
	  // need to do two steps
	  // 1. take key with alias key
	  var aliasMap = omitBy(appProps, function (value, k) { return !value[ALIAS_KEY$1]; } );
	  if (isEqual(aliasMap, {})) {
	    return config;
	  }
	  return mapKeys(config, function (v, key) { return findKey(aliasMap, function (o) { return o.alias === key; }) || key; })
	}

	/**
	 * We only want to run the valdiation against the config (user supplied) value
	 * but keep the defaultOptions untouch
	 * @param {object} config configuraton supplied by user
	 * @param {object} appProps the default options map
	 * @return {object} the pristine values that will add back to the final output
	 */
	function preservePristineValues(config, appProps) {
	  // @BUG this will filter out those that is alias key
	  // we need to first map the alias keys back to their full key
	  var _config = mapAliasConfigKeys(config, appProps);
	  // take the default value out
	  var pristineValues = mapValues(
	    omitBy(appProps, function (value, key) { return checkKeyInObject(_config, key); }),
	    function (value) { return value.args; }
	  );
	  // for testing the value
	  var checkAgainstAppProps = omitBy(appProps, function (value, key) { return !checkKeyInObject(_config, key); });
	  // output
	  return {
	    pristineValues: pristineValues,
	    checkAgainstAppProps: checkAgainstAppProps,
	    config: _config // passing this correct values back
	  }
	}

	/**
	 * This will take the value that is ONLY need to check
	 * @param {object} config that one
	 * @param {object} props map for creating checking
	 * @return {object} put that arg into the args
	 */
	function processConfigAction(config, props) {
	  // debugFn('processConfigAction', props)
	  // v.1.2.0 add checking if its mark optional and the value is empty then pass
	  return mapValues(props, function (value, key) {
	    var obj, obj$1;

	    return (
	    isUndefined(config[key]) || (value[OPTIONAL_KEY$1] === true && isEmpty(config[key]))
	      ? merge({}, value, ( obj = {}, obj[KEY_WORD$1] = true, obj ))
	      : ( obj$1 = {}, obj$1[ARGS_KEY$1] = config[key], obj$1[TYPE_KEY$1] = value[TYPE_KEY$1], obj$1[OPTIONAL_KEY$1] = value[OPTIONAL_KEY$1] || false, obj$1[ENUM_KEY$1] = value[ENUM_KEY$1] || false, obj$1[CHECKER_KEY$1] = value[CHECKER_KEY$1] || false, obj$1 )
	    );
	  }
	  )
	}

	/**
	 * Quick transform
	 * @TODO we should only validate those that is pass from the config
	 * and pass through those values that is from the defaultOptions
	 * @param {object} opts that one
	 * @param {object} appProps mutation configuration options
	 * @return {object} put that arg into the args
	 */
	function prepareArgsForValidation(opts, appProps) {
	  var ref = preservePristineValues(opts, appProps);
	  var config = ref.config;
	  var pristineValues = ref.pristineValues;
	  var checkAgainstAppProps = ref.checkAgainstAppProps;
	  // output
	  return [
	    processConfigAction(config, checkAgainstAppProps),
	    pristineValues
	  ]
	}

	// breaking the whole thing up to see what cause the multiple calls issue

	// import debug from 'debug';
	// const debugFn = debug('jsonql-params-validator:options:validation')

	/**
	 * just make sure it returns an array to use
	 * @param {*} arg input
	 * @return {array} output
	 */
	var toArray = function (arg) { return checkIsArray(arg) ? arg : [arg]; };

	/**
	 * DIY in array
	 * @param {array} arr to check against
	 * @param {*} value to check
	 * @return {boolean} true on OK
	 */
	var inArray = function (arr, value) { return (
	  !!arr.filter(function (v) { return v === value; }).length
	); };

	/**
	 * break out to make the code easier to read
	 * @param {object} value to process
	 * @param {function} cb the validateSync
	 * @return {array} empty on success
	 */
	function validateHandler$1(value, cb) {
	  var obj;

	  // cb is the validateSync methods
	  var args = [
	    [ value[ARGS_KEY$1] ],
	    [( obj = {}, obj[TYPE_KEY$1] = toArray(value[TYPE_KEY$1]), obj[OPTIONAL_KEY$1] = value[OPTIONAL_KEY$1], obj )]
	  ];
	  // debugFn('validateHandler', args)
	  return Reflect.apply(cb, null, args)
	}

	/**
	 * Check against the enum value if it's provided
	 * @param {*} value to check
	 * @param {*} enumv to check against if it's not false
	 * @return {boolean} true on OK
	 */
	var enumHandler = function (value, enumv) {
	  if (checkIsArray(enumv)) {
	    return inArray(enumv, value)
	  }
	  return true;
	};

	/**
	 * Allow passing a function to check the value
	 * There might be a problem here if the function is incorrect
	 * and that will makes it hard to debug what is going on inside
	 * @TODO there could be a few feature add to this one under different circumstance
	 * @param {*} value to check
	 * @param {function} checker for checking
	 */
	var checkerHandler = function (value, checker) {
	  try {
	    return isFunction$2(checker) ? checker.apply(null, [value]) : false;
	  } catch (e) {
	    return false;
	  }
	};

	/**
	 * Taken out from the runValidaton this only validate the required values
	 * @param {array} args from the config2argsAction
	 * @param {function} cb validateSync
	 * @return {array} of configuration values
	 */
	function runValidationAction(cb) {
	  return function (value, key) {
	    // debugFn('runValidationAction', key, value)
	    if (value[KEY_WORD$1]) {
	      return value[ARGS_KEY$1]
	    }
	    var check = validateHandler$1(value, cb);
	    if (check.length) {
	      // debugFn('runValidationAction', key, value)
	      throw new JsonqlTypeError(key, check)
	    }
	    if (value[ENUM_KEY$1] !== false && !enumHandler(value[ARGS_KEY$1], value[ENUM_KEY$1])) {
	      throw new JsonqlEnumError(key)
	    }
	    if (value[CHECKER_KEY$1] !== false && !checkerHandler(value[ARGS_KEY$1], value[CHECKER_KEY$1])) {
	      throw new JsonqlCheckerError(key)
	    }
	    return value[ARGS_KEY$1]
	  }
	}

	/**
	 * @param {object} args from the config2argsAction
	 * @param {function} cb validateSync
	 * @return {object} of configuration values
	 */
	function runValidation(args, cb) {
	  var argsForValidate = args[0];
	  var pristineValues = args[1];
	  // turn the thing into an array and see what happen here
	  // debugFn('_args', argsForValidate)
	  var result = mapValues(argsForValidate, runValidationAction(cb));
	  return merge(result, pristineValues)
	}

	// this is port back from the client to share across all projects

	// import debug from 'debug'
	// const debugFn = debug('jsonql-params-validator:check-options-async')

	/**
	 * Quick transform
	 * @param {object} config that one
	 * @param {object} appProps mutation configuration options
	 * @return {object} put that arg into the args
	 */
	var configToArgs = function (config, appProps) {
	  return Promise.resolve(
	    prepareArgsForValidation(config, appProps)
	  )
	};

	/**
	 * @param {object} config user provide configuration option
	 * @param {object} appProps mutation configuration options
	 * @param {object} constProps the immutable configuration options
	 * @param {function} cb the validateSync method
	 * @return {object} Promise resolve merge config object
	 */
	function checkOptionsAsync(config, appProps, constProps, cb) {
	  if ( config === void 0 ) config = {};

	  return configToArgs(config, appProps)
	    .then(function (args1) {
	      // debugFn('args', args1)
	      return runValidation(args1, cb)
	    })
	    // next if every thing good then pass to final merging
	    .then(function (args2) { return merge({}, args2, constProps); })
	}

	// create function to construct the config entry so we don't need to keep building object
	// import debug from 'debug';
	// const debugFn = debug('jsonql-params-validator:construct-config');
	/**
	 * @param {*} args value
	 * @param {string} type for value
	 * @param {boolean} [optional=false]
	 * @param {boolean|array} [enumv=false]
	 * @param {boolean|function} [checker=false]
	 * @return {object} config entry
	 */
	function constructConfigFn(args, type, optional, enumv, checker, alias) {
	  if ( optional === void 0 ) optional=false;
	  if ( enumv === void 0 ) enumv=false;
	  if ( checker === void 0 ) checker=false;
	  if ( alias === void 0 ) alias=false;

	  var base = {};
	  base[ARGS_KEY] = args;
	  base[TYPE_KEY] = type;
	  if (optional === true) {
	    base[OPTIONAL_KEY] = true;
	  }
	  if (checkIsArray(enumv)) {
	    base[ENUM_KEY] = enumv;
	  }
	  if (isFunction$2(checker)) {
	    base[CHECKER_KEY] = checker;
	  }
	  if (isString(alias)) {
	    base[ALIAS_KEY] = alias;
	  }
	  return base;
	}

	// export also create wrapper methods

	// import debug from 'debug';
	// const debugFn = debug('jsonql-params-validator:options:index');

	/**
	 * This has a different interface
	 * @param {*} value to supply
	 * @param {string|array} type for checking
	 * @param {object} params to map against the config check
	 * @param {array} params.enumv NOT enum
	 * @param {boolean} params.optional false then nothing
	 * @param {function} params.checker need more work on this one later
	 * @param {string} params.alias mostly for cmd
	 */
	var createConfig = function (value, type, params) {
	  if ( params === void 0 ) params = {};

	  // Note the enumv not ENUM
	  // const { enumv, optional, checker, alias } = params;
	  // let args = [value, type, optional, enumv, checker, alias];
	  var o = params[OPTIONAL_KEY];
	  var e = params[ENUM_KEY];
	  var c = params[CHECKER_KEY];
	  var a = params[ALIAS_KEY];
	  return constructConfigFn.apply(null,  [value, type, o, e, c, a])
	};

	/**
	 * We recreate the method here to avoid the circlar import
	 * @param {object} config user supply configuration
	 * @param {object} appProps mutation options
	 * @param {object} [constantProps={}] optional: immutation options
	 * @return {object} all checked configuration
	 */
	var checkConfigAsync = function(validateSync) {
	  return function(config, appProps, constantProps) {
	    if ( constantProps === void 0 ) constantProps= {};

	    return checkOptionsAsync(config, appProps, constantProps, validateSync)
	  }
	};

	// craete several helper function to construct / extract the payload

	/**
	 * Get name from the payload (ported back from jsonql-koa)
	 * @param {*} payload to extract from
	 * @return {string} name
	 */
	function getNameFromPayload(payload) {
	  return Object.keys(payload)[0]
	}

	/**
	 * @param {string} resolverName name of function
	 * @param {array} [args=[]] from the ...args
	 * @param {boolean} [jsonp = false] add v1.3.0 to koa
	 * @return {object} formatted argument
	 */
	function createQuery(resolverName, args, jsonp) {
	  var obj;

	  if ( args === void 0 ) args = [];
	  if ( jsonp === void 0 ) jsonp = false;
	  if (checkIsString(resolverName) && checkIsArray(args)) {
	    var payload =  {};
	    payload[QUERY_ARG_NAME] = args;
	    if (jsonp === true) {
	      return payload;
	    }
	    return ( obj = {}, obj[resolverName] = payload, obj )
	  }
	  throw new JsonqlValidationError("[createQuery] expect resolverName to be string and args to be array!", { resolverName: resolverName, args: args })
	}

	/**
	 * @param {string} resolverName name of function
	 * @param {*} payload to send
	 * @param {object} [condition={}] for what
	 * @param {boolean} [jsonp = false] add v1.3.0 to koa
	 * @return {object} formatted argument
	 */
	function createMutation(resolverName, payload, condition, jsonp) {
	  var obj;

	  if ( condition === void 0 ) condition = {};
	  if ( jsonp === void 0 ) jsonp = false;
	  var _payload = {};
	  _payload[PAYLOAD_PARAM_NAME] = payload;
	  _payload[CONDITION_PARAM_NAME] = condition;
	  if (jsonp === true) {
	    return _payload;
	  }
	  if (checkIsString(resolverName)) {
	    return ( obj = {}, obj[resolverName] = _payload, obj )
	  }
	  throw new JsonqlValidationError("[createMutation] expect resolverName to be string!", { resolverName: resolverName, payload: payload, condition: condition })
	}

	// export
	// PIA syntax
	var isObject$3 = checkIsObject;
	var isString$1 = checkIsString;
	var isArray$1 = checkIsArray;
	var validateAsync$1 = validateAsync;

	var createConfig$1 = createConfig;

	var checkConfigAsync$1 = checkConfigAsync(validateSync);

	var inArray$1 = isInArray;

	var isKeyInObject$1 = checkKeyInObject;

	var createQuery$1 = createQuery;
	var createMutation$1 = createMutation;
	var getNameFromPayload$1 = getNameFromPayload;

	// move some of the functions were in the class
	// since it's only use here there is no point of adding it to the constants module
	// or may be we add it back later
	var ENDPOINT_TABLE = 'endpoint';
	var USERDATA_TABLE = 'userdata';

	/**
	 * @return {number} timestamp
	 */
	var timestamp = function () { return Math.floor( Date.now() / 1000 ); };

	var cacheBurst = function () { return ({ _cb: timestamp() }); };

	/**
	 * @param {object} jsonqlInstance the init instance of jsonql client
	 * @param {object} contract the static contract
	 * @return {object} contract may be from server
	 */
	var getContractFromConfig = function(jsonqlInstance, contract) {
	  if ( contract === void 0 ) contract = {};

	  if (isJsonqlContract(contract)) {
	    return Promise.resolve(contract)
	  }
	  return jsonqlInstance.getContract()
	};

	/**
	 * handle the return data
	 * @param {object} result return from server
	 * @return {object} strip the data part out, or if the error is presented
	 */
	var resultHandler = function (result) { return (
	  (isKeyInObject$1(result, 'data') && !isKeyInObject$1(result, 'error')) ? result.data : result
	); };

	/**
	 * make sure it's a JSONQL contract
	 * @param {*} obj input
	 * @return {boolean} true is OK
	 */
	var isJsonqlContract = function (obj) { return (
	  obj && isObject$3(obj) && (isKeyInObject$1(obj, QUERY_NAME) || isKeyInObject$1(obj, MUTATION_NAME))
	); };

	/**
	 * The code was extracted from:
	 * https://github.com/davidchambers/Base64.js
	 */

	var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

	function InvalidCharacterError(message) {
	  this.message = message;
	}

	InvalidCharacterError.prototype = new Error();
	InvalidCharacterError.prototype.name = 'InvalidCharacterError';

	function polyfill (input) {
	  var str = String(input).replace(/=+$/, '');
	  if (str.length % 4 == 1) {
	    throw new InvalidCharacterError("'atob' failed: The string to be decoded is not correctly encoded.");
	  }
	  for (
	    // initialize result and counters
	    var bc = 0, bs, buffer, idx = 0, output = '';
	    // get next character
	    buffer = str.charAt(idx++);
	    // character found in table? initialize bit storage and add its ascii value;
	    ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
	      // and if not first of each 4 characters,
	      // convert the first 8 bits to one ascii character
	      bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
	  ) {
	    // try to find character in table (0-63, not found => -1)
	    buffer = chars.indexOf(buffer);
	  }
	  return output;
	}


	var atob = typeof window !== 'undefined' && window.atob && window.atob.bind(window) || polyfill;

	function b64DecodeUnicode(str) {
	  return decodeURIComponent(atob(str).replace(/(.)/g, function (m, p) {
	    var code = p.charCodeAt(0).toString(16).toUpperCase();
	    if (code.length < 2) {
	      code = '0' + code;
	    }
	    return '%' + code;
	  }));
	}

	var base64_url_decode = function(str) {
	  var output = str.replace(/-/g, "+").replace(/_/g, "/");
	  switch (output.length % 4) {
	    case 0:
	      break;
	    case 2:
	      output += "==";
	      break;
	    case 3:
	      output += "=";
	      break;
	    default:
	      throw "Illegal base64url string!";
	  }

	  try{
	    return b64DecodeUnicode(output);
	  } catch (err) {
	    return atob(output);
	  }
	};

	function InvalidTokenError(message) {
	  this.message = message;
	}

	InvalidTokenError.prototype = new Error();
	InvalidTokenError.prototype.name = 'InvalidTokenError';

	var lib = function (token,options) {
	  if (typeof token !== 'string') {
	    throw new InvalidTokenError('Invalid token specified');
	  }

	  options = options || {};
	  var pos = options.header === true ? 0 : 1;
	  try {
	    return JSON.parse(base64_url_decode(token.split('.')[pos]));
	  } catch (e) {
	    throw new InvalidTokenError('Invalid token specified: ' + e.message);
	  }
	};

	var InvalidTokenError_1 = InvalidTokenError;
	lib.InvalidTokenError = InvalidTokenError_1;

	// when the user is login with the jwt

	/**
	 * We only check the nbf and exp
	 * @param {object} token for checking
	 * @return {object} token on success
	 */
	function validate(token) {
	  var start = token.iat || Math.floor(Date.now() / 1000);
	  // we only check the exp for the time being
	  if (token.exp) {
	    if (start >= token.exp) {
	      var expired = new Date(token.exp).toISOString();
	      throw new JsonqlError(("Token has expired on " + expired), token)
	    }
	  }
	  return token;
	}

	/**
	 * The browser client version it has far fewer options and it doesn't verify it
	 * because it couldn't this is the job for the server
	 * @TODO we need to add some extra proessing here to check for the exp field
	 * @param {string} token to decrypted
	 * @return {object} decrypted object
	 */
	function jwtDecode(token) {
	  if (isString$1(token)) {
	    var t = lib(token);
	    return validate(t)
	  }
	  throw new JsonqlError('Token must be a string!')
	}

	var obj, obj$1, obj$2, obj$3, obj$4, obj$5, obj$6, obj$7, obj$8;

	var appProps = {
	  algorithm: createConfig$1(HSA_ALGO, [STRING_TYPE]),
	  expiresIn: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj = {}, obj[ALIAS_KEY] = 'exp', obj[OPTIONAL_KEY] = true, obj )),
	  notBefore: createConfig$1(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj$1 = {}, obj$1[ALIAS_KEY] = 'nbf', obj$1[OPTIONAL_KEY] = true, obj$1 )),
	  audience: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$2 = {}, obj$2[ALIAS_KEY] = 'iss', obj$2[OPTIONAL_KEY] = true, obj$2 )),
	  subject: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$3 = {}, obj$3[ALIAS_KEY] = 'sub', obj$3[OPTIONAL_KEY] = true, obj$3 )),
	  issuer: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$4 = {}, obj$4[ALIAS_KEY] = 'iss', obj$4[OPTIONAL_KEY] = true, obj$4 )),
	  noTimestamp: createConfig$1(false, [BOOLEAN_TYPE], ( obj$5 = {}, obj$5[OPTIONAL_KEY] = true, obj$5 )),
	  header: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$6 = {}, obj$6[OPTIONAL_KEY] = true, obj$6 )),
	  keyid: createConfig$1(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$7 = {}, obj$7[OPTIONAL_KEY] = true, obj$7 )),
	  mutatePayload: createConfig$1(false, [BOOLEAN_TYPE], ( obj$8 = {}, obj$8[OPTIONAL_KEY] = true, obj$8 ))
	};

	// ws client using native WebSocket

	function getWS() {
	  switch(true) {
	    case (typeof WebSocket !== 'undefined'):
	      return WebSocket;
	    case (typeof MozWebSocket !== 'undefined'):
	      return MozWebSocket;
	    // case (typeof global !== 'undefined'):
	    //  return global.WebSocket || global.MozWebSocket;
	    case (typeof window !== 'undefined'):
	      return window.WebSocket || window.MozWebSocket;
	    // case (typeof self !== 'undefined'):
	    //   return self.WebSocket || self.MozWebSocket;
	    default:
	      throw new JsonqlValidationError('WebSocket is NOT SUPPORTED!')
	  }
	}

	var WS = getWS();

	var fly = createCommonjsModule(function (module, exports) {
	(function webpackUniversalModuleDefinition(root, factory) {
		{ module.exports = factory(); }
	})(commonjsGlobal, function() {
	return /******/ (function(modules) { // webpackBootstrap
	/******/ 	// The module cache
	/******/ 	var installedModules = {};
	/******/
	/******/ 	// The require function
	/******/ 	function __webpack_require__(moduleId) {
	/******/
	/******/ 		// Check if module is in cache
	/******/ 		if(installedModules[moduleId]) {
	/******/ 			return installedModules[moduleId].exports;
	/******/ 		}
	/******/ 		// Create a new module (and put it into the cache)
	/******/ 		var module = installedModules[moduleId] = {
	/******/ 			i: moduleId,
	/******/ 			l: false,
	/******/ 			exports: {}
	/******/ 		};
	/******/
	/******/ 		// Execute the module function
	/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
	/******/
	/******/ 		// Flag the module as loaded
	/******/ 		module.l = true;
	/******/
	/******/ 		// Return the exports of the module
	/******/ 		return module.exports;
	/******/ 	}
	/******/
	/******/
	/******/ 	// expose the modules object (__webpack_modules__)
	/******/ 	__webpack_require__.m = modules;
	/******/
	/******/ 	// expose the module cache
	/******/ 	__webpack_require__.c = installedModules;
	/******/
	/******/ 	// identity function for calling harmony imports with the correct context
	/******/ 	__webpack_require__.i = function(value) { return value; };
	/******/
	/******/ 	// define getter function for harmony exports
	/******/ 	__webpack_require__.d = function(exports, name, getter) {
	/******/ 		if(!__webpack_require__.o(exports, name)) {
	/******/ 			Object.defineProperty(exports, name, {
	/******/ 				configurable: false,
	/******/ 				enumerable: true,
	/******/ 				get: getter
	/******/ 			});
	/******/ 		}
	/******/ 	};
	/******/
	/******/ 	// getDefaultExport function for compatibility with non-harmony modules
	/******/ 	__webpack_require__.n = function(module) {
	/******/ 		var getter = module && module.__esModule ?
	/******/ 			function getDefault() { return module['default']; } :
	/******/ 			function getModuleExports() { return module; };
	/******/ 		__webpack_require__.d(getter, 'a', getter);
	/******/ 		return getter;
	/******/ 	};
	/******/
	/******/ 	// Object.prototype.hasOwnProperty.call
	/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
	/******/
	/******/ 	// __webpack_public_path__
	/******/ 	__webpack_require__.p = "";
	/******/
	/******/ 	// Load entry module and return exports
	/******/ 	return __webpack_require__(__webpack_require__.s = 2);
	/******/ })
	/************************************************************************/
	/******/ ([
	/* 0 */
	/***/ (function(module, exports, __webpack_require__) {


	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

	module.exports = {
	    type: function type(ob) {
	        return Object.prototype.toString.call(ob).slice(8, -1).toLowerCase();
	    },
	    isObject: function isObject(ob, real) {
	        if (real) {
	            return this.type(ob) === "object";
	        } else {
	            return ob && (typeof ob === 'undefined' ? 'undefined' : _typeof(ob)) === 'object';
	        }
	    },
	    isFormData: function isFormData(val) {
	        return typeof FormData !== 'undefined' && val instanceof FormData;
	    },
	    trim: function trim(str) {
	        return str.replace(/(^\s*)|(\s*$)/g, '');
	    },
	    encode: function encode(val) {
	        return encodeURIComponent(val).replace(/%40/gi, '@').replace(/%3A/gi, ':').replace(/%24/g, '$').replace(/%2C/gi, ',').replace(/%20/g, '+').replace(/%5B/gi, '[').replace(/%5D/gi, ']');
	    },
	    formatParams: function formatParams(data) {
	        var str = "";
	        var first = true;
	        var that = this;
	        if (!this.isObject(data)) {
	            return data;
	        }

	        function _encode(sub, path) {
	            var encode = that.encode;
	            var type = that.type(sub);
	            if (type == "array") {
	                sub.forEach(function (e, i) {
	                    if (!that.isObject(e)) { i = ""; }
	                    _encode(e, path + ('%5B' + i + '%5D'));
	                });
	            } else if (type == "object") {
	                for (var key in sub) {
	                    if (path) {
	                        _encode(sub[key], path + "%5B" + encode(key) + "%5D");
	                    } else {
	                        _encode(sub[key], encode(key));
	                    }
	                }
	            } else {
	                if (!first) {
	                    str += "&";
	                }
	                first = false;
	                str += path + "=" + encode(sub);
	            }
	        }

	        _encode(data, "");
	        return str;
	    },

	    // Do not overwrite existing attributes
	    merge: function merge(a, b) {
	        for (var key in b) {
	            if (!a.hasOwnProperty(key)) {
	                a[key] = b[key];
	            } else if (this.isObject(b[key], 1) && this.isObject(a[key], 1)) {
	                this.merge(a[key], b[key]);
	            }
	        }
	        return a;
	    }
	};

	/***/ }),
	/* 1 */,
	/* 2 */
	/***/ (function(module, exports, __webpack_require__) {

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) { descriptor.writable = true; } Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) { defineProperties(Constructor.prototype, protoProps); } if (staticProps) { defineProperties(Constructor, staticProps); } return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var utils = __webpack_require__(0);
	var isBrowser = typeof document !== "undefined";

	var Fly = function () {
	    function Fly(engine) {
	        _classCallCheck(this, Fly);

	        this.engine = engine || XMLHttpRequest;

	        this.default = this; //For typeScript

	        /**
	         * Add  lock/unlock API for interceptor.
	         *
	         * Once an request/response interceptor is locked, the incoming request/response
	         * will be added to a queue before they enter the interceptor, they will not be
	         * continued  until the interceptor is unlocked.
	         *
	         * @param [interceptor] either is interceptors.request or interceptors.response
	         */
	        function wrap(interceptor) {
	            var resolve = void 0;
	            var reject = void 0;

	            function _clear() {
	                interceptor.p = resolve = reject = null;
	            }

	            utils.merge(interceptor, {
	                lock: function lock() {
	                    if (!resolve) {
	                        interceptor.p = new Promise(function (_resolve, _reject) {
	                            resolve = _resolve;
	                            reject = _reject;
	                        });
	                    }
	                },
	                unlock: function unlock() {
	                    if (resolve) {
	                        resolve();
	                        _clear();
	                    }
	                },
	                clear: function clear() {
	                    if (reject) {
	                        reject("cancel");
	                        _clear();
	                    }
	                }
	            });
	        }

	        var interceptors = this.interceptors = {
	            response: {
	                use: function use(handler, onerror) {
	                    this.handler = handler;
	                    this.onerror = onerror;
	                }
	            },
	            request: {
	                use: function use(handler) {
	                    this.handler = handler;
	                }
	            }
	        };

	        var irq = interceptors.request;
	        var irp = interceptors.response;
	        wrap(irp);
	        wrap(irq);

	        this.config = {
	            method: "GET",
	            baseURL: "",
	            headers: {},
	            timeout: 0,
	            params: {}, // Default Url params
	            parseJson: true, // Convert response data to JSON object automatically.
	            withCredentials: false
	        };
	    }

	    _createClass(Fly, [{
	        key: "request",
	        value: function request(url, data, options) {
	            var _this = this;

	            var engine = new this.engine();
	            var contentType = "Content-Type";
	            var contentTypeLowerCase = contentType.toLowerCase();
	            var interceptors = this.interceptors;
	            var requestInterceptor = interceptors.request;
	            var responseInterceptor = interceptors.response;
	            var requestInterceptorHandler = requestInterceptor.handler;
	            var promise = new Promise(function (resolve, reject) {
	                if (utils.isObject(url)) {
	                    options = url;
	                    url = options.url;
	                }
	                options = options || {};
	                options.headers = options.headers || {};

	                function isPromise(p) {
	                    // some  polyfill implementation of Promise may be not standard,
	                    // so, we test by duck-typing
	                    return p && p.then && p.catch;
	                }

	                /**
	                 * If the request/response interceptor has been locked，
	                 * the new request/response will enter a queue. otherwise, it will be performed directly.
	                 * @param [promise] if the promise exist, means the interceptor is  locked.
	                 * @param [callback]
	                 */
	                function enqueueIfLocked(promise, callback) {
	                    if (promise) {
	                        promise.then(function () {
	                            callback();
	                        });
	                    } else {
	                        callback();
	                    }
	                }

	                // make the http request
	                function makeRequest(options) {
	                    data = options.body;
	                    // Normalize the request url
	                    url = utils.trim(options.url);
	                    var baseUrl = utils.trim(options.baseURL || "");
	                    if (!url && isBrowser && !baseUrl) { url = location.href; }
	                    if (url.indexOf("http") !== 0) {
	                        var isAbsolute = url[0] === "/";
	                        if (!baseUrl && isBrowser) {
	                            var arr = location.pathname.split("/");
	                            arr.pop();
	                            baseUrl = location.protocol + "//" + location.host + (isAbsolute ? "" : arr.join("/"));
	                        }
	                        if (baseUrl[baseUrl.length - 1] !== "/") {
	                            baseUrl += "/";
	                        }
	                        url = baseUrl + (isAbsolute ? url.substr(1) : url);
	                        if (isBrowser) {

	                            // Normalize the url which contains the ".." or ".", such as
	                            // "http://xx.com/aa/bb/../../xx" to "http://xx.com/xx" .
	                            var t = document.createElement("a");
	                            t.href = url;
	                            url = t.href;
	                        }
	                    }

	                    var responseType = utils.trim(options.responseType || "");
	                    var needQuery = ["GET", "HEAD", "DELETE", "OPTION"].indexOf(options.method) !== -1;
	                    var dataType = utils.type(data);
	                    var params = options.params || {};

	                    // merge url params when the method is "GET" (data is object)
	                    if (needQuery && dataType === "object") {
	                        params = utils.merge(data, params);
	                    }
	                    // encode params to String
	                    params = utils.formatParams(params);

	                    // save url params
	                    var _params = [];
	                    if (params) {
	                        _params.push(params);
	                    }
	                    // Add data to url params when the method is "GET" (data is String)
	                    if (needQuery && data && dataType === "string") {
	                        _params.push(data);
	                    }

	                    // make the final url
	                    if (_params.length > 0) {
	                        url += (url.indexOf("?") === -1 ? "?" : "&") + _params.join("&");
	                    }

	                    engine.open(options.method, url);

	                    // try catch for ie >=9
	                    try {
	                        engine.withCredentials = !!options.withCredentials;
	                        engine.timeout = options.timeout || 0;
	                        if (responseType !== "stream") {
	                            engine.responseType = responseType;
	                        }
	                    } catch (e) {}

	                    var customContentType = options.headers[contentType] || options.headers[contentTypeLowerCase];

	                    // default content type
	                    var _contentType = "application/x-www-form-urlencoded";
	                    // If the request data is json object, transforming it  to json string,
	                    // and set request content-type to "json". In browser,  the data will
	                    // be sent as RequestBody instead of FormData
	                    if (utils.trim((customContentType || "").toLowerCase()) === _contentType) {
	                        data = utils.formatParams(data);
	                    } else if (!utils.isFormData(data) && ["object", "array"].indexOf(utils.type(data)) !== -1) {
	                        _contentType = 'application/json;charset=utf-8';
	                        data = JSON.stringify(data);
	                    }
	                    //If user doesn't set content-type, set default.
	                    if (!(customContentType || needQuery)) {
	                        options.headers[contentType] = _contentType;
	                    }

	                    for (var k in options.headers) {
	                        if (k === contentType && utils.isFormData(data)) {
	                            // Delete the content-type, Let the browser set it
	                            delete options.headers[k];
	                        } else {
	                            try {
	                                // In browser environment, some header fields are readonly,
	                                // write will cause the exception .
	                                engine.setRequestHeader(k, options.headers[k]);
	                            } catch (e) {}
	                        }
	                    }

	                    function onresult(handler, data, type) {
	                        enqueueIfLocked(responseInterceptor.p, function () {
	                            if (handler) {
	                                //如果失败，添加请求信息
	                                if (type) {
	                                    data.request = options;
	                                }
	                                var ret = handler.call(responseInterceptor, data, Promise);
	                                data = ret === undefined ? data : ret;
	                            }
	                            if (!isPromise(data)) {
	                                data = Promise[type === 0 ? "resolve" : "reject"](data);
	                            }
	                            data.then(function (d) {
	                                resolve(d);
	                            }).catch(function (e) {
	                                reject(e);
	                            });
	                        });
	                    }

	                    function onerror(e) {
	                        e.engine = engine;
	                        onresult(responseInterceptor.onerror, e, -1);
	                    }

	                    function Err(msg, status) {
	                        this.message = msg;
	                        this.status = status;
	                    }

	                    engine.onload = function () {
	                        try {
	                            // The xhr of IE9 has not response field
	                            var response = engine.response || engine.responseText;
	                            if (response && options.parseJson && (engine.getResponseHeader(contentType) || "").indexOf("json") !== -1
	                            // Some third engine implementation may transform the response text to json object automatically,
	                            // so we should test the type of response before transforming it
	                            && !utils.isObject(response)) {
	                                response = JSON.parse(response);
	                            }

	                            var headers = engine.responseHeaders;
	                            // In browser
	                            if (!headers) {
	                                headers = {};
	                                var items = (engine.getAllResponseHeaders() || "").split("\r\n");
	                                items.pop();
	                                items.forEach(function (e) {
	                                    if (!e) { return; }
	                                    var key = e.split(":")[0];
	                                    headers[key] = engine.getResponseHeader(key);
	                                });
	                            }
	                            var status = engine.status;
	                            var statusText = engine.statusText;
	                            var _data = { data: response, headers: headers, status: status, statusText: statusText };
	                            // The _response filed of engine is set in  adapter which be called in engine-wrapper.js
	                            utils.merge(_data, engine._response);
	                            if (status >= 200 && status < 300 || status === 304) {
	                                _data.engine = engine;
	                                _data.request = options;
	                                onresult(responseInterceptor.handler, _data, 0);
	                            } else {
	                                var e = new Err(statusText, status);
	                                e.response = _data;
	                                onerror(e);
	                            }
	                        } catch (e) {
	                            onerror(new Err(e.msg, engine.status));
	                        }
	                    };

	                    engine.onerror = function (e) {
	                        onerror(new Err(e.msg || "Network Error", 0));
	                    };

	                    engine.ontimeout = function () {
	                        onerror(new Err("timeout [ " + engine.timeout + "ms ]", 1));
	                    };
	                    engine._options = options;
	                    setTimeout(function () {
	                        engine.send(needQuery ? null : data);
	                    }, 0);
	                }

	                enqueueIfLocked(requestInterceptor.p, function () {
	                    utils.merge(options, JSON.parse(JSON.stringify(_this.config)));
	                    var headers = options.headers;
	                    headers[contentType] = headers[contentType] || headers[contentTypeLowerCase] || "";
	                    delete headers[contentTypeLowerCase];
	                    options.body = data || options.body;
	                    url = utils.trim(url || "");
	                    options.method = options.method.toUpperCase();
	                    options.url = url;
	                    var ret = options;
	                    if (requestInterceptorHandler) {
	                        ret = requestInterceptorHandler.call(requestInterceptor, options, Promise) || options;
	                    }
	                    if (!isPromise(ret)) {
	                        ret = Promise.resolve(ret);
	                    }
	                    ret.then(function (d) {
	                        //if options continue
	                        if (d === options) {
	                            makeRequest(d);
	                        } else {
	                            resolve(d);
	                        }
	                    }, function (err) {
	                        reject(err);
	                    });
	                });
	            });
	            promise.engine = engine;
	            return promise;
	        }
	    }, {
	        key: "all",
	        value: function all(promises) {
	            return Promise.all(promises);
	        }
	    }, {
	        key: "spread",
	        value: function spread(callback) {
	            return function (arr) {
	                return callback.apply(null, arr);
	            };
	        }
	    }]);

	    return Fly;
	}();

	//For typeScript


	Fly.default = Fly;

	["get", "post", "put", "patch", "head", "delete"].forEach(function (e) {
	    Fly.prototype[e] = function (url, data, option) {
	        return this.request(url, data, utils.merge({ method: e }, option));
	    };
	});
	["lock", "unlock", "clear"].forEach(function (e) {
	    Fly.prototype[e] = function () {
	        this.interceptors.request[e]();
	    };
	});
	module.exports = Fly;

	/***/ })
	/******/ ]);
	});
	});

	var Fly = unwrapExports(fly);

	// import the module from a seprate module so we can change this easily

	var flyio = new Fly();

	// base HttpClass
	// extract the one we need
	var POST = API_REQUEST_METHODS[0];
	var PUT = API_REQUEST_METHODS[1];

	var HttpClass = function HttpClass(opts) {
	  this.fly = flyio;
	  this.opts = opts;
	  this.extraHeader = {};
	  // run the set debug
	  this.debug();
	  // console.info('start up opts', opts);
	  this.reqInterceptor();
	  this.resInterceptor();
	};

	var prototypeAccessors = { headers: { configurable: true } };

	// set headers for that one call
	prototypeAccessors.headers.set = function (header) {
	  this.extraHeader = header;
	};

	/**
	 * Create the reusage request method
	 * @param {object} payload jsonql payload
	 * @param {object} options extra options add the request
	 * @param {object} headers extra headers add to the call
	 * @return {object} the fly request instance
	 */
	HttpClass.prototype.request = function request (payload, options, headers) {
	    var obj;

	    if ( options === void 0 ) options = {};
	    if ( headers === void 0 ) headers = {};
	  this.headers = headers;
	  var params = cacheBurst();
	  // @TODO need to add a jsonp url and payload
	  if (this.opts.enableJsonp) {
	    var resolverName = getNameFromPayload$1(payload);
	    params = merge({}, params, ( obj = {}, obj[JSONP_CALLBACK_NAME] = resolverName, obj ));
	    payload = payload[resolverName];
	  }
	  return this.fly.request(
	    this.jsonqlEndpoint,
	    payload,
	    merge({}, {method: POST, params: params }, options)
	  )
	};

	/**
	 * This will replace the create baseRequest method
	 *
	 */
	HttpClass.prototype.reqInterceptor = function reqInterceptor () {
	    var this$1 = this;

	  this.fly.interceptors.request.use(
	    function (req) {
	      console.info('request interceptor call');
	      var headers = this$1.getHeaders();
	      for (var key in headers) {
	        req.headers[key] = headers[key];
	      }
	      return req;
	    }
	  );
	};

	// @TODO
	HttpClass.prototype.processJsonp = function processJsonp (result) {
	  return resultHandler(result)
	};

	/**
	 * This will be replacement of the first then call
	 *
	 */
	HttpClass.prototype.resInterceptor = function resInterceptor () {
	  var self = this;
	  var jsonp = self.opts.enableJsonp;
	  this.fly.interceptors.response.use(
	    function (res) {
	      console.info('response interceptor call');
	      self.cleanUp();
	      // now more processing here
	      // there is a problem if we throw the result.error here
	      // the original data is lost, so we need to do what we did before
	      // deal with that error in the first then instead
	      var result = isString$1(res.data) ? JSON.parse(res.data) : res.data;
	      if (jsonp) {
	        return self.processJsonp(result)
	      }
	      return resultHandler(result)
	    },
	    // this get call when it's not 200
	    function (err) {
	      self.cleanUp();
	      throw new JsonqlServerError('Server side error', err)
	    }
	  );
	};

	/**
	 * Get the headers inject into the call
	 * @return {object} headers
	 */
	HttpClass.prototype.getHeaders = function getHeaders () {
	  if (this.opts.enableAuth) {
	    return merge({}, DEFAULT_HEADER, this.getAuthHeader(), this.extraHeader)
	  }
	  return merge({}, DEFAULT_HEADER, this.extraHeader)
	};

	/**
	 * Post http call operation to clean up things we need
	 */
	HttpClass.prototype.cleanUp = function cleanUp () {
	  this.extraHeader = {};
	};

	/**
	 * GET for contract only
	 */
	HttpClass.prototype.get = function get () {
	  return this.request({}, {method: 'GET'}, this.contractHeader)
	    .then(clientErrorsHandler)
	    .then(function (result) {
	      console.info('get contract result', result);
	      return result
	    })
	};

	 /**
	* POST to server - query
	* @param {object} name of the resolver
	* @param {array} args arguments
	* @return {object} promise resolve to the resolver return
	*/
	 HttpClass.prototype.query = function query (name, args) {
	   if ( args === void 0 ) args = [];

	 return this.request(createQuery$1(name, args))
	  .then(clientErrorsHandler)
	 };

	 /**
	* PUT to server - mutation
	* @param {string} name of resolver
	* @param {object} payload what it said
	* @param {object} conditions what it said
	* @return {object} promise resolve to the resolver return
	*/
	 HttpClass.prototype.mutation = function mutation (name, payload, conditions) {
	   if ( payload === void 0 ) payload = {};
	   if ( conditions === void 0 ) conditions = {};

	 return this.request(createMutation$1(name, payload, conditions), {method: PUT})
	  .then(clientErrorsHandler)
	 };

	Object.defineProperties( HttpClass.prototype, prototypeAccessors );

	// all the contract related methods will be here

	var ContractClass = /*@__PURE__*/(function (HttpClass) {
	  function ContractClass(opts) {
	    HttpClass.call(this, opts);
	  }

	  if ( HttpClass ) ContractClass.__proto__ = HttpClass;
	  ContractClass.prototype = Object.create( HttpClass && HttpClass.prototype );
	  ContractClass.prototype.constructor = ContractClass;

	  var prototypeAccessors = { contractHeader: { configurable: true } };

	  /**
	   * Set if we want to debug or not
	   * @return {undefined} nothing
	   */
	  ContractClass.prototype.debug = function debug () {
	    var key = 'debug';
	    if (this.opts.debugOn) {
	      localStore$1.set(key, 'jsonql-client*');
	    } else {
	      localStore$1.remove(key);
	    }
	  };

	  /**
	   * return the contract public api
	   * @return {object} contract
	   */
	  ContractClass.prototype.getContract = function getContract () {
	    var contract = this.readContract();
	    if (contract) {
	      return Promise.resolve(contract)
	    }
	    return this.get()
	      .then( this.storeContract.bind(this) )
	  };

	  /**
	   * We are changing the way how to auth to get the contract.json
	   * Instead of in the url, we will be putting that key value in the header
	   * @return {object} header
	   */
	  prototypeAccessors.contractHeader.get = function () {
	    var base = {};
	    if (this.opts.contractKey !== false) {
	      base[this.opts.contractKeyName] = this.opts.contractKey;
	    }
	    return base;
	  };

	  /**
	   * Save the contract to local store
	   * @param {object} contract to save
	   * @return {object|boolean} false when its not a contract or contract on OK
	   */
	  ContractClass.prototype.storeContract = function storeContract (contract) {
	    // first need to check if the contract is a contract
	    if (!isJsonqlContract(contract)) {
	      return false;
	    }
	    if (this.opts.contractExpired) {
	      var expired = parseFloat(this.opts.contractExpired);
	    }
	    this.jsonqlContract = contract;
	    return contract;
	  };

	  /**
	   * return the contract from options or localStore
	   * @return {object} contract
	   */
	  ContractClass.prototype.readContract = function readContract () {
	    var contract = isJsonqlContract(this.opts.contract);
	    return contract ? this.opts.contract : localStore$1.get(this.opts.storageKey)
	  };

	  Object.defineProperties( ContractClass.prototype, prototypeAccessors );

	  return ContractClass;
	}(HttpClass));

	// this is the new auth class that integrate with the jsonql-jwt
	// export
	var AuthClass = /*@__PURE__*/(function (ContractClass) {
	  function AuthClass(opts) {
	    ContractClass.call(this, opts);
	    if (opts.enableAuth && opts.useJwt) {
	      this.setDecoder = jwtDecode;
	    }
	  }

	  if ( ContractClass ) AuthClass.__proto__ = ContractClass;
	  AuthClass.prototype = Object.create( ContractClass && ContractClass.prototype );
	  AuthClass.prototype.constructor = AuthClass;

	  var prototypeAccessors = { userdata: { configurable: true },rawAuthToken: { configurable: true },setDecoder: { configurable: true } };

	  /**
	   * Getter to get the login userdata
	   * @return {mixed} userdata
	   */
	  prototypeAccessors.userdata.get = function () {
	    return this.jsonqlUserdata; // see base-cls
	  };

	  /**
	   * Return the token from session store
	   * @return {string} token
	   */
	  prototypeAccessors.rawAuthToken.get = function () {
	    // this should return from the base
	    return this.jsonqlToken; // see base-cls
	  };

	  /**
	   * Setter to add a decoder when retrieve user token
	   * @param {function} d a decoder
	   */
	  prototypeAccessors.setDecoder.set = function (d) {
	    if (typeof d === 'function') {
	      this.decoder = d;
	    }
	  };

	  /**
	   * Setter after login success
	   * @TODO this move to a new class to handle multiple login
	   * @param {string} token to store
	   * @return {*} success store
	   */
	  AuthClass.prototype.storeToken = function storeToken (token) {
	    return this.jsonqlToken = token;
	  };

	  /**
	   * for overwrite
	   * @param {string} token stored token
	   * @return {string} token
	   */
	  AuthClass.prototype.decoder = function decoder (token) {
	    return token;
	  };

	  /**
	   * Construct the auth header
	   * @return {object} header
	   */
	  AuthClass.prototype.getAuthHeader = function getAuthHeader () {
	    var obj;

	    var token = this.rawAuthToken;
	    return token ? ( obj = {}, obj[this.opts.AUTH_HEADER] = (BEARER + " " + token), obj ) : {};
	  };

	  Object.defineProperties( AuthClass.prototype, prototypeAccessors );

	  return AuthClass;
	}(ContractClass));

	// this the core of the internal storage management
	// This class will only focus on the storage system
	var JsonqlBaseClient = /*@__PURE__*/(function (AuthCls) {
	  function JsonqlBaseClient(opts) {
	    AuthCls.call(this, opts);
	  }

	  if ( AuthCls ) JsonqlBaseClient.__proto__ = AuthCls;
	  JsonqlBaseClient.prototype = Object.create( AuthCls && AuthCls.prototype );
	  JsonqlBaseClient.prototype.constructor = JsonqlBaseClient;

	  var prototypeAccessors = { storeIt: { configurable: true },jsonqlEndpoint: { configurable: true },jsonqlContract: { configurable: true },jsonqlToken: { configurable: true },jsonqlUserdata: { configurable: true } };

	  // @TODO
	  prototypeAccessors.storeIt.set = function (args) {
	    // the args MUST contain [0] the key , [1] the content [2] optional expired in
	    if (isArray$1(args) && args.length >= 2) {
	      Reflect.apply(localStore$1.set, localStore$1, args);
	    }
	    throw new JsonqlValidationError("Expect argument to be array and least 2 items!")
	  };

	  // this table index key will drive the contract
	  // also it should not allow to change dynamicly
	  // because this is how different client can id itself
	  // OK this could be self manage because when init a new client
	  // it will add a new endpoint and we will know if they are the same or not
	  // but the check here
	  prototypeAccessors.jsonqlEndpoint.set = function (endpoint) {
	    var urls = localStore$1.get(ENDPOINT_TABLE) || [];
	    // should check if this url already existed?
	    if (!inArray$1(urls, endpoint)) {
	      urls.push(endpoint);
	      this.storeId = [ENDPOINT_TABLE, urls];
	      this[ENDPOINT_TABLE + 'Index'] = urls.length - 1;
	    }
	  };

	  // by the time it call the save contract already been checked
	  prototypeAccessors.jsonqlContract.set = function (args) {
	    var key = this.opts.storageKey;
	    var contract = args[0];
	    var expired = args[1];
	    // get the endpoint index
	    var contracts = localStore$1.get(key) || [];
	    constracts[ this[ENDPOINT_TABLE + 'Index'] || 0 ] = contract;
	    var _args = [key].push(contracts);
	    if (expired) {
	      _args.push(expired);
	    }
	    this.storeIt = _args;
	  };

	  /**
	   * save token
	   * @param {string} token to store
	   * @return {string|boolean} false on failed
	   */
	  prototypeAccessors.jsonqlToken.set = function (token) {
	    var key = CREDENTIAL_STORAGE_KEY;
	    var tokens = localStorage.get(key) || [];
	    if (!inArray$1(tokens, token)) {
	      var index = tokens.length - 1;
	      tokens[ index ] = token;
	      this[key + 'Index'] = index;
	      var args = [key, tokens];
	      if (this.opts.tokenExpired) {
	        var expired = parseFloat(this.opts.tokenExpired);
	        if (!isNaN(expired) && expired > 0) {
	          var ts = timestamp();
	          args.push( ts + parseFloat(expired) );
	        }
	      }
	      this.storeIt = args;
	      // now decode it and store in the userdata
	      this.jsonqlUserdata = this.decoder(token);
	      return token;
	    }
	    return false;
	  };
	  // this one will use the sessionStore
	  // basically we hook this onto the token store and decode it to store here
	  prototypeAccessors.jsonqlUserdata.set = function (userdata) {
	    var args = [USERDATA_TABLE, userdata];
	    if (userdata.exp) {
	      args.push(userdata.exp);
	    }
	    return Reflect.apply(localStore$1.set, localStore$1, args)
	  };

	  // bunch of getters

	  /**
	   * This also manage the index internally
	   * @return {string} the end point to call
	   */
	  prototypeAccessors.jsonqlEndpoint.get = function () {
	    var urls = localStore$1.get(ENDPOINT_TABLE);
	    if (!urls) {
	      var ref = this.opts;
	      var hostname = ref.hostname;
	      var jsonqlPath = ref.jsonqlPath;
	      var url = [hostname, jsonqlPath].join('/');
	      this.jsonqlEndpoint = url;
	      return url;
	    }
	    return urls[this[ENDPOINT_TABLE + 'Index']]
	  };

	  /**
	   * If already stored then return it by the end point index
	   * or false when there is none
	   * @return {object|boolean} as described above
	   */
	  prototypeAccessors.jsonqlContract.get = function () {
	    var key = this.opts.storageKey;
	    var contracts = localStore$1.get(key) || [];
	    return contracts[ this[ENDPOINT_TABLE + 'Index'] ] || false;
	  };

	  prototypeAccessors.jsonqlToken.get = function () {
	    var key = CREDENTIAL_STORAGE_KEY;
	    var tokens = localStorage.get(key);
	    if (tokens) {
	      return tokens[ this[key + 'Index'] ]
	    }
	    return false;
	  };
	  // this one store in the session store
	  prototypeAccessors.jsonqlUserdata.get = function () {
	    return sessionStore$1.get(USERDATA_TABLE)
	  };

	  Object.defineProperties( JsonqlBaseClient.prototype, prototypeAccessors );

	  return JsonqlBaseClient;
	}(AuthClass));

	// export interface

	var NB_EVENT_SERVICE_PRIVATE_STORE = new WeakMap();
	var NB_EVENT_SERVICE_PRIVATE_LAZY = new WeakMap();

	/**
	 * generate a 32bit hash based on the function.toString()
	 * _from http://stackoverflow.com/questions/7616461/generate-a-hash-_from-string-in-javascript-jquery
	 * @param {string} s the converted to string function
	 * @return {string} the hashed function string
	 */
	function hashCode(s) {
		return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
	}

	// this is the new implementation without the hash key
	// export
	var EventService = function EventService(config) {
	  if ( config === void 0 ) config = {};

	  if (config.logger && typeof config.logger === 'function') {
	    this.logger = config.logger;
	  }
	  this.keep = config.keep;
	  // for the $done setter
	  this.result = config.keep ? [] : null;
	  // we need to init the store first otherwise it could be a lot of checking later
	  this.normalStore = new Map();
	  this.lazyStore = new Map();
	};

	var prototypeAccessors$1 = { $done: { configurable: true },normalStore: { configurable: true },lazyStore: { configurable: true } };

	/**
	 * logger function for overwrite
	 */
	EventService.prototype.logger = function logger () {};

	//////////////////////////
	//  PUBLIC METHODS  //
	//////////////////////////

	/**
	 * Register your evt handler, note we don't check the type here,
	 * we expect you to be sensible and know what you are doing.
	 * @param {string} evt name of event
	 * @param {function} callback bind method --> if it's array or not
	 * @param {object} [context=null] to execute this call in
	 * @return {number} the size of the store
	 */
	EventService.prototype.$on = function $on (evt , callback , context) {
	    var this$1 = this;
	    if ( context === void 0 ) context = null;

	  var type = 'on';
	  this.validate(evt, callback);
	  // first need to check if this evt is in lazy store
	  var lazyStoreContent = this.takeFromStore(evt);
	  // this is normal register first then call later
	  if (lazyStoreContent === false) {
	    this.logger('$on', (evt + " callback is not in lazy store"));
	    // @TODO we need to check if there was other listener to this
	    // event and are they the same type then we could solve that
	    // register the different type to the same event name

	    return this.addToNormalStore(evt, type, callback, context)
	  }
	  this.logger('$on', (evt + " found in lazy store"));
	  // this is when they call $trigger before register this callback
	  var size = 0;
	  lazyStoreContent.forEach(function (content) {
	    var payload = content[0];
	      var ctx = content[1];
	      var t = content[2];
	    if (t && t !== type) {
	      throw new Error(("You are trying to register an event already been taken by other type: " + t))
	    }
	    this$1.run(callback, payload, context || ctx);
	    size += this$1.addToNormalStore(evt, type, callback, context || ctx);
	  });
	  return size;
	};

	/**
	 * once only registered it once, there is no overwrite option here
	 * @NOTE change in v1.3.0 $once can add multiple listeners
	 *     but once the event fired, it will remove this event (see $only)
	 * @param {string} evt name
	 * @param {function} callback to execute
	 * @param {object} [context=null] the handler execute in
	 * @return {boolean} result
	 */
	EventService.prototype.$once = function $once (evt , callback , context) {
	    if ( context === void 0 ) context = null;

	  this.validate(evt, callback);
	  var type = 'once';
	  var lazyStoreContent = this.takeFromStore(evt);
	  // this is normal register before call $trigger
	  var nStore = this.normalStore;
	  if (lazyStoreContent === false) {
	    this.logger('$once', (evt + " not in the lazy store"));
	    // v1.3.0 $once now allow to add multiple listeners
	    return this.addToNormalStore(evt, type, callback, context)
	  } else {
	    // now this is the tricky bit
	    // there is a potential bug here that cause by the developer
	    // if they call $trigger first, the lazy won't know it's a once call
	    // so if in the middle they register any call with the same evt name
	    // then this $once call will be fucked - add this to the documentation
	    this.logger('$once', lazyStoreContent);
	    var list = Array.from(lazyStoreContent);
	    // should never have more than 1
	    var ref = list[0];
	      var payload = ref[0];
	      var ctx = ref[1];
	      var t = ref[2];
	    if (t && t !== type) {
	      throw new Error(("You are trying to register an event already been taken by other type: " + t))
	    }
	    this.run(callback, payload, context || ctx);
	    // remove this evt from store
	    this.$off(evt);
	  }
	};

	/**
	 * This one event can only bind one callbackback
	 * @param {string} evt event name
	 * @param {function} callback event handler
	 * @param {object} [context=null] the context the event handler execute in
	 * @return {boolean} true bind for first time, false already existed
	 */
	EventService.prototype.$only = function $only (evt, callback, context) {
	    var this$1 = this;
	    if ( context === void 0 ) context = null;

	  this.validate(evt, callback);
	  var type = 'only';
	  var added = false;
	  var lazyStoreContent = this.takeFromStore(evt);
	  // this is normal register before call $trigger
	  var nStore = this.normalStore;
	  if (!nStore.has(evt)) {
	    this.logger("$only", (evt + " add to store"));
	    added = this.addToNormalStore(evt, type, callback, context);
	  }
	  if (lazyStoreContent !== false) {
	    // there are data store in lazy store
	    this.logger('$only', (evt + " found data in lazy store to execute"));
	    var list = Array.from(lazyStoreContent);
	    // $only allow to trigger this multiple time on the single handler
	    list.forEach( function (l) {
	      var payload = l[0];
	        var ctx = l[1];
	        var t = l[2];
	      if (t && t !== type) {
	        throw new Error(("You are trying to register an event already been taken by other type: " + t))
	      }
	      this$1.run(callback, payload, context || ctx);
	    });
	  }
	  return added;
	};

	/**
	 * $only + $once this is because I found a very subtile bug when we pass a
	 * resolver, rejecter - and it never fire because that's OLD adeed in v1.4.0
	 * @param {string} evt event name
	 * @param {function} callback to call later
	 * @param {object} [context=null] exeucte context
	 * @return {void}
	 */
	EventService.prototype.$onlyOnce = function $onlyOnce (evt, callback, context) {
	    if ( context === void 0 ) context = null;

	  this.validate(evt, callback);
	  var type = 'onlyOnce';
	  var added = false;
	  var lazyStoreContent = this.takeFromStore(evt);
	  // this is normal register before call $trigger
	  var nStore = this.normalStore;
	  if (!nStore.has(evt)) {
	    this.logger("$onlyOnce", (evt + " add to store"));
	    added = this.addToNormalStore(evt, type, callback, context);
	  }
	  if (lazyStoreContent !== false) {
	    // there are data store in lazy store
	    this.logger('$onlyOnce', lazyStoreContent);
	    var list = Array.from(lazyStoreContent);
	    // should never have more than 1
	    var ref = list[0];
	      var payload = ref[0];
	      var ctx = ref[1];
	      var t = ref[2];
	    if (t && t !== 'onlyOnce') {
	      throw new Error(("You are trying to register an event already been taken by other type: " + t))
	    }
	    this.run(callback, payload, context || ctx);
	    // remove this evt from store
	    this.$off(evt);
	  }
	  return added;
	};

	/**
	 * This is a shorthand of $off + $on added in V1.5.0
	 * @param {string} evt event name
	 * @param {function} callback to exeucte
	 * @param {object} [context = null] or pass a string as type
	 * @param {string} [type=on] what type of method to replace
	 * @return {}
	 */
	EventService.prototype.$replace = function $replace (evt, callback, context, type) {
	    if ( context === void 0 ) context = null;
	    if ( type === void 0 ) type = 'on';

	  if (this.validateType(type)) {
	    this.$off(evt);
	    var method = this['$' + type];
	    return Reflect.apply(method, this, [evt, callback, context])
	  }
	  throw new Error((type + " is not supported!"))
	};

	/**
	 * trigger the event
	 * @param {string} evt name NOT allow array anymore!
	 * @param {mixed} [payload = []] pass to fn
	 * @param {object|string} [context = null] overwrite what stored
	 * @param {string} [type=false] if pass this then we need to add type to store too
	 * @return {number} if it has been execute how many times
	 */
	EventService.prototype.$trigger = function $trigger (evt , payload , context, type) {
	    if ( payload === void 0 ) payload = [];
	    if ( context === void 0 ) context = null;
	    if ( type === void 0 ) type = false;

	  this.validateEvt(evt);
	  var found = 0;
	  // first check the normal store
	  var nStore = this.normalStore;
	  this.logger('$trigger', nStore);
	  if (nStore.has(evt)) {
	    this.logger('$trigger', evt, 'found');
	    var nSet = Array.from(nStore.get(evt));
	    var ctn = nSet.length;
	    var hasOnce = false;
	    for (var i=0; i < ctn; ++i) {
	      ++found;
	      // this.logger('found', found)
	      var ref = nSet[i];
	        var _ = ref[0];
	        var callback = ref[1];
	        var ctx = ref[2];
	        var type$1 = ref[3];
	      this.run(callback, payload, context || ctx);
	      if (type$1 === 'once' || type$1 === 'onlyOnce') {
	        hasOnce = true;
	      }
	    }
	    if (hasOnce) {
	      nStore.delete(evt);
	    }
	    return found;
	  }
	  // now this is not register yet
	  this.addToLazyStore(evt, payload, context, type);
	  return found;
	};

	/**
	 * this is an alias to the $trigger
	 * @NOTE breaking change in V1.6.0 we swap the parameter around
	 * @param {string} evt event name
	 * @param {*} params pass to the callback
	 * @param {string} type of call
	 * @param {object} context what context callback execute in
	 * @return {*} from $trigger
	 */
	EventService.prototype.$call = function $call (evt, params, type, context) {
	    if ( type === void 0 ) type = false;
	    if ( context === void 0 ) context = null;

	  var args = [evt, params];
	  args.push(context, type);
	  return Reflect.apply(this.$trigger, this, args)
	};

	/**
	 * remove the evt from all the stores
	 * @param {string} evt name
	 * @return {boolean} true actually delete something
	 */
	EventService.prototype.$off = function $off (evt) {
	  this.validateEvt(evt);
	  var stores = [ this.lazyStore, this.normalStore ];
	  var found = false;
	  stores.forEach(function (store) {
	    if (store.has(evt)) {
	      found = true;
	      store.delete(evt);
	    }
	  });
	  return found;
	};

	/**
	 * return all the listener from the event
	 * @param {string} evtName event name
	 * @param {boolean} [full=false] if true then return the entire content
	 * @return {array|boolean} listerner(s) or false when not found
	 */
	EventService.prototype.$get = function $get (evt, full) {
	    if ( full === void 0 ) full = false;

	  this.validateEvt(evt);
	  var store = this.normalStore;
	  if (store.has(evt)) {
	    return Array
	      .from(store.get(evt))
	      .map( function (l) {
	        if (full) {
	          return l;
	        }
	        var key = l[0];
	          var callback = l[1];
	        return callback;
	      })
	  }
	  return false;
	};

	/**
	 * store the return result from the run
	 * @param {*} value whatever return from callback
	 */
	prototypeAccessors$1.$done.set = function (value) {
	  this.logger('set $done', value);
	  if (this.keep) {
	    this.result.push(value);
	  } else {
	    this.result = value;
	  }
	};

	/**
	 * @TODO is there any real use with the keep prop?
	 * getter for $done
	 * @return {*} whatever last store result
	 */
	prototypeAccessors$1.$done.get = function () {
	  if (this.keep) {
	    this.logger(this.result);
	    return this.result[this.result.length - 1]
	  }
	  return this.result;
	};

	/////////////////////////////
	//  PRIVATE METHODS    //
	/////////////////////////////

	/**
	 * validate the event name
	 * @param {string} evt event name
	 * @return {boolean} true when OK
	 */
	EventService.prototype.validateEvt = function validateEvt (evt) {
	  if (typeof evt === 'string') {
	    return true;
	  }
	  throw new Error("event name must be string type!")
	};

	/**
	 * Simple quick check on the two main parameters
	 * @param {string} evt event name
	 * @param {function} callback function to call
	 * @return {boolean} true when OK
	 */
	EventService.prototype.validate = function validate (evt, callback) {
	  if (this.validateEvt(evt)) {
	    if (typeof callback === 'function') {
	      return true;
	    }
	  }
	  throw new Error("callback required to be function type!")
	};

	/**
	 * Check if this type is correct or not added in V1.5.0
	 * @param {string} type for checking
	 * @return {boolean} true on OK
	 */
	EventService.prototype.validateType = function validateType (type) {
	  var types = ['on', 'only', 'once', 'onlyOnce'];
	  return !!types.filter(function (t) { return type === t; }).length;
	};

	/**
	 * Run the callback
	 * @param {function} callback function to execute
	 * @param {array} payload for callback
	 * @param {object} ctx context or null
	 * @return {void} the result store in $done
	 */
	EventService.prototype.run = function run (callback, payload, ctx) {
	  this.logger('run', callback, payload, ctx);
	  this.$done = Reflect.apply(callback, ctx, this.toArray(payload));
	};

	/**
	 * Take the content out and remove it from store id by the name
	 * @param {string} evt event name
	 * @param {string} [storeName = lazyStore] name of store
	 * @return {object|boolean} content or false on not found
	 */
	EventService.prototype.takeFromStore = function takeFromStore (evt, storeName) {
	    if ( storeName === void 0 ) storeName = 'lazyStore';

	  var store = this[storeName]; // it could be empty at this point
	  if (store) {
	    this.logger('takeFromStore', storeName, store);
	    if (store.has(evt)) {
	      var content = store.get(evt);
	      this.logger('takeFromStore', content);
	      store.delete(evt);
	      return content;
	    }
	    return false;
	  }
	  throw new Error((storeName + " is not supported!"))
	};

	/**
	 * The add to store step is similar so make it generic for resuse
	 * @param {object} store which store to use
	 * @param {string} evt event name
	 * @param {spread} args because the lazy store and normal store store different things
	 * @return {array} store and the size of the store
	 */
	EventService.prototype.addToStore = function addToStore (store, evt) {
	    var args = [], len = arguments.length - 2;
	    while ( len-- > 0 ) args[ len ] = arguments[ len + 2 ];

	  var fnSet;
	  if (store.has(evt)) {
	    this.logger('addToStore', (evt + " existed"));
	    fnSet = store.get(evt);
	  } else {
	    this.logger('addToStore', ("create new Set for " + evt));
	    // this is new
	    fnSet = new Set();
	  }
	  // lazy only store 2 items - this is not the case in V1.6.0 anymore
	  // we need to check the first parameter is string or not
	  if (args.length > 2) {
	    if (Array.isArray(args[0])) { // lazy store
	      // check if this type of this event already register in the lazy store
	      var t = args[2];
	      if (!this.checkTypeInLazyStore(evt, t)) {
	        fnSet.add(args);
	      }
	    } else {
	      if (!this.checkContentExist(args, fnSet)) {
	        this.logger('addToStore', "insert new", args);
	        fnSet.add(args);
	      }
	    }
	  } else { // add straight to lazy store
	    fnSet.add(args);
	  }
	  store.set(evt, fnSet);
	  return [store, fnSet.size]
	};

	/**
	 * @param {array} args for compare
	 * @param {object} fnSet A Set to search from
	 * @return {boolean} true on exist
	 */
	EventService.prototype.checkContentExist = function checkContentExist (args, fnSet) {
	  var list = Array.from(fnSet);
	  return !!list.filter(function (l) {
	    var hash = l[0];
	    if (hash === args[0]) {
	      return true;
	    }
	    return false;
	  }).length;
	};

	/**
	 * get the existing type to make sure no mix type add to the same store
	 * @param {string} evtName event name
	 * @param {string} type the type to check
	 * @return {boolean} true you can add, false then you can't add this type
	 */
	EventService.prototype.checkTypeInStore = function checkTypeInStore (evtName, type) {
	  this.validateEvt(evtName);
	  this.validateEvt(type);
	  var all = this.$get(evtName, true);
	  if (all === false) {
	     // pristine it means you can add
	    return true;
	  }
	  // it should only have ONE type in ONE event store
	  return !all.filter(function (list) {
	    var t = list[3];
	    return type !== t;
	  }).length;
	};

	/**
	 * This is checking just the lazy store because the structure is different
	 * therefore we need to use a new method to check it
	 */
	EventService.prototype.checkTypeInLazyStore = function checkTypeInLazyStore (evtName, type) {
	  this.validateEvt(evtName);
	  this.validateEvt(type);
	  var store = this.lazyStore.get(evtName);
	  this.logger('checkTypeInLazyStore', store);
	  if (store) {
	    return !!Array
	      .from(store)
	      .filter(function (l) {
	        var t = l[2];
	        return t !== type;
	      }).length
	  }
	  return false;
	};

	/**
	 * wrapper to re-use the addToStore,
	 * V1.3.0 add extra check to see if this type can add to this evt
	 * @param {string} evt event name
	 * @param {string} type on or once
	 * @param {function} callback function
	 * @param {object} context the context the function execute in or null
	 * @return {number} size of the store
	 */
	EventService.prototype.addToNormalStore = function addToNormalStore (evt, type, callback, context) {
	    if ( context === void 0 ) context = null;

	  this.logger('addToNormalStore', evt, type, 'add to normal store');
	  // @TODO we need to check the existing store for the type first!
	  if (this.checkTypeInStore(evt, type)) {
	    this.logger((type + " can add to " + evt + " store"));
	    var key = this.hashFnToKey(callback);
	    var args = [this.normalStore, evt, key, callback, context, type];
	    var ref = Reflect.apply(this.addToStore, this, args);
	      var _store = ref[0];
	      var size = ref[1];
	    this.normalStore = _store;
	    return size;
	  }
	  return false;
	};

	/**
	 * Add to lazy store this get calls when the callback is not register yet
	 * so we only get a payload object or even nothing
	 * @param {string} evt event name
	 * @param {array} payload of arguments or empty if there is none
	 * @param {object} [context=null] the context the callback execute in
	 * @param {string} [type=false] register a type so no other type can add to this evt
	 * @return {number} size of the store
	 */
	EventService.prototype.addToLazyStore = function addToLazyStore (evt, payload, context, type) {
	    if ( payload === void 0 ) payload = [];
	    if ( context === void 0 ) context = null;
	    if ( type === void 0 ) type = false;

	  // this is add in V1.6.0
	  // when there is type then we will need to check if this already added in lazy store
	  // and no other type can add to this lazy store
	  var args = [this.lazyStore, evt, this.toArray(payload), context];
	  if (type) {
	    args.push(type);
	  }
	  var ref = Reflect.apply(this.addToStore, this, args);
	    var _store = ref[0];
	    var size = ref[1];
	  this.lazyStore = _store;
	  return size;
	};

	/**
	 * make sure we store the argument correctly
	 * @param {*} arg could be array
	 * @return {array} make sured
	 */
	EventService.prototype.toArray = function toArray (arg) {
	  return Array.isArray(arg) ? arg : [arg];
	};

	/**
	 * setter to store the Set in private
	 * @param {object} obj a Set
	 */
	prototypeAccessors$1.normalStore.set = function (obj) {
	  NB_EVENT_SERVICE_PRIVATE_STORE.set(this, obj);
	};

	/**
	 * @return {object} Set object
	 */
	prototypeAccessors$1.normalStore.get = function () {
	  return NB_EVENT_SERVICE_PRIVATE_STORE.get(this)
	};

	/**
	 * setter to store the Set in lazy store
	 * @param {object} obj a Set
	 */
	prototypeAccessors$1.lazyStore.set = function (obj) {
	  NB_EVENT_SERVICE_PRIVATE_LAZY.set(this , obj);
	};

	/**
	 * @return {object} the lazy store Set
	 */
	prototypeAccessors$1.lazyStore.get = function () {
	  return NB_EVENT_SERVICE_PRIVATE_LAZY.get(this)
	};

	/**
	 * generate a hashKey to identify the function call
	 * The build-in store some how could store the same values!
	 * @param {function} fn the converted to string function
	 * @return {string} hashKey
	 */
	EventService.prototype.hashFnToKey = function hashFnToKey (fn) {
	  return hashCode(fn.toString()) + '';
	};

	Object.defineProperties( EventService.prototype, prototypeAccessors$1 );

	// default

	// this will generate a event emitter and will be use everywhere

	var ee = new EventService();

	// Generate the resolver for developer to use

	/**
	 * generate authorisation specific methods
	 * @param {object} jsonqlInstance instance of this
	 * @param {string} name of method
	 * @param {object} opts configuration
	 * @param {object} contract to match
	 * @return {function} for use
	 */
	var authMethodGenerator = function (jsonqlInstance, name, opts, contract) {
	  return function () {
	    var args = [], len = arguments.length;
	    while ( len-- ) args[ len ] = arguments[ len ];

	    var params = contract.auth[name].params;
	    var values = params.map(function (p, i) { return args[i]; });
	    var header = args[params.length] || {};
	    return validateAsync$1(args, params)
	      .then(function () { return jsonqlInstance
	          .query
	          .apply(jsonqlInstance, [name, values, header]); }
	      )
	      .catch(finalCatch)
	  }
	};

	/**
	 * @param {object} jsonqlInstance jsonql class instance
	 * @param {object} config options
	 * @param {object} contract the contract
	 * @return {object} constructed functions call
	 */
	var generator = function (jsonqlInstance, config, contract) {

	  var obj = {query: {}, mutation: {}, auth: {}};
	  // process the query first
	  var loop = function ( queryFn ) {
	    // to keep it clean we use a param to id the auth method
	    // const fn = (_contract.query[queryFn].auth === true) ? 'auth' : queryFn;
	    // generate the query method
	    obj.query[queryFn] = function () {
	      var args = [], len = arguments.length;
	      while ( len-- ) args[ len ] = arguments[ len ];

	      var params = contract.query[queryFn].params;
	      var _args = params.map(function (param, i) { return args[i]; });
	      // debug('query', queryFn, _params);
	      // @TODO this need to change
	      // the +1 parameter is the extra headers we want to pass
	      var header = args[params.length] || {};
	      // @TODO validate against the type
	      return validateAsync$1(_args, params)
	        .then(function () { return jsonqlInstance
	            .query
	            .apply(jsonqlInstance, [queryFn, _args, header]); }
	        )
	        .catch(finalCatch)
	    };
	  };

	  for (var queryFn in contract.query) loop( queryFn );
	  // process the mutation, the reason the mutation has a fixed number of parameters
	  // there is only the payload, and conditions parameters
	  // plus a header at the end
	  var loop$1 = function ( mutationFn ) {
	    obj.mutation[mutationFn] = function (payload, conditions, header) {
	      if ( header === void 0 ) header = {};

	      var args = [payload, conditions];
	      var params = contract.mutation[mutationFn].params;
	      return validateAsync$1(args, params)
	        .then(function () { return jsonqlInstance
	            .mutation
	            .apply(jsonqlInstance, [mutationFn, payload, conditions, header]); }
	        )
	        .catch(finalCatch)
	    };
	  };

	  for (var mutationFn in contract.mutation) loop$1( mutationFn );
	  // there is only one call issuer we want here
	  if (config.enableAuth && contract.auth) {
	    var loginHandlerName = config.loginHandlerName;
	    var logoutHandlerName = config.logoutHandlerName;
	    if (contract.auth[loginHandlerName]) {
	      // changing to the name the config specify
	      obj[loginHandlerName] = function () {
	        var args = [], len = arguments.length;
	        while ( len-- ) args[ len ] = arguments[ len ];

	        var fn = authMethodGenerator(jsonqlInstance, loginHandlerName, config, contract);
	        return fn.apply(null, args)
	          .then(jsonqlInstance.postLoginAction)
	          .then(function (token) {
	            ee.emitEvent(ISSUER_NAME, token);
	            return token;
	          })
	      };
	    }
	    if (contract.auth[logoutHandlerName]) {
	      obj[logoutHandlerName] = function () {
	        var args = [], len = arguments.length;
	        while ( len-- ) args[ len ] = arguments[ len ];

	        var fn = authMethodGenerator(jsonqlInstance, logoutHandlerName, config, contract);
	        return fn.apply(null, args)
	          .then(jsonqlInstance.postLogoutAction)
	          .then(function (r) {
	            ee.emitEvent(LOGOUT_NAME, r);
	            return r;
	          })
	      };
	    } else {
	      obj[logoutHandlerName] = function () {
	        jsonqlInstance.postLogoutAction(KEY_WORD);
	        ee.emitEvent(LOGOUT_NAME, KEY_WORD);
	      };
	    }
	    /**
	     * new method to allow retrieve the current login user data
	     * @return {*} userdata
	     */
	    obj.userdata = function () { return jsonqlInstance.userdata; };
	  }
	  // store this once again and export it
	  if (obj.returnInstance) {
	    obj.jsonqlClientInstance = jsonqlInstance;
	  }
	  // allow getting the token for valdiate agains the socket
	  obj.getToken = function () { return jsonqlInstance.rawAuthToken; };
	  // this will pass to the ws-client if needed
	  obj.eventEmitter = ee;
	  // output
	  return obj;
	};

	var constProps = {
	  contract: false,
	  MUTATION_ARGS: ['name', 'payload', 'conditions'],
	  CONTENT_TYPE: CONTENT_TYPE,
	  BEARER: BEARER,
	  AUTH_HEADER: AUTH_HEADER
	};

	// grab the localhost name and put into the hostname as default
	var getHostName = function () { return (
	  [window.location.protocol, window.location.host].join('//')
	); };

	var appProps$1 = {

	  hostname: createConfig$1(getHostName(), [STRING_TYPE]), // required the hostname
	  jsonqlPath: createConfig$1(JSONQL_PATH, [STRING_TYPE]), // The path on the server

	  loginHandlerName: createConfig$1(ISSUER_NAME, [STRING_TYPE]),
	  logoutHandlerName: createConfig$1(LOGOUT_NAME, [STRING_TYPE]),
	  // add to koa v1.3.0
	  enableJsonp: createConfig$1(false, [BOOLEAN_TYPE]),
	  enableAuth: createConfig$1(false, [BOOLEAN_TYPE]),
	  // enable useJwt by default
	  useJwt: createConfig$1(true, [BOOLEAN_TYPE]),

	   // the header
	  useLocalstorage: createConfig$1(true, [BOOLEAN_TYPE]), // should we store the contract into localStorage
	  storageKey: createConfig$1(CLIENT_STORAGE_KEY, [STRING_TYPE]),// the key to use when store into localStorage
	  authKey: createConfig$1(CLIENT_AUTH_KEY, [STRING_TYPE]),// the key to use when store into the sessionStorage
	  contractExpired: createConfig$1(0, [NUMBER_TYPE]),// -1 always fetch contract,
	                      // 0 never expired,
	                      // > 0 then compare the timestamp with the current one to see if we need to get contract again
	  // contract: createConfig(false, type: [BOOLEAN_TYPE] },
	  contractKey: createConfig$1(false, [BOOLEAN_TYPE]), // if the server side is lock by the key you need this
	  contractKeyName: createConfig$1(CONTRACT_KEY_NAME, [STRING_TYPE]), // same as above they go in pairs
	  enableTimeout: createConfig$1(false, [BOOLEAN_TYPE]), // @TODO
	  timeout: createConfig$1(5000, [NUMBER_TYPE]), // 5 seconds
	  returnInstance: createConfig$1(false, [BOOLEAN_TYPE]),
	  allowReturnRawToken: createConfig$1(false, [BOOLEAN_TYPE]),
	  debugOn: createConfig$1(false, [BOOLEAN_TYPE])
	};

	// we must ensure the user passing the correct options

	function checkOptions (config) {
	  var contract = config.contract;
	  return checkConfigAsync$1(config, appProps$1, constProps)
	    .then(function (opts) {
	      opts.contract = contract;
	      return opts;
	    })
	}

	// export interface

	// this is new for the flyio and normalize the name from now on

	/**
	 * Main interface for jsonql fetch api
	 * @param {object} config
	 * @return {object} jsonql client
	 */
	function jsonqlApi(config) {
	  if ( config === void 0 ) config = {};

	  return checkOptions(config)
	    .then(function (opts) { return (
	      {
	        baseClient: new JsonqlBaseClient(opts),
	        opts: opts
	      }
	    ); })
	    .then( function (ref) {
	      var baseClient = ref.baseClient;
	      var opts = ref.opts;

	      return (
	      getContractFromConfig(baseClient, opts.contract)
	        .then(function (contract) { return generator(baseClient, opts, contract); }
	        )
	      );
	  }
	    );
	}

	// Instead of a singleton, I should make it as a more proper class
	/**
	 * We need the good old function as class instead of
	 * the ES6 class because there is no private property
	 * @param {object} config
	 */
	function JsonqlClient(config) {
	  if ( config === void 0 ) config = {};


	  var self = this;
	  var queue = [];
	  var _client;

	  Object.defineProperty(self, 'client', {
	    set: function set(value) {
	      _client = value;
	      for (var i = 0; i < queue.length; ++i) {
	        Reflect.apply(queue[i], null, [value]);
	      }
	      queue = []; // unset the array
	    },
	    get: function get() {
	      return _client;
	    }
	  });

	  self.ready = function (fn) {
	    if (typeof fn === 'function') {
	      if (_client !== undefined) {
	        Reflect.apply(fn, null, [_client]);
	      } else {
	        queue.push(fn);
	      }
	      return true;
	    }
	    throw new JsonqlError('Exepct a function pass to ready call!')
	  };
	  // run
	  jsonqlApi(config)
	    .then(function (client) {
	      self.client = client;
	    });
	}

	return JsonqlClient;

}));
//# sourceMappingURL=jsonql-client.cls.js.map
