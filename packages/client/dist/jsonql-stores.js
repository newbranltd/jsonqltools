(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(global = global || self, factory(global.JsonqlStores = {}));
}(this, function (exports) { 'use strict';

	var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

	function createCommonjsModule(fn, module) {
		return module = { exports: {} }, fn(module, module.exports), module.exports;
	}

	var assign = make_assign();
	var create = make_create();
	var trim = make_trim();
	var Global = (typeof window !== 'undefined' ? window : commonjsGlobal);

	var util = {
		assign: assign,
		create: create,
		trim: trim,
		bind: bind,
		slice: slice,
		each: each,
		map: map,
		pluck: pluck,
		isList: isList,
		isFunction: isFunction,
		isObject: isObject,
		Global: Global
	};

	function make_assign() {
		if (Object.assign) {
			return Object.assign
		} else {
			return function shimAssign(obj, props1, props2, etc) {
				var arguments$1 = arguments;

				for (var i = 1; i < arguments.length; i++) {
					each(Object(arguments$1[i]), function(val, key) {
						obj[key] = val;
					});
				}			
				return obj
			}
		}
	}

	function make_create() {
		if (Object.create) {
			return function create(obj, assignProps1, assignProps2, etc) {
				var assignArgsList = slice(arguments, 1);
				return assign.apply(this, [Object.create(obj)].concat(assignArgsList))
			}
		} else {
			function F() {} // eslint-disable-line no-inner-declarations
			return function create(obj, assignProps1, assignProps2, etc) {
				var assignArgsList = slice(arguments, 1);
				F.prototype = obj;
				return assign.apply(this, [new F()].concat(assignArgsList))
			}
		}
	}

	function make_trim() {
		if (String.prototype.trim) {
			return function trim(str) {
				return String.prototype.trim.call(str)
			}
		} else {
			return function trim(str) {
				return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')
			}
		}
	}

	function bind(obj, fn) {
		return function() {
			return fn.apply(obj, Array.prototype.slice.call(arguments, 0))
		}
	}

	function slice(arr, index) {
		return Array.prototype.slice.call(arr, index || 0)
	}

	function each(obj, fn) {
		pluck(obj, function(val, key) {
			fn(val, key);
			return false
		});
	}

	function map(obj, fn) {
		var res = (isList(obj) ? [] : {});
		pluck(obj, function(v, k) {
			res[k] = fn(v, k);
			return false
		});
		return res
	}

	function pluck(obj, fn) {
		if (isList(obj)) {
			for (var i=0; i<obj.length; i++) {
				if (fn(obj[i], i)) {
					return obj[i]
				}
			}
		} else {
			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					if (fn(obj[key], key)) {
						return obj[key]
					}
				}
			}
		}
	}

	function isList(val) {
		return (val != null && typeof val != 'function' && typeof val.length == 'number')
	}

	function isFunction(val) {
		return val && {}.toString.call(val) === '[object Function]'
	}

	function isObject(val) {
		return val && {}.toString.call(val) === '[object Object]'
	}

	var slice$1 = util.slice;
	var pluck$1 = util.pluck;
	var each$1 = util.each;
	var bind$1 = util.bind;
	var create$1 = util.create;
	var isList$1 = util.isList;
	var isFunction$1 = util.isFunction;
	var isObject$1 = util.isObject;

	var storeEngine = {
		createStore: createStore
	};

	var storeAPI = {
		version: '2.0.12',
		enabled: false,
		
		// get returns the value of the given key. If that value
		// is undefined, it returns optionalDefaultValue instead.
		get: function(key, optionalDefaultValue) {
			var data = this.storage.read(this._namespacePrefix + key);
			return this._deserialize(data, optionalDefaultValue)
		},

		// set will store the given value at key and returns value.
		// Calling set with value === undefined is equivalent to calling remove.
		set: function(key, value) {
			if (value === undefined) {
				return this.remove(key)
			}
			this.storage.write(this._namespacePrefix + key, this._serialize(value));
			return value
		},

		// remove deletes the key and value stored at the given key.
		remove: function(key) {
			this.storage.remove(this._namespacePrefix + key);
		},

		// each will call the given callback once for each key-value pair
		// in this store.
		each: function(callback) {
			var self = this;
			this.storage.each(function(val, namespacedKey) {
				callback.call(self, self._deserialize(val), (namespacedKey || '').replace(self._namespaceRegexp, ''));
			});
		},

		// clearAll will remove all the stored key-value pairs in this store.
		clearAll: function() {
			this.storage.clearAll();
		},

		// additional functionality that can't live in plugins
		// ---------------------------------------------------

		// hasNamespace returns true if this store instance has the given namespace.
		hasNamespace: function(namespace) {
			return (this._namespacePrefix == '__storejs_'+namespace+'_')
		},

		// createStore creates a store.js instance with the first
		// functioning storage in the list of storage candidates,
		// and applies the the given mixins to the instance.
		createStore: function() {
			return createStore.apply(this, arguments)
		},
		
		addPlugin: function(plugin) {
			this._addPlugin(plugin);
		},
		
		namespace: function(namespace) {
			return createStore(this.storage, this.plugins, namespace)
		}
	};

	function _warn() {
		var _console = (typeof console == 'undefined' ? null : console);
		if (!_console) { return }
		var fn = (_console.warn ? _console.warn : _console.log);
		fn.apply(_console, arguments);
	}

	function createStore(storages, plugins, namespace) {
		if (!namespace) {
			namespace = '';
		}
		if (storages && !isList$1(storages)) {
			storages = [storages];
		}
		if (plugins && !isList$1(plugins)) {
			plugins = [plugins];
		}

		var namespacePrefix = (namespace ? '__storejs_'+namespace+'_' : '');
		var namespaceRegexp = (namespace ? new RegExp('^'+namespacePrefix) : null);
		var legalNamespaces = /^[a-zA-Z0-9_\-]*$/; // alpha-numeric + underscore and dash
		if (!legalNamespaces.test(namespace)) {
			throw new Error('store.js namespaces can only have alphanumerics + underscores and dashes')
		}
		
		var _privateStoreProps = {
			_namespacePrefix: namespacePrefix,
			_namespaceRegexp: namespaceRegexp,

			_testStorage: function(storage) {
				try {
					var testStr = '__storejs__test__';
					storage.write(testStr, testStr);
					var ok = (storage.read(testStr) === testStr);
					storage.remove(testStr);
					return ok
				} catch(e) {
					return false
				}
			},

			_assignPluginFnProp: function(pluginFnProp, propName) {
				var oldFn = this[propName];
				this[propName] = function pluginFn() {
					var args = slice$1(arguments, 0);
					var self = this;

					// super_fn calls the old function which was overwritten by
					// this mixin.
					function super_fn() {
						if (!oldFn) { return }
						each$1(arguments, function(arg, i) {
							args[i] = arg;
						});
						return oldFn.apply(self, args)
					}

					// Give mixing function access to super_fn by prefixing all mixin function
					// arguments with super_fn.
					var newFnArgs = [super_fn].concat(args);

					return pluginFnProp.apply(self, newFnArgs)
				};
			},

			_serialize: function(obj) {
				return JSON.stringify(obj)
			},

			_deserialize: function(strVal, defaultVal) {
				if (!strVal) { return defaultVal }
				// It is possible that a raw string value has been previously stored
				// in a storage without using store.js, meaning it will be a raw
				// string value instead of a JSON serialized string. By defaulting
				// to the raw string value in case of a JSON parse error, we allow
				// for past stored values to be forwards-compatible with store.js
				var val = '';
				try { val = JSON.parse(strVal); }
				catch(e) { val = strVal; }

				return (val !== undefined ? val : defaultVal)
			},
			
			_addStorage: function(storage) {
				if (this.enabled) { return }
				if (this._testStorage(storage)) {
					this.storage = storage;
					this.enabled = true;
				}
			},

			_addPlugin: function(plugin) {
				var self = this;

				// If the plugin is an array, then add all plugins in the array.
				// This allows for a plugin to depend on other plugins.
				if (isList$1(plugin)) {
					each$1(plugin, function(plugin) {
						self._addPlugin(plugin);
					});
					return
				}

				// Keep track of all plugins we've seen so far, so that we
				// don't add any of them twice.
				var seenPlugin = pluck$1(this.plugins, function(seenPlugin) {
					return (plugin === seenPlugin)
				});
				if (seenPlugin) {
					return
				}
				this.plugins.push(plugin);

				// Check that the plugin is properly formed
				if (!isFunction$1(plugin)) {
					throw new Error('Plugins must be function values that return objects')
				}

				var pluginProperties = plugin.call(this);
				if (!isObject$1(pluginProperties)) {
					throw new Error('Plugins must return an object of function properties')
				}

				// Add the plugin function properties to this store instance.
				each$1(pluginProperties, function(pluginFnProp, propName) {
					if (!isFunction$1(pluginFnProp)) {
						throw new Error('Bad plugin property: '+propName+' from plugin '+plugin.name+'. Plugins should only return functions.')
					}
					self._assignPluginFnProp(pluginFnProp, propName);
				});
			},
			
			// Put deprecated properties in the private API, so as to not expose it to accidential
			// discovery through inspection of the store object.
			
			// Deprecated: addStorage
			addStorage: function(storage) {
				_warn('store.addStorage(storage) is deprecated. Use createStore([storages])');
				this._addStorage(storage);
			}
		};

		var store = create$1(_privateStoreProps, storeAPI, {
			plugins: []
		});
		store.raw = {};
		each$1(store, function(prop, propName) {
			if (isFunction$1(prop)) {
				store.raw[propName] = bind$1(store, prop);			
			}
		});
		each$1(storages, function(storage) {
			store._addStorage(storage);
		});
		each$1(plugins, function(plugin) {
			store._addPlugin(plugin);
		});
		return store
	}

	var Global$1 = util.Global;

	var localStorage_1 = {
		name: 'localStorage',
		read: read,
		write: write,
		each: each$2,
		remove: remove,
		clearAll: clearAll,
	};

	function localStorage() {
		return Global$1.localStorage
	}

	function read(key) {
		return localStorage().getItem(key)
	}

	function write(key, data) {
		return localStorage().setItem(key, data)
	}

	function each$2(fn) {
		for (var i = localStorage().length - 1; i >= 0; i--) {
			var key = localStorage().key(i);
			fn(read(key), key);
		}
	}

	function remove(key) {
		return localStorage().removeItem(key)
	}

	function clearAll() {
		return localStorage().clear()
	}

	// cookieStorage is useful Safari private browser mode, where localStorage
	// doesn't work but cookies do. This implementation is adopted from
	// https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage


	var Global$2 = util.Global;
	var trim$1 = util.trim;

	var cookieStorage = {
		name: 'cookieStorage',
		read: read$1,
		write: write$1,
		each: each$3,
		remove: remove$1,
		clearAll: clearAll$1,
	};

	var doc = Global$2.document;

	function read$1(key) {
		if (!key || !_has(key)) { return null }
		var regexpStr = "(?:^|.*;\\s*)" +
			escape(key).replace(/[\-\.\+\*]/g, "\\$&") +
			"\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*";
		return unescape(doc.cookie.replace(new RegExp(regexpStr), "$1"))
	}

	function each$3(callback) {
		var cookies = doc.cookie.split(/; ?/g);
		for (var i = cookies.length - 1; i >= 0; i--) {
			if (!trim$1(cookies[i])) {
				continue
			}
			var kvp = cookies[i].split('=');
			var key = unescape(kvp[0]);
			var val = unescape(kvp[1]);
			callback(val, key);
		}
	}

	function write$1(key, data) {
		if(!key) { return }
		doc.cookie = escape(key) + "=" + escape(data) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
	}

	function remove$1(key) {
		if (!key || !_has(key)) {
			return
		}
		doc.cookie = escape(key) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
	}

	function clearAll$1() {
		each$3(function(_, key) {
			remove$1(key);
		});
	}

	function _has(key) {
		return (new RegExp("(?:^|;\\s*)" + escape(key).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(doc.cookie)
	}

	var defaults = defaultsPlugin;

	function defaultsPlugin() {
		var defaultValues = {};
		
		return {
			defaults: defaults,
			get: get
		}
		
		function defaults(_, values) {
			defaultValues = values;
		}
		
		function get(super_fn, key) {
			var val = super_fn();
			return (val !== undefined ? val : defaultValues[key])
		}
	}

	var namespace = 'expire_mixin';

	var expire = expirePlugin;

	function expirePlugin() {
		var expirations = this.createStore(this.storage, null, this._namespacePrefix+namespace);
		
		return {
			set: expire_set,
			get: expire_get,
			remove: expire_remove,
			getExpiration: getExpiration,
			removeExpiredKeys: removeExpiredKeys
		}
		
		function expire_set(super_fn, key, val, expiration) {
			if (!this.hasNamespace(namespace)) {
				expirations.set(key, expiration);
			}
			return super_fn()
		}
		
		function expire_get(super_fn, key) {
			if (!this.hasNamespace(namespace)) {
				_checkExpiration.call(this, key);
			}
			return super_fn()
		}
		
		function expire_remove(super_fn, key) {
			if (!this.hasNamespace(namespace)) {
				expirations.remove(key);
			}
			return super_fn()
		}
		
		function getExpiration(_, key) {
			return expirations.get(key)
		}
		
		function removeExpiredKeys(_) {
			var keys = [];
			this.each(function(val, key) {
				keys.push(key);
			});
			for (var i=0; i<keys.length; i++) {
				_checkExpiration.call(this, keys[i]);
			}
		}
		
		function _checkExpiration(key) {
			var expiration = expirations.get(key, Number.MAX_VALUE);
			if (expiration <= new Date().getTime()) {
				this.raw.remove(key);
				expirations.remove(key);
			}
		}
		
	}

	var bind$2 = util.bind;
	var each$4 = util.each;
	var create$2 = util.create;
	var slice$2 = util.slice;

	var events = eventsPlugin;

	function eventsPlugin() {
		var pubsub = _newPubSub();

		return {
			watch: watch,
			unwatch: unwatch,
			once: once,

			set: set,
			remove: remove,
			clearAll: clearAll
		}

		// new pubsub functions
		function watch(_, key, listener) {
			return pubsub.on(key, bind$2(this, listener))
		}
		function unwatch(_, subId) {
			pubsub.off(subId);
		}
		function once(_, key, listener) {
			pubsub.once(key, bind$2(this, listener));
		}

		// overwrite function to fire when appropriate
		function set(super_fn, key, val) {
			var oldVal = this.get(key);
			super_fn();
			pubsub.fire(key, val, oldVal);
		}
		function remove(super_fn, key) {
			var oldVal = this.get(key);
			super_fn();
			pubsub.fire(key, undefined, oldVal);
		}
		function clearAll(super_fn) {
			var oldVals = {};
			this.each(function(val, key) {
				oldVals[key] = val;
			});
			super_fn();
			each$4(oldVals, function(oldVal, key) {
				pubsub.fire(key, undefined, oldVal);
			});
		}
	}


	function _newPubSub() {
		return create$2(_pubSubBase, {
			_id: 0,
			_subSignals: {},
			_subCallbacks: {}
		})
	}

	var _pubSubBase = {
		_id: null,
		_subCallbacks: null,
		_subSignals: null,
		on: function(signal, callback) {
			if (!this._subCallbacks[signal]) {
				this._subCallbacks[signal] = {};
			}
			this._id += 1;
			this._subCallbacks[signal][this._id] = callback;
			this._subSignals[this._id] = signal;
			return this._id
		},
		off: function(subId) {
			var signal = this._subSignals[subId];
			delete this._subCallbacks[signal][subId];
			delete this._subSignals[subId];
		},
		once: function(signal, callback) {
			var subId = this.on(signal, bind$2(this, function() {
				callback.apply(this, arguments);
				this.off(subId);
			}));
		},
		fire: function(signal) {
			var args = slice$2(arguments, 1);
			each$4(this._subCallbacks[signal], function(callback) {
				callback.apply(this, args);
			});
		}
	};

	var lzString = createCommonjsModule(function (module) {
	/* eslint-disable */
	// Copyright (c) 2013 Pieroxy <pieroxy@pieroxy.net>
	// This work is free. You can redistribute it and/or modify it
	// under the terms of the WTFPL, Version 2
	// For more information see LICENSE.txt or http://www.wtfpl.net/
	//
	// For more information, the home page:
	// http://pieroxy.net/blog/pages/lz-string/testing.html
	//
	// LZ-based compression algorithm, version 1.4.4
	var LZString = (function() {

	// private property
	var f = String.fromCharCode;
	var keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$";
	var baseReverseDic = {};

	function getBaseValue(alphabet, character) {
	  if (!baseReverseDic[alphabet]) {
	    baseReverseDic[alphabet] = {};
	    for (var i=0 ; i<alphabet.length ; i++) {
	      baseReverseDic[alphabet][alphabet.charAt(i)] = i;
	    }
	  }
	  return baseReverseDic[alphabet][character];
	}

	var LZString = {
	  compressToBase64 : function (input) {
	    if (input == null) { return ""; }
	    var res = LZString._compress(input, 6, function(a){return keyStrBase64.charAt(a);});
	    switch (res.length % 4) { // To produce valid Base64
	    default: // When could this happen ?
	    case 0 : return res;
	    case 1 : return res+"===";
	    case 2 : return res+"==";
	    case 3 : return res+"=";
	    }
	  },

	  decompressFromBase64 : function (input) {
	    if (input == null) { return ""; }
	    if (input == "") { return null; }
	    return LZString._decompress(input.length, 32, function(index) { return getBaseValue(keyStrBase64, input.charAt(index)); });
	  },

	  compressToUTF16 : function (input) {
	    if (input == null) { return ""; }
	    return LZString._compress(input, 15, function(a){return f(a+32);}) + " ";
	  },

	  decompressFromUTF16: function (compressed) {
	    if (compressed == null) { return ""; }
	    if (compressed == "") { return null; }
	    return LZString._decompress(compressed.length, 16384, function(index) { return compressed.charCodeAt(index) - 32; });
	  },

	  //compress into uint8array (UCS-2 big endian format)
	  compressToUint8Array: function (uncompressed) {
	    var compressed = LZString.compress(uncompressed);
	    var buf=new Uint8Array(compressed.length*2); // 2 bytes per character

	    for (var i=0, TotalLen=compressed.length; i<TotalLen; i++) {
	      var current_value = compressed.charCodeAt(i);
	      buf[i*2] = current_value >>> 8;
	      buf[i*2+1] = current_value % 256;
	    }
	    return buf;
	  },

	  //decompress from uint8array (UCS-2 big endian format)
	  decompressFromUint8Array:function (compressed) {
	    if (compressed===null || compressed===undefined){
	        return LZString.decompress(compressed);
	    } else {
	        var buf=new Array(compressed.length/2); // 2 bytes per character
	        for (var i=0, TotalLen=buf.length; i<TotalLen; i++) {
	          buf[i]=compressed[i*2]*256+compressed[i*2+1];
	        }

	        var result = [];
	        buf.forEach(function (c) {
	          result.push(f(c));
	        });
	        return LZString.decompress(result.join(''));

	    }

	  },


	  //compress into a string that is already URI encoded
	  compressToEncodedURIComponent: function (input) {
	    if (input == null) { return ""; }
	    return LZString._compress(input, 6, function(a){return keyStrUriSafe.charAt(a);});
	  },

	  //decompress from an output of compressToEncodedURIComponent
	  decompressFromEncodedURIComponent:function (input) {
	    if (input == null) { return ""; }
	    if (input == "") { return null; }
	    input = input.replace(/ /g, "+");
	    return LZString._decompress(input.length, 32, function(index) { return getBaseValue(keyStrUriSafe, input.charAt(index)); });
	  },

	  compress: function (uncompressed) {
	    return LZString._compress(uncompressed, 16, function(a){return f(a);});
	  },
	  _compress: function (uncompressed, bitsPerChar, getCharFromInt) {
	    if (uncompressed == null) { return ""; }
	    var i, value,
	        context_dictionary= {},
	        context_dictionaryToCreate= {},
	        context_c="",
	        context_wc="",
	        context_w="",
	        context_enlargeIn= 2, // Compensate for the first entry which should not count
	        context_dictSize= 3,
	        context_numBits= 2,
	        context_data=[],
	        context_data_val=0,
	        context_data_position=0,
	        ii;

	    for (ii = 0; ii < uncompressed.length; ii += 1) {
	      context_c = uncompressed.charAt(ii);
	      if (!Object.prototype.hasOwnProperty.call(context_dictionary,context_c)) {
	        context_dictionary[context_c] = context_dictSize++;
	        context_dictionaryToCreate[context_c] = true;
	      }

	      context_wc = context_w + context_c;
	      if (Object.prototype.hasOwnProperty.call(context_dictionary,context_wc)) {
	        context_w = context_wc;
	      } else {
	        if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
	          if (context_w.charCodeAt(0)<256) {
	            for (i=0 ; i<context_numBits ; i++) {
	              context_data_val = (context_data_val << 1);
	              if (context_data_position == bitsPerChar-1) {
	                context_data_position = 0;
	                context_data.push(getCharFromInt(context_data_val));
	                context_data_val = 0;
	              } else {
	                context_data_position++;
	              }
	            }
	            value = context_w.charCodeAt(0);
	            for (i=0 ; i<8 ; i++) {
	              context_data_val = (context_data_val << 1) | (value&1);
	              if (context_data_position == bitsPerChar-1) {
	                context_data_position = 0;
	                context_data.push(getCharFromInt(context_data_val));
	                context_data_val = 0;
	              } else {
	                context_data_position++;
	              }
	              value = value >> 1;
	            }
	          } else {
	            value = 1;
	            for (i=0 ; i<context_numBits ; i++) {
	              context_data_val = (context_data_val << 1) | value;
	              if (context_data_position ==bitsPerChar-1) {
	                context_data_position = 0;
	                context_data.push(getCharFromInt(context_data_val));
	                context_data_val = 0;
	              } else {
	                context_data_position++;
	              }
	              value = 0;
	            }
	            value = context_w.charCodeAt(0);
	            for (i=0 ; i<16 ; i++) {
	              context_data_val = (context_data_val << 1) | (value&1);
	              if (context_data_position == bitsPerChar-1) {
	                context_data_position = 0;
	                context_data.push(getCharFromInt(context_data_val));
	                context_data_val = 0;
	              } else {
	                context_data_position++;
	              }
	              value = value >> 1;
	            }
	          }
	          context_enlargeIn--;
	          if (context_enlargeIn == 0) {
	            context_enlargeIn = Math.pow(2, context_numBits);
	            context_numBits++;
	          }
	          delete context_dictionaryToCreate[context_w];
	        } else {
	          value = context_dictionary[context_w];
	          for (i=0 ; i<context_numBits ; i++) {
	            context_data_val = (context_data_val << 1) | (value&1);
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	            value = value >> 1;
	          }


	        }
	        context_enlargeIn--;
	        if (context_enlargeIn == 0) {
	          context_enlargeIn = Math.pow(2, context_numBits);
	          context_numBits++;
	        }
	        // Add wc to the dictionary.
	        context_dictionary[context_wc] = context_dictSize++;
	        context_w = String(context_c);
	      }
	    }

	    // Output the code for w.
	    if (context_w !== "") {
	      if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate,context_w)) {
	        if (context_w.charCodeAt(0)<256) {
	          for (i=0 ; i<context_numBits ; i++) {
	            context_data_val = (context_data_val << 1);
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	          }
	          value = context_w.charCodeAt(0);
	          for (i=0 ; i<8 ; i++) {
	            context_data_val = (context_data_val << 1) | (value&1);
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	            value = value >> 1;
	          }
	        } else {
	          value = 1;
	          for (i=0 ; i<context_numBits ; i++) {
	            context_data_val = (context_data_val << 1) | value;
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	            value = 0;
	          }
	          value = context_w.charCodeAt(0);
	          for (i=0 ; i<16 ; i++) {
	            context_data_val = (context_data_val << 1) | (value&1);
	            if (context_data_position == bitsPerChar-1) {
	              context_data_position = 0;
	              context_data.push(getCharFromInt(context_data_val));
	              context_data_val = 0;
	            } else {
	              context_data_position++;
	            }
	            value = value >> 1;
	          }
	        }
	        context_enlargeIn--;
	        if (context_enlargeIn == 0) {
	          context_enlargeIn = Math.pow(2, context_numBits);
	          context_numBits++;
	        }
	        delete context_dictionaryToCreate[context_w];
	      } else {
	        value = context_dictionary[context_w];
	        for (i=0 ; i<context_numBits ; i++) {
	          context_data_val = (context_data_val << 1) | (value&1);
	          if (context_data_position == bitsPerChar-1) {
	            context_data_position = 0;
	            context_data.push(getCharFromInt(context_data_val));
	            context_data_val = 0;
	          } else {
	            context_data_position++;
	          }
	          value = value >> 1;
	        }


	      }
	      context_enlargeIn--;
	      if (context_enlargeIn == 0) {
	        context_enlargeIn = Math.pow(2, context_numBits);
	        context_numBits++;
	      }
	    }

	    // Mark the end of the stream
	    value = 2;
	    for (i=0 ; i<context_numBits ; i++) {
	      context_data_val = (context_data_val << 1) | (value&1);
	      if (context_data_position == bitsPerChar-1) {
	        context_data_position = 0;
	        context_data.push(getCharFromInt(context_data_val));
	        context_data_val = 0;
	      } else {
	        context_data_position++;
	      }
	      value = value >> 1;
	    }

	    // Flush the last char
	    while (true) {
	      context_data_val = (context_data_val << 1);
	      if (context_data_position == bitsPerChar-1) {
	        context_data.push(getCharFromInt(context_data_val));
	        break;
	      }
	      else { context_data_position++; }
	    }
	    return context_data.join('');
	  },

	  decompress: function (compressed) {
	    if (compressed == null) { return ""; }
	    if (compressed == "") { return null; }
	    return LZString._decompress(compressed.length, 32768, function(index) { return compressed.charCodeAt(index); });
	  },

	  _decompress: function (length, resetValue, getNextValue) {
	    var dictionary = [],
	        next,
	        enlargeIn = 4,
	        dictSize = 4,
	        numBits = 3,
	        entry = "",
	        result = [],
	        i,
	        w,
	        bits, resb, maxpower, power,
	        c,
	        data = {val:getNextValue(0), position:resetValue, index:1};

	    for (i = 0; i < 3; i += 1) {
	      dictionary[i] = i;
	    }

	    bits = 0;
	    maxpower = Math.pow(2,2);
	    power=1;
	    while (power!=maxpower) {
	      resb = data.val & data.position;
	      data.position >>= 1;
	      if (data.position == 0) {
	        data.position = resetValue;
	        data.val = getNextValue(data.index++);
	      }
	      bits |= (resb>0 ? 1 : 0) * power;
	      power <<= 1;
	    }

	    switch (next = bits) {
	      case 0:
	          bits = 0;
	          maxpower = Math.pow(2,8);
	          power=1;
	          while (power!=maxpower) {
	            resb = data.val & data.position;
	            data.position >>= 1;
	            if (data.position == 0) {
	              data.position = resetValue;
	              data.val = getNextValue(data.index++);
	            }
	            bits |= (resb>0 ? 1 : 0) * power;
	            power <<= 1;
	          }
	        c = f(bits);
	        break;
	      case 1:
	          bits = 0;
	          maxpower = Math.pow(2,16);
	          power=1;
	          while (power!=maxpower) {
	            resb = data.val & data.position;
	            data.position >>= 1;
	            if (data.position == 0) {
	              data.position = resetValue;
	              data.val = getNextValue(data.index++);
	            }
	            bits |= (resb>0 ? 1 : 0) * power;
	            power <<= 1;
	          }
	        c = f(bits);
	        break;
	      case 2:
	        return "";
	    }
	    dictionary[3] = c;
	    w = c;
	    result.push(c);
	    while (true) {
	      if (data.index > length) {
	        return "";
	      }

	      bits = 0;
	      maxpower = Math.pow(2,numBits);
	      power=1;
	      while (power!=maxpower) {
	        resb = data.val & data.position;
	        data.position >>= 1;
	        if (data.position == 0) {
	          data.position = resetValue;
	          data.val = getNextValue(data.index++);
	        }
	        bits |= (resb>0 ? 1 : 0) * power;
	        power <<= 1;
	      }

	      switch (c = bits) {
	        case 0:
	          bits = 0;
	          maxpower = Math.pow(2,8);
	          power=1;
	          while (power!=maxpower) {
	            resb = data.val & data.position;
	            data.position >>= 1;
	            if (data.position == 0) {
	              data.position = resetValue;
	              data.val = getNextValue(data.index++);
	            }
	            bits |= (resb>0 ? 1 : 0) * power;
	            power <<= 1;
	          }

	          dictionary[dictSize++] = f(bits);
	          c = dictSize-1;
	          enlargeIn--;
	          break;
	        case 1:
	          bits = 0;
	          maxpower = Math.pow(2,16);
	          power=1;
	          while (power!=maxpower) {
	            resb = data.val & data.position;
	            data.position >>= 1;
	            if (data.position == 0) {
	              data.position = resetValue;
	              data.val = getNextValue(data.index++);
	            }
	            bits |= (resb>0 ? 1 : 0) * power;
	            power <<= 1;
	          }
	          dictionary[dictSize++] = f(bits);
	          c = dictSize-1;
	          enlargeIn--;
	          break;
	        case 2:
	          return result.join('');
	      }

	      if (enlargeIn == 0) {
	        enlargeIn = Math.pow(2, numBits);
	        numBits++;
	      }

	      if (dictionary[c]) {
	        entry = dictionary[c];
	      } else {
	        if (c === dictSize) {
	          entry = w + w.charAt(0);
	        } else {
	          return null;
	        }
	      }
	      result.push(entry);

	      // Add w+entry[0] to the dictionary.
	      dictionary[dictSize++] = w + entry.charAt(0);
	      enlargeIn--;

	      w = entry;

	      if (enlargeIn == 0) {
	        enlargeIn = Math.pow(2, numBits);
	        numBits++;
	      }

	    }
	  }
	};
	  return LZString;
	})();

	if(  module != null ) {
	  module.exports = LZString;
	}
	});

	var compression = compressionPlugin;

	function compressionPlugin() {
		return {
			get: get,
			set: set,
		}

		function get(super_fn, key) {
			var val = super_fn(key);
			if (!val) { return val }
			var decompressed = lzString.decompress(val);
			// fallback to existing values that are not compressed
			return (decompressed == null) ? val : this._deserialize(decompressed)
		}

		function set(super_fn, key, val) {
			var compressed = lzString.compress(this._serialize(val));
			super_fn(key, compressed);
		}
	}

	// sort of persist on the user side

	var storages = [localStorage_1, cookieStorage];
	var plugins = [defaults, expire, events, compression];

	var localStore = storeEngine.createStore(storages, plugins);

	var Global$3 = util.Global;

	var sessionStorage_1 = {
		name: 'sessionStorage',
		read: read$2,
		write: write$2,
		each: each$5,
		remove: remove$2,
		clearAll: clearAll$2
	};

	function sessionStorage() {
		return Global$3.sessionStorage
	}

	function read$2(key) {
		return sessionStorage().getItem(key)
	}

	function write$2(key, data) {
		return sessionStorage().setItem(key, data)
	}

	function each$5(fn) {
		for (var i = sessionStorage().length - 1; i >= 0; i--) {
			var key = sessionStorage().key(i);
			fn(read$2(key), key);
		}
	}

	function remove$2(key) {
		return sessionStorage().removeItem(key)
	}

	function clearAll$2() {
		return sessionStorage().clear()
	}

	// session store with watch

	var storages$1 = [sessionStorage_1, cookieStorage];
	var plugins$1 = [defaults, expire];

	var sessionStore = storeEngine.createStore(storages$1, plugins$1);

	// export store interface

	// export back the raw version for development purposes
	var localStore$1 = localStore;
	var sessionStore$1 = sessionStore;

	exports.localStore = localStore$1;
	exports.sessionStore = sessionStore$1;

	Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=jsonql-stores.js.map
