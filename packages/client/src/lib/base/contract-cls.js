// all the contract related methods will be here
import { localStore } from '../stores';
import { timestamp, isJsonqlContract } from '../utils';
import HttpClass from './http-cls';

export default class ContractClass extends HttpClass {

  constructor(opts) {
    super(opts)
  }

  /**
   * Set if we want to debug or not
   * @return {undefined} nothing
   */
  debug() {
    const key = 'debug';
    if (this.opts.debugOn) {
      localStore.set(key, 'jsonql-client*')
    } else {
      localStore.remove(key)
    }
  }

  /**
   * return the contract public api
   * @return {object} contract
   */
  getContract() {
    const contract = this.readContract()
    if (contract) {
      return Promise.resolve(contract)
    }
    return this.get()
      .then( this.storeContract.bind(this) )
  }

  /**
   * We are changing the way how to auth to get the contract.json
   * Instead of in the url, we will be putting that key value in the header
   * @return {object} header
   */
  get contractHeader() {
    let base = {};
    if (this.opts.contractKey !== false) {
      base[this.opts.contractKeyName] = this.opts.contractKey;
    }
    return base;
  }

  /**
   * Save the contract to local store
   * @param {object} contract to save
   * @return {object|boolean} false when its not a contract or contract on OK
   */
  storeContract(contract) {
    // first need to check if the contract is a contract
    if (!isJsonqlContract(contract)) {
      return false;
    }
    let args = [contract]
    if (this.opts.contractExpired) {
      let expired = parseFloat(this.opts.contractExpired);
      if (!isNaN(expired) && expired > 0) {
        args.push(expired);
      }
    }
    this.jsonqlContract = contract;
    return contract;
  }

  /**
   * return the contract from options or localStore
   * @return {object} contract
   */
  readContract() {
    let contract = isJsonqlContract(this.opts.contract)
    return contract ? this.opts.contract : localStore.get(this.opts.storageKey)
  }
}
