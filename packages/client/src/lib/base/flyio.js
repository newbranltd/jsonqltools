// import the module from a seprate module so we can change this easily
import Fly from 'flyio/dist/npm/fly'

const flyio = new Fly()

export default flyio
