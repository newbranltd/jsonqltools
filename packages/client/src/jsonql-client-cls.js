// Instead of a singleton, I should make it as a more proper class
// this way the client will not only limited to call just one API end point
// Also the way it's call jsonqlClient.then is not very elegant
// instead I am going to make it like this
// const client = new JsonqlClient(config)
// client.ready(function(api) { /* now do your thing */ });

import jsonqlApi from './jsonql-api'
import { JsonqlError } from 'jsonql-errors'
/**
 * We need the good old function as class instead of
 * the ES6 class because there is no private property
 * @param {object} config
 */
export default function JsonqlClient(config = {}) {

  const self = this;
  var queue = [];
  var _client;

  Object.defineProperty(self, 'client', {
    set(value) {
      _client = value;
      for (let i = 0; i < queue.length; ++i) {
        Reflect.apply(queue[i], null, [value])
      }
      queue = []; // unset the array
    },
    get() {
      return _client;
    }
  });

  self.ready = (fn) => {
    if (typeof fn === 'function') {
      if (_client !== undefined) {
        Reflect.apply(fn, null, [_client])
      } else {
        queue.push(fn)
      }
      return true;
    }
    throw new JsonqlError('Exepct a function pass to ready call!')
  }
  // run
  jsonqlApi(config)
    .then(client => {
      self.client = client;
    })
}
