export const EXT = 'js'; // we might do a ts in the future
export const TS_EXT = 'ts';

export const HELLO = 'Hello world!';
export const HELLO_FN = 'helloWorld';
// the core stuff to id if it's calling with jsonql
export const DATA_KEY = 'data';
export const ERROR_KEY = 'error';

export const JSONQL_PATH = 'jsonql';
// according to the json query spec
export const CONTENT_TYPE = 'application/vnd.api+json';
export const CHARSET = 'charset=utf-8';
export const DEFAULT_HEADER = {
  'Accept': CONTENT_TYPE,
  'Content-Type': [ CONTENT_TYPE, CHARSET ].join(';')
}

export const INDEX = 'index';
export const DEFAULT_TYPE = 'any';
// contract file names
// export const DEFAULT_FILE_NAME = 'contract.json'; // @TODO remove once all changed
// export const PUBLIC_FILE_NAME = 'public-contract.json'; // @TODO remove once all changed
export const DEFAULT_CONTRACT_FILE_NAME = 'contract.json';
export const PUBLIC_CONTRACT_FILE_NAME = 'public-contract.json';
// this is for the ES6 module import and export
export const DEFAULT_RESOLVER_LIST_FILE_NAME = 'resolver.js';
export const DEFAULT_RESOLVER_IMPORT_FILE_NAME = 'import.js';
export const MODULE_TYPE = 'module';
export const SCRIPT_TYPE = 'script';

// @TODO remove this is not in use
// export const CLIENT_CONFIG_FILE = '.clients.json';
// export const CONTRACT_CONFIG_FILE = 'jsonql-contract-config.js';
// type of resolvers
export const QUERY_NAME = 'query';
export const MUTATION_NAME = 'mutation';
export const SOCKET_NAME = 'socket';
export const CONTRACT_NAME = 'contract';
export const RESOLVER_TYPES = [QUERY_NAME, MUTATION_NAME, SOCKET_NAME];
// for calling the mutation
export const PAYLOAD_PARAM_NAME = 'payload';
export const CONDITION_PARAM_NAME = 'condition';
export const RESOLVER_PARAM_NAME = 'resolverName';
export const QUERY_ARG_NAME = 'args';
export const MUTATION_ARGS = [RESOLVER_PARAM_NAME, PAYLOAD_PARAM_NAME, CONDITION_PARAM_NAME];
// new jsonp
export const JSONP_CALLBACK_NAME = 'jsonqlJsonpCallback';

// methods allow
export const API_REQUEST_METHODS = ['POST', 'PUT'];
export const CONTRACT_REQUEST_METHODS = ['GET', 'HEAD'];
// for  contract-cli
export const KEY_WORD = 'continue';
export const PUBLIC_KEY = 'public';
export const PRIVATE_KEY = 'private';

export const TYPE_KEY = 'type';
export const OPTIONAL_KEY = 'optional';
export const ENUM_KEY = 'enumv';  // need to change this because enum is a reserved word
export const ARGS_KEY = 'args';
export const CHECKER_KEY = 'checker';
export const ALIAS_KEY = 'alias';
// author
export const AUTH_TYPE = 'auth';
export const LOGIN_NAME = 'login';
export const ISSUER_NAME = LOGIN_NAME; // legacy issue need to replace them later
export const LOGOUT_NAME = 'logout';
export const VALIDATOR_NAME = 'validator';

export const AUTH_HEADER = 'Authorization';
export const AUTH_CHECK_HEADER = 'authorization'; // this is for checking so it must be lowercase
export const BEARER = 'Bearer';

// for client use @TODO need to clean this up some of them are not in use
export const CREDENTIAL_STORAGE_KEY = 'credential';
export const CLIENT_STORAGE_KEY = 'storageKey';
export const CLIENT_AUTH_KEY = 'authKey';
// for id the multiple storage engine
export const INDEX_KEY = 'index';
// contract key
export const CONTRACT_KEY_NAME = 'X-JSONQL-CV-KEY';
// directories
export const DEFAULT_RESOLVER_DIR = 'resolvers';
export const DEFAULT_CONTRACT_DIR = 'contracts';
export const DEFAULT_KEYS_DIR = 'keys';
// add in V1.3.4 start supporting socket

// for validation
export const CJS_TYPE = 'cjs';
export const ES_TYPE = 'es';
export const TS_TYPE = 'ts';
export const ACCEPTED_JS_TYPES = [CJS_TYPE, ES_TYPE, TS_TYPE];

export const OR_SEPERATOR = '|';

export const STRING_TYPE = 'string';
export const BOOLEAN_TYPE = 'boolean';
export const ARRAY_TYPE = 'array';
export const OBJECT_TYPE = 'object';
export const ANY_TYPE = 'any';

export const NUMBER_TYPE = 'number';
export const NUMBER_TYPES = ['int', 'integer', 'float', 'double', 'decimal'];
// supported types
export const SUPPORTED_TYPES = [NUMBER_TYPE, STRING_TYPE, BOOLEAN_TYPE, ARRAY_TYPE, OBJECT_TYPE, ANY_TYPE];

export const ARRAY_TS_TYPE_LFT = 'Array<';
export const ARRAY_TYPE_LFT = 'array.<';
export const ARRAY_TYPE_RGT = '>';
// for contract cli
export const RETURN_AS_FILE = 'file';
export const RETURN_AS_JSON = 'json';
export const RETURN_AS_ENUM = [RETURN_AS_FILE, RETURN_AS_JSON];

export const NO_ERROR_MSG = 'No message';
export const NO_STATUS_CODE = -1;

// use throughout the clients
export const SWITCH_USER_EVENT_NAME = '__switch__';
export const LOGIN_EVENT_NAME = '__login__';
export const LOGOUT_EVENT_NAME = '__logout__';

// for ws servers
export const WS_REPLY_TYPE = '__reply__';
export const WS_EVT_NAME = '__event__';
export const WS_DATA_NAME = '__data__';
export const WS_IS_REPLY_KEYS = [WS_REPLY_TYPE, WS_EVT_NAME, WS_DATA_NAME];
export const EMIT_REPLY_TYPE = 'emit';
export const ACKNOWLEDGE_REPLY_TYPE = 'acknowledge';
export const ERROR_TYPE = 'error';

export const JS_WS_SOCKET_IO_NAME = 'socket.io';
export const JS_WS_NAME = 'ws';
export const GO_WS_COOLPY7_NAME = 'coolpy7';

// for ws client
export const MESSAGE_PROP_NAME = 'onMessage';
export const RESULT_PROP_NAME = 'onResult';
export const ERROR_PROP_NAME = 'onError';
export const READY_PROP_NAME = 'onReady';
export const SEND_MSG_PROP_NAME = 'send';
// this is the default time to wait for reply if exceed this then we
// trigger an error --> 5 seconds
export const DEFAULT_WS_WAIT_TIME = 5000;
export const TIMEOUT_ERR_MSG = 'timeout';
export const NOT_LOGIN_ERR_MSG = 'NOT LOGIN';
// for crypto operation
export const BASE64_FORMAT = 'base64';
export const HEX_FORMAT = 'hex';
export const UTF8_FORMAT = 'utf8';
export const RSA_FORMATS = [BASE64_FORMAT, HEX_FORMAT];
export const RSA_ALGO = 'RS256';
export const HSA_ALGO = 'HS256';
export const JWT_SUPPORT_ALGOS = [RSA_ALGO, HSA_ALGO];
export const RSA_PRIVATE_KEY_HEADER = 'BEGIN RSA PRIVATE KEY';
export const RSA_MIN_MODULE_LEN = 1024;
export const RSA_MAX_MODULE_LEN = 4096;
export const TOKEN_PARAM_NAME = 'token';
export const IO_ROUNDTRIP_LOGIN = 'roundtip';
export const IO_HANDSHAKE_LOGIN = 'handshake';
export const IO_LOGIN_METHODS = [IO_ROUNDTRIP_LOGIN, IO_HANDSHAKE_LOGIN];

export const PEM_EXT = 'pem';
export const PUBLIC_KEY_NAME = 'publicKey';
export const PRIVATE_KEY_NAME = 'privateKey';

export const DEFAULT_PUBLIC_KEY_FILE = [PUBLIC_KEY_NAME, PEM_EXT].join('.');
export const DEFAULT_PRIVATE_KEY_FILE = [PRIVATE_KEY_NAME, PEM_EXT].join('.');
