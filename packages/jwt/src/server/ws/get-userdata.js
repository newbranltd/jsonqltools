// simple processing method to get the userdata form the req object for ws
/**
 * @param {object} req the request object
 * @return {object} userdata 
 */
module.exports = function getUserdata(req) {
  return req && req.state && req.state.userdata ? req.state.userdata : false;
}
