const socketIoJwtAuth = require('./socketio/socketio-jwt-auth')
const socketIoHandshakeAuth = require('./socketio/handshake-auth')
const socketIoGetUserdata = require('./socketio/get-userdata')

const {
  socketIoNodeHandshakeLogin,
  socketIoNodeRoundtripLogin,

  socketIoNodeClient,
  socketIoNodeClientAsync,

  socketIoChainHSConnect,
  socketIoChainRTConnect
} = require('./socketio/node-clients')

// ws
const wsVerifyClient = require('./ws/verify-client')
const { wsNodeClient, wsNodeAuthClient } = require('./ws/ws-client')
const wsGetUserdata = require('./ws/get-userdata')
// auth
const loginResultToJwt = require('./auth/login-result-to-jwt')
const provideUserdata = require('./auth/provide-userdata')
const createTokenValidator = require('./auth/create-token-validator')


module.exports = {
  socketIoNodeHandshakeLogin,
  socketIoNodeRoundtripLogin,

  socketIoNodeClient,
  socketIoNodeClientAsync,

  socketIoChainHSConnect,
  socketIoChainRTConnect,

  socketIoGetUserdata,
  socketIoJwtAuth,
  socketIoHandshakeAuth,

  wsVerifyClient,

  wsNodeClient,
  wsNodeAuthClient,

  wsGetUserdata,

  loginResultToJwt,
  provideUserdata,
  createTokenValidator
}
