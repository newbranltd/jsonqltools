const {
  RSA_ALGO,
  UTF8_FORMAT,
  RSA_PRIVATE_KEY_HEADER
} = require('jsonql-constants')
const { JsonqlValidationError } = require('jsonql-errors')
const jwtToken = require('./jwt-token')
const baseOptions = { algorithm: RSA_ALGO };
const debug = require('debug')('jsonql-jwt:jwt-rsa-token')
/**
 * Double check if provided with a correct private Key
 * @param {string|buffer} privateKey RSA format private key
 * @return {boolean} true on OK
 */
function checkPrivateKey(privateKey) {
  let s = Buffer.isBuffer(privateKey) ? privateKey.toString(UTF8_FORMAT) : privateKey;
  if (s.indexOf(RSA_PRIVATE_KEY_HEADER) > -1) {
    return true;
  }
  throw new JsonqlValidationError('Invalid RSA key format!')
}

/**
 * A wrapper method to generate RSA format token - the previous error was cause by the wrong options!
 * @param {object} payload
 * @param {string|buffer} privateKey the RSA format private key we should check if it's correct one
 * @return {string} generated token
 */
module.exports = function jwtRsaToken(payload, privateKey, options = {}) {
  debug('options', options)
  checkPrivateKey(privateKey)
  const config = Object.assign({}, options, baseOptions)
  return jwtToken(payload, privateKey, config)
}
