'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var jwt_decode = _interopDefault(require('jwt-decode'));
var jsonqlParamsValidator = require('jsonql-params-validator');
var jsonqlErrors = require('jsonql-errors');

// when the user is login with the jwt

/**
 * We only check the nbf and exp
 * @param {object} token for checking
 * @return {object} token on success
 */
function validate(token) {
  var start = token.iat || Math.floor(Date.now() / 1000);
  // we only check the exp for the time being
  if (token.exp) {
    if (start >= token.exp) {
      var expired = new Date(token.exp).toISOString();
      throw new jsonqlErrors.JsonqlError(("Token has expired on " + expired), token)
    }
  }
  return token;
}

/**
 * The browser client version it has far fewer options and it doesn't verify it
 * because it couldn't this is the job for the server
 * @TODO we need to add some extra proessing here to check for the exp field
 * @param {string} token to decrypted
 * @return {object} decrypted object
 */
function jwtDecode(token) {
  if (jsonqlParamsValidator.isString(token)) {
    var t = jwt_decode(token);
    return validate(t)
  }
  throw new jsonqlErrors.JsonqlError('Token must be a string!')
}

var OPTIONAL_KEY = 'optional';
var ALIAS_KEY = 'alias';

var STRING_TYPE = 'string';
var BOOLEAN_TYPE = 'boolean';

var NUMBER_TYPE = 'number';
var HSA_ALGO = 'HS256';

var obj, obj$1, obj$2, obj$3, obj$4, obj$5, obj$6, obj$7, obj$8;

var appProps = {
  algorithm: jsonqlParamsValidator.createConfig(HSA_ALGO, [STRING_TYPE]),
  expiresIn: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj = {}, obj[ALIAS_KEY] = 'exp', obj[OPTIONAL_KEY] = true, obj )),
  notBefore: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, NUMBER_TYPE, STRING_TYPE], ( obj$1 = {}, obj$1[ALIAS_KEY] = 'nbf', obj$1[OPTIONAL_KEY] = true, obj$1 )),
  audience: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$2 = {}, obj$2[ALIAS_KEY] = 'iss', obj$2[OPTIONAL_KEY] = true, obj$2 )),
  subject: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$3 = {}, obj$3[ALIAS_KEY] = 'sub', obj$3[OPTIONAL_KEY] = true, obj$3 )),
  issuer: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$4 = {}, obj$4[ALIAS_KEY] = 'iss', obj$4[OPTIONAL_KEY] = true, obj$4 )),
  noTimestamp: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE], ( obj$5 = {}, obj$5[OPTIONAL_KEY] = true, obj$5 )),
  header: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$6 = {}, obj$6[OPTIONAL_KEY] = true, obj$6 )),
  keyid: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE, STRING_TYPE], ( obj$7 = {}, obj$7[OPTIONAL_KEY] = true, obj$7 )),
  mutatePayload: jsonqlParamsValidator.createConfig(false, [BOOLEAN_TYPE], ( obj$8 = {}, obj$8[OPTIONAL_KEY] = true, obj$8 ))
};

function tokenValidator(config) {
  if (!jsonqlParamsValidator.isObject(config)) {
    return {}; // we just ignore it all together?
  }
  var result = {};
  var opts = jsonqlParamsValidator.checkConfig(config, appProps);
  // need to remove options that is false
  for (var key in opts) {
    if (opts[key]) {
      result[key] = opts[key];
    }
  }
  return result;
}

exports.decodeToken = jwtDecode;
exports.tokenValidator = tokenValidator;
