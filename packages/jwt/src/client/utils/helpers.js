const { BASE64_FORMAT } = require('jsonql-constants');
/**
 * create a buffer from string
 * @param {string} str to transform
 * @param {string} [format=BASE64_FORMAT] format to use
 * @return {buffer} tramsformed
 */
function buff(str, format = BASE64_FORMAT) {
  if (Buffer.isBuffer(str)) {
    return str;
  }
  return new Buffer.from(str, format)
}

/**
 * encode in base64 string
 * @param {*} in target
 * @return {string} base64 encoded
 */
const base64Encode = in => window.btoa(unescape(encodeURIComponent(str)))

/**
 * decode from base64 string
 * @param {string} in base64 encoded string
 * @return {*} decoded payload
 */
const base64Decode = in => decodeURIComponent(escape(window.atob(str)))


export {
  buff,
  base64Encode,
  base64Decode
}
