// This is ported back from ws-server and it will get use in the server / client side
import { isKeyInObject } from 'jsonql-params-validator'

function extractSocketPart(contract) {
  if (isKeyInObject(contract, 'socket')) {
    return contract.socket;
  }
  return contract;
}

/**
 * @BUG we should check the socket part instead of expect the downstream to read the menu!
 * We only need this when the enableAuth is true otherwise there is only one namespace
 * @param {object} contract the socket part of the contract file
 * @return {object} 1. remap the contract using the namespace --> resolvers
 * 2. the size of the object (1 all private, 2 mixed public with private)
 * 3. which namespace is public
 */
export default function groupByNamespace(contract) {
  let socket = extractSocketPart(contract)

  let nspSet = {};
  let size = 0;
  let publicNamespace;
  for (let resolverName in socket) {
    let params = socket[resolverName];
    let { namespace } = params;
    if (namespace) {
      if (!nspSet[namespace]) {
        ++size;
        nspSet[namespace] = {};
      }
      nspSet[namespace][resolverName] = params;
      if (!publicNamespace) {
        if (params.public) {
          publicNamespace = namespace;
        }
      }
    }
  }
  return { size, nspSet, publicNamespace }
}
