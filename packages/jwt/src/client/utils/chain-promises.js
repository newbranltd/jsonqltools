// This is ported back from ws-client
// the idea if from https://decembersoft.com/posts/promises-in-serial-with-array-reduce/
/**
 * previously we already make sure the order of the namespaces
 * and attach the auth client to it
 * @param {array} promises array of unresolved promises
 * @return {object} promise resolved with the array of promises resolved results
 */
export default function chainPromises(promises) {
  return promises.reduce((promiseChain, currentTask) => (
    promiseChain.then(chainResults => (
      currentTask.then(currentResult => (
        [...chainResults, currentResult]
      ))
    ))
  ), Promise.resolve([]))
}
