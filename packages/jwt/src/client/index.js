// this is the client side code interface using ES6

import {
  socketIoClient,
  socketIoClientAsync,
  socketIoHandshakeLogin,
  socketIoRoundtripLogin,
  socketIoChainConnect
} from './socketio'

import { decodeToken, tokenValidator } from './decode-token'
// ws
import { wsAuthClient, wsClient } from './ws/auth-client'

export {
  socketIoClient,
  socketIoClientAsync,
  socketIoHandshakeLogin,
  socketIoRoundtripLogin,
  socketIoChainConnect,

  decodeToken,
  tokenValidator,

  wsAuthClient,
  wsClient
}
