// handshake login
import { TOKEN_PARAM_NAME, DEFAULT_WS_WAIT_TIME } from 'jsonql-constants';
import socketIoClient from './socket-io-client'

/**
 * Create a async version to match up the rest of the api
 * @param {object} io socket io instance
 * @param {string} url end point
 * @param {object} [options={}] configuration
 * @return {object} Promise resolve to nsp instance
 */
export function socketIoClientAsync(...args) {
  return Promise.resolve(
    Reflect.apply(socketIoClient, null, args)
  )
}

/**
 * Login during handshake
 * @param {object} io the new socket.io instance
 * @param {string} token to send
 * @param {object} [options = {}] extra options
 * @return {object} the io object itself
 */
export function socketIoHandshakeLogin(io, url, token, options = {}) {
  const wait = options.timeout || DEFAULT_WS_WAIT_TIME;
  const config = Object.assign({}, options, {
    query: [TOKEN_PARAM_NAME, token].join('=')
  })
  let timer;
  const nsp = socketIoClient(io, url, config)
  return new Promise((resolver, rejecter) => {
    timer = setTimeout(() => {
      rejecter()
    }, wait)
    nsp.on('connect', () => {
      console.info('socketIoHandshakeLogin connected')
      resolver(nsp)
      clearTimeout(timer)
    })
  })
}
