// this should work on browser as well as node
// import io from 'socket.io-cilent'

/**
 * Create a normal client
 * @param {object} io socket io instance
 * @param {string} url end point
 * @param {object} [options={}] configuration
 * @return {object} nsp instance
 */
export default function socketIoClient(io, url, options = {}) {
  return io.connect(url, options)
}
