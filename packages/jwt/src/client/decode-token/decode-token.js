// when the user is login with the jwt
// we use call this to decode the token and then add the payload
// to the resolver so the user can call ResolverName.userdata
// and get back the payload
import jwt_decode from 'jwt-decode'
import { isString } from 'jsonql-params-validator'
import { JsonqlError } from 'jsonql-errors'

/**
 * We only check the nbf and exp
 * @param {object} token for checking
 * @return {object} token on success
 */
function validate(token) {
  const start = token.iat || Math.floor(Date.now() / 1000)
  // we only check the exp for the time being
  if (token.exp) {
    if (start >= token.exp) {
      const expired = new Date(token.exp).toISOString()
      throw new JsonqlError(`Token has expired on ${expired}`, token)
    }
  }
  return token;
}

/**
 * The browser client version it has far fewer options and it doesn't verify it
 * because it couldn't this is the job for the server
 * @TODO we need to add some extra proessing here to check for the exp field
 * @param {string} token to decrypted
 * @return {object} decrypted object
 */
export default function jwtDecode(token) {
  if (isString(token)) {
    const t = jwt_decode(token)
    return validate(t)
  }
  throw new JsonqlError('Token must be a string!')
}
