'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var jsonqlParamsValidator = require('jsonql-params-validator');

// This is ported back from ws-server and it will get use in the server / client side

function extractSocketPart(contract) {
  if (jsonqlParamsValidator.isKeyInObject(contract, 'socket')) {
    return contract.socket;
  }
  return contract;
}

/**
 * @BUG we should check the socket part instead of expect the downstream to read the menu!
 * We only need this when the enableAuth is true otherwise there is only one namespace
 * @param {object} contract the socket part of the contract file
 * @return {object} 1. remap the contract using the namespace --> resolvers
 * 2. the size of the object (1 all private, 2 mixed public with private)
 * 3. which namespace is public
 */
function groupByNamespace(contract) {
  var socket = extractSocketPart(contract);

  var nspSet = {};
  var size = 0;
  var publicNamespace;
  for (var resolverName in socket) {
    var params = socket[resolverName];
    var namespace = params.namespace;
    if (namespace) {
      if (!nspSet[namespace]) {
        ++size;
        nspSet[namespace] = {};
      }
      nspSet[namespace][resolverName] = params;
      if (!publicNamespace) {
        if (params.public) {
          publicNamespace = namespace;
        }
      }
    }
  }
  return { size: size, nspSet: nspSet, publicNamespace: publicNamespace }
}

// This is ported back from ws-client
// the idea if from https://decembersoft.com/posts/promises-in-serial-with-array-reduce/
/**
 * previously we already make sure the order of the namespaces
 * and attach the auth client to it
 * @param {array} promises array of unresolved promises
 * @return {object} promise resolved with the array of promises resolved results
 */
function chainPromises(promises) {
  return promises.reduce(function (promiseChain, currentTask) { return (
    promiseChain.then(function (chainResults) { return (
      currentTask.then(function (currentResult) { return (
        chainResults.concat( [currentResult])
      ); })
    ); })
  ); }, Promise.resolve([]))
}

exports.chainPromises = chainPromises;
exports.groupByNamespace = groupByNamespace;
