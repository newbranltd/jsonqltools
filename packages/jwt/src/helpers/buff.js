
const { BASE64_FORMAT } = require('jsonql-constants');
/**
 * create a buffer from string
 * @param {string} str to transform
 * @param {string} [format=BASE64_FORMAT] format to use
 * @return {buffer} tramsformed
 */
module.exports = function buff(str, format = BASE64_FORMAT) {
  if (Buffer.isBuffer(str)) {
    return str;
  }
  return new Buffer.from(str, format)
}
