# jsonql-jwt

> A jwt based authentication system for jsonql

This library provide several methods that will be use in different jsonql javascript frameworks.

## Installation

```sh
$ npm i jsonql-jwt
```

### Node command line utility

If you install this globally

```sh
$ jsonql-jwt rsa-pem

```

or

```sh
$ node ./node_modules/jsonql-jwt/cmd.js -- rsa-pem
```

This will create a pair of `RSA256` public / private keys in `PEM` format.

Or you can output that into a folder

```sh
$ jsonql-jwt rsa-pem --outputDir ./path/to/directory

```

### Browser tools

This module export several different modules, when you use in node, it points to the `main.js`
But including the `dist/jsonql-jwt.js` will get you a whole different tool set for browser only.

** T.B.C. **


---

ISC

[Joel Chu](https://joelchu.com)

[NEWBRAN LTD](https://newbran.ch) (c) 2019
