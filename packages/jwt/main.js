// main export interface for node modules
//  import export
const rsaKeys = require('./src/crypto/rsa-keys');
const rsaPemKeys = require('./src/crypto/rsa-pem-keys');
// const shaKey = require('./crypto/shaKey');
const jwtToken = require('./src/jwt/jwt-token');
const jwtDecode = require('./src/jwt/jwt-decode');
const jwtRsaToken = require('./src/jwt/jwt-rsa-token');

const { decodeToken, tokenValidator } = require('./src/jwt/decode-token');
const { encryptWithPublicPem, decryptWithPrivatePem } = require('./src/crypto/encrypt-decrypt')

const { groupByNamespace, chainPromises } = require('./src/helpers/client-utils')

// socket.io & ws server side methods
const {
  socketIoNodeHandshakeLogin,
  socketIoNodeRoundtripLogin,
  socketIoNodeClientAsync,
  socketIoNodeClient,

  socketIoChainHSConnect,
  socketIoChainRTConnect,

  socketIoGetUserdata,
  socketIoJwtAuth,
  socketIoHandshakeAuth,

  wsVerifyClient,

  wsNodeClient,
  wsNodeAuthClient,

  wsGetUserdata,

  loginResultToJwt,
  provideUserdata,
  createTokenValidator
} = require('./src/server')

// output
module.exports = {
  rsaKeys,
  jwtToken,
  jwtDecode,
  jwtRsaToken,

  rsaPemKeys,

  socketIoNodeHandshakeLogin,
  socketIoNodeRoundtripLogin,
  socketIoNodeClientAsync,
  socketIoNodeClient,

  socketIoChainHSConnect,
  socketIoChainRTConnect,

  socketIoGetUserdata,
  socketIoJwtAuth,
  socketIoHandshakeAuth,

  wsVerifyClient,
  wsNodeClient,
  wsNodeAuthClient,

  wsGetUserdata,

  loginResultToJwt,
  provideUserdata,
  createTokenValidator,

  decodeToken,
  tokenValidator,

  encryptWithPublicPem,
  decryptWithPrivatePem,

  groupByNamespace,
  chainPromises
}
