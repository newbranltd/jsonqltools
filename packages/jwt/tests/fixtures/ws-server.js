// just create a simple ws server to test out why the hell
// the ws client is not working in browser
const { join } = require('path')
const url = require('url')
const serverIoCore = require('server-io-core')
const debug = require('debug')('jsonql-jwt:ws-server')
const WebSocket = require('ws')
const ws1 = new WebSocket.Server({ noServer: true })
ws1.on('connection', function(ws) {
  ws.on('open', function() {
    debug('connection established')
  })
  ws.on('message', function(msg) {
    debug('I got something', msg)
    ws.send('I got your message')
  })
})

// setting up server-io-core
const { start, webserver } = serverIoCore({
  webroot: [
    join(__dirname, 'browser'),
    join(__dirname, '..', '..', 'dist')
  ],
  port: 3011,
  debugger: false,
  reload: false,
  socket: false,
  autoStart: false
})

// now setup WebSocket
webserver.on('upgrade', function upgrade(request, socket, head) {
  const pathname = url.parse(request.url).pathname;
  if (pathname === '/jsonql') {
    ws1.handleUpgrade(request, socket, head, function done(ws) {
      ws1.emit('connection', ws, request)
    })
  } else {
    socket.destory()
  }
})

// now run
start()
