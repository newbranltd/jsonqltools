// testing the client utils methods
const test = require('ava')
const { groupByNamespace, chainPromises } = require('../main')
const { join } = require('path')
const fsx = require('fs-extra')

const contract = fsx.readJsonSync(join(__dirname, 'fixtures', 'contract.json'))


test('It should able to give me two list from the contract', t => {
  const { size, nspSet, publicNamespace } = groupByNamespace(contract)

  t.is(size, 2)
  t.is(publicNamespace, 'jsonql/public')

})



test.cb('The promises should be resolve one after the other even the early one resolve in a timer', t => {
  t.plan(3)
  const results = ['first', 'second', 'third']

  const p1 = () => new Promise(resolver => {
    setTimeout(() => {
      resolver(results[0])
    }, 1000)
  })
  const p2 = () => new Promise(resolver => {
    setTimeout(() => {
      resolver(results[1])
    }, 300)
  })
  const p3 = () => new Promise(resolver => {
    resolver(results[2])
  })

  chainPromises([p1(), p2(), p3()])
    .then(res => {
      res.forEach((r, i) => {
        t.is(r, results[i])
      })
      t.end()
    })
})
