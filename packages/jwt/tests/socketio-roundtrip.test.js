// testing the socket.io auth with client and server
const test = require('ava')
const http = require('http')
const socketIo = require('socket.io')
// const socketIoClient = require('socket.io-client')
const debug = require('debug')('jsonql-jwt:test:socketio-auth')
// import server
const {
  socketIoJwtAuth,
  socketIoNodeRoundtripLogin,
  socketIoNodeClientAsync,
  socketIoGetUserdata,
  chainPromises
} = require('../main')

// import data
const { secret, token, namespace, msg } = require('./fixtures/options')

const namespace1 = '/' + namespace + '/a';
const namespace2 = '/' + namespace + '/b';
const port = 3001;

const url = `http://localhost:${port}`;

test.before( t => {
  // setup server
  const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.write('Hello world!')
    res.end()
  })
  t.context.server = server;
  // setup socket.io
  const io = socketIo(server)
  const nsp1 = io.of(namespace1)
  const nsp2 = io.of(namespace2)

  t.context.nsp1 = socketIoJwtAuth(nsp1, { secret })
  // doesn't require login
  t.context.nsp2 = nsp2;

  t.context.server.listen(port)
})

test.after( t => {
  t.context.server.close()
})

test.serial.cb('It should able to connect using the token', t => {
  t.plan(1)
  t.context.nsp1.then( socket => {
    setTimeout(() => {
      socket.emit('hello', msg)
    }, 500)
  })

  socketIoNodeRoundtripLogin(`${url}${namespace1}`, token)
    .then(client => {
      client.on('hello', message => {
        t.is(message, msg)
        client.disconnect()
        t.end()
      })
    })
    .catch(err => {
      debug('error?', err)
    })
})

test.serial.cb('It should able to connect to a public namespace using the socketIoNodeClientAsync', t => {
  t.plan(2)
  t.context.nsp2.on('connection', socket => {
    debug('connection established')
    socket.on('hello', function(msg) {
      t.is(msg, 'world')
      socket.disconnect()
      t.end()
    })
  })
  socketIoNodeClientAsync([url, namespace2].join('')).then(nsp => {
    t.truthy(nsp)
    nsp.emit('hello', 'world')
  })
})

test.serial.cb('It should able to round trip connect in sequence using the chain promises', t => {
  t.plan(3)
  let nsp1 = t.context.nsp1;
  let nsp2 = t.context.nsp2;
  let p1 = () => socketIoNodeRoundtripLogin(`${url}${namespace1}`, token)
  let p2 = () => socketIoNodeClientAsync([url, namespace2].join(''))

  // the jwtAuth is a promise interface

  nsp1.then(sock1 => {
    let userdata = socketIoGetUserdata(sock1)
    t.is(userdata.name, 'Joel', 'should able to decode the token to userdata')
    //t.end()
  })

  nsp2.on('connection', sock2 => {
    sock2.on('hello', msg => {
      t.is(msg, 'world', 'should get the message hello world')
      t.end()
    })
  })


  chainPromises([p1(), p2()])
    .then(nsps => {
      t.is(nsps.length, 2, 'should return two nsps')
      // send something to the public
      setTimeout(() => {
        nsps[1].emit('hello', 'world')
      }, 500)

    })

})
