// WS handshake test
const test = require('ava')
const http = require('http')
const WebSocket = require('ws')
const url = require('url')
const fsx = require('fs-extra')
const { join } = require('path')
const debug = require('debug')('jsonql-jwt:test:ws-handshake')

const {
  wsVerifyClient,
  jwtRsaToken,
  wsNodeClient,
  wsNodeAuthClient,
  wsGetUserdata
} = require('../main')

const baseDir = join(__dirname, 'fixtures', 'keys')
const publicKey = fsx.readFileSync(join(baseDir, 'publicKey.pem'))
const privateKey = fsx.readFileSync(join(baseDir, 'privateKey.pem'))

// set up
const msg = 'Hello there';
const payload = { name: 'John Doe' };
const token = jwtRsaToken(payload, privateKey)
const verifyClient = wsVerifyClient(publicKey)
const port = 3006;
const server = http.createServer()
const { JSONQL_PATH, PUBLIC_KEY, PRIVATE_KEY } = require('jsonql-constants')
const namespaces = [
  `/${JSONQL_PATH}/${PUBLIC_KEY}`,
  `/${JSONQL_PATH}/${PRIVATE_KEY}`
];

// test start
test.before( t => {
  const wss = namespaces.map((n, i) => {
    if (i > 0) {
      return new WebSocket.Server({noServer: true, verifyClient})
    }
    return new WebSocket.Server({ noServer: true })
  })
  t.context.wss = wss;
  server.on('upgrade', function upgrade(request, socket, head) {
    const pathname = url.parse(request.url).pathname;
    if (pathname === namespaces[0]) {
      wss[0].handleUpgrade(request, socket, head, function done(ws) {
        wss[0].emit('connection', ws, request)
      });
    } else if (pathname === namespaces[1]) {
      wss[1].handleUpgrade(request, socket, head, function done(ws) {
        wss[1].emit('connection', ws, request)
      });
    } else {
      socket.destroy()
    }
  });
  server.listen(port)
  t.context.server = server;
})

test.after( t => {
  t.context.server.close()
})

test.cb(`It should able to connect to the ${namespaces[0]} without token`, t => {
  t.plan(1);
  t.context.wss[0].on('connection', function connection(ws, request) {

    ws.on('message', function incoming(message) {
      t.is(message, msg)
      t.end()
    })
  })
  // client
  let client = wsNodeClient(`ws://localhost:${port}${namespaces[0]}`)
  client.on('open', function open() {
    client.send(msg)
  })
})

test.cb(`It should able to call the verifyClient when trying connect to ${namespaces[1]} with token`, t => {
  t.plan(2)
  t.context.wss[1].on('connection', function connection(ws, request) {
    const userdata = wsGetUserdata(request)
    ws.on('message', function incoming(message) {
      t.is(message, msg)
      ws.send(JSON.stringify(userdata))
    })
  })

  let client = wsNodeAuthClient(`ws://localhost:${port}${namespaces[1]}`, token);
  client.on('open', function open() {
    client.send(msg)
  })

  client.on('message', function incoming(data) {
    let userdata = JSON.parse(data)
    debug('userdata', userdata)
    t.truthy(userdata.name)
    t.end()
  })

})
